<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function getfile($id_table, $id, $filename)
{
	if (!bab_isAccessValid(FORM_TABLES_VIEW_GROUPS, $id_table))
		die(form_translate('Access denied'));
	
	$file = $GLOBALS['babAddonUpload'].'directories/'.form_tbl_name($id_table).'/'.$id.'/'.$filename;
	$mime = form_getFileMimeType($filename);

	if( strtolower(bab_browserAgent()) == "msie")
		header('Cache-Control: public');
	header("Content-Disposition: attachment; filename=\"$filename\"\n");
	header("Content-Type: ".$mime."\n");
	header("Content-Length: ". filesize($file)."\n");
	header("Content-transfert-encoding: binary\n");
	$fp=fopen($file,"rb");
	while (!feof($fp)) {
		print fread($fp, 8192);
		}
	fclose($fp);
	die();
}


/* main */

switch($_REQUEST['idx'])
{
	case 'getfile':
		getfile($_GET['id_table'], $_GET['form_row'], $_GET['filename']);
		break;
}


?>