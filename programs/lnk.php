<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function list_lnk()
{
global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = & $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_edit = form_translate('Properties');
			$this->t_delete = form_translate('Delete');
			$this->t_uncheckall = form_translate('Uncheck all');
			$this->t_checkall = form_translate('Check all');
			$this->t_in_use = form_translate('In use');
			
			$this->res = $this->db->db_query("
				SELECT 
					l.*,
					f.id id_field 
				FROM 
					".FORM_TABLES_LNK." l 
						LEFT JOIN ".FORM_FORMS_FIELDS." f ON f.id_lnk=l.id 
						LEFT JOIN ".FORM_WORKSPACE_ENTRIES." e ON e.id_object = l.id_table1 AND e.type='table' 
						
				WHERE 
					
					e.id_workspace IS NULL OR e.id_workspace=".$this->db->quote(form_getWorkspace())." 

				GROUP BY l.id
				ORDER BY l.name 
			");
			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			if ( $this->arr = $this->db->db_fetch_array($this->res))
				{
				$this->arr['name'] = bab_toHtml($this->arr['name']);
				$this->arr['description'] = bab_toHtml($this->arr['description']);
				return true;
				}
			else
				{
				return false;
				}
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."lnk.html", "list_lnk"));
}

function edit_lnk($id = '')
{
global $babBody;
    class temp
        {
		var $altbg = false;
		var $strlimit = 30;

        function temp($id)
            {
			$this->id = $id;
			$this->db = & $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_record = form_translate('Record');
			$this->t_table1 = form_translate('Table 1');
			$this->t_table2 = form_translate('Table 2');

			$this->t_lnk_auto = form_translate('Automatic table (read/write)');
			$this->t_lnktable = form_translate('Use a table (read only)');

			$this->t_lnk_table = form_translate('Link table');
			$this->t_field1 = form_translate('Field 1');
			$this->t_field2 = form_translate('Field 2');

			$this->js_error = form_translate('Name must be filled');


			$el_to_init = array('name','description','id_table1','id_table2','lnk_type','id_lnk_table','id_field1','id_field2');

			$default = array(   
			 'lnk_type' => 0
			);

			if (empty($this->id) || (isset($_POST) && count($_POST) > 0))
				{
				foreach($el_to_init as $v)
					{
					$this->arr[$v] = isset($_POST[$v]) ? $_POST[$v] :(isset($default[$v]) ? $default[$v] : '' );
					}
				}
			elseif (!empty($this->id))
				{
				$req = "SELECT *,
							CONCAT(id_lnk_f1,'-',id_field1) id_field1,
							CONCAT(id_lnk_f2,'-',id_field2) id_field2
						FROM 
							".FORM_TABLES_LNK." 
						WHERE 
							id='".$this->db->db_escape_string($this->id)."'";

				$this->arr = $this->db->db_fetch_array($this->db->db_query($req));
				}

			

			$this->res = $this->db->db_query("
				SELECT 
					t.id,
					t.name 
				FROM 
					".FORM_TABLES." t,
					".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					t.created='Y' 
					AND w.id_object = t.id 
					AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
					AND w.type='table'
				ORDER BY name 
			");
			

			
			$this->curtbl = 1;
			$this->curfield = 1;

			$this->reslnktable = $this->db->db_query("
				SELECT 
					t.id,
					t.name,
					COUNT(f.link_table_field_id) count
				FROM 
					".FORM_TABLES." t,
					".FORM_TABLES_FIELDS." f,
					".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					t.created='Y'
				AND f.id_table = t.id 
				AND f.link_table_field_id > 0 
				AND w.id_object = t.id 
				AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
				AND w.type='table' 
					GROUP BY t.id 
					ORDER BY t.name
			");


			if (!empty($this->arr['id_lnk_table']))
				{
				$this->resfields = $this->db->db_query("
					SELECT 
						CONCAT(f1.id,'-',f2.id) id,
						CONCAT(t.name,'.',f2.name) name
					FROM 
						".FORM_TABLES_FIELDS." f1,
						".FORM_TABLES_FIELDS." f2, 
						".FORM_TABLES." t,
						".FORM_WORKSPACE_ENTRIES." w 
					WHERE 
						f1.id_table='".$this->db->db_escape_string($this->arr['id_lnk_table'])."' 
					AND f1.field_function='' 
					AND f1.link_table_field_id = f2.id 
					AND f2.id_table = t.id 
					AND w.id_object = t.id 
					AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
					AND w.type = 'table'
					");

				}
			}

		function getnexttable()
			{
			if ($this->table = $this->db->db_fetch_array($this->res))
				{
				$this->table['name'] = bab_toHtml($this->table['name']);
				$this->selected = $this->table['id'] == $this->arr['id_table'.$this->curtbl];
				return true;
				}
			else
				{
				$this->curtbl = 2;
				if ($this->db->db_num_rows($this->res) > 0)
					$this->db->db_data_seek($this->res,0);
				return false;
				}
			}

		function getnextlnktable(&$skip)
			{
			if ($this->table = $this->db->db_fetch_array($this->reslnktable))
				{
				$this->table['name'] = bab_toHtml($this->table['name']);
				if ($this->table['count'] < 2) $skip = true;
				$this->selected = $this->table['id'] == $this->arr['id_lnk_table'];
				return true;
				}
			else
				{
				return false;
				}
			}

		function getnextfield()
			{
			if (isset($this->resfields) && $this->field = $this->db->db_fetch_array($this->resfields))
				{
				$this->field['name'] = bab_toHtml($this->field['name']);
				$this->selected = $this->field['id'] == $this->arr['id_field'.$this->curfield];
				return true;
				}
			else
				{
				if (isset($this->resfields) && $this->db->db_num_rows($this->resfields) > 0)
					{
					$this->curfield = 2;
					$this->db->db_data_seek($this->resfields,0);
					}
				return false;
				}
			}

        }
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."lnk.html", "edit_lnk" ));
}


// record


function record_lnk()
	{
	$db = &$GLOBALS['babDB'];

	if (empty($_POST['name']))
		{
		$GLOBALS['babBody']->msgerror = form_translate('Name must be defined');
		return false;
		}

	if ($_POST['lnk_type'] == 0)
		{

		$id_table1 = $_POST['id_table1'];
		$id_table2 = $_POST['id_table2'];
		$id_lnk_table = 0;
		$id_field1 = 0;
		$id_field2 = 0;
		$id_lnk_f1 = 0;
		$id_lnk_f2 = 0;
		}
	else
		{
		list($id_lnk_f1,$id_field1) = isset($_POST['id_field1']) ? explode('-',$_POST['id_field1']) : 0;
		list($id_lnk_f2,$id_field2) = isset($_POST['id_field2']) ? explode('-',$_POST['id_field2']) : 0;

		list($id_table1) = $db->db_fetch_array($db->db_query("SELECT id_table FROM ".FORM_TABLES_FIELDS." WHERE id='".$db->db_escape_string($id_field1)."'"));
		list($id_table2) = $db->db_fetch_array($db->db_query("SELECT id_table FROM ".FORM_TABLES_FIELDS." WHERE id='".$db->db_escape_string($id_field2)."'"));
		
		$id_lnk_table = $_POST['id_lnk_table'];
		}

	if (empty($_POST['id_lnk']))
		{
		$db->db_query("INSERT INTO ".FORM_TABLES_LNK." 
						(
							name,
							description,
							id_table1,
							id_table2,
							id_lnk_table,
							id_lnk_f1,
							id_lnk_f2,
							id_field1,
							id_field2,
							lnk_type
						) 
					VALUES 
						(
							'".$db->db_escape_string($_POST['name'])."',
							'".$db->db_escape_string($_POST['description'])."',
							'".$db->db_escape_string($id_table1)."',
							'".$db->db_escape_string($id_table2)."',
							'".$db->db_escape_string($id_lnk_table)."',
							'".$db->db_escape_string($id_lnk_f1)."',
							'".$db->db_escape_string($id_lnk_f2)."',
							'".$db->db_escape_string($id_field1)."',
							'".$db->db_escape_string($id_field2)."',
							'".$db->db_escape_string($_POST['lnk_type'])."'
						)
							");
		}
	else
		{
		$db->db_query("UPDATE ".FORM_TABLES_LNK." 
					SET 
						name			='".$db->db_escape_string($_POST['name'])."',
						description		='".$db->db_escape_string($_POST['description'])."',
						id_table1		='".$db->db_escape_string($id_table1)."',
						id_table2		='".$db->db_escape_string($id_table2)."',
						id_lnk_table	='".$db->db_escape_string($id_lnk_table)."',
						id_lnk_f1		='".$db->db_escape_string($id_lnk_f1)."',
						id_lnk_f2		='".$db->db_escape_string($id_lnk_f2)."',
						id_field1		='".$db->db_escape_string($id_field1)."',
						id_field2		='".$db->db_escape_string($id_field2)."',
						lnk_type		='".$db->db_escape_string($_POST['lnk_type'])."'
					WHERE 
						id='".$_POST['id_lnk']."'");
		}

	return true;
	}


function delete_lnk($lnk)
{
	if (count($lnk) > 0)
		{
		$db = & $GLOBALS['babDB'];
		$db->db_query("DELETE FROM ".FORM_TABLES_LNK." WHERE id IN(".$db->quote($lnk).")");
		}
}

// main

$idx = isset($_REQUEST['idx']) ? $_REQUEST['idx'] : 'list_lnk';

if (isset($_POST['action']))
{
switch ($_POST['action'])
	{
	case 'delete_lnk':
		delete_lnk($_POST['delete_lnk']);
		break;

	case 'record_lnk':
		if ($_POST['idx'] == 'list_lnk' && !record_lnk())
			{
			$idx = 'edit_lnk';
			}
		break;
	}
}

$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
$babBody->addItemMenu("list_lnk", form_translate("List link table"),$GLOBALS['babAddonUrl']."lnk&idx=list_lnk");

switch($idx)
{
	case "edit_lnk":
		$babBody->addItemMenu("edit_lnk", form_translate("Edit link table"),$GLOBALS['babAddonUrl']."lnk&idx=edit_lnk");
		$babBody->title = form_translate("Edit link table");
		edit_lnk($_REQUEST['id_lnk']);
		break;

	case "list_lnk":
		$babBody->addItemMenu("add_lnk", form_translate("Add link table"),$GLOBALS['babAddonUrl']."lnk&idx=add_lnk");
		$babBody->title = form_translate("List link table");
		list_lnk();
		break;

	case "add_lnk":
		$babBody->addItemMenu("add_lnk", form_translate("Add link table"),$GLOBALS['babAddonUrl']."lnk&idx=add_lnk");
		$babBody->title = form_translate("Add link table");
		edit_lnk();
		break;
}


$babBody->setCurrentItemMenu($idx);
?>