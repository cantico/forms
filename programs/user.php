<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function browse_tables()
{
global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_nb_rec = form_translate('Records');
			$this->t_browse = form_translate('Browse');
			$this->t_import = form_translate('Import');
			$this->t_export = form_translate('Export');
			$this->t_help = form_translate('Help');
			$this->t_refresh = form_translate('Refresh');
			$this->t_ov_directory = form_translate('Ovidentia directory');
			$this->t_ov_calendar = form_translate('Ovidentia calendar');
			$this->t_directory_config = form_translate('Configure the synchronisation with ovidentia directory');
			$this->t_calendar_config = form_translate('Configure the synchronisation with ovidentia calendar');
			
			$this->res = $this->db->db_query("
				SELECT 
					t.* 
				FROM 
					".FORM_TABLES." t, 
					".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					t.created='Y' 
					AND t.id = w.id_object 
					AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
					AND w.type = 'table' 
				ORDER BY t.name
			");
			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);

				list($this->nb_rec) = $this->db->db_fetch_array($this->db->db_query("SELECT COUNT(*) FROM ".$this->db->db_escape_string(form_tbl_name($this->arr['id'])).""));

				$this->browse_rows_url = bab_toHtml($GLOBALS['babAddonUrl']."user&idx=browse_rows&id_table=".$this->arr['id']);
				$this->import_url = bab_isAccessValid(FORM_TABLES_INSERT_GROUPS, $this->arr['id']) ? bab_toHtml($GLOBALS['babAddonUrl']."user&idx=import&id_table=".$this->arr['id']) : false;
				$this->export_url = bab_isAccessValid(FORM_TABLES_VIEW_GROUPS, $this->arr['id']) ? bab_toHtml($GLOBALS['babAddonUrl']."user&idx=export&id_table=".$this->arr['id']) : false;
				$this->ov_directory_url = bab_toHtml($GLOBALS['babAddonUrl']."user&idx=ov_directory&id_table=".$this->arr['id']);
				$this->ov_directory_upgrade_url =  bab_toHtml($GLOBALS['babAddonUrl']."user&idx=ov_directory_upgrade&id_table=".$this->arr['id']);
				$this->ov_calendar_url = bab_toHtml($GLOBALS['babAddonUrl']."user&idx=ov_calendar&id_table=".$this->arr['id']);
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "browse_tables"));
}


function browse_rows($id)
{
global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->db = $GLOBALS['babDB'];
			
			$this->id = $id;
			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_nb_rec = form_translate('Records');
			$this->t_edit = form_translate('Edit');
			$this->t_filing = form_translate('Archive');
			$this->t_lock = form_translate('Lock');
			$this->t_change = form_translate('Modify');
			$this->t_checkall = form_translate('Check all');
			$this->t_uncheckall = form_translate('Uncheck all');
			$this->t_rows_per_page = form_translate('Rows/page');
			$this->t_update = form_translate('Update');
			$this->t_delete = form_translate('Del.');
			$this->t_help = form_translate('Help');
			$this->t_approb = form_translate('Approbation');
			$this->t_view_cols = form_translate('View columns');

			$this->t_approb_w = form_translate('Wait.');
			$this->t_approb_a = form_translate('Accep.');
			$this->t_approb_r = form_translate('Refus.');
			$this->t_wf_instance = form_translate('Waiting validation');

			$this->rows_per_page = isset($_POST['rows_per_page']) ? $_POST['rows_per_page'] : 30;
			$this->pos = isset($_POST['pos']) ? $_POST['pos'] : 0;

			list($this->max) = $this->db->db_fetch_array($this->db->db_query("SELECT COUNT(*) FROM ".$this->db->db_escape_string(form_tbl_name($this->id)).""));

			$this->blob = array();
			$res = $this->db->db_query("DESCRIBE ".$this->db->db_escape_string(form_tbl_name($this->id)));
			while ($arr = $this->db->db_fetch_array($res))
				{
				if (substr_count(strtolower($arr['Type']),'blob') > 0)
					{
					$this->blob[$arr[0]] = 1;
					}
				}
			
			$this->res = $this->db->db_query("SELECT t.*,i.id_schi FROM ".$this->db->db_escape_string(form_tbl_name($this->id))." t LEFT JOIN ".FORM_APPROB_SCHI." i ON i.id_table='".$this->db->db_escape_string($this->id)."' AND i.id_line=t.form_id LIMIT ".$this->db->db_escape_string($this->pos).",".$this->db->db_escape_string($this->rows_per_page));
			$this->count = $this->db->db_num_rows($this->res);
			
			$this->resrows = $this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($id)."' AND field_function=''");
			$this->countrows = $this->db->db_num_rows($this->resrows);

			$rescols = $this->db->db_query("SELECT id_table_field FROM ".FORM_TABLES_VIEW_COLS." WHERE id_table='".$this->db->db_escape_string($id)."'");
			while ($arr = $this->db->db_fetch_array($rescols))
				{
				$this->form_view_cols[$arr['id_table_field']] = true;
				}

			/*
			$this->alowed_filing = bab_isAccessValid(FORM_TABLES_FILING_GROUPS, $this->id);
			$this->alowed_lock = bab_isAccessValid(FORM_TABLES_LOCK_GROUPS, $this->id);
			*/
			$this->alowed_filing = true;
			$this->alowed_lock = true;
			$this->alowed_delete = bab_isAccessValid(FORM_TABLES_DELETE_GROUPS, $this->id);

			$this->row = $this->db->db_fetch_array($this->res);
			$this->alowed_approb = isset($this->row['form_approb']);
			if ($this->count > 0)
				$this->db->db_data_seek($this->res,0);

			$this->colspan = 0;
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->row = $this->db->db_fetch_array($this->res);

				$this->filingchecked = $this->row['form_filed'] == 'Y' ? 'checked' : '';
				$this->lockchecked = $this->row['form_locked'] == 'Y' ? 'checked' : '';
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}

		function getnextrow()
			{
			static $i=0,$j=0;
			if( $i < $this->countrows)
				{
				$arr = $this->db->db_fetch_array($this->resrows);

				$colname = form_col_name($arr['id'],$arr['name']);

				if (empty($arr['field_function']))
					{
					$this->value = isset($this->row[$colname]) ? $this->row[$colname] : '';
					}
				else
					{
					$this->value = form_field_function($arr['field_function'],$this->row);
					}

				if (isset($this->blob[$colname]) && !empty($this->value))
					{
					$this->value = form_translate('Binary data');
					}

				$this->value = bab_toHtml(bab_abbr($this->value, BAB_ABBR_FULL_WORDS, FORM_VIEW_MAX_CHAR));

				$this->name = bab_toHtml($arr['name']);
				$this->id_col = $arr['id'];
				$this->selected = isset($this->form_view_cols[$arr['id']]);
				if ($this->selected && $j == 0)
					{
					$this->colspan++;
					}
				$i++;
				return true;
				}
			else
				{
				$j++;
				$i=0;
				if ($this->countrows > 0)
					$this->db->db_data_seek($this->resrows,0);
				return false;
				}
			}
		
		
        }
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "browse_rows"));
}


function import($id)
{
global $babBody;
    class temp
        { 
        function temp($id)
            {
			$this->t_separator = form_translate('Separator');
			$this->t_file = form_translate('File');
			$this->t_submit = form_translate('Submit');
			$this->t_help = form_translate('Help');

			$this->t_table_name = form_translate('Table');
			$this->t_cvs_name = form_translate('File');
			$this->t_reference = form_translate('Reference for modifications');

			$this->id = $id;
			$this->db = &$GLOBALS['babDB'];
			$this->head = array();
			if (isset($_FILES['csv']))
				{
				if (is_writable(CSV_SWAP)) {
					if (is_uploaded_file($_FILES['csv']['tmp_name']))
						{
						$handle_r = fopen($_FILES['csv']['tmp_name'], "r");
						$handle_w = fopen(CSV_SWAP,'w');
						$this->head = fgetcsv($handle_r, filesize($_FILES['csv']['tmp_name']), $_POST['separator']);
						fseek($handle_r, 0);
						while (!feof($handle_r))
							{
							$buffer = fgets($handle_r,4096);
							fwrite($handle_w,$buffer);
							}
						fclose($handle_r);
						fclose($handle_w);
						}
					else
						{
						$GLOBALS['babBody']->msgerror = form_translate('Upload error, check your server configuration');
						}
					}
				else
					{
					$GLOBALS['babBody']->msgerror = form_translate('The file').' '.CSV_SWAP.' '.form_translate('must be writable');
					}

				$this->separator = $_POST['separator'];

				$this->resfields = $this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($this->id)."' AND field_function=''");
				}
            }

		function getnexthead()
			{
			if (list($this->key,$this->value) = each($this->head))
				{
				$this->selected = $this->value == $this->fieldname;
				$this->key = bab_toHtml($this->key);
				$this->value = bab_toHtml($this->value);
				return true;
				}
			else
				{
				return false;
				}
			}
		
		function getnextfield()
			{
			reset($this->head);
			if ($this->field = $this->db->db_fetch_array($this->resfields))
				{
				$this->fieldname = form_col_name($this->field['id'],$this->field['name']);
				$this->field['id'] = bab_toHtml($this->field['id']);
				$this->field['name'] = bab_toHtml($this->field['name']);
				return true;
				}
			else
				{
				return false;
				}
			}
        }
	
	$template = isset($_POST['action']) && $_POST['action']=='import1' ? 'import2' : 'import1';
	
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", $template));
}

function export($id)
{
global $babBody;
    class temp
        { 
        function temp($id)
            {
			$this->db = & $GLOBALS['babDB'];
			
			$this->id = $id;
			$this->t_field = form_translate('Field');
			$this->t_separator = form_translate('Separator');
			$this->t_export = form_translate('Export');
			$this->t_help = form_translate('Help');
			$this->t_uncheckall = form_translate('Uncheck all');
			$this->t_checkall = form_translate('Check all');
			$this->t_fav_name = form_translate('Favorite name');
			$this->t_to_file = form_translate('Record to file');
			$this->t_index = form_translate('Index');

			$this->res = $this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($id)."' AND field_function='' AND name <>''");
            }

		function getnext()
			{
			return $this->arr = $this->db->db_fetch_array($this->res);
			}
        }
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "export"));
}


function help($id)
{
class temp
	{
	function temp($id)
		{
		$this->t_close = form_translate('Close');

		$this->title = form_translate($id.',title');
		$this->text = form_translate($id.',text');
		}
	}

$temp = new temp($id);
include_once $GLOBALS['babInstallPath']."utilit/uiutil.php";
$GLOBALS['babBodyPopup'] = new babBodyPopup();
$GLOBALS['babBodyPopup']->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."help.html", "help_view"));
printBabBodyPopup();
die();
}


function favlist()
{
	global $babBody;
    class temp
        {
		var $altbg = false;
        function temp()
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_filename = form_translate('File name');
			$this->t_help = form_translate('Help');
			$this->t_uncheckall = form_translate('Uncheck all');
			$this->t_checkall = form_translate('Check all');
			$this->t_delete = form_translate('Delete');
			$this->t_tablename = form_translate('Table name');
			$this->t_update = form_translate('Update');
			$this->t_copy = form_translate('Copy url');
			$this->t_close = form_translate('Close');
			$this->t_launch = form_translate('Launch');

			$this->res = $this->db->db_query("SELECT f.id, f.name, f.filename, t.name tablename FROM ".FORM_TABLES_EXPORT_FAV." f,".FORM_TABLES." t WHERE t.id=f.id_table");
            }

		function getnext()
			{
			$this->altbg = !$this->altbg;
			return $this->arr = $this->db->db_fetch_array($this->res);
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "favlist"));
}


function wf_users($idschi)
{
    class temp
        {
		var $altbg = false;
        function temp($idschi)
            {
			require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
			$this->t_user = form_translate('Waiting approvers');
			$this->arr = bab_WFGetWaitingApproversInstance($idschi, false);
			//reset($this->arr);
            }

		function getnext()
			{
			$this->altbg = !$this->altbg;
			if (list(,$uid) = each($this->arr))
				{
				
				$this->name = bab_getUserName($uid);
				$this->email = bab_getUserEmail($uid);
				return true;
				}
			else
				return false;
			}
        }
    $tp = new temp($idschi);
	$html = bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "wf_users");

	include_once $GLOBALS['babInstallPath']."utilit/uiutil.php";
	$GLOBALS['babBodyPopup'] = new babBodyPopup();
	$GLOBALS['babBodyPopup']->babecho($html);
	printBabBodyPopup();
	die();
}

function favexec($id)
{
	global $babBody;
	$db = &$GLOBALS['babDB'];
	$arr = $db->db_fetch_array($db->db_query("SELECT * FROM ".FORM_TABLES_EXPORT_FAV." WHERE id='".$db->db_escape_string($id)."'"));
	$line = & file_export_gettable($arr['id_table'],explode(',',$arr['cols']),$arr['csv_separator']);
	if (file_export_record($arr['filename'], $line))
		die((count($line)-1).' '.form_translate('records'));
	else
		die($babBody->msgerror);
}




function upload_file_to_ftp($conn_id, $filename, $content)
{
	$tmp_dir = $GLOBALS['babAddonUpload'];
	if (!is_dir($tmp_dir))
		bab_mkdir($tmp_dir);
	$tmp_dir.='tmp/';
	if (!is_dir($tmp_dir))
		bab_mkdir($tmp_dir);

	if ($handle = fopen($tmp_dir.'ftp_tmp_file', 'w+')) {
        fwrite($handle, $content);
		fflush($handle);
		rewind($handle);
		ftp_fput($conn_id,$filename,$handle,FTP_BINARY);
		fclose($handle);
   }

}


function get_content_from_ftp($conn_id, $filename)
{
	$tmp_dir = $GLOBALS['babAddonUpload'];
	if (!is_dir($tmp_dir))
		bab_mkdir($tmp_dir);
	$tmp_dir.='tmp/';
	if (!is_dir($tmp_dir))
		bab_mkdir($tmp_dir);

	$contents = '';
	if ($handle = fopen($tmp_dir.'ftp_tmp_file', 'w+')) {
		ftp_fget($conn_id, $handle, $filename, FTP_BINARY);
		fflush($handle);
		rewind($handle);
		while (!feof($handle)) {
			  $contents .= fread($handle, 8192);
			}
		fclose($handle);
   }

	return $contents;
}


function fieldname_as_filename($fieldname, $extention, $field_id)
{
	$str = $fieldname.'.'.$extention;
	if ($str == FORM_FTP_ROW_INDEX)
		$str = $fieldname.$field_id.'.'.$extention;
	$replace = array('\\','/','~','|',':','*','?','"','<','>');
	return str_replace($replace, '-', $str);
}




function ov_directory($id_table) {

	global $babBody;
    class temp{
    
		var $res;
		var $directory;
		var $groups;
		
        function temp($id_table) {
			global $babDB;
			$this->t_submit = form_translate('Record');
			$this->t_id_group = form_translate('Ovidentia Group');
			$this->t_delete_password = form_translate('Delete password after the user creation');
			$this->id_table = $id_table;
			
			

			$this->res = $babDB->db_query("
				SELECT 
					f.id, 
					f.name, 
					d.directory_field 
				FROM 
					".FORM_TABLES_FIELDS." f 
					 LEFT JOIN ".FORM_TABLES_FIELDS_DIRECTORY." d ON d.id_table_field=f.id 
				WHERE 
					f.id_table=".$babDB->quote($id_table)." 
					AND f.field_function=''"
				);
				
			$this->directory = bab_getDirEntry();
			unset($this->directory['jpegphoto']);
			$this->directory['id_user'] = array('name' => form_translate('Id user'));
			$this->directory['nickname'] = array('name' => form_translate('Nickname'));
			$this->directory['password'] = array('name' => form_translate('Password'));

			$table = $babDB->db_fetch_assoc($babDB->db_query("
				SELECT id_group, delete_password FROM ".FORM_TABLES." WHERE id=".$babDB->quote($this->id_table)."
			"));
			
			$this->id_group = bab_toHtml($table['id_group']);
			$this->group_name = bab_toHtml(bab_getGroupName($table['id_group'], false), BAB_HTML_JS );
			$this->delete_password = 1 === (int) $table['delete_password'];
			
        }
        
        function getnextdirfield() {
        	if (list($name, $arr) = each($this->directory)) {
     			$this->selection = $name;
        		$this->dirfieldname = bab_toHtml($name);
        		$this->dirfieldlabel = bab_toHtml($arr['name']);
        		return true;
        	}
        	
        	return false;
        }

		function getnexttablefield() {
			global $babDB;

			if ($arr = $babDB->db_fetch_array($this->res)) {
				$this->fieldid = bab_toHtml($arr['id']);
				$this->fieldname = bab_toHtml($arr['name']);
				
				if (isset($_POST['ov_directory'][$this->dirfieldname]) && $_POST['ov_directory'][$this->dirfieldname] == $arr['id']) {
					$this->selected = true;
				} elseif (!$this->dirfieldname && $_POST['id_field_id_user'] == $arr['id']) {
					$this->selected = true;
				} else {
					$this->selected = $this->selection === $arr['directory_field'];
				}
				return true;
			}
			$babDB->db_data_seek($this->res, 0);
			return false;
		}
		
    }
    
    if (empty($id_table)) {
		return;
	}
    
    $tp = new temp($id_table);
    $babBody->addJavaScriptFile($GLOBALS['babScriptPath']."bab_dialog.js");
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "ov_directory"));
}


function ov_directory_upgrade($id_table) {

	global $babBody;
    class temp{
    
		var $modified;
		var $name;
		
        function temp($id_table) {
        
        	$this->t_modifications = form_translate('The users has been modified');
        	$this->t_nochanges = form_translate('The table is up to date');
        
			include_once $GLOBALS['babAddonPhpPath']."tabledirectory.php";
			
			$obj = new form_tableDirectory(bab_gp('id_table'));
			$members = $obj->members();
			
			foreach($members as $arr) {
				if ($obj->updateRowFromUser($arr['id'])) {
					$this->modified[] = $arr['name'];
				}
			}
			
			$this->modifications = !empty($this->modified);
        }
        
        
        function getnext() {
        	if (list(,$name) = each($this->modified)) {
        		$this->name = bab_toHtml($name);
        		return true;
        	}
        	
        	return false;
        }
    }
    
    if (empty($id_table)) {
		return;
	}
    
    $tp = new temp($id_table);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "ov_directory_upgrade"));
}









function ov_calendar($id_table) {

	global $babBody;
    class temp{
    
		var $res;
		var $directory;
		var $groups;
		
        public function __construct($id_table) {
			global $babDB;
			$this->t_submit = form_translate('Record');
			$this->t_calendar_name = form_translate('Calendar name');
			$this->t_SUMMARY = form_translate('Titre');
			$this->t_DESCRIPTION = form_translate('Description');
			$this->t_CATEGORIES = form_translate('Category');
			$this->t_LOCATION = form_translate('Location');
			$this->t_DTSTART = form_translate('Start date and time');
			$this->t_DTEND = form_translate('End date and time');
			$this->t_daydate = form_translate('Event date');
			$this->t_dates_help = form_translate('The events can use two date time fields or one date field');
			$this->id_table = $id_table;
			
			
			$res = $babDB->db_query("
				SELECT calendar_name FROM ".FORM_TABLES." WHERE id=".$babDB->quote($id_table)." 
			");
			
			$arr = $babDB->db_fetch_assoc($res);
			$this->calendar_name = bab_toHtml($arr['calendar_name']);
			

			$this->res = $babDB->db_query("
				SELECT 
					f.id, 
					f.name, 
					f.calendar_property 
				FROM 
					".FORM_TABLES_FIELDS." f 
				WHERE 
					f.id_table=".$babDB->quote($id_table)." 
					AND f.field_function=''"
				);
				
				
			$this->resdate = $babDB->db_query("
				SELECT 
					f.id, 
					f.name, 
					f.calendar_property 
				FROM 
					".FORM_TABLES_FIELDS." f, 
					".FORM_TABLES_FIELDS_TYPES." t
				WHERE 
					f.id_table=".$babDB->quote($id_table)." 
					AND f.field_function='' 
					AND f.id_type=t.id 
					AND t.col_type LIKE 'date'" 
				);
				
				
			$this->resdatetime = $babDB->db_query("
				SELECT 
					f.id, 
					f.name, 
					f.calendar_property 
				FROM 
					".FORM_TABLES_FIELDS." f, 
					".FORM_TABLES_FIELDS_TYPES." t
				WHERE 
					f.id_table=".$babDB->quote($id_table)." 
					AND f.field_function='' 
					AND f.id_type=t.id 
					AND t.col_type LIKE 'datetime'" 
				);

			
        }
        

		private function getnexttablefield() {
			global $babDB;

			if ($arr = $babDB->db_fetch_array($this->res)) {
				$this->fieldid = bab_toHtml($arr['id']);
				$this->fieldname = bab_toHtml($arr['name']);
				
				
				return $arr;
			}
			
			if ($babDB->db_num_rows($this->res))
			{
				$babDB->db_data_seek($this->res, 0);
			}
			return false;
		}
		
		
		public function getnextsummaryfield()
		{
			if ($arr = $this->getnexttablefield())
			{
				$this->selected = 'SUMMARY' === $arr['calendar_property'];
				return true;
			}
			return false;
		}
		
		
    	public function getnextdescriptionfield()
		{
			if ($arr = $this->getnexttablefield())
			{
				$this->selected = 'DESCRIPTION' === $arr['calendar_property'];
				return true;
			}
			return false;
		}
		
		
    	public function getnextcategoriesfield()
		{
			if ($arr = $this->getnexttablefield())
			{
				$this->selected = 'CATEGORIES' === $arr['calendar_property'];
				return true;
			}
			return false;
		}
		
		
    	public function getnextlocationfield()
		{
			if ($arr = $this->getnexttablefield())
			{
				$this->selected = 'LOCATION' === $arr['calendar_property'];
				return true;
			}
			return false;
		}
		
		
		public function getnextdatetablefield()
		{
			global $babDB;

			if ($arr = $babDB->db_fetch_assoc($this->resdate)) {
				$this->fieldid = bab_toHtml($arr['id']);
				$this->fieldname = bab_toHtml($arr['name']);
				$this->selected = 'date' === $arr['calendar_property'];
				
				return true;
			}
			
			if ($babDB->db_num_rows($this->resdate))
			{
				$babDB->db_data_seek($this->resdate, 0);
			}
			return false;
		}
		
		
    	private function getnextdatetimetablefield()
		{
			global $babDB;

			if ($arr = $babDB->db_fetch_assoc($this->resdatetime)) {
				$this->fieldid = bab_toHtml($arr['id']);
				$this->fieldname = bab_toHtml($arr['name']);
				
				return $arr;
			}
			
			if ($babDB->db_num_rows($this->resdatetime))
			{
				$babDB->db_data_seek($this->resdatetime, 0);
			}
			return false;
		}
		
		
		
		public function getnextdtstartfield()
		{
			if ($arr = $this->getnextdatetimetablefield())
			{
				$this->selected = 'DTSTART' === $arr['calendar_property'];
				return true;
			}
			return false;
		}
		
		
    	public function getnextdtendfield()
		{
			if ($arr = $this->getnextdatetimetablefield())
			{
				$this->selected = 'DTEND' === $arr['calendar_property'];
				return true;
			}
			return false;
		}
		
    }
    
    if (empty($id_table)) {
		return;
	}
    
    $tp = new temp($id_table);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."user.html", "ov_calendar"));
}











/* record */

function record_filing_lock_table()
{
require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
$db = &$GLOBALS['babDB'];

if (!isset($_POST['pageid']) || count($_POST['pageid']) <= 0) {
	return false;
}

$db->db_query("UPDATE ".form_tbl_name($_POST['id_table'])." SET form_filed='N',form_locked='N' WHERE form_id IN('".implode("','",$_POST['pageid'])."')");

if (isset($_POST['filing']) && is_array($_POST['filing']) && count($_POST['filing']) > 0)
	{
	foreach ($_POST['filing'] as $value)
		{
		$db->db_query("UPDATE ".$db->db_escape_string(form_tbl_name($_POST['id_table']))." SET form_filed='Y' WHERE form_id='".$db->db_escape_string($value)."'");
		}
	}

if (isset($_POST['lock']) && is_array($_POST['lock']) && count($_POST['lock']) > 0)
	{
	foreach ($_POST['lock'] as $value)
		{
		$db->db_query("UPDATE ".form_tbl_name($_POST['id_table'])." SET form_locked='Y' WHERE form_id='".$db->db_escape_string($value)."'");
		}
	}

if (isset($_POST['delete']) && is_array($_POST['delete']))
	{
	form_deleteTableRows($_POST['id_table'], $_POST['delete']);
	}
}

function record_approbations()
{
require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");


$db = &$GLOBALS['babDB'];

$res = $db->db_query("SELECT id_line, id_schi FROM ".FORM_APPROB_SCHI." WHERE id_table='".$db->db_escape_string($_POST['id_table'])."'");
$schi = array();
while ($arr = $db->db_fetch_array($res))
	{
	$schi[$arr['id_line']] = $arr['id_schi'];
	}


$prefix = 'approb_';

foreach ($_POST as $field => $value)
	{
	if (substr($field,0,strlen($prefix)) == $prefix)
		{
		list(,$form_id) = explode('_',$field);
		if (isset($schi[$form_id]) && $value != 0)
			{
			bab_WFDeleteInstance($schi[$form_id]);
			$db->db_query("DELETE FROM ".FORM_APPROB_SCHI." WHERE id_schi='".$db->db_escape_string($schi[$form_id])."'");
			}

		$db->db_query("UPDATE ".$db->db_escape_string(form_tbl_name($_POST['id_table']))." SET form_approb='".$db->db_escape_string($value)."' WHERE form_id='".$db->db_escape_string($form_id)."'");

		}
	}
}

function record_view_cols()
{
$db = &$GLOBALS['babDB'];

$res = $db->db_query("SELECT id_table_field FROM ".FORM_TABLES_VIEW_COLS." WHERE id_table='".$db->db_escape_string($_POST['id_table'])."'");
$selected = array();
while ($arr = $db->db_fetch_array($res))
	{
	$selected[$arr['id_table_field']] = true;
	}

if (isset($_POST['table_view_cols']) && count($_POST['table_view_cols']) > 0)
	{
	foreach ($_POST['table_view_cols'] as $id_col)
		{
		if (!isset($selected[$id_col]))
			{
			$db->db_query("INSERT INTO ".FORM_TABLES_VIEW_COLS." (id_table_field,id_table) VALUES ('".$db->db_escape_string($id_col)."','".$db->db_escape_string($_POST['id_table'])."')");
			}
		else
			{
			unset($selected[$id_col]);
			}
		}

	if (count($selected) > 0)
		{
		$db->db_query("DELETE FROM ".FORM_TABLES_VIEW_COLS." WHERE id_table_field IN(".$db->quote(array_keys($selected)).") AND id_table = '".$db->db_escape_string($_POST['id_table'])."'");
		}
	}
}

function file_export_gettable($id_table,$fields,$separator)
	{
	
	$db = &$GLOBALS['babDB'];
	$res = $db->db_query("SELECT * FROM ".$db->db_escape_string(form_tbl_name($id_table))."");
	$l = 1;
	$line = array(0=>'');
	while($arr = $db->db_fetch_array($res))
		{
		$e = 0;
		foreach ($arr as $key => $value)
			{
			$change = array("\r" => " ","\n" => " ",'"'=>'""');
			$value = strtr($value, $change);
			$key = strtr($key, $change);
			
			if (preg_match('/^\d{4}_\w*$/',$key))
				{
				
				list($col) = explode('_',$key);
				if (is_numeric($col) && in_array(intval($col),$fields))
					{
					if (!isset($line[$l])) $line[$l] = '';
					if ($l == 1)
						$line[0] .= $e == 0 ? '"'.$key.'"' : $separator.'"'.$key.'"';
					$line[$l] .= $e == 0 ? '"'.$value.'"' : $separator.'"'.$value.'"';
					$e++;
					}
				}
			elseif ($key == 'form_id' && in_array('id',$fields))
				{
				if (!isset($line[$l])) $line[$l] = '';
				if ($l == 1)
					$line[0] .= $e == 0 ? '"id"' : $separator.'"id"';
				$line[$l] .= $e == 0 ? '"'.$value.'"' : $separator.'"'.$value.'"';
				$e++;
				}
			}
		
		$l++;
		}

	return $line;
	}

function file_export_record($to_file, &$line)
	{
	global $babBody;
	$dir = dirname($to_file);
	if (!is_dir($dir))
		{
		$babBody->msgerror = form_translate('The folder does not exist');
		return false;
		}

	if (!is_writable($dir))
		{
		$babBody->msgerror = form_translate('The folder is not writable');
		return false;
		}

	$file = sprintf($to_file,date('YmdHis'));

	if (!$handle = fopen($file, 'w')) {
		$babBody->msgerror = form_translate('Cannot open/create file').' : '.basename($file);
		return false;
		}

	foreach ($line as $k => $value)
		{
		if (fwrite($handle, $value."\n") === false) {
			$babBody->msgerror = form_translate('Cannot write to file').' '.basename($file).' '.form_translate('line').' ('.($k+1).')';
			return false;
			}
		}

	fclose($handle);

	return true;
	}

function file_export()
	{
	$db = &$GLOBALS['babDB'];
	global $babBody;

	if (isset($_POST['field']) && is_array($_POST['field']) && count($_POST['field']) > 0)
		{
		$line = & file_export_gettable($_POST['id_table'],$_POST['field'],$_POST['separator']);

		if (!empty($_POST['fav_name']))
			{
			if (list($id) = $db->db_fetch_array($db->db_query("SELECT id FROM ".FORM_TABLES_EXPORT_FAV." WHERE name='".$db->db_escape_string($_POST['fav_name'])."'")))
				{
				$db->db_query("UPDATE ".FORM_TABLES_EXPORT_FAV." SET id_table='".$db->db_escape_string($_POST['id_table'])."', name='".$db->db_escape_string($_POST['fav_name'])."', filename='".$db->db_escape_string($_POST['to_file'])."', cols='".$db->db_escape_string(implode(',',$_POST['field']))."', csv_separator='".$db->db_escape_string($_POST['separator'])."' WHERE id='".$db->db_escape_string($id)."'");
				}
			else
				{
				$db->db_query("INSERT INTO ".FORM_TABLES_EXPORT_FAV." (id_table, name, filename, cols, csv_separator) VALUES ('".$db->db_escape_string($_POST['id_table'])."', '".$db->db_escape_string($_POST['fav_name'])."', '".$db->db_escape_string($_POST['to_file'])."', '".$db->db_escape_string(implode(',',$_POST['field']))."', '".$db->db_escape_string($_POST['separator'])."')");
				}
			
			}

		if (!empty($_POST['to_file']))
			{
			file_export_record($_POST['to_file'], $line);
			return true;
			}
		
		if (count($line) > 1)
			{
			header('Content-Disposition: attachment; filename="'.form_getTableName($_POST['id_table']).'.csv"');
			header('Content-Type: text/csv');
			foreach ($line as $value)
				echo $value."\n";
			die();
			}
		}
	}


function file_import()
{
	$db = &$GLOBALS['babDB'];
	$cols = array();
	$col_to_import = array();
	foreach($_POST as $field => $value)
		{
		$tmp = explode('_',$field);
		if ($tmp[0] == 'field' && is_numeric($tmp[1]) && is_numeric($value))
			{
			$cols[] = $tmp[1];
			$col_to_import[$tmp[1]] = $value;
			}
		}
	
	$col_name = array();
	$col_index = array();
	$reference = array();
	
	$res = $db->db_query("SELECT id,name FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$db->db_escape_string($_POST['id_table'])."' AND id IN(".implode(',',$cols).")");
	while($arr = $db->db_fetch_array($res))
		{
		if (is_numeric($col_to_import[$arr['id']]))
			{
			$colname_str = form_col_name($arr['id'],$arr['name']);
			$col_name[] = $colname_str;
			$col_index[] = $col_to_import[$arr['id']];
			if (isset($_POST['reference']) && in_array($arr['id'],$_POST['reference'])) {
				$reference[$colname_str] = $col_to_import[$arr['id']];
				}
			}
		}
	
	
	
	ini_set('max_execution_time',3600);
	
	$handle = fopen(CSV_SWAP, "r");
	$size = filesize(CSV_SWAP);
	fgetcsv($handle, $size, $_POST['separator']);
	while ($data = fgetcsv($handle, $size, $_POST['separator'])) 
		{
		$insert = true;
		if (0 < count($reference)) {
			$params = array();
			
			foreach($reference as $colname => $csv) {
				$params[] = $colname." LIKE '".$db->db_escape_like($data[$csv])."'";
				}
	
			list($n) = $db->db_fetch_array($db->db_query("SELECT COUNT(*) FROM ".$db->db_escape_string(form_tbl_name($_POST['id_table']))." WHERE ".implode(' AND ',$params)));
	
			if (0 < $n) {
				$insert = false;
				}
			}
			
			
		$col = array();
	
		if (true == $insert) {
	
			
	
			for ($i = 0 ; $i < count($col_index) ; $i++)
				{
				$col[$i] = isset($data[$col_index[$i]]) ? "'".$db->db_escape_string($data[$col_index[$i]])."'" : "''";
				}
			$query = "INSERT INTO ".$db->db_escape_string(form_tbl_name($_POST['id_table']))." (form_lastupdate, form_lastupdate_user, form_create, form_create_user,".implode(',',$col_name).") VALUES (NOW(), '".$db->db_escape_string($GLOBALS['BAB_SESS_USERID'])."', NOW(), '".$db->db_escape_string($GLOBALS['BAB_SESS_USERID'])."', ".implode(',',$col).")";
			$db->db_query($query);
			}
		else 
			{
			for ($i = 0 ; $i < count($col_index) ; $i++)
				{
				$coldata = isset($data[$col_index[$i]]) ? $db->db_escape_string($data[$col_index[$i]]) : "";
				$col[$i] = $col_name[$i]."='".$coldata."'";
				}
			$query = "UPDATE ".$db->db_escape_string(form_tbl_name($_POST['id_table']))." SET form_lastupdate=NOW(), form_lastupdate_user='".$db->db_escape_string($GLOBALS['BAB_SESS_USERID'])."' , ".implode(',',$col)." WHERE ".implode(' AND ',$params);
			$db->db_query($query);
			
			}
		}
	fclose($handle);
	
	$handle = fopen(CSV_SWAP, "w");
	fclose($handle);
}



function delete_fav()
{

if (isset($_POST['fav']) && is_array($_POST['fav']) && count($_POST['fav']) > 0)
	{
	$db = &$GLOBALS['babDB'];
	$db->db_query("DELETE FROM ".FORM_TABLES_EXPORT_FAV." WHERE id IN(".$db->quote($_POST['fav']).")");
	}
}


function record_ov_directory() {

	global $babBody, $babDB;
	$id_field_id_user = $_POST['ov_directory']['id_user'];
	$id_group = bab_pp('id_group');
	
	if (empty($id_field_id_user)) {
		$babBody->addError(form_translate('the id user must be defined'));
		return false;
	}
	
	if (empty($id_group)) {
		$babBody->addError(form_translate('You must select a group'));
		return false;
	}
	
	$res = $babDB->db_query("
	
		SELECT f.id FROM 
			".FORM_TABLES_FIELDS." f,
			".FORM_TABLES_FIELDS_TYPES." t 
		WHERE 
			f.id_type = t.id 
			AND t.id_extend='4' 
			AND f.id=".$babDB->quote($id_field_id_user));
			
	if (0 == $babDB->db_num_rows($res)) {
		$babBody->addError(form_translate('the id user must be a field with a Ovidentia User type'));
		return false;
	}
	
	$babDB->db_query("UPDATE ".FORM_TABLES." SET id_group=".$babDB->quote($id_group)." WHERE id=".$babDB->quote(bab_pp('id_table')));
	
	$fields = array();
	
	foreach($_POST['ov_directory'] as $dirfield => $id_field) {
		if (!empty($id_field)) {
			$fields[$id_field] = $dirfield;
		}
	}
	
	$res = $babDB->db_query("SELECT id FROM ".FORM_TABLES_FIELDS." WHERE id_table=".$babDB->quote(bab_pp('id_table')));
	while ($arr = $babDB->db_fetch_assoc($res)) {
		$babDB->db_query("DELETE FROM ".FORM_TABLES_FIELDS_DIRECTORY." WHERE id_table_field=".$babDB->quote($arr['id']));
	}
	
	foreach($fields as $id_field => $dirfield) {
		$babDB->db_query("INSERT INTO ".FORM_TABLES_FIELDS_DIRECTORY." 
		(id_table_field, directory_field) VALUES (".$babDB->quote($id_field).",".$babDB->quote($dirfield).")");
	}
	
	return true;
}







function record_ov_calendar()
{
	global $babBody, $babDB;
	
	$id_table = bab_pp('id_table');
	$fields = bab_pp('ov_calendar');
	$calendar_name = bab_pp('calendar_name');
	
	// update calendar_name in table
	
	$babDB->db_query('UPDATE '.FORM_TABLES.' SET calendar_name='.$babDB->quote($calendar_name).' WHERE id='.$babDB->quote($id_table));
	
	
	
	// update fields
	
	$babDB->db_query('UPDATE '.FORM_TABLES_FIELDS." SET calendar_property='' WHERE id_table=".$babDB->quote($id_table));
	
	foreach($fields as $property => $id_field)
	{
		$babDB->db_query('UPDATE '.FORM_TABLES_FIELDS." SET calendar_property=".$babDB->quote($property)." WHERE id=".$babDB->quote($id_field)." AND id_table=".$babDB->quote($id_table));
	}
	
	
	
	return true;
}


/* main */

$idx = bab_rp('idx');

if (isset($_POST['action']))
	switch ($_POST['action'])
		{
		case "filing_lock_table":
			record_filing_lock_table();
			record_approbations();
			record_view_cols();
			break;

		case "import2":
			file_import();
			break;
		
		case "export":
			file_export();
			break;

		case "favlist":
			delete_fav();
			break;
			
		case 'ov_directory':
			if (!record_ov_directory()) {
				$idx = 'ov_directory';
			}
			break;
			
			
		case 'ov_calendar':
			if (!record_ov_calendar()) {
				$idx = 'ov_calendar';
			}
			break;

		}

$FORM_USER = form_getUserFormAcces();

if (!$FORM_USER['ADMIN'] && $idx != 'favexec')
	{
	$babBody->msgerror = form_translate('Access denied');
	return;
	}

$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
$babBody->addItemMenu("browse_tables", form_translate("Browse tables"),$GLOBALS['babAddonUrl']."user&idx=browse_tables");
$babBody->addItemMenu("favlist", form_translate("Favorites"),$GLOBALS['babAddonUrl']."user&idx=favlist");


$id_table = bab_rp('id_table');

switch ($idx)
	{
	case 'favlist':
		$babBody->title = form_translate("Export table from favorite");
		favlist();
		break;
		
	case 'favexec':
		favexec($_GET['id_fav']);
		break;
		
	case 'help':
		if (!empty($_GET['help_id']))
			help($_GET['help_id']);
		else
			die('need help_id');
		break;
		
	case "browse_rows":
		if (!isset($id_table))
			break;
		$babBody->setTitle(form_translate("Browse rows from table").' "'.form_getTableName($id_table).'"');
		$babBody->addItemMenu("browse_rows", form_translate("Browse rows"),$GLOBALS['babAddonUrl']."user&idx=browse_rows");
		browse_rows($id_table);
		break;
		
	case "wf_users":
		wf_users($_GET['idschi']);
		break;
		
	case "import":

		$babBody->addItemMenu("import", form_translate("Import"),$GLOBALS['babAddonUrl']."user&idx=import");
		$babBody->setTitle(form_translate("Import into").' "'.form_getTableName($id_table).'"');
		import($id_table);
		break;
		
	case "export":

		$babBody->addItemMenu("export", form_translate("Export"),$GLOBALS['babAddonUrl']."user&idx=export");
		$babBody->setTitle(form_translate("Export").' "'.form_getTableName($id_table).'"');
		export($id_table);
		break;


	case 'ov_directory':
		$babBody->addItemMenu("ov_directory", form_translate("Ovidentia directory"),$GLOBALS['babAddonUrl']."user&idx=ov_directory");
		$babBody->setTitle(form_translate("Link to Ovidentia directory"));
		ov_directory($id_table);
		break;
		
	case 'ov_directory_upgrade':
		$babBody->addItemMenu("ov_directory_upgrade", form_translate("Ovidentia directory"),$GLOBALS['babAddonUrl']."user&idx=ov_directory_upgrade");
		$babBody->setTitle(form_translate("Upgrade table from directory"));
		ov_directory_upgrade($id_table);
		break;
		
	case 'ov_calendar':
		$babBody->addItemMenu("ov_calendar", form_translate("Ovidentia calendar"),$GLOBALS['babAddonUrl']."user&idx=ov_calendar");
		$babBody->setTitle(form_translate("Link to Ovidentia calendar"));
		ov_calendar($id_table);
		break;

	default:
	case "browse_tables":
		$babBody->title = form_translate("Browse tables");
		browse_tables();
		break;
	}
$babBody->setCurrentItemMenu($idx);
?>