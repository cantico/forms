<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function help_list()
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = & $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_help_title = form_translate('Title');
			$this->t_help_text = form_translate('Text');
			$this->t_delete = form_translate('Delete');
			$this->js_alert = form_translate('Do you really want to delete the selected items').'?';

			$this->res = $this->db->db_query("
				SELECT h.* 
				FROM 
					".FORM_HELP." h,
					".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					h.id=w.id_object 
					AND w.type='form_help' 
					AND w.id_workspace=".$this->db->quote(form_getWorkspace())."
			");
			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);

				$this->arr['name'] = bab_toHtml($this->arr['name']);
				$this->arr['help_title'] = bab_toHtml($this->arr['help_title']);

				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."help.html", "help_list"));
}

function help_edit()
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = & $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_help_title = form_translate('Title');
			$this->t_help_text = form_translate('Text');
			$this->t_delete = form_translate('Delete');
			$this->t_submit = form_translate('Submit');
			
			if (isset($_POST) && count($_POST) > 0)
				{
				$this->arr = $_POST;
				}
			elseif (isset($_GET['id_help']))
				{
				$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_HELP." WHERE id='".$this->db->db_escape_string($_GET['id_help'])."'"));
				}
			else
				{
				$el_to_init = array('id','name','help_title','help_text');
				foreach($el_to_init as $field)
					{
					$this->arr[$field] = '';
					}
				}
           
			$this->arr['name'] = bab_toHtml($this->arr['name']);
			$this->arr['help_title'] = bab_toHtml($this->arr['help_title']);
			$this->arr['help_text'] = bab_toHtml($this->arr['help_text']);
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."help.html", "help_edit"));
}

function help_view()
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = & $GLOBALS['babDB'];

			
			$arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_HELP." WHERE id='".$this->db->db_escape_string($_GET['help_id'])."'"));
			
			$this->title = & $arr['help_title'];
			$this->text = & $arr['help_text'];
			$this->t_close = form_translate('Close');
            }

		
        }

	$tp = & new temp();

	include_once $GLOBALS['babInstallPath']."utilit/uiutil.php";
	$GLOBALS['babBodyPopup'] = new babBodyPopup();
	$GLOBALS['babBodyPopup']->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."help.html", "help_view"));
	printBabBodyPopup();
	die();
}

function help_delete()
	{
	if (isset($_POST['id_help']) && count($_POST['id_help']) > 0)
		{
		$db = & $GLOBALS['babDB'];
		$db->db_query("DELETE FROM ".FORM_HELP." WHERE id IN (".$db->quote($_POST['id_help']).")");
		}
	}

function record_help()
	{
	$db = & $GLOBALS['babDB'];

	if (empty($_POST['id_help']))
		{
		
		$db->db_query("INSERT INTO ".FORM_HELP." (name, help_title, help_text) VALUES ('".$db->db_escape_string($_POST['name'])."','".$db->db_escape_string($_POST['help_title'])."','".$db->db_escape_string($_POST['help_text'])."')");
		
		form_ieO::inserted('form_help', $db->db_insert_id());
		}
	else
		{
		$db->db_query("UPDATE ".FORM_HELP." 
		SET 
			name = '".$db->db_escape_string($_POST['name'])."', 
			help_title = '".$db->db_escape_string($_POST['help_title'])."', 
			help_text = '".$db->db_escape_string($_POST['help_text'])."' 
		WHERE 
			id = '".$db->db_escape_string($_POST['id_help'])."'");
		}
	}

// main

$idx = bab_rp('idx','list');

if (isset($_POST['action']))
{

switch ($_POST['action'])
	{
	case 'edit':
		record_help();
		break;

	case 'delete':
		help_delete();
		break;
	}
}

$babBody->addItemMenu("forms", form_translate("Forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
$babBody->addItemMenu("list", form_translate("List"),$GLOBALS['babAddonUrl']."help&idx=list");



switch ($idx)
	{
	case 'view':
		help_view();
		break;

	case 'edit':
		$babBody->title = form_translate("Edit help");
		$babBody->addItemMenu("edit", form_translate("Edit"), $GLOBALS['babAddonUrl']."help&idx=edit");
		help_edit();

		break;

	default:
	case 'list':
		$babBody->addItemMenu("edit", form_translate("Add"), $GLOBALS['babAddonUrl']."help&idx=edit");
		$babBody->title = form_translate("Help list");
		help_list();
		break;
	}

$babBody->setCurrentItemMenu($idx);



?>