<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");
require_once( $GLOBALS['babInstallPath']."addons/forms/linksincl.php");
require_once( $GLOBALS['babInstallPath']."addons/forms/form_parents.php");


function tpl_list($id)
	{
    $tp = new form_list($id);
	$tp->list_query();
	$tp->echohtml($tp->html_file,"list_mode");
	}

function tpl_waiting($id)
	{
	global $babBody;
    
    $tp = new form_list($id,0);
	if (!$tp->approbCtrl($tp->arr['id_table']))
		{
		$babBody->msgerror = form_translate('This form type is allowed only with approbation tables');
		return false;
		}
	
	$tp->list_query();
	$tp->echohtml($tp->html_file,"list_mode");
	}



function tpl_edit($id)
	{
    class temp extends form_edit
        { 
		
        function temp($id)
            {
			$this->id = $id;
			$this->form_edit();
            }
        }

	$tp = new temp($id);
	$tp->echoOvml('default/edit.ovml');
	}




function tpl_view($id)
	{
    $tp = new form_view($id);
	//$tp->echohtml($tp->html_file,"view_mode");
	$tp->echoOvml('default/view.ovml');
	}


function tpl_sl($id)
	{
    class temp extends form_list
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->t_qte = form_translate('Quantities');
			$this->t_record_quantities = form_translate('Record quantities');

			$this->id = $id;
			$this->form_list($id);
			
			$this->quantities = & $this->cardCtrl($this->arr['id_table']);

			$this->list_query();
            }

	
        }
    $tp = new temp($id);
	$tp->echohtml($tp->html_file,"sl_mode");
	}


function tpl_sc($id)
	{
    class temp extends form_application
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->t_qte = form_translate('Quantities');
			$this->t_record_quantities = form_translate('Record quantities');
			$this->t_total_prod = form_translate('Total');

			$this->id = $id;
			$this->form_application();
			$this->listCtrl();
			$this->max = 0;

			if (empty($this->arr['id_table']))
				{
				return;
				}

			$this->orderCtrl();
			
			$this->card = & $this->cardCtrl($this->arr['id_table']);


			if ($this->card)
				{
				$req = "SELECT * FROM ".form_tbl_name($this->arr['id_table'])." WHERE form_id IN(".$this->db->quote(array_keys($this->card)).") ";
				
				if (isset($this->query_order)) {
					$req .= $this->query_order;
				}	
				
				
				$this->resrow = $this->db->db_query($req);
				$this->countrow = $this->db->db_num_rows($this->resrow);
				}
			else
				{
				$GLOBALS['babBody']->msgerror = form_translate('Shopping cart is empty');
				}

			$this->countcol = $this->db->db_num_rows($this->rescol);

			$this->total_qte = 0;
			$this->total_sub = 0;

            }

		function getnextrow()
			{
			static $i=0;
			if( isset($this->resrow) && $this->resrow !== false && $i < $this->countrow)
				{
				$this->altbg = !$this->altbg;
				$this->row = $this->db->db_fetch_array($this->resrow);
				$this->row['qte'] = $this->card[$this->row['form_id']];
				
				$this->total_qte += $this->row['qte'];
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}

		function getnextcol()
			{
			static $i=0;
			if( $i < $this->countcol)
				{
				$this->col = $this->db->db_fetch_array($this->rescol);

				$this->help_url = $GLOBALS['babAddonUrl']."help&idx=view&help_id=".$this->col['help_id'];

				$this->orderby = !empty($this->arr['id_order_by']) && $this->arr['id_order_by'] == $this->col['id_table_field'] ? $this->arr['order_type'] : 'NO';
				
				if (!empty($this->id_app) && isset($this->row['form_id']) && ($this->col['id_print'] == 8 || $this->col['id_print'] == 5 ) && isset($this->form_links[$this->col['id']]))
					{
					$this->link = true;
					$this->popup = $this->col['id_print'] == 8 ? true : false ;
					$this->type = $this->form_links[$this->col['id']]['id_type'];
					$this->link_url = $GLOBALS['babAddonUrl']."form&idx=".$this->type."&id_app=".$this->id_app."&id_step=".$this->form_links[$this->col['id']]['id_step']."&id_form=".$this->form_links[$this->col['id']]['id_form']."&form_row=".$this->row['form_id']."&popup=".$this->form_links[$this->col['id']]['popup']."&parent_id_form=".$this->id."&parent_id_step=".$this->id_step."&form_value=";
					}
				else
					{
					$this->link = false;
					}


				if ($this->col['search'] == 'Y')
					$this->find = form_translate('Find');

				$col = form_col_name($this->col['id_table_field'],$this->col['name_table_field']);
				if (!empty($this->col['field_function']))
					{
					$this->full = form_field_function($this->col['field_function'],$this->row);
					$this->bloburl = '';
					}
				elseif (isset($this->row[$col]))
					{
					if (!empty($this->col['id_table_field_link']))
						{
						$this->full = form_getLinkedValue($this->col['link_table_field_id'], $this->col['id_table_field_link'], $this->row[$col]);
						}
					elseif(!empty($this->col['blob_link']))
						{
						if (!empty($this->row[$col]))
							{
							$this->bloburl = $GLOBALS['babAddonUrl'].'form&idx=blob&id_table_field='. $this->col['id_table_field'].'&form_row='. $this->row['form_id'];
							}
						$this->full = '';
						}
					else
						{
						$this->bloburl = '';
						$this->full = $this->row[$col];
						}
					}

				if (!empty($this->link_url) && !empty($this->full) && strlen($this->full) < FORM_VIEW_MAX_CHAR) 
					$this->link_url .= urlencode(urlencode($this->full));

				if (!empty($this->col['format_date']))
					form_dateFormat($this->full);
				
				if (isset($this->full) && is_numeric($this->full)) {
					$this->card_product_cols[$this->col['id_table_field']] = $this->full;
				}
				
				if (isset($this->full) && strlen($this->full) > FORM_VIEW_MAX_CHAR)
					$this->short = substr($this->full,0,FORM_VIEW_MAX_CHAR).'...';
				else
					{
					$this->short = isset($this->full) ? $this->full : '';
					$this->full = '';
					}
					
				$this->full = bab_toHtml($this->full);
				$this->short = bab_toHtml($this->short, BAB_HTML_ENTITIES | BAB_HTML_BR | BAB_HTML_LINKS);

				$i++;
				return true;
				}
			else
				{
				$i=0;
				if ($this->countcol > 0) $this->db->db_data_seek($this->rescol,0);

				$this->row['total'] = isset($this->row) && isset($this->card_product_cols) ? form_formula_sc($this->arr['sc_formula'],$this->card_product_cols,$this->row['qte']) : 0;
				$this->total_sub += $this->row['total'];
				return false;
				}
			}

		function main_total()
			{
			$this->total_full = form_formula_trans($this->arr['trans_formula'],$this->total_sub,$this->total_qte);
			return false;
			}
        }
    $tp = new temp($id);
	$tp->echohtml($tp->html_file,"sc_mode");
	}


function tpl_trans($id)
	{
    class temp extends form_edit
        { 
		
        function temp($id)
            {
			$this->id = $id;
			$this->form_edit();

			if ($this->approbCtrl($this->arr['id_table']))
				$this->access = false;

			$this->t_card_msg = form_translate('Shopping cart');
			$this->t_total_qte = form_translate('Total quantities');
			$this->t_total = form_translate('Total');

			// trouver table associ�e au shopping cart
			$this->sc_card = $this->db->db_fetch_array($this->db->db_query("SELECT id_table, sc_formula, trans_formula, unit_concat_before, unit_concat FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->arr['id_form_card'])."'"));

			$this->card = & $this->cardCtrl( $this->sc_card['id_table'] );

			$this->total_qte = 0;
			$this->total_sub = 0;
			$this->total_full = 0;


			if ($this->card)
				{
				// colonnes
				$cols = form_getTableCols($this->sc_card['id_table']);

				// card
				$req = "SELECT * FROM ".form_tbl_name($this->sc_card['id_table'])." WHERE form_id IN(".$this->db->quote(array_keys($this->card)).")";
				$resrow = $this->db->db_query($req);

				while($arr = $this->db->db_fetch_array($resrow))
					{
					$arr['qte'] = $this->card[$arr['form_id']];
					$this->total_qte += $arr['qte'];

					$card_product_cols = array();
					foreach ($cols as $id => $name)
						{
						$card_product_cols[$id] = $arr[$name];
						}

					$arr['total'] = form_formula_sc($this->sc_card['sc_formula'], $card_product_cols, $arr['qte']);

					$this->total_sub += $arr['total'];
					}

				$this->total_full = form_formula_trans($this->sc_card['trans_formula'], $this->total_sub, $this->total_qte);
				}
			else
				{
				
				$GLOBALS['babBody']->msgerror = form_translate('Shopping cart is empty');
				}
            }

        }

	$tp = new temp($id);
	$tp->echohtml($tp->html_file,"trans_mode");
	}


function tpl_approb($id)
	{
    class temp extends form_edit
        { 
		
        function temp($id)
            {
			$this->id = $id;
			$this->form_edit();

			

			$this->t_approbation = form_translate('Approbation');
			$this->t_validation = form_translate('Validation');

			$this->t_accept = form_translate('Accept');
			$this->t_refuse = form_translate('Refuse');
			$this->t_novalidate = form_translate('Do not validate');

			if (!$this->approbCtrl($this->arr['id_table']))
				{
				$this->access = false;
				}

			$this->form_validation = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath']."html_utilities.html", 'form_validation');

            }

        }

	$tp = new temp($id);
	$tp->echoOvml('default/edit.ovml');
	}


// utilities


function application()
{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("SELECT id_first_step id FROM ".FORM_APP_APPLICATIONS." WHERE id='".$db->db_escape_string($_GET['id_app'])."'");
	$step = $db->db_fetch_array($res);
	if (empty($step['id']))
		{
		$query = "SELECT id FROM ".FORM_APP_STEPS." WHERE id_application='".$db->db_escape_string($_GET['id_app'])."' LIMIT 0,1";
		$step = $db->db_fetch_array($db->db_query($query));
		}
		
	if (!$step) {
		global $babBody;
		$babBody->addError(form_translate('Error : no application step'));
		return false;
	}
	
	$url = $GLOBALS['formAddonUrl']."form&id_app=".urlencode($_GET['id_app'])."&trt_step=1&id_step=".urlencode($step['id']);
	header("location:".$url);
	exit;
}







	

/**
 * Traitement des �tapes
 */
function trt_step()
{

	$id_step = form_currentIdStep();

	if (empty($id_step)) {
	
		global $babDB, $id_app;
		
		$arr = $babDB->db_fetch_array($babDB->db_query("
					SELECT 
						a.id_first_step,
						s.id 
					FROM 
						".FORM_APP_STEPS." s,
						".FORM_APP_APPLICATIONS." a
					WHERE 
						a.id='".$babDB->db_escape_string($id_app)."' AND 
						s.id_application='".$babDB->db_escape_string($id_app)."' 
					LIMIT 0,1
				"));
	
		$id_step = empty($arr['id_first_step']) ? $arr['id'] : $arr['id_first_step'];
	}

	include_once $GLOBALS['babAddonPhpPath']."steptypeincl.php";
	
	$stepFlow = new form_stepFlow();
	$stepFlow->gotoStep($id_step);

}


function blob()
	{
	$db = &$GLOBALS['babDB'];

	$disposition = isset($_GET['disposition']) ? $_GET['disposition'] : 'attachment';

	$arr = $db->db_fetch_array($db->db_query("
	
	SELECT 
		t.id col_id,
		t.name col_name, 
		t.id_table, 
		d.file_name, 
		d.file_type, 
		d.file_size 
	FROM 
		".FORM_TABLES_FIELDS." t 
			LEFT JOIN ".FORM_EXTERNAL_BLOB_DATA." d 
			ON d.id_table_field='".$db->db_escape_string($_GET['id_table_field'])."' 
			AND d.form_row='".$db->db_escape_string($_GET['form_row'])."' 
	WHERE 
		t.id='".$db->db_escape_string($_GET['id_table_field'])."'
	
	"));
	
	$arr2 = $db->db_fetch_array($db->db_query("
	    
	SELECT
		t.id_form
	FROM
		".FORM_FORMS_FIELDS." t
	WHERE
		t.id_table_field='".$db->db_escape_string($_GET['id_table_field'])."'
	"));
	
	if (!bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$arr['id_table']) || !bab_isAccessValid(FORM_FORMS_GROUPS,$arr2['id_form']))
	{
	    header("HTTP/1.0 403 Forbidden");
		$GLOBALS['babBody']->msgerror = form_translate('Access denied');
		return;
	}

	list($blob) = $db->db_fetch_array($db->db_query("
	SELECT ".form_col_name($arr['col_id'],$arr['col_name'])." 
	FROM ".form_tbl_name($arr['id_table'])." 
	WHERE form_id='".$db->db_escape_string($_GET['form_row'])."'
	"));

	$file_name = !empty($arr['file_name']) ? $arr['file_name'] : 'temporary_filename';
	$file_type = !empty($arr['file_type']) ? $arr['file_type'] : 'application/octet-stream';

	ini_set('zlib.output_compression', 0);
	
	header('Content-Disposition: '.$disposition.'; filename="'.$file_name.'"'."\n");
	header('Content-Type:'. $file_type."\n");
	if( strtolower(bab_browserAgent()) == "msie")
		header('Cache-Control: public');
	if (!empty($arr['file_size']))
		header('Content-Length: '.$arr['file_size']."\n");
	header('Content-transfert-encoding: binary'."\n");
	
	die($blob);
	}

function delete_rows()
	{
	if (isset($_POST['delete']) && count($_POST['delete']) > 0)
		{
		$db = & $GLOBALS['babDB'];
		list($id_table) = $db->db_fetch_array($db->db_query("SELECT f.id_table FROM ".FORM_FORMS." f,".FORM_TABLES." t WHERE f.id='".$db->db_escape_string($_POST['id_form'])."' AND f.id_table=t.id"));
		if (bab_isAccessValid(FORM_TABLES_DELETE_GROUPS, $id_table))
			{
			form_deleteTableRows($id_table, $_POST['delete']);
			}
		else
			{
			$GLOBALS['babBody']->msgerror = form_translate('Access denied');
			}
		}
	}

function delete_card_rows()
	{
	if (isset($_POST['delete']) && count($_POST['delete']) > 0)
		{
		require_once( $GLOBALS['babInstallPath']."addons/forms/card.php" );
		$form_card = new form_card($_POST['id_app']);

		$db = & $GLOBALS['babDB'];

		list($id_table) = $db->db_fetch_array($db->db_query("SELECT id_table FROM ".FORM_FORMS." WHERE id='".$db->db_escape_string($_POST['id_form'])."'"));
		$arr = $form_card->get($id_table);


		foreach ($arr as $k => $v)
			if (in_array($k,$_POST['delete']))
				unset($arr[$k]);

		$form_card->put($id_table,$arr);

		}
	}

function record_quantities()
	{
	$db = & $GLOBALS['babDB'];

	// enregistrer les quantities dans la session

	require_once( $GLOBALS['babInstallPath']."addons/forms/card.php" );
	$form_card = new form_card($_POST['id_app']);

	list($id_table) = $db->db_fetch_array($db->db_query("SELECT id_table FROM ".FORM_FORMS." WHERE id='".$db->db_escape_string($_POST['id_form'])."'"));

	$arr2 = $form_card->get($id_table);
	if (!is_array($arr2))
		$arr2 = array();

	foreach ($_POST as $field => $qte)
		{
		$arr = explode('_',$field);
		if (is_array($arr) && $arr[0]== 'qte' && count($arr) == 2)
			{
			if (isset($arr2[$arr[1]]) && empty($qte))
				{
				unset($arr2[$arr[1]]);
				}
			elseif(!empty($qte))
				{
				$arr2[$arr[1]] = $qte;
				}
			}
		}

	$form_card->put($id_table,$arr2);
	}

function form_empty_card($id_app)
	{
	require_once( $GLOBALS['babInstallPath']."addons/forms/card.php");

	$card = new form_card($id_app);
	$card->kill();
	}

function processPayboxReturn()
{
	include_once $GLOBALS['babAddonPhpPath']."steptypeincl.php";
	
	$action = isset($_GET['paction'])?$_GET['paction']:'';
	
	$stepFlow = new form_stepFlow();
	$stepFlow->id_step = $_GET['pid_step'];
	$arr = $stepFlow->getStep();
	switch($action)
	{
		case 'effectue':
			$url = $GLOBALS['formAddonUrl']."form&id_app=".urlencode($arr['id_application'])."&trt_step=1&id_step=".urlencode($arr['id_paybox_step_effectue']);
			//$stepFlow->gotoStep($arr['id_paybox_step_effectue']);
			break;
		case 'annule':
			$url = $GLOBALS['formAddonUrl']."form&id_app=".urlencode($arr['id_application'])."&trt_step=1&id_step=".urlencode($arr['id_paybox_step_annule']);
			//$stepFlow->gotoStep($arr['id_paybox_step_annule']);
			break;
		case 'refuse':
			$url = $GLOBALS['formAddonUrl']."form&id_app=".urlencode($arr['id_application'])."&trt_step=1&id_step=".urlencode($arr['id_paybox_step_refuse']);
			//$stepFlow->gotoStep($arr['id_paybox_step_refuse']);
			break;
	}
	
	
	header("location:".$url);
	exit;
}

// main


$idx = bab_rp('idx');
$id_app = bab_rp('id_app');
$id_form = bab_rp('id_form');
form_currentIdStep(bab_rp('id_step'));
$GLOBALS['formAddonUrl'] = $GLOBALS['babAddonUrl'];

if (empty($id_app))
	{
	$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
	$babBody->addItemMenu($id_form, form_translate("Preview"),'');
	}

$trt_step = true;

if (isset($_POST['action']))
	{
	require_once( $GLOBALS['babInstallPath']."addons/forms/form_record.php");

	switch ($_POST['action'])
		{
		case 'edit_mode':
		case 'trans_mode':
			if (!empty($_POST['id_btn_type']) && $_POST['id_btn_type'] == 10)
				break;
			
			if (!form_record())
				{
				$idx = bab_rp('parent_idx');
				form_currentIdStep(bab_rp('parent_step'));
				//$trt_step = false;
				break;
				}
			
			if (!isset($_POST['id_app']) && $idx!='pop_closer')
				header("location:".$GLOBALS['babAddonUrl']."main&idx=list_forms");
			break;

		case 'delete_rows':
			delete_rows();
			break;

		case 'delete_card_rows':
			delete_card_rows();
			break;

		case 'sl_mode':
			record_quantities();
			break;
		}
	}
	
	
if( $idx == 'paybox')
{
	$id_app = processPayboxReturn();
}

if ($trt_step && $idx != 'pop_closer' && !empty($id_app))
	{
	trt_step();
	}




if (!empty($id_app) && isset($_REQUEST['id_btn_type']))
	{
	switch ($_REQUEST['id_btn_type'])
		{
		case 8:
			if (!$trt_step)
				break;
		case 9:
			form_empty_card($id_app);
			break;
		}
	}

switch ($idx)
	{
	case 1:
		tpl_list($id_form);
		break;
	case 2:
		tpl_edit($id_form);
		break;
	case 3:
		tpl_view($id_form);
		break;
	case 4:
		tpl_trans($id_form);
		break;
	case 5:
		tpl_sc($id_form);
		break;
	case 6:
		tpl_sl($id_form);
		break;
	case 7:
		tpl_approb($id_form);
		break;
	case 8:
		tpl_waiting($id_form);
		break;

	case "pop_closer":
	    $msg = bab_rp('msg', null);
		if (!isset($msg))
			$msg = form_translate("Modifications has been saved");
		pop_closer($msg,true);
		break;
	case "application":
		application();
		break;
	case "blob":
		blob();
		break;
	}

$babBody->setCurrentItemMenu($id_form);

?>