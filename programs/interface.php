<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

/*
	INTERFACE

	form_getTable($id_table)					propri�t�s d'une table
	form_getTableField($id_field)				propri�t�s d'un champ de table
	form_getLnkTable($id_lnk)					propri�t�s d'une table de liaison
	form_setTableLock($id_table, $message)		Mettre un verrou sur une table
	form_getTableLocks()						Liste des verrous
	form_removeTableLock($id_lock)				Enlever un verrou
	form_linkToStep($id_step,$form_row)			Lien vers une �tape d'application

*/

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");


function form_getTable($id)
{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("
				SELECT 
					f1.id,
					f1.name,
					f1.description,
					f1.default_val,
					f1.link_table_field_id,
					f1.field_function,
					IFNULL(t1.name,t2.name) type_name,
					IFNULL(t1.col_type,t2.col_type) type,
					f2.id_table link_table_id
				FROM ".FORM_TABLES_FIELDS." f1
				LEFT JOIN  
					".FORM_TABLES_FIELDS_TYPES." t1 
					ON t1.id=f1.id_type 
				LEFT JOIN  
					".FORM_TABLES_FIELDS." f2 
					ON f2.id=f1.link_table_field_id  
				LEFT JOIN  
					".FORM_TABLES_FIELDS_TYPES." t2 
					ON t2.id=f2.id_type 
				WHERE 
					f1.id_table='".$db->db_escape_string($id)."' 
					AND (f1.id_type > 0 OR f1.link_table_field_id > 0 OR f1.field_function='index')
				");

	$fields = array();
	$linked_tables = array();

	while ($arr = $db->db_fetch_array($res))
		{
		if ($arr['field_function'] == 'index')
			{
			$arr['type_name'] = 'Index';
			$arr['type'] = 'int(10) unsigned NOT NULL auto_increment';
			}

		if ($arr['link_table_id'] > 0)
			{
			$linked_tables[$arr['link_table_id']] = $arr['link_table_id'];
			}

		$fields[$arr['id']] = array(
								'id'					=> $arr['id'],
								'name'					=> form_col_name($arr['id'],$arr['name'],$arr['field_function']),
								'description'			=> !empty($arr['description']) ? $arr['description'] : $arr['name'],
								'default_val'			=> $arr['default_val'],
								'link_table_field_id'	=> $arr['link_table_field_id'],
								'link_table_id'			=> $arr['link_table_id'],
								'link_table_name'		=> empty($arr['link_table_id']) ? false : form_tbl_name($arr['link_table_id']),
								'type_name'				=> $arr['type_name'],
								'type'					=> $arr['type']
								);
		}


	$res = $db->db_query("
				SELECT 
					*
					FROM ".FORM_TABLES." 
					WHERE id='".$db->db_escape_string($id)."' AND created='Y'
				");


	$arr = $db->db_fetch_array($res);


	return array(
				'id'			=> $id,
				'name'			=> form_tbl_name($id),
				'description'	=> !empty($arr['description']) ? $arr['description'] : $arr['name'],
				'approbation'	=> empty($arr['approbation']) ? false : true,
				'fields'		=> $fields,
				'linked_tables' => $linked_tables,
				'access'		=> array(	'read'		=> bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$id),
											'insert'	=> bab_isAccessValid(FORM_TABLES_INSERT_GROUPS,$id),
											'change'	=> bab_isAccessValid(FORM_TABLES_CHANGE_GROUPS,$id)
										)
				);
	
}


function form_getTableField($id_field)
{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("
		SELECT 
			f1.id,
			f1.id_table,
			f1.name,
			f1.description,
			f1.default_val,
			f1.link_table_field_id,
			f1.field_function,
			IFNULL(t1.name,t2.name) type_name,
			IFNULL(t1.col_type,t2.col_type) type,
			f2.id_table link_table_id
		FROM ".FORM_TABLES_FIELDS." f1
		LEFT JOIN  
			".FORM_TABLES_FIELDS_TYPES." t1 
			ON t1.id=f1.id_type 
		LEFT JOIN  
			".FORM_TABLES_FIELDS." f2 
			ON f2.id=f1.link_table_field_id  
		LEFT JOIN  
			".FORM_TABLES_FIELDS_TYPES." t2 
			ON t2.id=f2.id_type 
		WHERE 
			f1.id='".$db->db_escape_string($id_field)."' 
			AND (f1.id_type > 0 OR f1.link_table_field_id > 0 OR f1.field_function='index')
	");

	if ($db->db_num_rows($res) == 0)
		{
		return false;
		}

	$arr = $db->db_fetch_array($res);

	return array(
				'id'					=> $arr['id'],
				'id_table'				=> $arr['id_table'],
				'name'					=> form_col_name($arr['id'],$arr['name'],$arr['field_function']),
				'description'			=> !empty($arr['description']) ? $arr['description'] : $arr['name'],
				'default_val'			=> $arr['default_val'],
				'link_table_field_id'	=> $arr['link_table_field_id'],
				'link_table_id'			=> $arr['link_table_id'],
				'link_table_name'		=> empty($arr['link_table_id']) ? false : form_tbl_name($arr['link_table_id']),
				'type_name'				=> $arr['type_name'],
				'type'					=> $arr['type'],
				'values'				=> false !== strpos($arr['type'],'enum') || false !== strpos($arr['type'],'set') ? enumToArray($arr['type']) : false
				);

}



function form_getLnkTable($id)
{
	$db = &$GLOBALS['babDB'];


	$lnk = $db->db_fetch_array($db->db_query("
	
							SELECT 
								lnk.*,
								f1.name name_lnk_f1,
								f2.name name_lnk_f2,
								f3.name name_field1,
								f4.name name_field2 
							FROM 
								".FORM_TABLES_LNK." lnk 
							LEFT JOIN 
								".FORM_TABLES_FIELDS." f1
								ON f1.id = lnk.id_lnk_f1
							LEFT JOIN 
								".FORM_TABLES_FIELDS." f2
								ON f2.id = lnk.id_lnk_f2

							LEFT JOIN 
								".FORM_TABLES_FIELDS." f3
								ON f3.id = lnk.id_field1
							LEFT JOIN 
								".FORM_TABLES_FIELDS." f4
								ON f4.id = lnk.id_field2

							WHERE lnk.id='".$db->db_escape_string($id)."'

							
							"));

	if ($lnk['lnk_type'] == 0)
	{
		return array(
					'table1'	=> array(
										'id'			=> $lnk['id_table1'],
										'name'			=> form_tbl_name($lnk['id_table1']),
										'field_id'		=> 0,
										'field_name'	=> 'form_id'
										),

					'lnk'		=> array(
										'id'			=> 0,
										'name'			=> FORM_TABLES_LNK_VALUES,
										'field1_id'		=> 0,
										'field1_name'	=> 'row1',
										'field2_id'		=> 0,
										'field2_name'	=> 'row2',
										'where'			=> 'id_lnk=\''.$id.'\''
										),

					'table2'	=> array(
										'id'			=> $lnk['id_table2'],
										'name'			=> form_tbl_name($lnk['id_table2']),
										'field_id'		=> 0,
										'field_name'	=> 'form_id'
										)
					);
	}
	elseif ($lnk['lnk_type'] == 1)
	{

		return array(
					'table1'	=> array(
										'id'			=> $lnk['id_table1'],
										'name'			=> form_tbl_name($lnk['id_table1']),
										'field_id'		=> $lnk['id_field1'],
										'field_name'	=> $lnk['name_field2']
										),

					'lnk'		=> array(
										'id'			=> $lnk['id_lnk_table'],
										'name'			=> form_tbl_name($lnk['id_lnk_table']),
										'field1_id'		=> $lnk['id_lnk_f1'],
										'field1_name'	=> $lnk['name_lnk_f1'],
										'field2_id'		=> $lnk['id_lnk_f2'],
										'field2_name'	=> $lnk['name_lnk_f1'],
										'where'			=> false
										),

					'table2'	=> array(
										'id'			=> $lnk['id_table2'],
										'name'			=> form_tbl_name($lnk['id_table2']),
										'field_id'		=> $lnk['id_field2'],
										'field_name'	=> $lnk['name_field2']
										)
					);
	}
}



function form_setTableLock($id_table, $message, $locked_by = 0)
{
	
	if (empty($locked_by))
		{
		 $locked_by = $GLOBALS['babAddonFolder'];	
		}

	$db = &$GLOBALS['babDB'];
	list($n) = $db->db_fetch_array($db->db_query("SELECT COUNT(*) FROM ".FORM_TABLES_LOCKS." WHERE id_table='".$db->db_escape_string($id_table)."' AND locked_by = '".$db->db_escape_string($locked_by)."'"));

	if ($n == 0)
		{
		$db->db_query("INSERT INTO ".FORM_TABLES_LOCKS." (id_table, message, locked_by, creationdate) VALUES ('".$db->db_escape_string($id_table)."', '".$db->db_escape_string($message)."', '".$db->db_escape_string($locked_by)."', NOW())");
		return $db->db_insert_id();
		}

	return false;
}


function form_getTableLocks($locked_by = 0)
{
	if (empty($locked_by))
		{
		 $locked_by = $GLOBALS['babAddonFolder'];	
		}


	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("
		SELECT 
			l.id, 
			l.id_table, 
			t.name tname, 
			t.description tdescription, 
			l.message, 
			UNIX_TIMESTAMP(l.creationdate) creationdate 
		FROM 
			".FORM_TABLES_LOCKS." l,
			".FORM_TABLES." t
		WHERE 
			t.id = l.id_table 
			AND l.locked_by = '".$db->db_escape_string($locked_by)."'
		");

	$out = array();

	while ($arr = $db->db_fetch_array($res))
		{
		$out[] = array(
				'id'			=> $arr['id'],
				'id_table'		=> $arr['id_table'],
				'table_name'	=> !empty($arr['tdescription']) ? $arr['tdescription'] : $arr['tname'],
				'message'		=> $arr['message'],
				'creationdate'	=> $arr['creationdate']
					);
		}
	
	return $out;
}


function form_removeTableLock($id_lock)
{
	$db = &$GLOBALS['babDB'];
	$db->db_query("DELETE FROM ".FORM_TABLES_LOCKS." WHERE id='".$db->db_escape_string($id_lock)."'");
}


function form_linkToStep($id_step, $form_row)
{
	$db = &$GLOBALS['babDB'];
	$arr = $db->db_fetch_array($db->db_query("SELECT id_application, id_form FROM ".FORM_APP_STEPS." WHERE id='".$db->db_escape_string($id_step)."'"));
	
	return $GLOBALS['babUrlScript'].'?tg=addon/forms/form&idx=3&id_app='.$arr['id_application'].'&id_step='.$id_step.'&id_form='.$arr['id_form'].'&form_row='.$form_row.'&popup=0&parent_id_form=0&parent_id_step=1&form_menu=&trt_step=1';
}


?>