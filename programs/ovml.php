<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");
require_once( $GLOBALS['babInstallPath']."addons/forms/linksincl.php");
require_once( $GLOBALS['babInstallPath']."addons/forms/form_parents.php");

$GLOBALS['formAddonUrl'] = $GLOBALS['babAddonUrl'];


function form_dateToTimestamp(&$str) {
	if (is_string($str)) {
		$n = strlen($str);

		if (($n == 10 && substr_count($str,'-') == 2) || ($n == 19 && substr_count($str,'-') == 2 && substr_count($str,':') == 2))
			{
			$str = bab_mktime($str);
			}
		}
	}
	
	
function form_getFormIdByName($name) {
	global $babDB;
	
	$res = $babDB->db_query('SELECT id FROM '.FORM_FORMS.' WHERE name LIKE '.$babDB->quote($name).' LIMIT 1');
	$arr = $babDB->db_fetch_assoc($res);
	
	if (!$arr) {
		trigger_error('form_name does not exists');
		return false;
	}
	
	return $arr['id'];
}


function forms_ovmlHtml($html)
{
    static $supportFormat;
    if (!isset($supportFormat)) {
        $supportFormat = version_compare(bab_getIniVersion(), '8.4.93', '>=');
    }
    
    if (!$supportFormat) {
        return $html;
    }
    
    $variable = new stdClass();
    $variable->value = $html;
    $variable->format = bab_context::HTML;
    
    return $variable;
}


function forms_ovml($args)
	{
	if (isset($args['action']))
		{
		$approb_status = 1;
		
		

		switch($args['action'])
			{

			case 'applicationcontext':
				$obj = form_staticContext();

				$ovmlContext = array(
					
					'StyleSheetUrl'					=> $obj->css_file,
					'FormHtmlHead'					=> forms_ovmlHtml($obj->getFormHead()),
					'FormHtmlFoot'					=> forms_ovmlHtml($obj->getFormFoot()),
					'ApplicationStepDescription'	=> isset($obj->app_message) ? $obj->app_message : '',
					'FormDescription'				=> $obj->arr['description'],
					'FormLongDescription'			=> $obj->arr['longdesc'],
					'ApplicationHtmlButtons'		=> forms_ovmlHtml($obj->buttonbar)
				);
				
				switch($obj->arr['id_type']) {
					case 2: // edit form
					case 7: // approbation

						$ovmlContext['FormViewNotEmpty']		= $obj->arr['view_notempty'];
						$ovmlContext['FormHtmlNotEmptyLegend']	= $obj->arr['view_notempty'] ? forms_ovmlHtml(bab_printTemplate($obj, $GLOBALS['babAddonHtmlPath']."html_utilities.html", 'view_notempty')) : '';
						$ovmlContext['FormHtmlApprobation']		= isset($obj->form_validation) ? forms_ovmlHtml($obj->form_validation) : '';
						break;
				}

				return array(
					0 => $ovmlContext
				);
				break;

			case 'editfieldscontext':
			case 'viewfieldscontext':
				$obj = form_staticContext();
				$return = array();

				if (isset($args['field_id'])) {
					$field_id = array();
					$tmp = explode(',',$args['field_id']);
					foreach($tmp as $v) {
						$field_id[$v] = 1;
						}
					}
					
				

				while ($obj->getnextcol()) {
					if (!isset($field_id) || isset($field_id[$obj->col['id']])) {
						$field = array();
						$field['FieldId']		= $obj->col['id'];
						$field['FieldLabel']	= empty($obj->col['description']) ? $obj->col['name'] : $obj->col['description'];
						$field['FormHtmlLabel'] = forms_ovmlHtml($obj->html_label);
						$field['FormHtmlField'] = forms_ovmlHtml($obj->html_field);
						
						
						$field['ClassName']		= $obj->field_classname;
						$return[] = $field;
					}
				}
				return $return;
				break;

			

			/**
 			 * List display
			 * without context
			 */
			case 'waitingform':
			case 'listform':
				if (!isset($args['form_id']))
					{
					
					if (isset($args['name'])) {
						$args['form_id'] = form_getFormIdByName($args['form_name']);
						if (!$args['form_id']) {
							return false;
						}
					} else {
					trigger_error($args['action'].':form_id is required in the OVML');
					return false;
					}
				}
				
				$list = new form_list($args['form_id']);
				
				if (!$list->list_query()) {
					return array();
				}
				
				$return = array();
				$row = array();
				$i = 0;

				$skip = false;

				while ($list->getnextrow())
					{
					if (isset($args['limit']) && $i >= $args['limit'])
						break;


					while ($list->getnextcol($skip))
						{

						
						$row['Name_'.$list->col['id']] = $list->col['name'];
						$row['Description_'.$list->col['id']] = !empty($list->col['description']) ? $list->col['description'] : $list->col['name'];
						$row['Value_'.$list->col['id']] = $list->short;
						$row['FullValue_'.$list->col['id']] = empty($list->full) ? $list->short : $list->full;
						
						$ovmlDatas = form_getTransformedValue($list->col['id'], $list->row['form_id'], $list->value, FORM_TRANS_OVML);
						if (is_array($ovmlDatas)) {
							$row['OvmlValue_'.$list->col['id']] = $ovmlDatas['file_name'];			
							$row['Download_'.$list->col['id']]  = $ovmlDatas['file_url'];

						} else {
							$row['OvmlValue_'.$list->col['id']] = $ovmlDatas;			
							$row['Download_'.$list->col['id']]  = false;
						}

						$row['Link_'.$list->col['id']] = $list->link ? $list->link_url : '';
						$row['HelpId_'.$list->col['id']] = isset($list->col['help_id']) ? $list->col['help_id'] : '';
						$row['HelpTitle_'.$list->col['id']] = isset($list->col['help_title']) ? $list->col['help_title'] : '';
						}
					$return[] = $row;
					$i++;
					}
				
				bab_debug($return);
				
				return $return;
				break;


			case 'simplelist':
			
				if (!isset($args['form_id']))
					{
					
					if (isset($args['name'])) {
						$args['form_id'] = form_getFormIdByName($args['form_name']);
						if (!$args['form_id']) {
							return false;
						}
					} else {
					trigger_error($args['action'].':form_id is required in the OVML');
					return false;
					}
				}
				
				$list = new form_list($args['form_id']);
				if (isset($args['pos'])) {
					$list->pos = (int) $args['pos'];
				}
				$list->list_query();
				$return = array();
				$row = array();
				$i = 0;

				$skip = false;

				while ($list->getnextrow())
					{
					if (isset($args['limit']) && $i >= $args['limit']) {
						break;
					}

					while ($list->getnextcol($skip))
						{
							$ovmlDatas = form_getTransformedValue($list->col['id'], $list->row['form_id'], $list->value, FORM_TRANS_OVML);
	
							if (is_string($ovmlDatas)) {
								$row[$list->col['name']] = $ovmlDatas;	
							} else {
								$row[$list->col['name']] = isset($ovmlDatas['file_url']) ? $ovmlDatas['file_url'] : '';
							}			
						
						}
					$return[] = $row;
					$i++;
					}

				return $return;
				break;


			case 'viewform':
				if (!isset($args['form_id']))
					{
					if (isset($args['name'])) {
						$args['form_id'] = form_getFormIdByName($args['form_name']);
						if (!$args['form_id']) {
							return false;
						}
					} else {
					trigger_error('viewform:form_id is required in the OVML');
					return false;
					}
				}

				if (!isset($args['form_row']))
					{
					trigger_error('viewform:form_row is required in the OVML');
					return false;
					}

				form_currentDbRow($args['form_row']);

				$sheet = new form_view($args['form_id']);
				$sheet->form_row = $args['form_row'];
				$sheet->editColCtrl();

				$field_id = array();

				if (isset($args['field_name'])) {
					$tmp = explode(',',$args['field_name']);

					global $babDB;
					$res = $babDB->db_query('SELECT id FROM '.FORM_FORMS_FIELDS.' WHERE name IN('.$babDB->quote($tmp).')');
					while ($arr = $babDB->db_fetch_assoc($res)) {
						$field_id[$arr['id']] = 1;
					}
				}

				if (isset($args['field_id'])) {
					$tmp = explode(',',$args['field_id']);
					foreach($tmp as $v) {
						$field_id[$v] = 1;
					}
				}

				$return = array();
				while ($sheet->getnextcol())
					{
					if (!$field_id || isset($field_id[$sheet->col['id']])) {

						$row = array();
						$row['FieldId']		= $sheet->col['id'];
						$row['Name']		= $sheet->col['name'];
						$row['Description'] = !empty($sheet->col['description']) ? $sheet->col['description'] : $sheet->col['name'];
						$row['DbValue']		= is_string($sheet->value) ? $sheet->value : '';
						$row['Value']		= $sheet->t_value;

						$ovmlValue = form_getTransformedValue($sheet->col['id'], $sheet->form_row, $sheet->value, FORM_TRANS_OVML);

						if (is_array($ovmlValue)) {
							
							$row['Value']		= $ovmlValue['file_name'];
							$row['FileUrl']		= $ovmlValue['file_url'];
							$row['FileSize']	= $ovmlValue['file_size'];
						} else {

							$row['OvmlValue']	= $ovmlValue;
						}

						if ($sheet->loopoption) {
							$tmparr = array();
							while ($sheet->getnextoption()) {
								if ($sheet->selected) {
									$tmparr[] = $sheet->option['opt'];
									}
								}
							$row['Value'] = implode(', ', $tmparr);
							}
						
						$return[] = $row;
						}
					}


				static $incrementCtrl = true;
				
				if ($incrementCtrl) {
					$sheet->incrementCtrl(false);
					$incrementCtrl = false;
					}

				return $return;
				break;


			case 'applications':
				
				if (!isset($args['right_id']))
					$args['right_id'] = false;

				$list = new form_app_list($args['right_id']);
				$return = array();

				while ($list->getnextlink())
					{
					$row = array();

					$row['ApplicationName'] = $list->name;
					$row['ApplicationDescription'] = $list->description;
					$row['ApplicationUrl'] = $list->url;

					$return[] = $row;
					}

				return $return;
				break;


			case 'filedirectory':
				
				if (!isset($args['id_table']))
					{
					trigger_error('id_table is required in the OVML');
					return false;
					}
				
				if (!isset($args['form_row']))
					{
					trigger_error('form_row is required in the OVML');
					return false;
					}

				if (!bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$args['id_table']))
					return false;

				$row_directory = $GLOBALS['babAddonUpload'].'directories/'.form_tbl_name($args['id_table']).'/'.$args['form_row'].'/';

				$dirfiles = array();
				if (is_dir($row_directory))
					{
					if ($dh = opendir($row_directory)) {
						   while (($file = readdir($dh)) !== false) {
							   if (is_file($row_directory . $file))
									{
									$dirfiles[] = $file;
									}
						   }
						   closedir($dh);
					   }
					natcasesort($dirfiles);
					}

				$return = array();
				foreach($dirfiles as $file)
					{
					$row = array();

					$row['FileName'] = $file;
					$row['FileType'] = form_getFileMimeType($file);
					$row['FileSize'] = sprintf("%01.2f %s",(filesize($row_directory . $file)/1024), form_translate('Ko'));
					$row['FileCreateTime'] = filectime($row_directory . $file);
					$row['FileModifyTime'] = filemtime($row_directory . $file);
					$row['FileUrl'] = $GLOBALS['babAddonUrl'].'directory&idx=getfile&id_table='.$args['id_table'].'&form_row='.$args['form_row'].'&filename='.urlencode($file);

					$return[] = $row;
					}

				return $return;
				break;


			case 'listformPages':
				if (!isset($args['form_id']))
					{
					trigger_error('form_id is required in the OVML');
					return false;
					}

				$p_offset = isset($args['offset']) ? $args['offset'] : 1;
				$p_row = isset($args['row']) ? $args['row'] : false;

				$list = new form_list($args['form_id']);
				$list->list_query();
				$rows_per_page = &$list->arr['rows_per_page'];

				$return = array();

				for ($pos = (($p_offset-1) * $rows_per_page ) ; $pos < $list->max; $pos += $rows_per_page) {
					if ($p_row && $pos > $p_row)
						break;
					
					
					$row = array();
					$row['PageNumber'] = $p_offset;
					$p_offset++;
					$row['PagePos'] = $pos;

					if (isset($_GET['pos'])) {
						$row['PageUrl'] = str_replace('pos='.$_GET['pos'],'pos='.$pos,$_SERVER['REQUEST_URI']);
						}
					else {
						$row['PageUrl'] = empty($_SERVER['QUERY_STRING']) ? $_SERVER['REQUEST_URI'].'?pos='.$pos : $_SERVER['REQUEST_URI'].'&pos='.$pos;
						}
					$row['CurrentPage'] = (isset($_REQUEST['pos']) && $_REQUEST['pos'] == $pos) ? 1 : 0;
					$return[] = $row;
					}

				return $return;
				break;
			}

		}

	}

?>