<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

class form_card
{
	function form_card($id_app)
	{
	$this->id_app = !empty($id_app) ? $id_app : 0;
	
	}

	function open()
	{
	if (isset($this->card)) return;

	if (isset($_SESSION['form_card']))
		{
		$this->cards = unserialize($_SESSION['form_card']);
		}
	else
		{
		$this->cards = array();
		}

	if (!isset($this->cards[$this->id_app]) || !is_array($this->cards[$this->id_app]))
		{
		$this->cards[$this->id_app] = array();
		}

	$this->card = & $this->cards[$this->id_app];
	}

	function put($key,$value)
	{
	$this->open();
	$this->card[$key] = $value;
	$_SESSION['form_card'] = serialize($this->cards);
	}

	function get($key)
	{
	$this->open();
	if (!isset($this->card[$key]))
		return false;
	else
		return $this->card[$key];
	}

	function kill()
	{
		$this->open();
		unset($this->cards[$this->id_app]);
		$_SESSION['form_card'] = serialize($this->cards);
	}

	function killall()
	{
		unset($_SESSION['form_card']);
	}
}

?>