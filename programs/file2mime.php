<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

function form_load_file2mime()
{
$file2mime = array(
 "3dm"=>"x-world/x-3dmf", //Fichiers DMF
 "3dmf"=>"x-world/x-3dmf", //Fichiers DMF
 "ai"=>"application/postscript",
 "aif"=>"audio/x-aiff",
 "aifc"=>"audio/x-aiff",
 "aiff"=>"audio/x-aiff",
 "asc"=>"text/plain",
 "asd"=>"application/astound", //Fichiers Astound
 "asn"=>"application/astound", //Fichiers Astound
 "au"=>"audio/basic",
 "avi"=>"video/x-msvideo",
 "bcpio"=>"application/x-bcpio",
 "bin"=>"application/octet-stream",
 "c"=>"text/plain",
 "cab"=>"application/x-shockwave-flash", //Fichiers Flash Shockwave
 "cc"=>"text/plain",
 "ccad"=>"application/clariscad",
 "cdf"=>"application/x-netcdf",
 "chm"=>"application/mshelp", //Fichiers d'aide Microsoft Windows
 "cht"=>"audio/x-dspeeh", //Fichiers parole
 "class"=>"application/octet-stream",
 "cod"=>"image/cis-cod", //Fichiers CIS-Cod
 "com"=>"application/octet-stream", //Fichiers ex�cutables
 "cpio"=>"application/x-cpio",
 "cpt"=>"application/mac-compactpro",
 "csh"=>"application/x-csh",
 "css"=>"text/css",
 "csv"=>"text/comma-separated-values", //Fichiers de donn�es s�par�es par des virgules
 "dcr"=>"application/x-director",
 "dir"=>"application/x-director",
 "dll"=>"application/octet-stream", //Fichiers ex�cutables
 "dms"=>"application/octet-stream",
 "doc"=>"application/msword",
 "dot"=>"application/msword", //Fichiers Microsoft Word
 "drw"=>"application/drafting",
 "dus"=>"audio/x-dspeeh", //Fichiers parole
 "dvi"=>"application/x-dvi",
 "dwf"=>"drawing/x-dwf", //Fichiers Drawing
 "dwg"=>"application/acad",
 "dxf"=>"application/dxf",
 "dxr"=>"application/x-director",
 "eps"=>"application/postscript",
 "es"=>"audio/echospeech", //Fichiers Echospeed
 "etx"=>"text/x-setext",
 "evy"=>"application/x-envoy", //Fichiers Envoy
 "exe"=>"application/octet-stream",
 "ez"=>"application/andrew-inset",
 "f"=>"text/plain",
 "f90"=>"text/plain",
 "fh4"=>"image/x-freehand", //Fichiers Freehand
 "fh5"=>"image/x-freehand", //Fichiers Freehand
 "fhc"=>"image/x-freehand", //Fichiers Freehand
 "fli"=>"video/x-fli",
 "gif"=>"image/gif",
 "gtar"=>"application/x-gtar",
 "gz"=>"application/x-gzip",
 "h"=>"text/plain",
 "hdf"=>"application/x-hdf",
 "hh"=>"text/plain",
 "hlp"=>"application/mshelp", //Fichiers d'aide Microsoft Windows
 "hqx"=>"application/mac-binhex40",
 "htm"=>"text/html",
 "html"=>"text/html",
 "ice"=>"x-conference/x-cooltalk",
 "ief"=>"image/ief",
 "iges"=>"model/iges",
 "igs"=>"model/iges",
 "ips"=>"application/x-ipscript",
 "ipx"=>"application/x-ipix",
 "jpe"=>"image/jpeg",
 "jpeg"=>"image/jpeg",
 "jpg"=>"image/jpeg",
 "js"=>"application/x-javascript",
 "kar"=>"audio/midi",
 "latex"=>"application/x-latex",
 "lha"=>"application/octet-stream",
 "lsp"=>"application/x-lisp",
 "lzh"=>"application/octet-stream",
 "m"=>"text/plain",
 "man"=>"application/x-troff-man",
 "mbd"=>"application/mbedlet", //Fichiers Mbedlet
 "mcf"=>"image/vasa", //Fichiers Vasa
 "me"=>"application/x-troff-me",
 "mesh"=>"model/mesh",
 "mid"=>"audio/midi",
 "midi"=>"audio/midi",
 "mif"=>"application/vnd.mif",
 "mime"=>"www/mime",
 "mov"=>"video/quicktime",
 "movie"=>"video/x-sgi-movie",
 "mp2"=>"audio/mpeg",
 "mp3"=>"audio/mpeg",
 "mpe"=>"video/mpeg",
 "mpeg"=>"video/mpeg",
 "mpg"=>"video/mpeg",
 "mpga"=>"audio/mpeg",
 "ms"=>"application/x-troff-ms",
 "msh"=>"model/mesh",
 "nc"=>"application/x-netcdf",
 "nsc"=>"application/x-nschat", //Fichiers NS Chat
 "oda"=>"application/oda",
 "pbm"=>"image/x-portable-bitmap",
 "pdb"=>"chemical/x-pdb",
 "pdf"=>"application/pdf",
 "pgm"=>"image/x-portable-graymap",
 "pgn"=>"application/x-chess-pgn",
 "php"=>"application/x-httpd-php", //Fichiers PHP
 "phtml"=>"application/x-httpd-php", //Fichiers PHP
 "png"=>"image/png",
 "pnm"=>"image/x-portable-anymap",
 "pot"=>"application/mspowerpoint",
 "ppm"=>"image/x-portable-pixmap",
 "pps"=>"application/mspowerpoint",
 "ppt"=>"application/mspowerpoint",
 "ppz"=>"application/mspowerpoint",
 "pre"=>"application/x-freelance",
 "prt"=>"application/pro_eng",
 "ps"=>"application/postscript",
 "ptlk"=>"application/listenup", //Fichiers Listenup
 "qd3"=>"x-world/x-3dmf", //Fichiers DMF
 "qd3d"=>"x-world/x-3dmf", //Fichiers DMF
 "qt"=>"video/quicktime",
 "ra"=>"audio/x-realaudio",
 "ram"=>"audio/x-pn-realaudio",
 "ras"=>"image/cmu-raster",
 "rgb"=>"image/x-rgb",
 "rm"=>"audio/x-pn-realaudio",
 "roff"=>"application/x-troff",
 "rpm"=>"audio/x-pn-realaudio-plugin",
 "rtc"=>"application/rtc", //Fichiers RTC
 "rtf"=>"text/rtf",
 "rtx"=>"text/richtext",
 "sca"=>"application/x-supercard", //Fichiers Supercard
 "scm"=>"application/x-lotusscreencam",
 "set"=>"application/set",
 "sgm"=>"text/sgml",
 "sgml"=>"text/sgml",
 "sh"=>"application/x-sh",
 "shar"=>"application/x-shar",
 "shtml"=>"text/plain", //Fichiers shtml
 "silo"=>"model/mesh",
 "sit"=>"application/x-stuffit",
 "skd"=>"application/x-koan",
 "skm"=>"application/x-koan",
 "skp"=>"application/x-koan",
 "skt"=>"application/x-koan",
 "smi"=>"application/smil",
 "smil"=>"application/smil",
 "smp"=>"application/studiom", //Fichiers Studiom
 "snd"=>"audio/basic",
 "sol"=>"application/solids",
 "spc"=>"text/x-speech", //Fichiers Speech
 "spl"=>"application/x-futuresplash",
 "spr"=>"application/x-sprite", //Fichiers Sprite
 "sprite"=>"application/x-sprite", //Fichiers Sprite
 "src"=>"application/x-wais-source",
 "step"=>"application/STEP",
 "stl"=>"application/SLA",
 "stp"=>"application/STEP",
 "stream"=>"audio/x-qt-stream", //Fichiers stream
 "sv4cpio"=>"application/x-sv4cpio",
 "sv4crc"=>"application/x-sv4crc",
 "swf"=>"application/x-shockwave-flash",
 "t"=>"application/x-troff",
 "tar"=>"application/x-tar",
 "tbk"=>"application/toolbook", //Fichiers Toolbook
 "talk"=>"text/x-speech", //Fichiers Speech
 "tcl"=>"application/x-tcl",
 "tex"=>"application/x-tex",
 "texi"=>"application/x-texinfo",
 "texinfo - application/x-texinfo",
 "tif"=>"image/tiff",
 "tiff"=>"image/tiff",
 "tr"=>"application/x-troff",
 "tsi"=>"audio/TSP-audio",
 "tsp"=>"application/dsptype",
 "tsv"=>"text/tab-separated-values",
 "txt"=>"text/plain",
 "unv"=>"application/i-deas",
 "ustar"=>"application/x-ustar",
 "vcd"=>"application/x-cdlink",
 "vda"=>"application/vda",
 "viv"=>"video/vnd.vivo",
 "vivo"=>"video/vnd.vivo",
 "vmd"=>"application/vocaltec-media-desc", //Fichiers Vocaltec Mediadesc
 "vmf"=>"application/vocaltec-media-file", //Fichiers Vocaltec Media
 "vox"=>"audio/voxware", //Fichiers Vox
 "vrml"=>"model/vrml",
 "vts"=>"workbook/formulaone", //Fichiers FormulaOne
 "vtts"=>"workbook/formulaone", //Fichiers FormulaOne
 "wav"=>"audio/x-wav",
 "wbmp"=>"image/vnd.wap.wbmp", //Fichiers Bitmap (WAP)
 "wml"=>"text/vnd.wap.wml", //Fichiers WML (WAP)
 "wmlc"=>"application/vnd.wap.wmlc", //Fichiers WMLC (WAP)
 "wmls"=>"text/vnd.wap.wmlscript", //Fichiers script WML (WAP)
 "wmlsc"=>"application/vnd.wap.wmlscriptc", //Fichiers script C WML (WAP)
 "wrl"=>"model/vrml",
 "xbm"=>"image/x-xbitmap",
 "xlc"=>"application/vnd.ms-excel",
 "xll"=>"application/vnd.ms-excel",
 "xlm"=>"application/vnd.ms-excel",
 "xls"=>"application/vnd.ms-excel",
 "xlw"=>"application/vnd.ms-excel",
 "xml"=>"text/xml",
 "xpm"=>"image/x-xpixmap",
 "xwd"=>"image/x-xwindowdump",
 "xyz"=>"chemical/x-pdb",
 "z"=>"application/x-compress", //Fichiers Compress�
 "zip"=>"application/zip"
 );
return $file2mime;
}
?>