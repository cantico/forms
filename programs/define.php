<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

define('FORM_GEST_RIGHTS','form_gest_rights');
define('FORM_GEST_GROUPS','form_gest_groups');
define('FORM_GEST_RIGHTS_APP','form_gest_rights_app');

define('FORM_APP_APPLICATIONS','form_app_applications');
define('FORM_APP_GROUPS','form_app_groups');
define('FORM_APP_STEP_CASES','form_app_step_cases');
define('FORM_APP_STEPS','form_app_steps');
define('FORM_APP_TYPES','form_app_types');
define('FORM_APP_LINKS','form_app_links');
define('FORM_APP_STEP_TYPE','form_app_step_type');
define('FORM_APP_STEP_BTN','form_app_step_btn');
define('FORM_APP_STEP_BTN_TYPE','form_app_step_btn_type');
define('FORM_APP_STEP_APPROB','form_app_step_approb');
define('FORM_APP_STEP_MENU','form_app_step_menu');

define('FORM_FORMS','form_forms');
define('FORM_FORMS_TYPES','form_forms_types');
define('FORM_FORMS_GROUPS','form_forms_groups');
define('FORM_FORMS_FIELDS','form_forms_fields');
define('FORM_FORMS_FIELDS_PRINT','form_forms_fields_print');
define('FORM_FORMS_FIELDS_VALIDATION','form_forms_fields_validation');
define('FORM_FORMS_FIELDS_CTRL','form_forms_fields_ctrl');
define('FORM_FORMS_FIELDS_INIT','form_forms_fields_init');
define('FORM_FORMS_FIELDS_CLASSNAME','form_forms_fields_classname');
define('FORM_FORMS_FIELDS_TRANSFORM','form_forms_fields_transform');
define('FORM_FORMS_ELEMENTS','form_forms_elements');

define('FORM_SQL_TYPES','form_sql_types');
define('FORM_TABLES','form_tables');
define('FORM_TABLES_CHANGE_GROUPS','form_tables_change_groups');
define('FORM_TABLES_VIEW_GROUPS','form_tables_view_groups');
define('FORM_TABLES_INSERT_GROUPS','form_tables_insert_groups');
define('FORM_TABLES_DELETE_GROUPS','form_tables_delete_groups');
define('FORM_TABLES_FILING_GROUPS','form_tables_filing_groups');
define('FORM_TABLES_LOCK_GROUPS','form_tables_lock_groups');
define('FORM_TABLES_FIELDS','form_tables_fields');
define('FORM_TABLES_FIELDS_TYPES','form_tables_fields_types');
define('FORM_TABLES_FIELDS_TYPES_EXTEND','form_tables_fields_types_extend');
define('FORM_TABLES_FIELDS_DIRECTORY','form_tables_fields_directory');
define('FORM_TABLES_VIEW_COLS','form_tables_view_cols');
define('FORM_TABLES_EXPORT_FAV','form_tables_export_fav');
define('FORM_TABLES_LNK','form_tables_lnk');
define('FORM_TABLES_LNK_VALUES','form_tables_lnk_values');
define('FORM_TABLES_FILTERS','form_tables_filters');
define('FORM_TABLES_FILTERS_FIELDS','form_tables_filters_fields');
define('FORM_TABLES_LOCKS','form_tables_locks');
define('FORM_TABLES_SYNC','form_tables_sync');

define('FORM_DBDIR_FIELDS','form_dbdir_fields');
define('FORM_CONFIG','form_config');
define('FORM_EXTERNAL_BLOB_DATA','form_external_blob_data');
define('FORM_HELP','form_help');
define('FORM_PROPOSAL','form_proposal');
define('FORM_PROPOSAL_ITEM','form_proposal_item');
define('FORM_APPROB_SCHI','form_approb_schi');

define('FORM_TRANS_API','form_trans_api');
define('FORM_TRANS_FIELDS','form_trans_fields');


define('FORM_WORKSPACES', 'form_workspaces');
define('FORM_WORKSPACE_ENTRIES', 'form_workspace_entries');

define('FORM_VIEW_MAX_CHAR',100);
define('FORM_DROPDOWN_MAX_CHAR',70);
define('FORM_WAITING',0);
define('FORM_APPROVED',1);
define('FORM_REFUSED',2);
define('FORM_CREATOR_OR_APPROVED',3);
define('MAIL_SEPARATOR',',');
define('CSV_SWAP',$GLOBALS['babInstallPath'].'addons/forms/swap.txt');
//define('FORM_CURL', function_exists('curl_init'));
define('FORM_CURL', false);
define('FORM_CSS','default.css');

define('FORM_FTP_ROW_INDEX','index.txt');

// Format de transformation

define('FORM_TRANS_TXT',1);				// Format texte pour l'affichage
define('FORM_TRANS_DB_HTML',2);			// Format pour les champs de formulaire html
define('FORM_TRANS_HTML',3);			// Format pour le contenu html (sans balises)
define('FORM_TRANS_RICH_HTML',4);		// Format html avec balises
define('FORM_TRANS_RICH_HTML_SMALL',5);	// Format html avec balises, icones de tÚlÚchargement uniquement
define('FORM_TRANS_CSV',6);				// Format valeur csv
define('FORM_TRANS_OVML',7);			// Format texte avec dates en timestamp

/**
 * Create access on create object
 */
// define('FORM_ACCESS_ON_CREATE', 1);



?>