<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

function form_getElementsFromField($id_type, $id_table_field,$id_table_field_link,$id_table_field_lnk)
	{
	$db = &$GLOBALS['babDB'];
	
	if ($id_table_field_link > 0)
		{
		return array(3,5,10);
		}
	elseif ($id_table_field_lnk > 0)
		{
		return array(4,6,12);
		}
	elseif ($id_table_field > 0)
		{
		$req = "
		
		SELECT 
			t.id_forms_elements, 
			f.field_function 
		FROM 
			".FORM_TABLES_FIELDS." f 
			LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t ON t.id=f.id_type 
		WHERE 
			f.id='".$db->db_escape_string($id_table_field)."'
		";

		$arr = $db->db_fetch_array($db->db_query($req));
		if (!empty($arr['field_function']))
			{
			return array(9); // read-only
			}
		else
			return explode(',',$arr['id_forms_elements']);
		}
	elseif ($id_type > 0)
		{
		$res = $db->db_query("SELECT t.id_forms_elements FROM ".FORM_TABLES_FIELDS_TYPES." t WHERE t.id='".$db->db_escape_string($id_type)."'");
		$arr = $db->db_fetch_array($res);
		return explode(',',$arr['id_forms_elements']);
		}
	else
		{
		return array(1);
		}
	}



function record_table()
	{
	if (!isset($_POST['name']) || $_POST['name'] == '') return false;
	global $babBody;
	$db = $GLOBALS['babDB'];
	if (!is_numeric($_POST['id_table']))
		{
		list($n) = $db->db_fetch_array($db->db_query("SELECT count(*) FROM ".FORM_TABLES." WHERE name='".$db->db_escape_string($_POST['name'])."'"));
		if ($n > 0)
			{
			$babBody->msgerror = form_translate('This name allready exist');
			return false;
			}
		$res = $db->db_query("INSERT INTO ".FORM_TABLES." (name,description,filing_delay,approbation) VALUES ('".$db->db_escape_string($_POST['name'])."','".$db->db_escape_string($_POST['description'])."','".$db->db_escape_string($_POST['filing_delay'])."','".$db->db_escape_string($_POST['approbation'])."')");
		$id_table = $db->db_insert_id($res);
		
		form_ieO::inserted('table', $id_table);
		form_ieO::hasAccess(FORM_TABLES_CHANGE_GROUPS, $id_table);
		form_ieO::hasAccess(FORM_TABLES_VIEW_GROUPS, $id_table);
		form_ieO::hasAccess(FORM_TABLES_INSERT_GROUPS, $id_table);
		form_ieO::hasAccess(FORM_TABLES_DELETE_GROUPS, $id_table);
		}
	else
		{
		
		$res = $db->db_query("SELECT message FROM ".FORM_TABLES_LOCKS." WHERE id_table='".$db->db_escape_string($_POST['id_table'])."'");
		if ($db->db_num_rows($res) > 0)
			{
			list($message) = $db->db_fetch_array($res);
			$babBody->msgerror = $message;
			return false;
			}


		$res = $db->db_query("UPDATE ".FORM_TABLES." SET name='".$db->db_escape_string($_POST['name'])."', description='".$db->db_escape_string($_POST['description'])."', filing_delay='".$db->db_escape_string($_POST['filing_delay'])."', approbation='".$db->db_escape_string($_POST['approbation'])."' WHERE id='".$db->db_escape_string($_POST['id_table'])."'");
		$id_table = $_POST['id_table'];
		}

	include_once $GLOBALS['babAddonPhpPath'].'data_table_editor.php';
	$data_table = new data_table_editor($id_table,$_POST['approbation']);

	

	// record fields
	$list_fields_id = array();
	$worked_id = array();

	$res = $db->db_query("SELECT id,name FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$db->db_escape_string($id_table)."'");
	$field_name = array();
	while ($arr = $db->db_fetch_array($res))
		{
		$list_fields_id[] = $arr['id'];
		$field_name[$arr['id']] = $arr['name'];
		}

	$prefix = 'field_id_';
	foreach ($_POST as $field => $value)
		{
		if (substr($field,0,strlen($prefix)) == $prefix)
			{
			$tmp = explode('_',$field);
			$idx = $tmp[2];

			$type = isset($_POST['field_type_'.$idx]) ? $_POST['field_type_'.$idx] : '';
			$link = isset($_POST['field_link_table_field_id_'.$idx]) ? $_POST['field_link_table_field_id_'.$idx] : 0;
			
			/*
			if (!$type && empty($link))
			{
				// $babBody->addError(sprintf(form_translate('Missing type in field %s'), $_POST['name']));
				continue;
			}
			*/
			
			$field_function = '';

			if (is_numeric($value) && in_array($value,$list_fields_id))
				{
				$worked_id[] = $value;
				$data_table->get_old_col_name($value);
				$db->db_query("
					UPDATE ".FORM_TABLES_FIELDS." 
					SET 
						name				='".$db->db_escape_string($_POST['field_name_'.$idx])."', 
						id_type				='".$db->db_escape_string($type)."', 
						default_val			='".$db->db_escape_string($_POST['field_default_val_'.$idx])."', 
						link_table_field_id	='".$db->db_escape_string($link)."', 
						ordering			='".$db->db_escape_string($idx)."', 
						field_function		='".$db->db_escape_string($field_function)."' 
					WHERE 
						id='".$db->db_escape_string($value)."'
				");
				$data_table->modify_field(form_col_name($value,$_POST['field_name_'.$idx]),$type, $link , $_POST['field_default_val_'.$idx], '' );
				}
			elseif (!empty($_POST['field_name_'.$idx]))
				{
				$res = $db->db_query("
				
				INSERT INTO ".FORM_TABLES_FIELDS." 
					(id_table, name, id_type, default_val, link_table_field_id, ordering, field_function) 
					
				VALUES 
					(
						'".$db->db_escape_string($id_table)."', 
						'".$db->db_escape_string($_POST['field_name_'.$idx])."', 
						'".$db->db_escape_string($type)."', 
						'".$db->db_escape_string($_POST['field_default_val_'.$idx])."', 
						'".$db->db_escape_string($link)."',
						'".$db->db_escape_string($idx)."',
						'".$db->db_escape_string($field_function)."'
					)
				");
				
				$value = $db->db_insert_id($res);
				$data_table->add_field(form_col_name($value,$_POST['field_name_'.$idx]),$type, $link, $_POST['field_default_val_'.$idx], '');
				$worked_id[] = $value;
				}
			
			}
		}

	foreach ($list_fields_id as $id_to_delete)
		{
		if (count($worked_id) > 0 && !in_array($id_to_delete,$worked_id) )
			{
			$db->db_query("DELETE FROM ".FORM_TABLES_FIELDS." WHERE id='".$db->db_escape_string($id_to_delete)."' AND field_function=''");
			$data_table->remove_field(form_col_name($id_to_delete,$field_name[$id_to_delete]));
			}
		}

	return $id_table;
	}



function record_type()
	{
	global $babBody;
	if (!isset($_POST['name']) || $_POST['name'] == '')
		{
		$babBody->msgerror = form_translate('Name must be defined');
		return false;
		}
	
	$db = &$GLOBALS['babDB'];

	list($sql_type_name) = $db->db_fetch_array($db->db_query("SELECT name FROM ".FORM_SQL_TYPES." WHERE id='".$_POST['sql_type']."'"));
	$col_type = strtolower($sql_type_name);

	if (isset($_POST['sql_values']) && count($_POST['sql_values']) > 0)
		{
		function prepare_value($str)
			{
			$str = str_replace('(','[',$str);
			$str = str_replace(')',']',$str);
			$str = str_replace("'",'\\\'',$str);
			return "'".$str."'";
			}
		function strip_comma(&$str)
			{
			$str = str_replace(',',';',$str);
			}
		$values = array_map('prepare_value',$_POST['sql_values']);
		if ('set' == $col_type) 
			array_walk($values,'strip_comma');
		$col_type .= '('.implode(',',$values).')';
	}
	else
		{
		if (isset($_POST['sql_size']) && trim($_POST['sql_size']) != '') {
			$col_type .= '('.$_POST['sql_size'];
		}
		if (isset($_POST['sql_decimal']) && trim($_POST['sql_decimal']) != '') {
			$col_type .= ','.$_POST['sql_decimal'].')';
		}
		elseif (isset($_POST['sql_size']) && trim($_POST['sql_size']) != '') {
			$col_type .= ')';
		}
	}
	if (isset($_POST['sql_attrib']) && trim($_POST['sql_attrib']) != '')
		$col_type .= ' '.$_POST['sql_attrib'];
		

	list($id_forms_elements) = $db->db_fetch_array($db->db_query("SELECT id_forms_elements FROM ".FORM_SQL_TYPES." WHERE id='".$db->db_escape_string($_POST['sql_type'])."'"));
		
	if (isset($_POST['id_type']) && is_numeric($_POST['id_type']))
		{
		$db->db_query("
				UPDATE ".FORM_TABLES_FIELDS_TYPES." 
					SET 
					name				='".$db->db_escape_string($_POST['name'])."', 
					description			='".$db->db_escape_string($_POST['description'])."', 
					id_forms_elements 	='".$db->db_escape_string($id_forms_elements)."', 
					sql_type			='".$db->db_escape_string($_POST['sql_type'])."', 
					col_type			='".$db->db_escape_string($col_type)."' 
				WHERE 
					id='".$db->db_escape_string($_POST['id_type'])."'");
		}
	else
		{
		list($n) = $db->db_fetch_array($db->db_query("
			SELECT COUNT(*) 
			FROM 
				".FORM_TABLES_FIELDS_TYPES." t,
				".FORM_WORKSPACE_ENTRIES." w 
			WHERE 
				t.name='".$db->db_escape_string($_POST['name'])."' 
				AND w.id_object = t.id 
				AND w.id_workspace='".$db->db_escape_string(form_getWorkspace())."' 
		"));

		
		if ($n > 0)
			{
			$babBody->msgerror = form_translate('Sorry, this name allready exist');
			return false;
			}

		$db->db_query("
				INSERT INTO ".FORM_TABLES_FIELDS_TYPES." 
					(
						name, 
						description, 
						id_forms_elements, 
						sql_type, 
						col_type
					) 
				VALUES 
					(
						'".$db->db_escape_string($_POST['name'])."',
						'".$db->db_escape_string($_POST['description'])."',
						'".$db->db_escape_string($id_forms_elements)."',
						'".$db->db_escape_string($_POST['sql_type'])."',
						'".$db->db_escape_string($col_type)."'
					)
			");
			
		form_ieO::inserted('type', $db->db_insert_id());
		}
	return true;
	}



function record_extended_type() {

	global $babBody;
	if (!isset($_POST['name']) || $_POST['name'] == '')
		{
		$babBody->msgerror = form_translate('Name must be defined');
		return false;
		}
	
	$db = &$GLOBALS['babDB'];

	$id_type = isset($_POST['id_type']) ? $_POST['id_type'] : 0;

	list($n) = $db->db_fetch_array($db->db_query("
		SELECT COUNT(*) 
		FROM 
			".FORM_TABLES_FIELDS_TYPES." t,
			".FORM_WORKSPACE_ENTRIES." w 
		WHERE 
			t.name='".$db->db_escape_string($_POST['name'])."' 
			AND t.id<>'".$db->db_escape_string($id_type)."' 
			AND t.id=w.id_object 
			AND w.id='".$db->db_escape_string(form_getWorkspace())."'
		"));
		
	if ($n > 0) {
		$babBody->msgerror = form_translate('Sorry, this name allready exist');
		return false;
		}


	$arr = $db->db_fetch_assoc($db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS_TYPES_EXTEND." WHERE id='".$db->db_escape_string($_POST['extend'])."'"));
	
	if (!$arr) {
		$babBody->msgerror = form_translate('Error');
		return false;
	}
	
	if ((4 == $arr['id'] || 5 == $arr['id']) && empty($_POST['id_directory'])) {
		$babBody->msgerror = form_translate('This type need at least one directory accessible');
		return false;
	}

	
	$posted_elements = isset($_POST['id_forms_elements']) ? array_flip($_POST['id_forms_elements']) : array();
	$extend_elements = explode(',',$arr['id_forms_elements']);
	
	
	

	$tmp = array();
	foreach($extend_elements as $id) {
		if (isset($posted_elements[$id])) {
			$tmp[] = $id;
		}
	}
	$id_forms_elements = implode(',',$tmp);

	if (empty($id_forms_elements))
		$id_forms_elements = $arr['id_forms_elements'];

	$options = array();

	if (isset($_POST['view_date_format'])) {
		$options['view_date_format'] = $_POST['view_date_format'];
	}
	
	

	if (isset($_POST['id_directory'])) {
		$options['id_directory'] = $_POST['id_directory'];
	}

	$str_options = serialize($options);

	$on_delete_user = isset($_POST['on_delete_user']) ? 1 : 0;


	if (!empty($id_type))
		{
		$db->db_query("
			UPDATE 
				".FORM_TABLES_FIELDS_TYPES." 
			SET 
				name				='".$db->db_escape_string($_POST['name'])."', 
				description			='".$db->db_escape_string($_POST['description'])."', 
				sql_type			='".$db->db_escape_string($arr['sql_type'])."',
				col_type			='".$db->db_escape_string($arr['col_type'])."', 
				id_forms_elements	='".$db->db_escape_string($id_forms_elements)."', 
				str_options			='".$db->db_escape_string($str_options)."', 
				id_extend			='".$db->db_escape_string($_POST['extend'])."',
				on_delete_user		='".$db->db_escape_string($on_delete_user)."' 
			WHERE 
				id='".$db->db_escape_string($id_type)."'
			");
		}
	else
		{
		$db->db_query("
			INSERT INTO 
				".FORM_TABLES_FIELDS_TYPES." 
				(
				name, 
				description, 
				sql_type,
				col_type,
				id_forms_elements,
				str_options,
				id_extend,
				on_delete_user
				) 
			VALUES 
				(
				'".$db->db_escape_string($_POST['name'])."',
				'".$db->db_escape_string($_POST['description'])."',
				'".$db->db_escape_string($arr['sql_type'])."',
				'".$db->db_escape_string($arr['col_type'])."',
				'".$db->db_escape_string($id_forms_elements)."',
				'".$db->db_escape_string($str_options)."',
				'".$db->db_escape_string($_POST['extend'])."',
				'".$db->db_escape_string($on_delete_user)."' 
				)
			");
			
			form_ieO::inserted('type', $db->db_insert_id());
		}
	return true;

	}



function record_form()
	{

	global $babBody;
	$db = &$GLOBALS['babDB'];

	if (!isset($_POST['name']) || $_POST['name'] == '')
		{
		$babBody->msgerror = form_translate('Name must be defined');
		return false;
		}
	
	$form_fields = array();
	$ignore = array('id');
	$default = array('rows_per_page' => 30);
	$update = array();
	$insert1 = array();
	$insert2 = array();

	$res = $db->db_query("DESCRIBE ".FORM_FORMS."");
	while ($arr = $db->db_fetch_array($res))
		{
		if (!in_array($arr['Field'],$ignore))
			{
			
			if ("enum('y','n')" == strtolower($arr['Type']))
				{
				$form_fields[$arr['Field']] = isset($_POST[$arr['Field']]) ? 'Y' : 'N';
				}
			elseif (isset($_POST[$arr['Field']]))
				{
				$form_fields[$arr['Field']] = isset($default[$arr['Field']]) && empty($_POST[$arr['Field']]) ? $default[$arr['Field']] : $_POST[$arr['Field']];
				}

			if (isset($form_fields[$arr['Field']]))
				{
				$update[] = $arr['Field'].'='.$db->quote($form_fields[$arr['Field']]);
				$insert1[] = $arr['Field'];
				$insert2[] = $form_fields[$arr['Field']];
				}
			}
		}


	$ordering_counter = 0;

	if (isset($_POST['id_form']) && is_numeric($_POST['id_form']))
		{
		$db->db_query("UPDATE ".FORM_FORMS." SET ".implode(',',$update)." WHERE id=".$db->quote($_POST['id_form']));

		$res = $db->db_query("SELECT id,name, ordering FROM ".FORM_FORMS_FIELDS." WHERE id_form=".$db->quote($_POST['id_form']));
		$list_fields_id = array();

		while ($arr = $db->db_fetch_array($res))
			{
			$list_fields_id[] = $arr['id'];
			$ordering_counter = $ordering_counter < $arr['ordering'] ? $arr['ordering'] : $ordering_counter;
			}
		}
	else
		{
		$n = $db->db_num_rows($db->db_query("SELECT id FROM ".FORM_FORMS." WHERE name='".$db->db_escape_string($_POST['name'])."'"));
		if ($n > 0)
			{
			$babBody->msgerror = form_translate('Name allready exist');
			return false;
			}

		$res = $db->db_query("INSERT INTO ".FORM_FORMS." (".implode(',',$insert1).") VALUES (".$db->quote($insert2).")");
		$_GET['id_form'] = $_POST['id_form'] = $db->db_insert_id($res);
		form_ieO::inserted('form', $_GET['id_form']);
		form_ieO::hasAccess(FORM_FORMS_GROUPS, $_GET['id_form']);
		}

	$worked_id = array();
	$prefix = 'field_id_';

	foreach ($_POST as $field => $value)
		{
		if (substr($field,0,strlen($prefix)) == $prefix)
			{
			$tmp = explode('_',$field);
			$idx = $tmp[2];

			$id_table_field = isset($_POST['field_table_field_'.$idx]) ? $_POST['field_table_field_'.$idx] : 0;
			$id_table_field_link = 0;
			$id_table_field_lnk = 0;
			$id_lnk = 0;

			$t = substr_count($id_table_field,'-');

			if ($t == 1)
				{
				$tmp = explode('-',$id_table_field);
				$id_table_field = $tmp[0];
				$id_table_field_link = $tmp[1];
				}
			elseif ($t == 2)
				{
				$tmp = explode('-',$id_table_field);
				$id_table_field = 0;
				$id_table_field_lnk = $tmp[2];
				$id_lnk = $tmp[1];
				}
			elseif ($t == 3)
				{
				$tmp = explode('-',$id_table_field);
				$id_table_field = $tmp[3];
				$id_table_field_lnk = $tmp[2];
				$id_lnk = $tmp[1];
				}

			$field_id_type = isset($_POST['field_type_'.$idx]) ? $_POST['field_type_'.$idx] : '';
			$field_id_print = isset($_POST['field_print_'.$idx]) && $_POST['field_print_'.$idx]>0 ? $_POST['field_print_'.$idx] : 1;
			$field_filter_on_dir = isset($_POST['field_filter_on_dir_'.$idx]) ? $_POST['field_filter_on_dir_'.$idx] : '';
			$field_filter_on = isset($_POST['field_filter_on_'.$idx]) ? $_POST['field_filter_on_'.$idx] : '';
			$field_filter_type = isset($_POST['field_filter_type_'.$idx]) ? $_POST['field_filter_type_'.$idx] : '';
			$field_use_key_val = isset($_POST['field_use_key_val_'.$idx]) ? $_POST['field_use_key_val_'.$idx] : '';
			$field_copy_table_field = isset($_POST['field_copy_table_field_'.$idx]) ? $_POST['field_copy_table_field_'.$idx] : '';
			$field_copy_on_change = isset($_POST['field_copy_on_change_'.$idx]) ? $_POST['field_copy_on_change_'.$idx] : '';
			$field_help = isset($_POST['field_help_'.$idx]) ? $_POST['field_help_'.$idx] : '';
			$field_link_to_fill = isset($_POST['field_link_to_fill_'.$idx]) ? $_POST['field_link_to_fill_'.$idx] : '';

	
			
			if (is_numeric($value) && in_array($value,$list_fields_id))
				{

				$db->db_query("
					UPDATE ".FORM_FORMS_FIELDS." 
					SET 
						id_type					='".$db->db_escape_string($field_id_type)."',
						id_table_field			='".$db->db_escape_string($id_table_field)."',
						id_table_field_link		='".$db->db_escape_string($id_table_field_link)."',
						id_lnk					='".$db->db_escape_string($id_lnk)."',
						id_table_field_lnk		='".$db->db_escape_string($id_table_field_lnk)."',
						id_print				='".$db->db_escape_string($field_id_print)."',
						name					='".$db->db_escape_string($_POST['field_name_'.$idx])."',
						description				='".$db->db_escape_string($_POST['field_description_'.$idx])."',
						filter_on_dir			='".$db->db_escape_string($field_filter_on_dir)."',
						filter_on				='".$db->db_escape_string($field_filter_on)."',
						field_use_key_val		='".$db->db_escape_string($field_use_key_val)."',
						filter_type				='".$db->db_escape_string($field_filter_type)."',
						field_copy_table_field 	='".$db->db_escape_string($field_copy_table_field)."',
						field_copy_on_change 	='".$db->db_escape_string($field_copy_on_change)."', 
						field_help 				='".$db->db_escape_string($field_help)."',
						field_link_to_fill 		='".$db->db_escape_string($field_link_to_fill)."' 
					WHERE 
						id ='".$db->db_escape_string($value)."'");


				$last_id = $value;
				$worked_id[] = $value;
				
				if (1 !== (int) $field_id_print)
					{
					// mode autre que visualisation
					$db->db_query("DELETE FROM ".FORM_FORMS_FIELDS_CTRL." WHERE id_field=".$db->quote($value));
					}
				}
			else
				{
				$ordering_counter++;
				$res = $db->db_query("
						INSERT INTO ".FORM_FORMS_FIELDS."
							(id_form,
							id_type,
							id_table_field,
							id_table_field_link,
							id_lnk,
							id_table_field_lnk,
							id_print,
							name,
							description,
							filter_on_dir,
							filter_on,
							filter_type,
							field_use_key_val,
							ordering,
							field_copy_table_field,
							field_copy_on_change,
							field_help,
							field_link_to_fill,
							id_form_element
							) 
						VALUES 
							(
							'".$db->db_escape_string($_POST['id_form'])."',
							'".$db->db_escape_string($field_id_type)."',
							'".$db->db_escape_string($id_table_field)."',
							'".$db->db_escape_string($id_table_field_link)."',
							'".$db->db_escape_string($id_lnk)."',
							'".$db->db_escape_string($id_table_field_lnk)."',
							'".$db->db_escape_string($field_id_print)."',
							'".$db->db_escape_string($_POST['field_name_'.$idx])."',
							'".$db->db_escape_string($_POST['field_description_'.$idx])."',
							'".$db->db_escape_string($field_filter_on_dir)."',
							'".$db->db_escape_string($field_filter_on)."',
							'".$db->db_escape_string($field_filter_type)."',
							'".$db->db_escape_string($field_use_key_val)."',
							'".$db->db_escape_string($ordering_counter)."',
							'".$db->db_escape_string($field_copy_table_field)."',
							'".$db->db_escape_string($field_copy_on_change)."',
							'".$db->db_escape_string($field_help)."',
							'".$db->db_escape_string($field_link_to_fill)."',
							'".$db->db_escape_string(form_getDefaultFormElement($id_table_field))."'
							)
						");

				$last_id = $db->db_insert_id($res);
				$worked_id[] = $last_id;
				}

			if (in_array($_POST['id_type'], array(2,4,7)))
				{
				$arr = form_getElementsFromField($field_id_type, $id_table_field,$id_table_field_link ,$id_table_field_lnk);
				}
			else
				{
				$arr = array(9); // force read only
				}

			list($field_form_element) = $db->db_fetch_array($db->db_query("SELECT id_form_element FROM ".FORM_FORMS_FIELDS." WHERE id='".$db->db_escape_string($last_id)."'"));

			if (!in_array($field_form_element,$arr))
				{
				$db->db_query("UPDATE ".FORM_FORMS_FIELDS." SET id_form_element='".$db->db_escape_string($arr[0])."' WHERE id='".$db->db_escape_string($last_id)."'");
				}
				
			
			
			}
		}
		
		
	

	if (isset($list_fields_id) && is_array($list_fields_id) && count($list_fields_id) > 0)
		foreach ($list_fields_id as $id_to_delete)
			{
			if (count($worked_id) > 0 && !in_array($id_to_delete,$worked_id) )
				{
				$db->db_query("DELETE FROM ".FORM_FORMS_FIELDS." WHERE id='".$db->db_escape_string($id_to_delete)."'");
				}
			}
	return true;
	}

function record_form_mail()
	{
	$db = &$GLOBALS['babDB'];	

	$mail_user = isset($_POST['mail_user']) ? 'Y' : 'N';
	$mail_ignore_empty_values = (int) bab_pp('mail_ignore_empty_values', 0);

	$db->db_query("
				UPDATE ".FORM_FORMS." 
				SET 
					mail_user				='".$db->db_escape_string($mail_user)."',
					mail					='".$db->db_escape_string($_POST['mail'])."',
					id_table_field_mail		='".$db->db_escape_string($_POST['id_table_field_mail'])."',
					bcc						='".$db->db_escape_string($_POST['bcc'])."',
					id_table_field_bcc		='".$db->db_escape_string($_POST['id_table_field_bcc'])."', 
					id_group_mail			='".$db->db_escape_string($_POST['id_group_mail'])."', 
					id_group_bcc			='".$db->db_escape_string($_POST['id_group_bcc'])."', 
					mail_subject			='".$db->db_escape_string($_POST['mail_subject'])."',
					mail_ignore_empty_values='".$db->db_escape_string($mail_ignore_empty_values)."' 
				WHERE
					id='".$db->db_escape_string($_POST['id_form'])."'
				");

	$res = $db->db_query("SELECT f.id FROM ".FORM_FORMS_FIELDS." f, ".FORM_FORMS_FIELDS_PRINT." p WHERE f.id_form='".$db->db_escape_string($_POST['id_form'])."' AND f.id_print=p.id AND p.widget='0'");
	while (list($index) = $db->db_fetch_array($res))
		{
		$field_mail = isset($_POST['field_mail_'.$index]) ? 'Y' : 'N';
		$field_mailonchange = isset($_POST['field_mailonchange_'.$index]) ? 1 : 0;
		$field_usemail = $_POST['field_usemail_'.$index];

		$db->db_query("
					UPDATE 
					".FORM_FORMS_FIELDS." 
					SET 
						mail = '".$db->db_escape_string($field_mail)."',
						field_mailonchange = '".$db->db_escape_string($field_mailonchange)."',
						field_usemail = '".$db->db_escape_string($field_usemail)."'
					WHERE 
						id='".$db->db_escape_string($index)."'
					");
		}

	return true;
	}


function record_form_search()
	{
	$db = &$GLOBALS['babDB'];

	$search_description = isset($_POST['search_description']) ? $_POST['search_description'] : 0;

	$db->db_query("
				UPDATE ".FORM_FORMS." 
				SET 
					search_box='".$db->db_escape_string($_POST['search_box'])."',
					id_search_description='".$db->db_escape_string($search_description)."'
				WHERE
					id='".$db->db_escape_string($_POST['id_form'])."'
				");


	$res = $db->db_query("SELECT id FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	while (list($index) = $db->db_fetch_array($res))
		{
		$field_search = isset($_POST['field_search_'.$index]) ? 'Y' : 'N';
		$field_search_crit = isset($_POST['field_s_crit_'.$index]) ? 1 : 0;
		$field_search_type = isset($_POST['field_s_type_'.$index]) ? $_POST['field_s_type_'.$index] : 0;


		$db->db_query("
					UPDATE 
					".FORM_FORMS_FIELDS." 
					SET 
						search = '".$db->db_escape_string($field_search)."',
						search_crit = '".$db->db_escape_string($field_search_crit)."',
						search_type = '".$db->db_escape_string($field_search_type)."'
					WHERE 
						id='".$db->db_escape_string($index)."'
					");
		}


	return true;
	}


function record_form_elements()
	{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("SELECT id FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	while (list($index) = $db->db_fetch_array($res))
		{
		$field_element = isset($_POST['field_element_'.$index]) ? $_POST['field_element_'.$index] : 9; // default : force read-only
		$field_element_param = isset($_POST['field_element_param_'.$index]) ? $_POST['field_element_param_'.$index] : '';
		$field_filter = isset($_POST['field_filter_'.$index]) ? $_POST['field_filter_'.$index] : 0;
		$field_notempty = isset($_POST['field_notempty_'.$index]) ? 'Y' : 'N';
		$id_table_field_optgroup = (isset($_POST['id_table_field_optgroup_'.$index]) && 16 ==$field_element) ? ((int) $_POST['id_table_field_optgroup_'.$index]) : 0;
		$id_proposal = (isset($_POST['id_proposal_'.$index]) && 17 ==$field_element) ? ((int) $_POST['id_proposal_'.$index]) : 0;

		$db->db_query("
					UPDATE 
					".FORM_FORMS_FIELDS." 
					SET 
						id_form_element 		= '".$db->db_escape_string($field_element)."',
						id_list_filter 			= '".$db->db_escape_string($field_filter)."',
						notempty 				= '".$db->db_escape_string($field_notempty)."',
						form_element_param 		= '".$db->db_escape_string($field_element_param)."', 
						id_table_field_optgroup	= ".$db->quote($id_table_field_optgroup).", 
						id_proposal				= ".$db->quote($id_proposal)." 
					WHERE 
						id='".$db->db_escape_string($index)."'
					");
		}

	return true;
	}


function record_field_validations()
	{
	$db = &$GLOBALS['babDB'];
	$worked_id = array();


	if (isset($_POST['validation']) && is_array($_POST['validation']) && count($_POST['validation']) > 0)
		{
		foreach ($_POST['validation'] as $id_validation)
			{
			if (isset($_POST['id_'.$id_validation]))
				{
				$db->db_query("UPDATE ".FORM_FORMS_FIELDS_CTRL." SET 
									 parameter = '".$db->db_escape_string($_POST['param_'.$id_validation])."',
										alert='".$db->db_escape_string($_POST['alert_'.$id_validation])."' 
							WHERE id='".$db->db_escape_string($_POST['id_'.$id_validation])."'
							");
				$worked_id[] = $_POST['id_'.$id_validation];
				}
			else
				{
				$db->db_query("INSERT INTO ".FORM_FORMS_FIELDS_CTRL." 
								(id_field,id_validation,parameter,alert) 
									VALUES 
								(
									'".$db->db_escape_string($_POST['id_field'])."',	
									'".$db->db_escape_string($id_validation)."',
									'".$db->db_escape_string($_POST['param_'.$id_validation])."',
									'".$db->db_escape_string($_POST['alert_'.$id_validation])."' 
								)
							");

				$worked_id[] = $db->db_insert_id();
				}
			}

		}

	$db->db_query("UPDATE ".FORM_FORMS_FIELDS." SET validations='".$db->db_escape_string(count($worked_id))."' WHERE id='".$db->db_escape_string($_POST['id_field'])."'");
	
	$req = "DELETE FROM ".FORM_FORMS_FIELDS_CTRL." WHERE id_field='".$db->db_escape_string($_POST['id_field'])."'";
	if (count($worked_id) > 0)
		$req .= " AND id NOT IN(".$db->quote($worked_id).")";

	$db->db_query($req);

	return true;
	}



function record_form_styles()
	{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("SELECT id FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	while (list($index) = $db->db_fetch_array($res))
		{
		$classname = isset($_POST['field_style_'.$index]) ? $_POST['field_style_'.$index] : 0;

		$db->db_query("
					UPDATE 
					".FORM_FORMS_FIELDS." 
					SET 
						classname = '".$db->db_escape_string($classname)."'
					WHERE 
						id='".$db->db_escape_string($index)."'
					");
		}


	return true;
	}


function record_form_init()
	{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("SELECT id FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	while (list($index) = $db->db_fetch_array($res))
		{
		$init_dir = isset($_POST['init_dir_'.$index]) ? $_POST['init_dir_'.$index] : 0;
		$init = isset($_POST['field_init_'.$index]) ? $_POST['field_init_'.$index] : '';
		$init_on_modify = isset($_POST['field_init_on_modify_'.$index]) ? 'Y' : 'N';
		$id_table_field_init = isset($_POST['link_col_'.$index]) ? $_POST['link_col_'.$index] : 0;

		$db->db_query("
					UPDATE 
					".FORM_FORMS_FIELDS." 
					SET 
						init_dir = '".$db->db_escape_string($init_dir)."',
						init = '".$db->db_escape_string($init)."',
						init_on_modify ='".$db->db_escape_string($init_on_modify)."',
						id_table_field_init ='".$db->db_escape_string($id_table_field_init)."' 
					WHERE 
						id='".$db->db_escape_string($index)."'
					");
		}

	return true;
	}


function record_form_transform()
{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("SELECT id FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	while (list($index) = $db->db_fetch_array($res))
		{
		$transform_to = isset($_POST['transform_to_'.$index]) ? $_POST['transform_to_'.$index] : '';

		$db->db_query("
				UPDATE 
				".FORM_FORMS_FIELDS." 
				SET 
					transform_to = '".$db->db_escape_string($transform_to)."'
				WHERE 
					id='".$db->db_escape_string($index)."'
				");

		}

	return true;
}



function record_form_validation()
{
	$db = &$GLOBALS['babDB'];


	$db->db_query("
			UPDATE ".FORM_FORMS." 
			SET form_approb_text = '".$db->db_escape_string($_POST['form_approb_text'])."' 
			WHERE id ='".$db->db_escape_string($_POST['id_form'])."'
			");		
		

	$res = $db->db_query("SELECT * FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	while ($arr = $db->db_fetch_array($res))
		{
		$refused = isset($_POST['form_approb_refused_'.$arr['id']]) ? $_POST['form_approb_refused_'.$arr['id']] : '';
		$accepted = isset($_POST['form_approb_accepted_'.$arr['id']]) ? $_POST['form_approb_accepted_'.$arr['id']] : '';
		$description = isset($_POST['form_approb_description']) && in_array($arr['id'], $_POST['form_approb_description']) ? 1 : 0;
		
		$db->db_query("
				UPDATE ".FORM_FORMS_FIELDS." 
				SET form_approb_refused = '".$db->db_escape_string($refused)."', 
					form_approb_accepted = '".$db->db_escape_string($accepted)."', 
					form_approb_description = '".$db->db_escape_string($description)."' 
				WHERE id ='".$db->db_escape_string($arr['id'])."'
				");
		}


	return true;

}



function record_form_approbation_list()
{
	$db = &$GLOBALS['babDB'];
	if (empty($_POST['id_form'])) return false;
	$id_form = $_POST['id_form'];

	$str = isset($_POST['approb_list_status']) ? $_POST['approb_list_status'] : array();

	$db->db_query("UPDATE ".FORM_FORMS." SET approb_list_status='".$db->db_escape_string(implode(',',$str))."' WHERE id='".$db->db_escape_string($id_form)."'");
	return true;
}


function record_form_curl()
	{
	$db = &$GLOBALS['babDB'];	

	list($id_type) = $db->db_fetch_array($db->db_query("SELECT id_type FROM ".FORM_FORMS." WHERE id='".$db->db_escape_string($_POST['id_form'])."'"));

	if ($id_type == 4)
		{
		$db->db_query("
				UPDATE ".FORM_FORMS." 
				SET 
					id_form_card='".$db->db_escape_string($_POST['id_form_card'])."', 
					card_field_quantities='".$db->db_escape_string($_POST['card_field_quantities'])."', 
					card_field_total='".$db->db_escape_string($_POST['card_field_total'])."', 
					card_field_products_summary='".$db->db_escape_string($_POST['card_field_products_summary'])."', 
					card_field_transaction_id='".$db->db_escape_string($_POST['card_field_transaction_id'])."', 
					card_field_status='".$db->db_escape_string($_POST['card_field_status'])."', 
					card_field_product_name='".$db->db_escape_string($_POST['card_field_product_name'])."' 
				WHERE
					id='".$db->db_escape_string($_POST['id_form'])."'
				");

		$res = $db->db_query("SELECT id FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
		while (list($index) = $db->db_fetch_array($res))
			{
			$db->db_query("UPDATE ".FORM_FORMS_FIELDS." SET field_transaction = '".$db->db_escape_string($_POST['field_transaction_'.$index])."' WHERE id='".$db->db_escape_string($index)."'");
			}

		}

	if ($id_type == 5)
		{
		$db->db_query("
				UPDATE ".FORM_FORMS." 
				SET 
					sc_formula			='".$db->db_escape_string($_POST['sc_formula'])."', 
					trans_formula		='".$db->db_escape_string($_POST['trans_formula'])."', 
					trans_formula_msg	='".$db->db_escape_string($_POST['trans_formula_msg'])."', 
					unit_concat_before	='".$db->db_escape_string($_POST['unit_concat_before'])."', 
					unit_concat			='".$db->db_escape_string($_POST['unit_concat'])."'
				WHERE
					id	='".$db->db_escape_string($_POST['id_form'])."'
				");

		}
	
	
	return true;
	}


function record_form_ordgrp()
	{
	$db = &$GLOBALS['babDB'];
	$db->db_query("UPDATE ".FORM_FORMS." SET 
					order_type		='".$db->db_escape_string($_POST['order_type'])."' ,
					order_type2		='".$db->db_escape_string($_POST['order_type2'])."' ,
					id_group_by		='".$db->db_escape_string($_POST['id_group_by'])."' ,
					id_group_by2	='".$db->db_escape_string($_POST['id_group_by2'])."',
					id_order_by		='".$db->db_escape_string($_POST['id_order_by'])."' ,
					id_order_by2	='".$db->db_escape_string($_POST['id_order_by2'])."'
				WHERE 
					id='".$db->db_escape_string($_POST['id_form'])."'
				");
	return true;
	}


function record_form_increment() {

	$int_increment = &$_POST['int_increment'];
	$db = &$GLOBALS['babDB'];

	$db->db_query("UPDATE ".FORM_FORMS_FIELDS." SET 
					int_increment = '0' 
				WHERE 
					id_form='".$db->db_escape_string($_POST['id_form'])."'
				");
	
	if (!isset($int_increment) || 0 == count($int_increment)) {
		return true;
		}
	
	
	$db->db_query("UPDATE ".FORM_FORMS_FIELDS." SET 
					int_increment = '1' 
				WHERE 
					id_form='".$db->db_escape_string($_POST['id_form'])."' AND id IN(".$db->quote($int_increment).")
				");
	return true;
}



function record_order_form()
	{
	$db = &$GLOBALS['babDB'];	
	for($i=0; $i < count($_POST['listfields']); $i++)
		{
		$db->db_query("UPDATE ".FORM_FORMS_FIELDS." SET ordering='".($i+1)."' WHERE id='".$db->db_escape_string($_POST['listfields'][$i])."'");
		}
	}
	
	
/**
 * Delete a working space object.
 */
function delete_wsO($type, $id_object) {

	include_once $GLOBALS['babAddonPhpPath']."impexp.php";
	$error = '';
	$classname = 'form_wsO'.$type;
	$obj = new $classname();
	$return = $obj->delete(array($id_object), $error);
	
	if ('' !== $error) {
		global $babBody;
		$babBody->addError($error);
	}
	
	return $return;
}


function delete_type()
	{
	delete_wsO('type', $_POST['id_type']);
	}

function delete_table()
	{
	$db = &$GLOBALS['babDB'];
	
	$db->db_query("DELETE FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$db->db_escape_string($_POST['id_table'])."'");
	delete_wsO('table', $_POST['id_table']);
	}

function delete_form()
	{
	$db = &$GLOBALS['babDB'];
	$db->db_query("DELETE FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	delete_wsO('form', $_POST['id_form']);
	}

function delete_step($id_step)
	{
	$db = &$GLOBALS['babDB'];
	$db->db_query("DELETE FROM ".FORM_APP_STEP_APPROB." WHERE id_step='".$db->db_escape_string($id_step)."'");
	$db->db_query("DELETE FROM ".FORM_APP_STEP_CASES." WHERE id_step='".$db->db_escape_string($id_step)."'");
	delete_wsO('app_step', $id_step);
	}

function delete_application()
	{
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("SELECT id FROM ".FORM_APP_STEPS." WHERE id_application='".$db->db_escape_string($_POST['id_app'])."'");
	while (list($id_step) = $db->db_fetch_array($res))
		{
		delete_step($id_step);
		}
	$db->db_query("DELETE FROM ".FORM_APP_APPLICATIONS." WHERE id='".$db->db_escape_string($_POST['id_app'])."'");
	$db->db_query("DELETE FROM ".FORM_APP_LINKS." WHERE id_application='".$db->db_escape_string($_POST['id_app'])."'");
	
	require_once $GLOBALS['babInstallPath']."admin/acl.php";
	aclDelete(FORM_APP_GROUPS	, $_POST['id_app']);
	}


function record_application()
	{
	global $babBody;
	$db = &$GLOBALS['babDB'];
	
	if (empty($_POST['name']))
		{
		$babBody->msgerror = form_translate('Name must be defined');
		return false;
		}
	
	
	if (isset($_POST['id_app']) && is_numeric($_POST['id_app']))
		{
		$id_app = $_POST['id_app'];
		
		$db->db_query("UPDATE ".FORM_APP_APPLICATIONS." SET name='".$db->db_escape_string($_POST['name'])."', description='".$db->db_escape_string($_POST['description'])."',https='".$db->db_escape_string($_POST['https'])."',lastupdate=NOW() WHERE id='".$db->db_escape_string($_POST['id_app'])."'");
		}
	else
		{
		$db->db_query("INSERT INTO ".FORM_APP_APPLICATIONS." (name, description, https,lastupdate) VALUES ('".$db->db_escape_string($_POST['name'])."', '".$db->db_escape_string($_POST['description'])."', '".$db->db_escape_string($_POST['https'])."',NOW())");
		$id_app = $db->db_insert_id();
		
		form_ieO::inserted('application', $id_app);
		form_ieO::hasAccess(FORM_APP_GROUPS, $id_app);
		}

	$db->db_query("DELETE FROM ".FORM_GEST_RIGHTS_APP." WHERE id_app='".$db->db_escape_string($id_app)."'");

	if (isset($_POST['app_grp']) && count($_POST['app_grp']) > 0)
		{
		foreach($_POST['app_grp'] as $idr)
			{
			$db->db_query("INSERT INTO ".FORM_GEST_RIGHTS_APP." (id_app,id_right) VALUES ('".$db->db_escape_string($id_app)."','".$db->db_escape_string($idr)."')");
			}
		}

	return true;
	}


function record_step(&$id_step)
	{
	global $babBody;
	$db = &$GLOBALS['babDB'];

	if (empty($_POST['name']))
		{
		$babBody->msgerror = form_translate('Name must be defined');
		return false;
		}

	if (empty($_POST['id_app']))
		{
		$babBody->msgerror = form_translate('Internal error');
		return false;
		}


	$db->db_query("UPDATE ".FORM_APP_APPLICATIONS." SET lastupdate=NOW() WHERE id='".$db->db_escape_string($_POST['id_app'])."'");


	if (isset($_POST['id_step']) && is_numeric($_POST['id_step']))
		{
		list($n) = $db->db_fetch_array($db->db_query("SELECT COUNT(*) FROM ".FORM_APP_STEPS." s, ".FORM_APPROB_SCHI." i WHERE s.id='".$db->db_escape_string($_POST['id_step'])."' AND s.id=i.id_step"));
		if ($n > 0)
			{
			$babBody->msgerror = form_translate("Step can't be modified while there is waiting approbations");
			return false;
			}
		}
	
	
	
	$n = $db->db_num_rows($db->db_query("SELECT id FROM ".FORM_APP_STEPS." WHERE name='".$db->db_escape_string($_POST['name'])."' AND id_application='".$db->db_escape_string($_POST['id_app'])."' AND id<>".$db->quote($id_step)));
	if ($n > 0)
		{
		$babBody->msgerror = form_translate('Name allready exist');
		return false;
		}

		
		
		
	include_once $GLOBALS['babAddonPhpPath']."steptypeincl.php";
	$id_type = (int) $_POST['id_type'];
	include_once $GLOBALS['babAddonPhpPath'].'steps/'.$id_type.'.php';
	$classname = 'form_stepType_'.$id_type;
	
	return call_user_func(
			array($classname, 'record')
		);



	

	
	// approbation

	$worked_id = array();

	$prefix = 'approb_id_approb_';
	foreach ($_POST as $field => $value)
		{
		if (substr($field,0,strlen($prefix)) == $prefix)
			{
			$tmp = explode('_',$field);
			$idx = $tmp[3];

			$form_el = array('id_form_field', 'field_value','id_sh','id_form');

			if ( !empty($value) && is_numeric($value))
				{
				$query = '';
				foreach ($form_el as $field)
					{
						$query = $query == '' ? $query : $query.',';
						$query .= $field."='".(isset($_POST['approb_'.$field.'_'.$idx]) ? $db->db_escape_string($_POST['approb_'.$field.'_'.$idx]) : '')."'";
					}
				
				$db->db_query("UPDATE ".FORM_APP_STEP_APPROB." SET ".$query." WHERE id='".$db->db_escape_string($value)."'");
				$worked_id[] = $value;
				}
			else
				{
				$query = '';
				foreach ($form_el as $field)
					{
						$query = $query == '' ? $query : $query.',';
						$query .= "'".(isset($_POST['approb_'.$field.'_'.$idx]) ? $db->db_escape_string($_POST['approb_'.$field.'_'.$idx]) : '')."'";
					}

				$res = $db->db_query("INSERT INTO ".FORM_APP_STEP_APPROB." (id_step,".implode(',',$form_el).") VALUES ('".$db->db_escape_string($id_step)."',".$query.")");
				$worked_id[] = $db->db_insert_id($res);
				}
			}
		}

	if (count($worked_id) > 0 )
		$db->db_query("DELETE FROM ".FORM_APP_STEP_APPROB." WHERE id_step='".$db->db_escape_string($id_step)."' AND id NOT IN(".$db->quote($worked_id).")");
	else
		$db->db_query("DELETE FROM ".FORM_APP_STEP_APPROB." WHERE id_step='".$db->db_escape_string($id_step)."'");


	// menu contextuel

	$worked_id = array();

	$prefix = 'menu_id_menu_';
	foreach ($_POST as $field => $value)
		{
		if (substr($field,0,strlen($prefix)) == $prefix)
			{
			$tmp = explode('_',$field);
			$idx = $tmp[3];

			if (empty($_POST['menu_name_'.$idx]))
				{
				list($name) = $db->db_fetch_array($db->db_query("SELECT name FROM ".FORM_APP_STEPS." WHERE id='".$db->db_escape_string($_POST['menu_id_menustep_'.$idx])."'"));
				}
			else
				{
				$name = $_POST['menu_name_'.$idx];
				}

			

			$id_menustep = $_POST['menu_id_menustep_'.$idx];
			$selmenu = isset($_POST['menu_selmenu']) && $_POST['menu_selmenu'] == $idx ? 1 : 0;

			if ( !empty($value) && is_numeric($value))
				{
				
				$db->db_query("UPDATE ".FORM_APP_STEP_MENU." 
								SET 
									name='".$db->db_escape_string($name)."', 
									id_menustep='".$db->db_escape_string($id_menustep)."',
									selmenu='".$db->db_escape_string($selmenu)."'
								WHERE id='".$db->db_escape_string($value)."'
								");

				$worked_id[] = $value;
				}
			else
				{

				$res = $db->db_query("
				INSERT INTO ".FORM_APP_STEP_MENU." 
					(id_step,name,id_menustep,selmenu) 
				VALUES 
					('".$db->db_escape_string($id_step)."','".$db->db_escape_string($name)."','".$db->db_escape_string($id_menustep)."','".$db->db_escape_string($selmenu)."')
					");

				$worked_id[] = $db->db_insert_id($res);
				}
			}
		}

	
		
	if (count($worked_id) > 0 )
		{
		list($selected_menu) = $db->db_fetch_array($db->db_query("SELECT id FROM ".FORM_APP_STEP_MENU." WHERE id IN(".$db->quote($worked_id).") AND selmenu='1'"));

		if (empty($selected_menu))
			{
			$db->db_query("UPDATE ".FORM_APP_STEP_MENU." SET selmenu='1' WHERE id='".$db->db_escape_string($worked_id[0])."'");
			$selected_menu = $worked_id[0];
			}

		if ($_POST['id_type'] == 6)
			{
			list($menu_id_form) = $db->db_fetch_array($db->db_query("SELECT s.id_form FROM ".FORM_APP_STEP_MENU." m,".FORM_APP_STEPS." s WHERE m.id='".$db->db_escape_string($selected_menu)."' AND m.id_menustep=s.id"));
			$db->db_query("UPDATE ".FORM_APP_STEPS." SET id_form='".$db->db_escape_string($menu_id_form)."' WHERE id='".$db->db_escape_string($_POST['id_step'])."'");
			}

		$db->db_query("DELETE FROM ".FORM_APP_STEP_MENU." WHERE id_step='".$db->db_escape_string($id_step)."' AND id NOT IN(".$db->quote($worked_id).")");
		}
	else
		$db->db_query("DELETE FROM ".FORM_APP_STEP_MENU." WHERE id_step='".$db->db_escape_string($id_step)."'");

	
	if (isset($_SESSION['form_ctxMenu_'.$_POST['id_app']]))
		unset($_SESSION['form_ctxMenu_'.$_POST['id_app']]);

	return true;
	
	}


function record_link()
	{
	if (!empty($_POST['id_app']))
		{
		$db = & $GLOBALS['babDB'];
		
		if (empty($_POST['id_link']))
			{
			$db->db_query("INSERT INTO ".FORM_APP_LINKS." 
						(id_application,  id_field, id_step, id_index, id_if_field, if_operator, if_value) 
					VALUES 
						('".$db->db_escape_string($_POST['id_app'])."', 
							'".$db->db_escape_string($_POST['link'])."', 
							'".$db->db_escape_string($_POST['target'])."', 
							'".$db->db_escape_string($_POST['id_index'])."', 
							'".$db->db_escape_string($_POST['id_if_field'])."',
							'".$db->db_escape_string($_POST['if_operator'])."',
							'".$db->db_escape_string($_POST['if_value'])."'
							)
					");
			}
		else
			{
			$db->db_query("UPDATE ".FORM_APP_LINKS." SET 
						id_field = '".$db->db_escape_string($_POST['link'])."',
						id_step = '".$db->db_escape_string($_POST['target'])."',
						id_index = '".$db->db_escape_string($_POST['id_index'])."',
						id_if_field = '".$db->db_escape_string($_POST['id_if_field'])."',
						if_operator = '".$db->db_escape_string($_POST['if_operator'])."',
						if_value = '".$db->db_escape_string($_POST['if_value'])."'
						
						WHERE
							id = '".$db->db_escape_string($_POST['id_link'])."'
					");
			}
		}
	}


function link_delete() {
	
	$db = & $GLOBALS['babDB'];

	$db->db_query("DELETE FROM ".FORM_APP_LINKS." WHERE id='".$db->db_escape_string($_POST['id_link'])."'");

	}



function record_first_step()
	{
	if (isset($_POST['id_first_step']))
		{
		$db = &$GLOBALS['babDB'];
		$db->db_query("UPDATE ".FORM_APP_APPLICATIONS." SET id_first_step = '".$db->db_escape_string($_POST['id_first_step'])."' WHERE  id='".$db->db_escape_string($_POST['id_app'])."'");
		}
	}

function import_application()
	{
	global $babBody;

	require_once( $GLOBALS['babInstallPath']."addons/forms/impexp.php");
	$form_impexp = new form_impexp();

	if( !empty($_FILES['uploadf']['name']))
		{
		if( $_FILES['uploadf']['size'] > $GLOBALS['babMaxFileSize'])
			{
			$babBody->msgerror = bab_translate("The file was greater than the maximum allowed") ." :". $GLOBALS['babMaxFileSize'];
			return false;
			}

		if (!is_dir($GLOBALS['babUploadPath'].'/tmp/')) {
			bab_mkdir($GLOBALS['babUploadPath'].'/tmp/');
		}

		$ul = $GLOBALS['babUploadPath'].'/tmp/'.$_FILES['uploadf']['name'];
		move_uploaded_file($_FILES['uploadf']['tmp_name'],$ul);

		$handle = fopen($ul,'rb');
		$string = '';
		while (!feof($handle)) {
			$string .= fread($handle, 8192);
			}
		fclose($handle);
		

		if (!$form_impexp->import($string))
			{
			$babBody->msgerror = bab_translate("Error, wrong file format");
			}
		}
	}

function record_tabledesc($id_table)
	{

	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("SELECT id FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$db->db_escape_string($id_table)."'");

	while ($arr = $db->db_fetch_array($res))
		{
		$db->db_query("UPDATE ".FORM_TABLES_FIELDS." SET description='".$db->db_escape_string($_POST['description_'.$arr['id']])."' WHERE id='".$db->db_escape_string($arr['id'])."'");
		}

	return true;
	}



function record_copy_form()
{
	$db = &$GLOBALS['babDB'];

	if (empty($_POST['id_form']) || empty($_POST['newname']))
		{
		return false;
		}

	list($n) = $db->db_fetch_array($db->db_query("SELECT COUNT(*) FROM ".FORM_FORMS." WHERE name='".$db->db_escape_string($_POST['newname'])."'"));
	if ($n > 0)
		{
		$GLOBALS['babBody']->msgerror = form_translate("This name allready exist");
		return false;
		}

	$req = "SELECT * FROM ".FORM_FORMS." WHERE id='".$db->db_escape_string($_POST['id_form'])."'";
	$form = $db->db_fetch_assoc($db->db_query($req));

	unset($form['id']);
	$form['name'] = $_POST['newname'];

	$cols = array();
	$values = array();

	foreach($form as $field => $value)
		{
		$cols[] = $field;
		$values[] = $value;
		}
	
	$db->db_query("INSERT INTO ".FORM_FORMS." (".implode(',',$cols).") VALUES (".$db->quote($values).")");
	$newid = $db->db_insert_id();
	
	$db->db_query("INSERT INTO ".FORM_WORKSPACE_ENTRIES." (type, id_object, id_workspace) 
	VALUES ('form', ".$db->quote($newid).", ".$db->quote(form_getWorkspace()).")");

	$res = $db->db_query("SELECT * FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$db->db_escape_string($_POST['id_form'])."'");
	while ($arr = $db->db_fetch_assoc($res))
		{
		$field_id = $arr['id'];
		unset($arr['id']);
		$arr['id_form'] = $newid;
		$cols = array();
		$values = array();
		foreach($arr as $field => $value)
			{
			$cols[] = $field;
			$values[] = $value;
			}

		$db->db_query("INSERT INTO ".FORM_FORMS_FIELDS." (".implode(',',$cols).") VALUES (".$db->quote($values).")");

		$update = array();
		
		if ($field_id == $form['id_order_by'])
			$update[] = "id_order_by='".$db->db_insert_id()."'";

		if ($field_id == $form['id_order_by2'])
			$update[] = "id_order_by2='".$db->db_insert_id()."'";

		if ($field_id == $form['id_group_by'])
			$update[] = "id_group_by='".$db->db_insert_id()."'";

		if ($field_id == $form['id_group_by2'])
			$update[] = "id_group_by2='".$db->db_insert_id()."'";

		if (count($update) > 0)
			{
			$db->db_query("UPDATE ".FORM_FORMS." SET ".implode(',',$update)." WHERE id='".$db->db_escape_string($newid)."'");
			}
		}

	return true;
} 








?>