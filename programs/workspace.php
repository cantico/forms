<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function list_workspaces() {
	class temp {
		function temp() {
			
			global $babDB;
			
			$this->t_edit = form_translate('Edit');
			$this->t_export = form_translate('Export');
			$this->t_assign = form_translate('Assign objects');
			
			$this->t_types = form_translate('Types');
			$this->t_tables = form_translate('Tables');
			$this->t_forms = form_translate('Forms');
			$this->t_application = form_translate('Applications');
			
			$this->res = $babDB->db_query('
				SELECT id, name 
				FROM '.FORM_WORKSPACES.' 
				ORDER BY name 
			');
			
			if (isset($_GET['id_workspace'])) {
				$this->id_workspace = (int) $_GET['id_workspace'];
				if (isset($_SESSION['form_current_workspace'])) {
					unset($_SESSION['form_current_workspace']);
				}
				
				$reg = bab_getRegistryInstance();
				$reg->changeDirectory('/forms/user/'.$GLOBALS['BAB_SESS_USERID']);
				$reg->setKeyValue('workspace', $this->id_workspace);
				
			} else {
				$this->id_workspace = (int) form_getWorkspace();
			}
			
			
			$res = $babDB->db_query('SELECT * FROM '.FORM_WORKSPACES.' WHERE id='.$babDB->quote($this->id_workspace));
			$arr = $babDB->db_fetch_assoc($res);
			$this->workspace_description = bab_toHtml($arr['description'], BAB_HTML_ALL);
			
		}
	
		function getnextworkspace() {
			global $babDB;
			if ($arr = $babDB->db_fetch_assoc($this->res)) {
				$this->url = bab_toHtml($GLOBALS['babAddonUrl'].'workspace&id_workspace='.$arr['id']);
				$this->name = bab_toHtml($arr['name']);
				$this->selected = $this->id_workspace === (int) $arr['id'];

				return true;
			}
			
			return false;
		}
	}

	
	global $babBody;
	$temp = new temp();
	$babBody->addStyleSheet($GLOBALS['babAddonHtmlPath'].'forms.css');
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath'].'workspace.html', 'list'));
}






function edit_workspace() {
	class temp {
		function temp() {
			
			global $babDB;
			
			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_record = form_translate('Record');
			$this->t_delete = form_translate('Delete');
			$this->t_delete_confirm = form_translate('Do you really want to delete the workspace?');
			
			$this->id_workspace = (int) bab_rp('id_workspace',0 );
			
			if (0 === $this->id_workspace) {
			
				$this->name = '';
				$this->description = '';
			
			} else {
			
				$res = $babDB->db_query('
					SELECT name, description 
					FROM '.FORM_WORKSPACES.' WHERE id='.$babDB->quote($this->id_workspace).'
				');
				
				$arr = $babDB->db_fetch_assoc($res);
				
				$this->name = bab_toHtml($arr['name']);
				$this->description = bab_toHtml($arr['description']);
			}
			
		}
	

	}

	
	global $babBody;
	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath'].'workspace.html', 'edit'));

}



function assign_workspace() {

	class temp {
	
		var $type;
		var $altbg = true;
	
		function temp() {
			
			global $babDB;
			
			include_once $GLOBALS['babAddonPhpPath']."impexp.php";
			
			$this->inst('workspace');
			
			$this->t_next = form_translate('Next');
		}
	
		function inst($type) {
			$classname = 'form_wsO'.$type;
			if (!class_exists($classname)) {
				return;
			}
			$workspace = new $classname();
			if ($workspace->isSelectable()) {
				$this->type[$type] = $workspace->getTypeTitle();
			}
			$arr = $workspace->getSub();
			foreach($arr as $type) {
				$this->inst($type);
			}
		}
		
		
		function getnexttype() {
			if (list($type,$label) = each($this->type)) {
				$this->altbg	= !$this->altbg;
				$this->value 	= bab_toHtml($type);
				$this->label 	= bab_toHtml($label);
				
				if (!isset($this->checked)) {
					$this->checked = true;
				} else {
					$this->checked = false;
				}
				return true;
			}
			
			return false;
		}
	}

	
	global $babBody;
	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath'].'workspace.html', 'assign'));
}



function assign_workspace_objects() {

	class temp {
	
		var $type;
		var $altbg = true;
	
		function temp() {
			
			global $babDB, $babBody;
			
			$this->t_submit = form_translate('Record');
			
			include_once $GLOBALS['babAddonPhpPath']."impexp.php";
			
			$this->type = bab_pp('type', false);
			
			if (false === $this->type) {
				$this->objects = array();
				$babBody->addError(form_translate('Unexpected error, no type selected'));
				return;
			}
			
			
			$classname = 'form_wsO'.$this->type;
			$obj = new $classname();
			$this->objects = $obj->getItems();

		}
		
		
		function getnext() {
			if (list($id,$arr) = each($this->objects)) {
				$this->altbg	= !$this->altbg;
				$this->value 	= bab_toHtml($id);
				$this->label 	= bab_toHtml($arr['name']);
				$this->selected = $arr['selected'];
				return true;
			}
			
			return false;
		}
	}

	
	global $babBody;
	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath'].'workspace.html', 'assign2'));
}






function ws_export() {

	require_once $GLOBALS['babAddonPhpPath']."impexp.php";
	
	$export = array();

	function ws_export_addObject(&$export, $type) {
		$classname = 'form_wsO'.$type;
		$obj = new $classname();
		$obj->record();
		$export[] = array($type,serialize($obj));
		$arr = $obj->getSub();

		foreach($arr as $type) {
			ws_export_addObject($export, $type);
		}
	}
	
	ws_export_addObject($export, 'workspace');
	
	
	$id_workspace = form_getWorkspace();
	global $babDB;
	
	$res = $babDB->db_query('SELECT name FROM '.FORM_WORKSPACES.' WHERE id='.$babDB->quote($id_workspace));
	$ws = $babDB->db_fetch_assoc($res);
	
	include_once $GLOBALS['babAddonPhpPath']."zip.lib.php";
	
	$zip = new Zip;
	$zip->Add($export,1);
	header('Content-Type:application/zip');
	header('Content-Disposition: attachment; filename="'.$ws['name'].'.zip"');
	die($zip->get_file());
}





function ws_setaccess() {

	require_once $GLOBALS['babInstallPath']."admin/acl.php";
	include_once $GLOBALS['babAddonPhpPath']."impexp.php";
	$export = array();

	function ws_setaccess_addObject($type) {
		$classname = 'form_wsO'.$type;
		$obj = new $classname();
		$obj->setAccess();
		
		$arr = $obj->getSub();

		foreach($arr as $type) {
			ws_setaccess_addObject($type);
		}
	}
	
	ws_setaccess_addObject('workspace');
}





function import_workspace() {


	class temp {

		function temp() {

			$this->t_submit = form_translate('Record');

		}
	}

	
	global $babBody;
	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath'].'workspace.html', 'import'));
}







function forms_editor_popup()
{
	class temp {
		
		var $resws;
		var $resapp;
		
		var $workspace;
		

		function temp() {
			global $babDB;

			$this->resws = $babDB->db_query('
				SELECT id, name 
				FROM '.FORM_WORKSPACES.' 
				ORDER BY name 
			');
			

		}
		
		
		function getnextws()
		{
			global $babDB;
			if ($arr = $babDB->db_fetch_assoc($this->resws))
			{
				$this->workspace = bab_toHtml($arr['name']);
				$this->resapp = $babDB->db_query('SELECT 
					a.id,
					a.name 
					 
				FROM 
					'.FORM_APP_APPLICATIONS.' a, 
					'.FORM_WORKSPACE_ENTRIES.' we 
				WHERE 
					we.type=\'application\' 
					AND we.id_object=a.id  
					AND we.id_workspace='.$babDB->quote($arr['id']));
				
				return true;
			}
			
			return false;
		}
		
		function getnextapp()
		{
			global $babDB;
			if ($arr = $babDB->db_fetch_assoc($this->resapp))
			{
				$this->application = bab_toHtml($arr['name']);
				$this->appurl = bab_toHtml($GLOBALS['babUrlScript']."?tg=addon/forms/form&idx=application&id_app=".$arr['id']);;
				return true;
			}
			
			return false;
		}
	}

	
	global $babBody;
	$temp = new temp();
	$babBody->babpopup(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath'].'workspace.html', 'editor'));
	
}















function record_import_workspace() {
	include_once $GLOBALS['babAddonPhpPath']."impexp.php";
	include_once $GLOBALS['babInstallPath']."utilit/uploadincl.php";
	
	global $babBody;
	
	$ul = bab_fmFile::upload('workspace');
	if (!$ul) {
		return false;
	}
	
	$directory = $GLOBALS['babAddonUpload'];
	if (!is_dir($directory)) {
		bab_mkdir($directory);
	}
	
	$directory .= 'tmp/';
	if (!is_dir($directory)) {
		bab_mkdir($directory);
	}

	
	$destination = $directory.$ul->filename;
	$ul->import($destination);
	
	include_once $GLOBALS['babAddonPhpPath']."zip.lib.php";
	$zip = new Zip;
	$zipcontents = $zip->get_List($destination);
	$extract = $GLOBALS['babAddonUpload'].'tmp/extract/';
	$zip->Extract($destination,$extract);
	
	$import = array();
	
	foreach ($zipcontents as $arr) {
		$contents = '';
		$fp = fopen($extract.$arr['filename'], 'rb');
		if ($fp) {
			while(false === feof($fp)) {
				$contents .= fread($fp, 8192);
			}
			
			fclose($fp);
		}
		
		$obj = unserialize($contents);
		if ($obj->error) {
			$babBody->addError($obj->error);
			return false;
		}
		$import[] = $obj;
	}

	unlink($destination);
	form_deldir($extract);
	
	// restaurer les liens
	
	
	$currentkey = 0;
	
	while(isset($import[$currentkey])) {
		$obj1 = $import[$currentkey];
	
		foreach($import as $key => $obj2) {
			$obj1->otherObject($import[$key]);
		}
		
		$currentkey++;
	}
	
	// create table
	
	foreach($import as $key => $obj) {
		$import[$key]->end();
	}
}




function record_workspace() {
	global $babDB;
	global $babBody;
	
	$id_workspace = (int) bab_pp('id_workspace',0);
	
	
	$res = $babDB->db_query('SELECT id FROM '.FORM_WORKSPACES.' WHERE name LIKE \''.$babDB->db_escape_like(bab_pp('name')).'\' AND id <>'.$babDB->quote($id_workspace));
	$n = (int) $babDB->db_num_rows($res);
	

	
	
	
	
	
	if (0 === $id_workspace) {
	
		if (0 < $n) {
			
			$babBody->addError(form_translate('A workspace with the same name allready exists'));
			return false;
		}
			
	
		$babDB->db_query('INSERT INTO '.FORM_WORKSPACES.' 
		(name, description, uid) 
		VALUES
		(
			'.$babDB->quote(bab_pp('name')).',
			'.$babDB->quote(bab_pp('description')).',
			'.$babDB->quote(md5(uniqid(rand(), true))).'
		)');
		
		
		$id_workspace = (int) $babDB->db_insert_id();
		
		
		// associer les types par d�faut id de 1 � 8
		
		$res = $babDB->db_query('SELECT id FROM '.FORM_TABLES_FIELDS_TYPES." WHERE id<'9'");
		while ($arr = $babDB->db_fetch_assoc($res)) {
			$id_object = (int) $arr['id'];
			form_ieO::linkToWorkspace('type', $id_object, $id_workspace);
		}
		
		
		// si c'est le premier workspace, le mettre comme workspace en cours
		
		$res= $babDB->db_query('SELECT id FROM '.FORM_WORKSPACES.'');
		if (1 === $babDB->db_num_rows($res))
		{
			if (isset($_SESSION['form_current_workspace'])) {
				unset($_SESSION['form_current_workspace']);
			}
			
			$reg = bab_getRegistryInstance();
			$reg->changeDirectory('/forms/user/'.$GLOBALS['BAB_SESS_USERID']);
			$reg->setKeyValue('workspace', $id_workspace);
		}
		
		
		return true;
		
	} else {
	
		if (isset($_POST['delete_ws'])) {
		
			include_once $GLOBALS['babAddonPhpPath']."impexp.php";
			
			$error = '';
			$obj = new form_wsOworkspace();
			form_wsOworkspace::onDeleteWorkspace($obj, $error);
			
			if (!empty($error)) {
				$babBody->addError($error);
			}
			
			//$babDB->db_query('
			//DELETE FROM '.FORM_WORKSPACES.' WHERE id='.$babDB->quote($id_workspace));
			if (isset($_SESSION['form_current_workspace'])) {
				unset($_SESSION['form_current_workspace']);
			}
			
			return true;
			
		} else {
		
			if (0 < $n) {
			
				$babBody->addError(form_translate('A workspace with the same name allready exists'));
				return false;
			}
		

			$babDB->db_query('UPDATE '.FORM_WORKSPACES.' 
			SET 
			name='.$babDB->quote(bab_pp('name')).',
			description='.$babDB->quote(bab_pp('description')).' 
			WHERE id='.$babDB->quote($id_workspace));
			
			if (class_exists('bab_siteMap')) {
				bab_siteMap::clearAll();
				header('location:'.$GLOBALS['babAddonUrl'].'workspace');
				exit;
			}
			
			return true;
		}
	}
}



function record_assign() {

	global $babDB;
	
	$id_workspace = form_getWorkspace();
	$type = bab_pp('type', false);
	
	if (false === $type) {
		return false;
	}
	
	include_once $GLOBALS['babAddonPhpPath']."impexp.php";
	
	$classname = 'form_wsO'.$type;
	$obj = new $classname();
	if (!$obj->isSelectable()) {
		return false;
	}
	
	$babDB->db_query('
		DELETE FROM '.FORM_WORKSPACE_ENTRIES.' 
		WHERE 
			id_workspace='.$babDB->quote($id_workspace).' 
			AND type='.$babDB->quote($type)
	);
			
	if (isset($_POST['object']) && is_array($_POST['object'])) {
		foreach($_POST['object'] as $id_object) {
			$babDB->db_query('
			
			INSERT INTO '.FORM_WORKSPACE_ENTRIES.' 
				(id_workspace, id_object, type) 
			VALUES 
				(
					'.$babDB->quote($id_workspace).',
					'.$babDB->quote($id_object).',
					'.$babDB->quote($type).'
				)');
		}
	}
}














// main



$idx = bab_rp('idx', 'list');


$FORM_USER = form_getUserFormAcces();


if (!$FORM_USER['ADMIN'])
	{
	$babBody->msgerror = form_translate('Acess denied');
	return;
	}

if (isset($_POST['action'])) {
	switch($_POST['action']) {
		case 'workspace_edit':
				if (false === record_workspace()) {
 					$idx = 'edit';
				}
			break;
			
		case 'assign_object':
				record_assign();
			break;
			
		case 'import_workspace':
				record_import_workspace();
			break;
	}
}



$babBody->addItemMenu('list', form_translate('List'),$GLOBALS['babAddonUrl'].'workspace&idx=list');

if ('list' === $idx && 0 === form_getWorkspace()) {
	//include_once 'init.php';
	//require_once( $GLOBALS['babInstallPath']."utilit/upgradeincl.php");
	//forms_upgrade(0,0);
	$idx = 'edit';
}

switch($idx) {

	case 'edit':
		$babBody->setTitle(form_translate('Edit workspace'));
		$babBody->addItemMenu('edit', form_translate('Edit'),$GLOBALS['babAddonUrl'].'workspace&idx=edit');
		$babBody->addItemMenu('import', form_translate('Import workspace'),$GLOBALS['babAddonUrl'].'workspace&idx=import');
		edit_workspace();
		break;

	case 'list':
		$babBody->setTitle(form_translate('List workspaces'));
		$babBody->addItemMenu('edit', form_translate('Add workspace'),$GLOBALS['babAddonUrl'].'workspace&idx=edit');
		$babBody->addItemMenu('import', form_translate('Import workspace'),$GLOBALS['babAddonUrl'].'workspace&idx=import');
		list_workspaces();
		break;
		
	case 'assign':
		$babBody->setTitle(form_translate('Attach objects to workspace'));
		$babBody->addItemMenu('assign', form_translate('Attach'),$GLOBALS['babAddonUrl'].'workspace&idx=assign');
		assign_workspace();
		break;
		
	case 'assign2':
		$babBody->setTitle(form_translate('Attach objects to workspace'));
		$babBody->addItemMenu('assign2', form_translate('Attach'),$GLOBALS['babAddonUrl'].'workspace&idx=assign');
		assign_workspace_objects();
		break;
		
	case 'export':
		ws_export();
		break;
		
	case 'setaccess':
		ws_setaccess();
		break;
		
	case 'import':
		$babBody->setTitle(form_translate('Import a workspace'));
		$babBody->addItemMenu('import', form_translate('Import workspace'),$GLOBALS['babAddonUrl'].'workspace&idx=import');
		import_workspace();
		break;	
		
	case 'editor':
		$babBody->setTitle(form_translate('Applications'));
		forms_editor_popup();
		break;
}

$babBody->setCurrentItemMenu($idx);

