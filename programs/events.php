<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

include_once $GLOBALS['babAddonPhpPath']."functions.php";




function forms_onUserDeleteEvt($event)
{
	$db = &$GLOBALS['babDB'];
	
	$id = $event->id_user;

	$res = $db->db_query("SELECT id FROM ".FORM_TABLES." WHERE created='Y'");
	while($arr = $db->db_fetch_array($res))
		{
		$db->db_query("UPDATE ".form_tbl_name($arr['id'])." SET form_lastupdate_user='0' WHERE form_lastupdate_user='".$db->db_escape_string($id)."'");
		$db->db_query("UPDATE ".form_tbl_name($arr['id'])." SET form_create_user='0' WHERE form_create_user='".$db->db_escape_string($id)."'");
		}

	
	
	include_once $GLOBALS['babAddonPhpPath']."tabledirectory.php";
	
	$tables = form_tableDirectory::getDirectoryTables();
	foreach($tables as $id_table) {
		$obj = new form_tableDirectory($id_table);
		$obj->updateRowFromUser($event->id_user);
	}
	
	
	form_modifiyUserRows($id, FORM_USER_DELETE_IF_OPTION);
}


function forms_onGroupDeleteEvt($event)
{
	require_once( $GLOBALS['babInstallPath']."admin/acl.php");
	
	$id = $event->id_group;
	$db = &$GLOBALS['babDB'];

	aclDeleteGroup(FORM_GEST_GROUPS				, $id);
	aclDeleteGroup(FORM_APP_GROUPS				, $id);
	aclDeleteGroup(FORM_FORMS_GROUPS			, $id);
	aclDeleteGroup(FORM_TABLES_CHANGE_GROUPS	, $id);
	aclDeleteGroup(FORM_TABLES_VIEW_GROUPS		, $id);
	aclDeleteGroup(FORM_TABLES_INSERT_GROUPS	, $id);
	aclDeleteGroup(FORM_TABLES_FILING_GROUPS	, $id);
	aclDeleteGroup(FORM_TABLES_LOCK_GROUPS		, $id);

	$db->db_query("UPDATE ".FORM_FORMS." SET id_group_mail='0' WHERE id_group_mail='".$db->db_escape_string($id)."'");
	$db->db_query("UPDATE ".FORM_FORMS." SET id_group_bcc='0' WHERE id_group_bcc='".$db->db_escape_string($id)."'");
}



function forms_onUserAttachToGroup($event) {
	
	include_once $GLOBALS['babAddonPhpPath']."tabledirectory.php";
	
	$tables = form_tableDirectory::getDirectoryTables();
	foreach($tables as $id_table) {
		$obj = new form_tableDirectory($id_table);
		$obj->updateRowFromUser($event->id_user);
	}
}


function forms_onUserDetachFromGroup($event) {
	
	include_once $GLOBALS['babAddonPhpPath']."tabledirectory.php";
	
	$tables = form_tableDirectory::getDirectoryTables();
	foreach($tables as $id_table) {
		$obj = new form_tableDirectory($id_table);
		$obj->updateRowFromUser($event->id_user);
	}
}

/**
 * sitemap
 * 
 */ 
function forms_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event) {

	
	global $babDB;
	bab_functionality::includefile('Icons');
	
	$query = 'SELECT 
			a.id, 
			a.name, 
			a.description, 
			w.id_workspace 
		FROM 
			'.FORM_APP_APPLICATIONS.' a,
			'.FORM_WORKSPACE_ENTRIES.' w  
		WHERE 
			a.id IN('.$babDB->quote(bab_getUserIdObjects(FORM_APP_GROUPS)).') 
			AND w.id_object = a.id 
			AND w.type=\'application\'
	';
	
	bab_debug($query);
	
	$res = $babDB->db_query($query);
	
	$applications = array();
	while ($arr = $babDB->db_fetch_assoc($res)) {
		$applications[$arr['id_workspace']][$arr['id']] = array($arr['name'], $arr['description']);
	}

	if (function_exists('bab_getUserSitemapDelegations'))
	{
		$delegations = bab_getUserSitemapDelegations();
	} else {
		$delegations = bab_getUserVisiblesDelegations();
	}
	
	$user = form_getUserFormAcces();
	$addon = bab_getAddonInfosInstance('forms');
 
	foreach($delegations as $id_delegation => $deleg) {
	 
		$core_prefix = false === $deleg['id'] ? 'bab' : 'babDG'.$deleg['id'];
		$addon_prefix = false === $deleg['id'] ? 'forms' : 'formsDG'.$deleg['id'];
		
		
		if (bab_isUserAdministrator())
		{
			$item = $event->createItem('formsAdmin');
		
			$item->setLabel(form_translate('Forms'));
			$item->setDescription(form_translate('Manage form applications'));
			$item->setLink($addon->getUrl().'admin');
			$item->addIconClassname(Func_Icons::CATEGORIES_APPLICATIONS_EDUCATION);
			$item->setPosition(array('root', $id_delegation, 'babAdmin', 'babAdminSection'));
		
			$event->addFunction($item);
		
		}
		
		
		if ($user['ADMIN'])
		{
			$item = $event->createItem('formsUser');
		
			$item->setLabel(form_translate('Forms'));
			$item->setDescription(form_translate('Manage form applications'));
			$item->setLink($addon->getUrl().'workspace');
			$item->addIconClassname(Func_Icons::CATEGORIES_APPLICATIONS_EDUCATION);
			$item->setPosition(array('root', $id_delegation, 'babUser', 'babUserSection'));
		
			$event->addFunction($item);
		
		}
		
		

		$folder = $event->createItem($addon_prefix.'Applications');
		$folder->setLabel(form_translate('Forms applications'));
		$folder->setPosition(array('root', $id_delegation));
		$folder->addIconClassname('places-user-applications');
		$event->addFolder($folder);

		
		$res = $babDB->db_query('
			SELECT id, name, description  
			FROM '.FORM_WORKSPACES.' 
			
			WHERE id in('.$babDB->quote(array_keys($applications)).')
			ORDER BY name 
		');
			
		while ($workspace = $babDB->db_fetch_assoc($res)) {
			$workspaceNodeId = $addon_prefix.'Workspace'.$workspace['id'];
			$folder = $event->createItem($workspaceNodeId);
			$folder->setLabel($workspace['name']);
			$folder->setDescription($workspace['description']);
			$folder->setPosition(array('root', $id_delegation, $addon_prefix.'Applications'));
			$folder->addIconClassname(Func_Icons::PLACES_FOLDER);
			$event->addFolder($folder);
			
			
			foreach($applications[$workspace['id']] as $id_application => $application) {
				
				$link = $event->createItem($addon_prefix.'Ws'.$workspace['id'].'Application'.$id_application);
				$link->setLabel($application[0]);
				$link->setDescription($application[1]);
				$link->setLink('?tg=addon/forms/form&idx=application&id_app='.$id_application);
				$link->setPosition(array('root', $id_delegation, $addon_prefix.'Applications', $workspaceNodeId));
				$link->addIconClassname(Func_Icons::CATEGORIES_APPLICATIONS_EDUCATION);
				$event->addFunction($link);

				
			}
		}
	}
}








/**
 * functionality list of wysiwyg editor
 * @param bab_eventEditorFunctions $event
 * @return unknown_type
 */
function forms_onEditorFunctions(bab_eventEditorFunctions $event)
{
	$event->addFunction(
		form_translate('Forms'), 
		form_translate('Insert a link to a form application'), 
		$GLOBALS['babAddonUrl']."workspace&idx=editor", 
		'skins/ovidentia/images/addons/forms/launch.gif'
	);
}








function forms_onBeforePeriodsCreated(bab_eventBeforePeriodsCreated $event)
{
	
	$backend = bab_functionality::get('CalendarBackend/Forms');

	$periods = $backend->selectPeriods($event->getCriteria());
	foreach ($periods as $period)
	{
		bab_debug($period->toHtml());
		$event->periods->addPeriod($period);
	}

}


function forms_onCollectCalendarsBeforeDisplay(bab_eventCollectCalendarsBeforeDisplay $event)
{
	global $babDB;
	$backend = bab_functionality::get('CalendarBackend/Forms');
	/*@var $backend Func_CalendarBackend_Forms */
	
	if (!$backend)
	{
		return;
	}
	
	$res = $babDB->db_query("SELECT id, calendar_name FROM form_tables WHERE calendar_name<>''");
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$calendar = $backend->PublicCalendar();
		$calendar->init($arr['id'], $arr['calendar_name'], $backend);
		
		$event->addCalendar($calendar);
	}
}