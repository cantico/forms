<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

include_once $GLOBALS['babInstallPath']."admin/acl.php";
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function options()
{
global $babBody;
    class temp
        {
        function temp()
            {
			$this->t_ovidentia = form_translate('Ovidentia core');
			$this->t_submit = form_translate('Submit');
			$this->t_yes = form_translate('Yes');
			$this->t_no = form_translate('No');
			$this->t_help = form_translate('Help');

			$this->db = &$GLOBALS['babDB'];
			$res = $this->db->db_query("SELECT * FROM ".FORM_CONFIG."");
			while ($arr = $this->db->db_fetch_array($res))
				{
				$varname = 't_'.$arr['name'];
				$this->$varname = form_translate($arr['description']);
				$varname = 'v_'.$arr['name'];
				$this->$varname = $arr['value'];
				}

			$prefix = 'template_';
			$this->templates = array();
			if ($dh = opendir($GLOBALS['babInstallPath'].'skins/ovidentia/templates/'.$GLOBALS['babAddonHtmlPath'])) 
				{
				while (($file = readdir($dh)) !== false) 
					{
					if (substr($file,0,strlen($prefix)) == $prefix)
						$this->templates[] = $file;
					}
				}

			$this->restrans = $this->db->db_query("SELECT id,name FROM ".FORM_TRANS_API."");
            }


		function getnexttemplate()
			{
			if ($this->value = current($this->templates)) 
				{
				$this->selected = $this->v_html_file == $this->value ? 'selected' : '';
				next($this->templates);
				return true;
				}
			else return false;
			}

		function getnexttrans()
			{
			if ($this->trans = $this->db->db_fetch_array($this->restrans))
				{
				$this->selected = $this->trans['id'] == $this->v_transaction_api ? 'selected' : '';
				return true;
				}
			else
				return false;
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."admin.html", "options"));
}

function translist()
{
	class temp
		{
		function temp()
			{
			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_login_varname = form_translate('Login');

			$this->db = &$GLOBALS['babDB'];


			$this->res = $this->db->db_query("SELECT * FROM ".FORM_TRANS_API."");
			}


		function getnext()
			{
			return $this->arr = $this->db->db_fetch_array($this->res);
			}
		}

	$tp = new temp();
	$GLOBALS['babBody']->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."admin.html", "translist"));
}


function transedit()
{
	class temp
		{
		function temp()
			{
			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_login = form_translate('Login');
			$this->t_password = form_translate('Password');
			$this->t_tran_key = form_translate('Transaction key');
			$this->t_url = form_translate('Url');
			$this->t_login_varname = form_translate('Login field name');
			$this->t_password_varname = form_translate('Password field name');
			$this->t_tran_key_varname = form_translate('Transaction key field name');
			$this->t_amount_varname = form_translate('Amount field name');
			$this->t_additional_parameters = form_translate('Additional parameters');
			$this->t_pos_transaction_id = form_translate('Transaction ID');
			$this->t_pos_response_code = form_translate('Response code position');
			$this->t_pos_response_text = form_translate('Response text position');

			$this->t_submit = form_translate('Submit');
			$this->t_delete = form_translate('Delete');

			$this->db = &$GLOBALS['babDB'];

			if (isset($_POST) && count($_POST) > 0)
				{
				$this->arr = $_POST;
				$this->arr['id'] = $_POST['id_trans'];
				}
			elseif (!empty($_GET['id_trans']))
				{
				$this->arr = $this->db->db_fetch_array($this->db->db_query('SELECT * FROM '.FORM_TRANS_API.' WHERE id='.$this->db->quote($_GET['id_trans'])));
				}
			else
				{
				$res = $this->db->db_query("DESCRIBE ".FORM_TRANS_API."");
				while ($arr = $this->db->db_fetch_array($res))
					{
					if (!isset($this->arr[$arr['Field']]))
						$this->arr[$arr['Field']] = '';
					}
				}
			}
		}

	$tp = new temp();
	$GLOBALS['babBody']->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."admin.html", "transedit"));
}


function rights_list()
{
	class temp
		{
		function temp()
			{
			$this->t_name = form_translate('Name');
			$this->t_manager = form_translate('Manager');
			$this->t_section = form_translate('Section');
			$this->t_rights = form_translate('Rights');
			$this->t_update = form_translate('Update');
			$this->t_access = form_translate('Access');
			$this->t_app_grp = form_translate('Application group');
			
			$this->db = &$GLOBALS['babDB'];


			$this->res = $this->db->db_query("SELECT * FROM ".FORM_GEST_RIGHTS."");
			}


		function getnext()
			{
			return $this->arr = $this->db->db_fetch_array($this->res);
			}
		}

	$tp = new temp();
	$GLOBALS['babBody']->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."admin.html", "rightslist"));

}


function rights_edit($id = false)
{
	class temp
		{
		function temp($id)
			{
			$this->t_name = form_translate('Name');
			$this->t_submit = form_translate('Submit');
			$this->t_delete = form_translate('Delete');

			$this->db = &$GLOBALS['babDB'];

			if (isset($_POST) && count($_POST) > 0)
				{
				$this->arr = $_POST;
				}
			elseif (!empty($id))
				{
				$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT id,name FROM ".FORM_GEST_RIGHTS." WHERE id=".$this->db->quote($id)));
				}
			else
				{
				$this->arr['id'] = '';
				$this->arr['name'] = '';
				}
			}
		}

	$tp = new temp($id);
	$GLOBALS['babBody']->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."admin.html", "rightsedit"));
}

// record

function record_options()
{
$db = &$GLOBALS['babDB'];
$res = $db->db_query("SELECT id,name FROM ".FORM_CONFIG."");
while($arr = $db->db_fetch_array($res))
	{
	if (isset($_POST[$arr['name']]))
		{
		$db->db_query("UPDATE ".FORM_CONFIG." SET 
		value='".$db->db_escape_string($_POST[$arr['name']])."' 
		WHERE id='".$db->db_escape_string($arr['id'])."'");
		}
	}
}

function record_trans()
	{
	if (empty($_POST['name']))
		{
		$GLOBALS['babBody']->msgerror = form_translate('Name is required');
		return false;
		}


	$db = &$GLOBALS['babDB'];

	$form_fields = array();
	$ignore = array('id');
	$default = array();
	$update = array();
	$insert1 = array();
	$insert2 = array();

	$res = $db->db_query("DESCRIBE ".FORM_TRANS_API."");
	while ($arr = $db->db_fetch_array($res))
		{
		if (!in_array($arr['Field'],$ignore))
			{
			if ("enum('y','n')" == strtolower($arr['Type']))
				{
				$form_fields[$arr['Field']] = isset($_POST[$arr['Field']]) ? 'Y' : 'N';
				}
			else
				{
				$form_fields[$arr['Field']] = isset($_POST[$arr['Field']]) ? $_POST[$arr['Field']] : (isset($default[$arr['Field']]) ? $default[$arr['Field']] : '');
				}

			$update[] = $db->db_escape_string($arr['Field']).'=\''.$db->db_escape_string($form_fields[$arr['Field']]).'\'';
			$insert1[] = $db->db_escape_string($arr['Field']);
			$insert2[] = '\''.$db->db_escape_string($form_fields[$arr['Field']]).'\'';
			}
		}


	if (isset($_POST['id_trans']) && is_numeric($_POST['id_trans']))
		{
		$db->db_query("UPDATE ".FORM_TRANS_API." SET ".implode(',',$update)." WHERE id='".$db->db_escape_string($_POST['id_trans'])."'" );
		}
	else
		{
		$db->db_query("INSERT INTO ".FORM_TRANS_API." (".implode(',',$insert1).") VALUES (".implode(',',$insert2).")");
		}
	return true;
	}

function transdelete()
	{
	$db = &$GLOBALS['babDB'];
	$db->db_query("DELETE FROM ".FORM_TRANS_API." WHERE id='".$db->db_escape_string($_POST['id_trans'])."'");
	}


function rights_edit_record()
	{
	$db = &$GLOBALS['babDB'];

	if (empty($_POST['name']))
		{
		$GLOBALS['babBody']->msgerror = form_translate("Name must be set");
		return false;
		}

	if (empty($_POST['id_right']))
		{
		$db->db_query("INSERT INTO ".FORM_GEST_RIGHTS." (name, manager, section, app_grp) VALUES ('".$db->db_escape_string($_POST['name'])."', 0, 1, 1)");
		}
	else
		{
		switch ($_POST['id_right'])
			{
			case 1:
				$manager = 1;
				$section = 0;
				$app_grp = 0;
				break;

			case 2:
				$manager = 0;
				$section = 1;
				$app_grp = 1;
				break;

			default:
				$manager = 0;
				$section = 0;
				$app_grp = 1;
				break;
			}

		$db->db_query("UPDATE ".FORM_GEST_RIGHTS." 
		
		SET 
			name='".$db->db_escape_string($_POST['name'])."', 
			manager='".$db->db_escape_string($manager)."', 
			section='".$db->db_escape_string($section)."', 
			app_grp='".$db->db_escape_string($app_grp)."' 
		WHERE 
			id='".$db->db_escape_string($_POST['id_right'])."'" );
		}

	return true;
	}

function rights_delete($id)
	{
	$db = &$GLOBALS['babDB'];

	$db->db_query("DELETE FROM ".FORM_GEST_RIGHTS." WHERE id='".$db->db_escape_string($id)."'");
	$db->db_query("DELETE FROM ".FORM_GEST_GROUPS." WHERE id_object='".$db->db_escape_string($id)."'");
	$db->db_query("DELETE FROM ".FORM_GEST_RIGHTS_APP." WHERE id_right='".$db->db_escape_string($id)."'");
	}

// main

if (!bab_isUserAdministrator())
{
	$babBody->msgerror = form_translate("Access denied");
	return;
}

$babBody->addItemMenu("rights_list", form_translate("Rights list"),$GLOBALS['babAddonUrl']."admin&idx=rights_list");
$babBody->addItemMenu("options", form_translate("Options"),$GLOBALS['babAddonUrl']."admin&idx=options");
if (FORM_CURL)
{
	$babBody->addItemMenu("translist", form_translate("Transaction services"),$GLOBALS['babAddonUrl']."admin&idx=translist");
	//$babBody->addItemMenu("transedit", form_translate("New service"),$GLOBALS['babAddonUrl']."admin&idx=transedit");
}

$idx = isset($_REQUEST['idx']) ? $_REQUEST['idx'] : 'rights_list' ;

if (isset($_POST['action']))
	{

	switch ($_POST['action'])
		{
		case 'options':
			record_options();
			break;

		case 'transedit':
			if (!record_trans())
				$idx = 'transedit';
			break;

		case 'transdelete':
			transdelete();
			break;

		case 'rights_edit':
			if (!rights_edit_record())
				$idx = 'rights_edit';
			break;

		case 'rights_delete':
			rights_delete($_POST['id_right']);
			break;
		}
	}

if (isset($_POST['acl2']))
	maclGroups();

switch ($idx)
	{
	case 'rights_list':
		$babBody->addItemMenu("rights_edit", form_translate("Add right"),$GLOBALS['babAddonUrl']."admin&idx=rights_edit");
		isset($_POST['action']) ? $babBody->title = form_translate("Options has been updated") : $babBody->title = form_translate("Access rights list");
		rights_list();
		break;

	case 'rights_edit':
		$babBody->addItemMenu("rights_edit", form_translate("Edit right"),$GLOBALS['babAddonUrl']."admin&idx=rights_edit");
		$babBody->title = form_translate("Edit");
		if (isset($_REQUEST['id_right']))
			rights_edit($_REQUEST['id_right']);
		else
			rights_edit();
		break;

	case 'rights_acl':
		$babBody->addItemMenu("rights_acl", form_translate("Change right"),$GLOBALS['babAddonUrl']."admin&idx=rights_acl");
		$babBody->title = form_translate("Access rights");
		aclGroups($GLOBALS['babAddonTarget']."/admin","rights_list",FORM_GEST_GROUPS,$_REQUEST['id_right'],"acl2");
		break;

	case 'options':
		$babBody->title = form_translate("Options");
		options();
		break;

	case 'translist':
		$babBody->title = form_translate("Transaction services");
		translist();
		break;

	case 'transedit':
		$babBody->title = form_translate("Transaction service");
		$babBody->addItemMenu("transedit", form_translate("Edit service"),$GLOBALS['babAddonUrl']."admin&idx=transedit");
		transedit();
		break;
	}
$babBody->setCurrentItemMenu($idx);
?>