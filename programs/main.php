<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");
require_once( $GLOBALS['babInstallPath']."addons/forms/main_functions.php");

$FORM_USER = form_getUserFormAcces();

// main
if (!$FORM_USER['ADMIN'])
	{
	$babBody->msgerror = form_translate('Acess denied');
	return;
	}

$idx = isset($_POST['idx']) ? $_POST['idx'] : (isset($_GET['idx']) ? $_GET['idx'] : 'list_applications');

if (isset($_POST['action']))
	{
	require_once( $GLOBALS['babInstallPath']."addons/forms/main_record.php");

	switch ($_POST['action'])
		{
		case 'edit_table':
			$id_table = record_table();
			if ($id_table == false) $idx = 'edit_table';
			break;

		case 'delete_table':
			delete_table();
			break;

		case 'delete_type':
			delete_type();
			break;

		case 'edit_type':
			if (!record_type())
				{
				if (empty($_POST['id_type']))
					{
					$idx = 'add_type';
					}
				else
					{
					$id_type = $_POST['id_type'];
					$idx = 'edit_type';
					}
				}
			break;


		case 'edit_extended_type':
			if (!record_extended_type()) {
				if (empty($_POST['id_type']))
					{
					$idx = 'add_type';
					}
				else
					{
					$id_type = $_POST['id_type'];
					$idx = 'edit_type';
					}
				}
			break;

		case 'init_fields':
		case 'edit_form':
			if (!record_form())
				{
				if (empty($_POST['id_form']))
					$idx = 'add_form';
				else
					{
					$id_form = $_POST['id_form'];
					$idx = 'edit_form';
					}
				}
			break;

		case 'edit_form_mail':
			if (!record_form_mail())
				{
				$idx = 'edit_form_mail';
				}
			break;

		case 'edit_form_search':
			if (!record_form_search())
				{
				$idx = 'edit_form_search';
				}
			break;

		case 'edit_form_elements':
			if (!record_form_elements())
				{
				$idx = 'edit_form_elements';
				}
			break;

		case 'edit_field_validations':
			if (!record_field_validations())
				{
				$idx = 'edit_field_validations';
				}
			break;

		case 'edit_form_styles':
			if (!record_form_styles())
				{
				$idx = 'edit_form_styles';
				}
			break;

		case 'edit_form_init':
			if (!record_form_init())
				{
				$idx = 'edit_form_init';
				}
			break;

		case 'edit_form_transform':
			if (!record_form_transform())
				{
				$idx = 'edit_form_transform';
				}
			break;

		case 'edit_form_validation':
			if (!record_form_validation())
				{
				$idx = 'edit_form_validation';
				}
			break;


		case 'edit_form_approbation_list':
			if (!record_form_approbation_list())
				{
				$idx = 'edit_form_approbation_list';
				}
			break;

		case 'edit_form_curl':
			if (!record_form_curl())
				{
				$idx = 'edit_form_curl';
				}
			break;

		case 'edit_form_ordgrp':
			if (!record_form_ordgrp()) {
				$idx = 'edit_form_ordgrp';
				}
			break;

		case 'edit_form_increment':
			if (!record_form_increment()) {
				$idx = 'edit_form_increment';
				}
			break;

		case 'order_form':
			record_order_form();
			break;

		case 'delete_form':
			delete_form();
			break;

		case 'edit_application':
			if (!record_application())
				{
				if (empty($_POST['id_app']))
					$idx = 'add_application';
				else
					{
					$id_app = $_POST['id_app'];
					$idx = 'edit_application';
					}
				}
			break;

		case 'edit_step':
			$id_step = bab_rp('id_step');
			if (!record_step($id_step))
				{
				$idx = 'edit_step';
				}
			break;

		case 'delete_step':
			delete_step($_POST['id_step']);
			break;

		case 'delete_application':
			delete_application();
			break;

		case 'link_edit':
			record_link();
			break;

		case 'link_delete':
			link_delete();
			break;

		case 'list_steps':
			record_first_step();
			break;

		case 'import_application':
			import_application();
			break;

		case 'record_tabledesc':
			record_tabledesc($_POST['id_table']);
			break;

		case 'copy_form':
			if (!record_copy_form())
				$idx = 'copy_form';
			break;
		}

	}
	
	
$babBody->addItemMenu("workspace", form_translate("Workspaces"),$GLOBALS['babAddonUrl']."workspace");





if ( $idx == 'list_steps' && bab_rp('id_app') == '' ) 
	$idx = 'list_applications';


switch ($idx)
	{
	case 'edit_table':
		$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
		$babBody->addItemMenu("edit_table", form_translate("Edit table"),$GLOBALS['babAddonUrl']."main&idx=edit_table");
		$babBody->title = form_translate("Edit table").' "'.form_getTableName(bab_rp('id_table')).'"';
		edit_table(bab_rp('id_table'));
		break;
	
	case 'list_tables':
		$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
		$babBody->addItemMenu("add_table", form_translate("Add table"),$GLOBALS['babAddonUrl']."main&idx=add_table");
		$babBody->title = form_translate("List tables");
		list_tables();
		break;

	case 'add_table':
		$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
		$babBody->addItemMenu("add_table", form_translate("Add table"),$GLOBALS['babAddonUrl']."main&idx=add_table");
		$babBody->title = form_translate("Add table");
		edit_table();
		break;
		
	case 'list_types':
		$babBody->addItemMenu("list_types", form_translate("List types"),$GLOBALS['babAddonUrl']."main&idx=list_types");
		$babBody->addItemMenu("add_type", form_translate("Add type"),$GLOBALS['babAddonUrl']."main&idx=extend_type");
		$babBody->title = form_translate("List types");
		list_types();
		break;

	case 'extend_type':
		$babBody->addItemMenu("list_types", form_translate("List types"),$GLOBALS['babAddonUrl']."main&idx=list_types");
		$babBody->addItemMenu("extend_type", form_translate("Add type"),$GLOBALS['babAddonUrl']."main&idx=add_type");
		$babBody->title = form_translate("Add type");
		extend_type();
		break;
		
	case 'add_type':
		$babBody->addItemMenu("list_types", form_translate("List types"),$GLOBALS['babAddonUrl']."main&idx=list_types");
		$babBody->addItemMenu("add_type", form_translate("Add type"),$GLOBALS['babAddonUrl']."main&idx=add_type");
		$babBody->title = form_translate("Add type");
		if (isset($_POST['extend']) && 1 != $_POST['extend']) {
			edit_extended_type($_POST['extend']);
			}
		else {
			edit_type_sql();
			}
		break;

	case 'edit_type':
		$babBody->addItemMenu("list_types", form_translate("List types"),$GLOBALS['babAddonUrl']."main&idx=list_types");
		$babBody->addItemMenu("edit_type", form_translate("Edit type"),$GLOBALS['babAddonUrl']."main&idx=edit_type");
		$babBody->title = form_translate("Edit type");
		edit_type($_REQUEST['id_type']);

		break;
		
	case 'list_forms':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("add_form", form_translate("Add form"),$GLOBALS['babAddonUrl']."main&idx=add_form");
		$babBody->title = form_translate("List forms");
		list_forms();
		break;
		
	case 'add_form':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("add_form", form_translate("Add form"),$GLOBALS['babAddonUrl']."main&idx=add_form");
		$babBody->title = form_translate("Add form");
		edit_form();
		break;

	case 'edit_form':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form", form_translate("Edit form"),$GLOBALS['babAddonUrl']."main&idx=edit_form");
		$babBody->title = form_translate("Edit form");
		if (isset($_GET['id_form'])) $id_form = $_GET['id_form'];
		edit_form($id_form);
		break;

	case 'edit_form_mail':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_mail", form_translate("Mail tools"),$GLOBALS['babAddonUrl']."main&idx=edit_form_mail");
		$babBody->title = form_translate("Email with form");
		edit_form_mail($_REQUEST['id_form']);
		break;

	case 'edit_form_search':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_search", form_translate("Search tools"),$GLOBALS['babAddonUrl']."main&idx=edit_form_search");
		$babBody->title = form_translate("Search on list form");
		edit_form_search($_REQUEST['id_form']);
		break;

	case 'edit_form_elements':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_elements", form_translate("Form elements"),$GLOBALS['babAddonUrl']."main&idx=edit_form_elements");
		$babBody->title = form_translate("Form elements");
		edit_form_elements($_REQUEST['id_form']);
		break;

	case 'edit_field_validations':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_elements", form_translate("Form elements"),$GLOBALS['babAddonUrl']."main&idx=edit_form_elements&id_form=".$_REQUEST['id_form']);
		$babBody->addItemMenu("edit_field_validations", form_translate("Field validations"),$GLOBALS['babAddonUrl']."main&idx=edit_field_validations");
		$babBody->title = form_translate("Field validations");
		edit_field_validations($_REQUEST['id_form'], $_REQUEST['id_field']);
		break;

	case 'edit_form_styles':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_styles", form_translate("Form styles"),$GLOBALS['babAddonUrl']."main&idx=edit_form_styles");
		$babBody->title = form_translate("Form styles");
		edit_form_styles($_REQUEST['id_form']);
		break;

	case 'edit_form_init':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_init", form_translate("Initialisation"),$GLOBALS['babAddonUrl']."main&idx=edit_form_init");
		$babBody->title = form_translate("Form initialisation");
		edit_form_init($_REQUEST['id_form']);
		break;

	case 'edit_form_transform':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_transform", form_translate("Transform"),$GLOBALS['babAddonUrl']."main&idx=edit_form_init");
		$babBody->title = form_translate("Fields transformation");
		edit_form_transform($_REQUEST['id_form']);
		break;


	case 'edit_form_validation':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_validation", form_translate("Validation"),$GLOBALS['babAddonUrl']."main&idx=edit_form_validation");
		$babBody->title = form_translate("Validation list");
		edit_form_validation($_REQUEST['id_form']);
		break;


	case 'edit_form_approbation_list':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_approbation_list", form_translate("Approbation"),$GLOBALS['babAddonUrl']."main&idx=edit_form_approbation_list");
		$babBody->title = form_translate("Transaction tools");
		edit_form_approbation_list($_REQUEST['id_form']);
		break;

	case 'edit_form_curl':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_curl", form_translate("Transaction"),$GLOBALS['babAddonUrl']."main&idx=edit_form_curl");
		$babBody->title = form_translate("Transaction tools");
		edit_form_curl($_REQUEST['id_form']);
		break;

	case 'edit_form_ordgrp':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_ordgrp", form_translate("Sort"),$GLOBALS['babAddonUrl']."main&idx=edit_form_ordgrp");
		$babBody->title = form_translate("Group by and order by");
		edit_form_ordgrp($_REQUEST['id_form']);
		break;

	case 'edit_form_increment':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("edit_form_increment", form_translate("Increment"),$GLOBALS['babAddonUrl']."main&idx=edit_form_increment");
		$babBody->title = form_translate("Increment values on view form");
		edit_form_increment($_REQUEST['id_form']);
		break;

	case 'options_field':
		options_field($_GET['index'],$_GET['id_type'],$_GET['id_field']);
		break;

	case 'order_form':
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("order_form", form_translate("Order form fields"),$GLOBALS['babAddonUrl']."main&idx=order_form");
		$babBody->title = form_translate("Order form fields");
		if (isset($_GET['id_form'])) $id_form = $_GET['id_form'];
		order_form($id_form);
		break;

	case 'add_step':
		$id_app = bab_rp('id_app');
		$babBody->title = form_translate("Add step");
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("list_steps", form_translate("List steps"),$GLOBALS['babAddonUrl']."main&idx=list_steps&id_app=".$id_app);
		$babBody->addItemMenu("add_step", form_translate("Add step"),$GLOBALS['babAddonUrl']."main&idx=add_step&id_app=".$id_app);
		add_step($id_app);
		break;

	case 'edit_step':
		$id_app = bab_rp('id_app');
		
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("list_steps", form_translate("List steps"),$GLOBALS['babAddonUrl']."main&idx=list_steps&id_app=".$id_app);
		$babBody->addItemMenu("edit_step", form_translate("Edit step"),$GLOBALS['babAddonUrl']."main&idx=edit_step&id_app=".$id_app);

		edit_step($id_app,bab_rp('id_step', 0));
		break;

	case 'list_steps':
		$id_app = &$_REQUEST['id_app'];
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("list_steps", form_translate("List steps"),$GLOBALS['babAddonUrl']."main&idx=list_steps&id_app=".$id_app);
		$babBody->addItemMenu("add_step", form_translate("Add step"),$GLOBALS['babAddonUrl']."main&idx=add_step&id_app=".$id_app);
		list_steps($id_app);
		break;

	case 'link_list':
		$id_app = &$_REQUEST['id_app'];
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("link_list", form_translate("Link list"),$GLOBALS['babAddonUrl']."main&idx=link_list&id_app=".$id_app);
		$babBody->addItemMenu("link_edit", form_translate("Link add"),$GLOBALS['babAddonUrl']."main&idx=link_edit&id_app=".$id_app);
		$babBody->title = form_translate("Link list");
		link_list($id_app);
		break;

	case 'link_edit':
		$id_app = &$_REQUEST['id_app'];
		$id_link= isset($_REQUEST['id_link']) ? $_REQUEST['id_link'] : '';
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("link_list", form_translate("Link list"),$GLOBALS['babAddonUrl']."main&idx=link_list&id_app=".$id_app);
		$babBody->addItemMenu("link_edit", form_translate("Link edit"),$GLOBALS['babAddonUrl']."main&idx=link_edit&id_app=".$id_app);
		$babBody->title = form_translate("Link edit");
		link_edit($id_app, $id_link);
		break;

	case 'add_application':
		$babBody->title = form_translate("Add application");
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("add_application", form_translate("Add application"),$GLOBALS['babAddonUrl']."main&idx=add_application");
		edit_application();
		break;

	case 'edit_application':
		$babBody->title = form_translate("Edit application");
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("edit_application", form_translate("Edit application"),$GLOBALS['babAddonUrl']."main&idx=edit_application");
		if (isset($_GET['id_app'])) $id_app = $_GET['id_app'];
			edit_application($id_app);
		break;

	case 'formula':
		$babBody->title = form_translate("Formula editor");
		forms_formula();
		break;

	case 'card_fields':
		$babBody->title = form_translate("Shopping cart table fields");
		if (!empty($_GET['id_table']) && !empty($_GET['id_form_card']))
			card_fields($_GET['id_table'], $_GET['id_form_card']);

		break;

	case 'export':
		require_once( $GLOBALS['babInstallPath']."addons/forms/impexp.php");
		$form_impexp = new form_impexp($_GET['id_app']);
		$form_impexp->export();
		break;

	case 'import':
		$babBody->title = form_translate("Import application");
		$babBody->addItemMenu("import", form_translate("Import"),$GLOBALS['babAddonUrl']."main&idx=import");
		forms_import();
		break;

	case 'copy_form':
		$babBody->title = form_translate("Copy form");
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("copy_form", form_translate("Copy form"),$GLOBALS['babAddonUrl']."main&idx=copy_form");
		copy_form();
		break;

	default:
	case 'list_applications':
		$babBody->title = form_translate("List applications");
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("add_application", form_translate("Add application"),$GLOBALS['babAddonUrl']."main&idx=add_application");
		list_applications();
		break;
	}
$babBody->setCurrentItemMenu($idx);
bab_sitemap::setPosition('formsUser');