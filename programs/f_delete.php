<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

function form_f_deleteTableRows($id_table, $arr_id) {

	if (0 == count($arr_id)) {
		return;
	}

	$db = $GLOBALS['babDB'];

	$db->db_query("DELETE FROM ".form_tbl_name($id_table)." WHERE form_id IN(".$db->quote($arr_id).")");

	$res = $db->db_query("SELECT id FROM ".FORM_TABLES_FIELDS." WHERE id_table=".$db->quote($id_table));
	$id_table_field = array();
	while (list($fieldid) = $db->db_fetch_array($res))
		{
		$id_table_field[] = $fieldid;
		}
	if (count($id_table_field) > 0)
		{
		$db->db_query("DELETE FROM ".FORM_EXTERNAL_BLOB_DATA." WHERE id_table_field IN(".$db->quote($id_table_field).") AND form_row IN(".$db->quote($arr_id).")");
		}

	
	foreach($arr_id as $id) {
		$directory = $GLOBALS['babAddonUpload'].'directories/'.form_tbl_name($id_table).'/'.$id.'/';
		if (is_dir($directory)) {
			form_deldir($directory);
		}
	}

	$res = $db->db_query("SELECT id_line, id_schi FROM ".FORM_APPROB_SCHI." WHERE id_table=".$db->quote($id_table)." AND id_line IN(".$db->quote($arr_id).")");

	if (0 < $db->db_num_rows($res)) {
		require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
	}

	while ($arr = $db->db_fetch_array($res)) {
		bab_WFDeleteInstance($arr['id_schi']);
		$db->db_query("DELETE FROM ".FORM_APPROB_SCHI." WHERE id_schi=".$db->quote($arr['id_schi']));
	}
}


?>