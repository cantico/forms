<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';





class form_stepTypeTemplateTemp_9 extends form_stepTypeTemplate
	{ 
	var $altbg = false;
	function form_stepTypeTemplateTemp_9($id_app, $id_step)
		{
		$this->form_stepTypeTemplate($id_app, $id_step);
		
		$this->db = & $GLOBALS['babDB'];

		
		$this->resstep = $this->db->db_query("
			SELECT 
				id,
				name 
			FROM 
				".FORM_APP_STEPS." 
			WHERE 
				id_application='".$this->db->db_escape_string($id_app)."' 
			ORDER BY name
		");
	
			
		
		if (isset($_POST['name'])) {
		
			$this->name 			= bab_toHtml($_POST['name']);
			$this->description  	= bab_toHtml($_POST['description']);
			$this->arr['id_import_user_next_step'] = $_POST['id_import_user_next_step'];
			
		} elseif (!empty($id_step)) {
		
			$res = $this->db->db_query("
				SELECT 
					s.name,
					s.description, 
					s.id_import_user_next_step 
				FROM 
					".FORM_APP_STEPS." s 
				WHERE 
					s.id=".$this->db->quote($id_step));
					
			$arr = $this->db->db_fetch_assoc($res);
	
			$this->name 		= bab_toHtml($arr['name']);
			$this->description  = bab_toHtml($arr['description']);
			$this->arr['id_import_user_next_step'] = (int) $arr['id_import_user_next_step'];
	
		} else {
		
			$this->name 		= '';
			$this->description 	= '';
			$this->arr['id_import_user_next_step']  = 0;
		}
		
		
	}
	
	
	function getnextstep()
		{
		static $i = 0;
		$this->step = $this->db->db_fetch_array($this->resstep);
		if (!$this->step)
			{
			if (isset($i)) $i++;
			if ($this->db->db_num_rows($this->resstep) > 0)
				{
				$this->db->db_data_seek($this->resstep,0);
				}
			return false;
			}
		else
			{

			$this->selected = $this->arr['id_import_user_next_step'] == $this->step['id'];
				
			$this->step['name'] = bab_toHtml($this->step['name']);
			return true;
			}
		}
}




/**
 * Type Importer un utilisateur dans la table
 */
class form_stepType_9 extends form_stepType {


	function getEditHtml($id_app, $id_step) {
		
		$tp = new form_stepTypeTemplateTemp_9($id_app, $id_step);
		return bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."steps/9.html", "edit" );
	}
	
	
	
	function record() {
	
		$db = &$GLOBALS['babDB'];
		
		
		
		
		if (isset($_POST['id_step']) && !empty($_POST['id_step']))
			{
			$id_step = $_POST['id_step'];

			$db->db_query("UPDATE ".FORM_APP_STEPS." SET 
			 
			 	name=".$db->quote($_POST['name']).",
			 	description=".$db->quote($_POST['description']).", 
			 	id_import_user_next_step=".$db->quote($_POST['id_import_user_next_step'])." 
			 WHERE 
			 	id='".$db->db_escape_string($id_step)."' 
			 ");
	
			
			}
		else
			{

			$res = $db->db_query("INSERT INTO ".FORM_APP_STEPS." 
				(
					id_application,
					id_type,
					name,
					description, 
					id_import_user_next_step 
				) 
			VALUES 
				(
					".$db->quote($_POST['id_app']).",
					'9',
					".$db->quote($_POST['name']).",
					".$db->quote($_POST['description']).", 
					".$db->quote($_POST['id_import_user_next_step'])." 
				)
			");
			$id_step = $db->db_insert_id($res);
			}
		
		
		
		
		
			
		return true;
	}



	function fire($stepFlow) {
	    
	    $row = $stepFlow->getStep();
		
		if (!isset($_POST['id_table']) || !isset($_POST['form_row'])) {
			$stepFlow->addError(form_translate("This step must be summoned by a submit button"));
			$stepFlow->gotoStep($row['id_import_user_next_step']);
			return false;
			}
		
			
		$form_row = form_currentDbRow();
		if (NULL === $form_row) {
			$form_row = (int) bab_pp('form_row');
		}
		
		include_once $GLOBALS['babAddonPhpPath']."tabledirectory.php";
		$obj = new form_tableDirectory($_POST['id_table']);
		$obj->updateRowFromUser(
			form_tableDirectory::getIdUser($_POST['id_table'], $form_row),
			true
		);

		$stepFlow->gotoStep($row['id_import_user_next_step']);

		return true;
	}
}

