<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';





class form_stepTypeTemplateTemp_6 extends form_stepTypeTemplate
	{ 
	var $altbg = false;
	function form_stepTypeTemplateTemp_6($id_app, $id_step)
		{
		$this->form_stepTypeTemplate($id_app, $id_step);
		
		$this->db = & $GLOBALS['babDB'];

		
		$this->resmenu = $this->db->db_query("SELECT * FROM ".FORM_APP_STEP_MENU." WHERE id_step='".$this->db->db_escape_string($id_step)."'");
		
		
		$this->resmenustep = $this->db->db_query("SELECT id,name FROM ".FORM_APP_STEPS." WHERE id_application='".$this->db->db_escape_string($id_app)."' AND id_type='1' ORDER BY name");
	
			
		
		if (isset($_POST['name'])) {
		
			$this->name 			= bab_toHtml($_POST['name']);
			$this->description  	= bab_toHtml($_POST['description']);
			$this->arr['nb_menu'] 	= $_POST['nb_menu'];
			
		} elseif (!empty($id_step)) {
		
			$res = $this->db->db_query("
				SELECT 
					s.name,
					s.description 
				FROM 
					".FORM_APP_STEPS." s 
				WHERE 
					s.id=".$this->db->quote($this->id_step));
			$arr = $this->db->db_fetch_assoc($res);
	
			$this->name 		= bab_toHtml($arr['name']);
			$this->description  = bab_toHtml($arr['description']);
			$this->arr['nb_menu'] = $this->db->db_num_rows($this->resmenu);
	
		} else {
		
			$this->name 		= '';
			$this->description 	= '';
			$this->arr['nb_menu'] = 0;
		}
		
		
	}
	
	
	
	
	function getnextmenustep()
		{
		if ($this->menustep = $this->db->db_fetch_array($this->resmenustep))
			{
			$this->selected = isset($this->menu['id_menustep']) && $this->menu['id_menustep'] == $this->menustep['id'];
			$this->menustep['name'] = bab_toHtml($this->menustep['name']);
			return true;
			}
		else
			{
			if ($this->db->db_num_rows($this->resmenustep) > 0)
				$this->db->db_data_seek($this->resmenustep,0);
			return false;
			}
		}
	
	
	
	function getnextmenu()
		{
		static $i=0,$j = 0, $deleted=0;
		
		if(isset($this->arr['nb_menu']) && $i < ($this->arr['nb_menu'] + $deleted))
			{
			$this->altbg = !$this->altbg;

			if (isset($_POST['menu_del_fields']) && count($_POST['menu_del_fields']) >0 && in_array($i,$_POST['menu_del_fields']))
				{
				$skip = true;
				$i++;
				$deleted++;
				return true;
				}
			$el_to_init = array('id', 'name','id_menustep','selmenu');
			$index = $i;
			
			$default = array();

			if (isset($this->resmenu))
				{
				$this->menu = $this->db->db_fetch_array($this->resmenu);
				
				$arr = & $this->menu;
				foreach ($el_to_init as $el)
					$this->menu[$el] = isset($arr[$el]) ? bab_toHtml($arr[$el]) : (isset($default[$el]) ? $default[$el] : '');
				}
			else
				{
				$this->menu = $_POST;
				$arr = & $_POST;
				foreach ($el_to_init as $el)
					$this->menu[$el] = isset($arr['menu_'.$el.'_'.$index]) ? bab_toHtml($arr['menu_'.$el.'_'.$index]) : (isset($default[$el]) ? $default[$el] : '');
				}


			$this->index = $j;
			$i++;
			$j++;
			return true;
			}
		else
			{
			$deleted = 0;
			$i = 0;
			$j =0;
			return false;
			}
		}
}




/**
 * Type menu contextuel
 */
class form_stepType_6 extends form_stepType {


	function getEditHtml($id_app, $id_step) {
		
		$tp = new form_stepTypeTemplateTemp_6($id_app, $id_step);
		return bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."steps/6.html", "edit" );
	}
	
	
	
	function record() {
	
		$db = &$GLOBALS['babDB'];
		
		
		if (isset($_POST['id_step']) && !empty($_POST['id_step']))
			{
			$id_step = $_POST['id_step'];

			$db->db_query("UPDATE ".FORM_APP_STEPS." SET 
			 
			 	name=".$db->quote($_POST['name']).",
			 	description=".$db->quote($_POST['description'])." 
			 
			 WHERE 
			 	id='".$db->db_escape_string($id_step)."' 
			 ");
	
			
			}
		else
			{

			$res = $db->db_query("INSERT INTO ".FORM_APP_STEPS." 
				(
					id_application,
					id_type,
					name,
					description 
				) 
			VALUES 
				(
					".$db->quote($_POST['id_app']).",
					'6',
					".$db->quote($_POST['name']).",
					".$db->quote($_POST['description'])."
				)
			");
			$id_step = $db->db_insert_id($res);
			$_POST['id_step'] = $id_step;
			}
		
		
		
		
		
		
	
		$worked_id = array();

		$prefix = 'menu_id_menu_';
		foreach ($_POST as $field => $value)
			{
			if (substr($field,0,strlen($prefix)) == $prefix)
				{
				$tmp = explode('_',$field);
				$idx = $tmp[3];
	
				if (empty($_POST['menu_name_'.$idx]))
					{
					list($name) = $db->db_fetch_array($db->db_query("SELECT name FROM ".FORM_APP_STEPS." WHERE id='".$db->db_escape_string($_POST['menu_id_menustep_'.$idx])."'"));
					}
				else
					{
					$name = $_POST['menu_name_'.$idx];
					}
	
				
	
				$id_menustep = $_POST['menu_id_menustep_'.$idx];
				$selmenu = isset($_POST['menu_selmenu']) && $_POST['menu_selmenu'] == $idx ? 1 : 0;
	
				if ( !empty($value) && is_numeric($value))
					{
					
					$db->db_query("UPDATE ".FORM_APP_STEP_MENU." 
									SET 
										name='".$db->db_escape_string($name)."', 
										id_menustep='".$db->db_escape_string($id_menustep)."',
										selmenu='".$db->db_escape_string($selmenu)."'
									WHERE id='".$db->db_escape_string($value)."'
					");
	
					$worked_id[] = $value;
					}
				else
					{
	
					$res = $db->db_query("
					INSERT INTO ".FORM_APP_STEP_MENU." 
						(id_step,name,id_menustep,selmenu) 
					VALUES 
						('".$db->db_escape_string($id_step)."',
						'".$db->db_escape_string($name)."',
						'".$db->db_escape_string($id_menustep)."',
						'".$db->db_escape_string($selmenu)."')
						");
	
					$worked_id[] = $db->db_insert_id($res);
					}
				}
			}
	
		
			
		if (count($worked_id) > 0 )
			{
			list($selected_menu) = $db->db_fetch_array($db->db_query("SELECT id FROM ".FORM_APP_STEP_MENU." WHERE id IN(".$db->quote($worked_id).") AND selmenu='1'"));
	
			if (empty($selected_menu))
				{
				$db->db_query("UPDATE ".FORM_APP_STEP_MENU." SET selmenu='1' WHERE id='".$db->db_escape_string($worked_id[0])."'");
				$selected_menu = $worked_id[0];
				}
	
			if ($_POST['id_type'] == 6)
				{
				list($menu_id_form) = $db->db_fetch_array($db->db_query("SELECT s.id_form FROM ".FORM_APP_STEP_MENU." m,".FORM_APP_STEPS." s WHERE m.id='".$db->db_escape_string($selected_menu)."' AND m.id_menustep=s.id"));
				$db->db_query("UPDATE ".FORM_APP_STEPS." SET id_form='".$db->db_escape_string($menu_id_form)."' WHERE id='".$db->db_escape_string($_POST['id_step'])."'");
				}
	
			$db->db_query("DELETE FROM ".FORM_APP_STEP_MENU." WHERE id_step='".$db->db_escape_string($id_step)."' AND id NOT IN(".$db->quote($worked_id).")");
			}
		else
			$db->db_query("DELETE FROM ".FORM_APP_STEP_MENU." WHERE id_step='".$db->db_escape_string($id_step)."'");
	
		
		if (isset($_SESSION['form_ctxMenu_'.$_POST['id_app']]))
			unset($_SESSION['form_ctxMenu_'.$_POST['id_app']]);
			
		return true;
	}



	function fire($stepFlow) {
	
		global $babDB;
		
		
		$res = $babDB->db_query("
			SELECT 
				m.name,
				m.selmenu,
				s.id id_step,
				f.id id_form,
				f.id_type idx
			FROM 
				".FORM_APP_STEP_MENU." m,
				".FORM_APP_STEPS." s,
				".FORM_FORMS." f
			WHERE 
				m.id_step='".$babDB->db_escape_string($stepFlow->id_step)."' 
				AND m.id_menustep = s.id
				AND s.id_form = f.id
				");
		$menu = array();
		while ($arr = $babDB->db_fetch_array($res))
			{
			

			if ($arr['selmenu'] == 1)
				{
				$next_step = $arr['id_step'];
				}
				
			if (6 === (int) $arr['idx']) {
					$stepFlow->addError(sprintf(form_translate("The step %s is not allowed under a contextual menu"), $arr['name']) );
				}
			else {
				
					$menu[] = array(
							'name' => $arr['name'],
							'id_step' => $arr['id_step'],
							'id_form' => $arr['id_form'],
							'idx' => $arr['idx'],
							);
			
				}
			
			}

		if (count($menu) == 0)
			{
			$stepFlow->addError(form_translate("A contextual menu must have one entry in the configuration"));
			return false;
			}
			
			
		$row = $stepFlow->getStep();

		$_SESSION['form_ctxMenu_'.bab_rp('id_app')] = serialize($menu);

		$GLOBALS['form_menu_enter_menu'] = isset($_REQUEST['form_row']) ? $_REQUEST['form_row'] : '';
		$id_step = $next_step;
		
		$stepFlow->gotoStep($id_step);
		return true;
	}
}


?>