<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';





class form_stepTypeTemplateTemp_3 extends form_stepTypeTemplate
	{ 
	var $altbg = false;
	function form_stepTypeTemplateTemp_3($id_app, $id_step)
		{
		$this->form_stepTypeTemplate($id_app, $id_step);

		$this->db = & $GLOBALS['babDB'];
	
			
		if (!empty($this->id_step) && isset($_GET['id_step'])) {
		
			$res = $this->db->db_query("SELECT 
					s.*
				FROM 
					".FORM_APP_STEPS." s 
				WHERE 
					s.id=".$this->db->quote($this->id_step));
			$this->arr = $this->db->db_fetch_assoc($res);
			
			$this->rescases = $this->db->db_query("SELECT * FROM ".FORM_APP_STEP_CASES." WHERE id_step='".$this->db->db_escape_string($this->id_step)."'");
				$this->arr['nb_fields'] = $this->db->db_num_rows($this->rescases);

		} elseif (isset($_POST['nb_fields'])) {
			$this->arr = $_POST;
		} else {
			$this->arr['nb_fields'] = 0;
		}
		
		
		$this->resstep = $this->db->db_query("
			SELECT 
				id,
				name 
			FROM 
				".FORM_APP_STEPS." 
			WHERE 
				id_application='".$this->db->db_escape_string($id_app)."' 
			ORDER BY name
		");
		
		
		$this->reseditform = $this->db->db_query("
			SELECT f.id, f.name FROM 
			
				".FORM_FORMS." f, 
				".FORM_WORKSPACE_ENTRIES." w 
				
			WHERE 
				w.id_object = f.id 
				AND w.type = 'form' 
			 	AND f.id_type='2' 
			 	AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
			
			ORDER BY name
		");
		
		
		
		
		
		$resfield = $this->db->db_query("SELECT id,id_form,name FROM ".FORM_FORMS_FIELDS." ORDER BY name");
		$this->formfields = array();
		while ($arr = $this->db->db_fetch_array($resfield))
			{
			if (!isset($this->formfields[$arr['id_form']])) $this->formfields[$arr['id_form']] = array();
			$this->formfields[$arr['id_form']][] = array('id' => $arr['id'],'name' => $arr['name']);
			}
		}


	function getnexteditform()
		{
		static $i = 0;
		$this->form = $this->db->db_fetch_array($this->reseditform);
		if (!$this->form)
			{
			if (isset($i)) $i++;
			if ($this->db->db_num_rows($this->reseditform) > 0)
				{
				$this->db->db_data_seek($this->reseditform,0);
				}
			return false;
			}
		else
			{
			$this->form['name'] = bab_toHtml($this->form['name']);
			if ($i == 0)
				$this->selected = $this->arr['id_form'] == $this->form['id'] ? 'selected' : '';
			else
				$this->selected = isset($this->cases['id_form']) && $this->cases['id_form'] == $this->form['id'] ? 'selected' : '';

			return true;
			}
		}
		
		
	function getnextfield()
		{
		if (!isset($this->formfields[$this->form['id']]) || !is_array($this->formfields[$this->form['id']]))
			return false;
		$this->field =  current($this->formfields[$this->form['id']]);
		if (!$this->field)
			{
			reset($this->formfields[$this->form['id']]);
			return false;
			}
		else
			{
			next($this->formfields[$this->form['id']]);
			if (isset($this->cases['id_form_field']))
				{
				$this->selected = $this->field['id'] == $this->cases['id_form_field'] ? 'selected' : '';
				}
			elseif(isset($this->fieldapprob['id_form_field']))
				{
				$this->selected = $this->field['id'] == $this->fieldapprob['id_form_field'] ? 'selected' : '';
				}
			$this->field['name'] = bab_toHtml($this->field['name']);
			return true;
			}
		}


	
	function getnextcases(&$skip)
		{
		static $i=0,$j = 0, $deleted=0;
		if( $i < $this->arr['nb_fields'] + $deleted)
			{
			$this->altbg = !$this->altbg;
			if (isset($_POST['cases_del_fields']) && count($_POST['cases_del_fields']) > 0 && in_array($i,$_POST['cases_del_fields']))
				{
				$skip = true;
				$i++;
				$deleted++;
				return true;
				}
			$el_to_init = array('id_form_field','field_value','step', 'id','message','disabled');
			$index = $i;
			
			$default = array();
			$default['disabled'] = 'N';
			

			if (isset($this->rescases))
				{
				$this->cases = $this->db->db_fetch_array($this->rescases);
				
				$arr = $this->cases;
				foreach ($el_to_init as $el)
					$this->cases[$el] = isset($arr[$el]) ? $arr[$el] : (isset($default[$el]) ? $default[$el] : '');
				}
			else
				{
				$this->cases = $_POST;
				$arr = $_POST;
				foreach ($el_to_init as $el)
					$this->cases[$el] = isset($arr[$el.'_'.$index]) ? $arr[$el.'_'.$index] : (isset($default[$el]) ? $default[$el] : '');
				}

			$this->index = $j;
			$i++;
			$j++;
			return true;
			}
		else
			{
			$deleted = 0;
			$i = 0;
			$j =0;
			return false;
			}
		}


	function getnextstep()
		{
		static $i = 0;
		$this->step = $this->db->db_fetch_array($this->resstep);
		if (!$this->step)
			{
			if (isset($i)) $i++;
			if ($this->db->db_num_rows($this->resstep) > 0)
				{
				$this->db->db_data_seek($this->resstep,0);
				}
			return false;
			}
		else
			{
			if (isset($this->cases['step']))
				{
				$this->selected = $this->cases['step'] == $this->step['id'] ? 'selected' : '';
				}
			else
				{
				$this->selected = $this->arr['switch_id_step'] == $this->step['id'] ? 'selected' : '';
				}
				
			$this->step['name'] = bab_toHtml($this->step['name']);
			return true;
			}
		}
	
	
	}




/**
 * Type aiguillage
 */
class form_stepType_3 extends form_stepType {


	function getEditHtml($id_app, $id_step) {
		
		$tp = new form_stepTypeTemplateTemp_3($id_app, $id_step);
		return bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."steps/3.html", "edit" );
	}
	
	
	
	function record() {
	
		$db = &$GLOBALS['babDB'];
	
		if (isset($_POST['id_step']) && !empty($_POST['id_step']))
			{
			$id_step = $_POST['id_step'];

			$db->db_query("UPDATE ".FORM_APP_STEPS." SET 
			 
			 	name=".$db->quote($_POST['name']).",
			 	description=".$db->quote($_POST['description']).",
			 	switch_id_step=".$db->quote($_POST['switch_id_step'])." 
			 	
			 WHERE 
			 	id='".$db->db_escape_string($id_step)."' 
			 ");
	
			$res = $db->db_query("SELECT id FROM ".FORM_APP_STEP_CASES." WHERE id_step='".$db->db_escape_string($id_step)."'");
			$list_fields_id = array();
			while ($arr = $db->db_fetch_array($res))
				{
				$list_fields_id[] = $arr['id'];
				}
			}
		else
			{

			$res = $db->db_query("INSERT INTO ".FORM_APP_STEPS." 
				(
					id_application,
					id_type,
					name,
					description,
					switch_id_step
				) 
			VALUES 
				(
					".$db->quote($_POST['id_app']).",
					'3',
					".$db->quote($_POST['name']).",
					".$db->quote($_POST['description']).",
					".$db->quote($_POST['switch_id_step'])."
				)
			");
			$id_step = $db->db_insert_id($res);
			}
			
			
		// cases

		$worked_id = array();
	
		$prefix = 'id_cases_';
		
		$default = array(
		    'disabled' => 'N'
		);
		
		foreach ($_POST as $field => $value)
			{
			if (substr($field,0,strlen($prefix)) == $prefix)
				{
				$tmp = explode('_',$field);
				$idx = $tmp[2];
	
				$form_el = array('id_form_field', 'id_form_field_validation', 'form_field_validation_param', 'field_value', 'step', 'message', 'disabled');
	
				
	
				if (is_numeric($value) && in_array($value,$list_fields_id))
					{
					$query = '';
					foreach ($form_el as $field)
						{
							$query = $query == '' ? $query : $query.',';
							$query .= $field."='".(isset($_POST[$field.'_'.$idx]) ? $db->db_escape_string($_POST[$field.'_'.$idx]) : (isset($default[$field]) ? $default[$field] : ''))."'";
						}
	
					$db->db_query("UPDATE ".FORM_APP_STEP_CASES." SET ".$query." WHERE id='".$db->db_escape_string($value)."'");
					$worked_id[] = $value;
					}
				else
					{
					$query = '';
					foreach ($form_el as $field)
						{
							$query = $query == '' ? $query : $query.',';
							$query .= "'".(isset($_POST[$field.'_'.$idx]) ? $db->db_escape_string($_POST[$field.'_'.$idx]) : (isset($default[$field]) ? $default[$field] : ''))."'";
						}
	
					$res = $db->db_query("INSERT INTO ".FORM_APP_STEP_CASES." (id_step,".implode(',',$form_el).") VALUES ('".$db->db_escape_string($id_step)."',".$query.")");
					$worked_id[] = $db->db_insert_id($res);
					}
				}
			}
			
		if (count($worked_id) > 0 ) {
			$db->db_query("DELETE FROM ".FORM_APP_STEP_CASES." WHERE id_step='".$db->db_escape_string($id_step)."' AND id NOT IN(".$db->quote($worked_id).")");
		}
		else {
			$db->db_query("DELETE FROM ".FORM_APP_STEP_CASES." WHERE id_step='".$db->db_escape_string($id_step)."'");
		}

		return true;
	}



	function fire($stepFlow) {
	
		global $babDB;
	
		$row = $stepFlow->getStep();
		$next_id_step = $row['switch_id_step']; 

		$res = $babDB->db_query("SELECT id_form_field, step, field_value FROM ".FORM_APP_STEP_CASES." WHERE id_step='".$babDB->db_escape_string($stepFlow->id_step)."' AND step > 0 AND disabled='N'");

		while($arr = $babDB->db_fetch_array($res))
			{
			bab_debug($arr);
			
			
			if (isset($_POST['form_field_'.$arr['id_form_field']]))
				{
				$val = $_POST['form_field_'.$arr['id_form_field']];
				
				if (!empty($arr['step']) && ((string) $val === (string) $arr['field_value']))
					{
					$next_id_step = $arr['step'];
					break;
					}
				}
			}

		$stepFlow->gotoStep($next_id_step);
	}
}


?>