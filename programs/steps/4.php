<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';





class form_stepTypeTemplateTemp_4 extends form_stepTypeTemplate
	{ 
	var $altbg = false;
	function form_stepTypeTemplateTemp_4($id_app, $id_step)
		{
		$this->form_stepTypeTemplate($id_app, $id_step);

		$this->db = & $GLOBALS['babDB'];
	
			
		if (!empty($this->id_step) && isset($_GET['id_step'])) {
		
			$res = $this->db->db_query("SELECT 
					s.*
				FROM 
					".FORM_APP_STEPS." s 
				WHERE 
					s.id=".$this->db->quote($this->id_step));
			$this->arr = $this->db->db_fetch_assoc($res);
			
			$this->resapprob = $this->db->db_query("SELECT * FROM ".FORM_APP_STEP_APPROB." WHERE id_step='".$this->db->db_escape_string($this->id_step)."'");
				$this->arr['nb_approb'] = $this->db->db_num_rows($this->resapprob);

		} elseif (isset($_POST['nb_approb'])) {
			$this->arr = $_POST;
		} else {
			$this->arr['nb_approb'] = 0;
		}
		
		
		$this->resstep = $this->db->db_query("
			SELECT 
				id,
				name 
			FROM 
				".FORM_APP_STEPS." 
			WHERE 
				id_application='".$this->db->db_escape_string($id_app)."' 
			ORDER BY name
		");
		
		require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
		$this->applist = bab_WFGetApprobationsList();
		
		$this->resappform = $this->db->db_query("
			SELECT 
				f.id, f.name  
			FROM 
					".FORM_FORMS." f,
					".FORM_WORKSPACE_ENTRIES." w 
					
			WHERE 
				w.id_object = f.id 
				AND w.type = 'form' 
				AND f.id_type = '7' 
				AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
				
			ORDER BY f.name
		");
		
		$this->ov_groups = bab_getGroups();
		
		$this->reseditform = $this->db->db_query("
			SELECT f.id, f.name 
				FROM 
					".FORM_FORMS." f,
					".FORM_WORKSPACE_ENTRIES." w 
			 WHERE 
			 	w.id_object = f.id 
			 	AND w.type = 'form' 
			 	AND f.id_type='2' 
			 	AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
			 	
			 ORDER BY f.name
		");
		
		$resfield = $this->db->db_query("SELECT id,id_form,name FROM ".FORM_FORMS_FIELDS." ORDER BY name");
		$this->formfields = array();
		while ($arr = $this->db->db_fetch_array($resfield))
			{
			if (!isset($this->formfields[$arr['id_form']])) $this->formfields[$arr['id_form']] = array();
			$this->formfields[$arr['id_form']][] = array('id' => $arr['id'],'name' => $arr['name']);
			}
		}


	function getnextstep()
		{
		static $i = 0;
		$this->step = $this->db->db_fetch_array($this->resstep);
		if (!$this->step)
			{
			if (isset($i)) $i++;
			if ($this->db->db_num_rows($this->resstep) > 0)
				{
				$this->db->db_data_seek($this->resstep,0);
				}
			return false;
			}
		else
			{
			$this->selected = $this->arr['id_approb_next_step'] == $this->step['id'] ? 'selected' : '';
				
			$this->step['name'] = bab_toHtml($this->step['name']);
			return true;
			}
		}
		
		
	function getnextappform()
		{
		if ( $this->appform = $this->db->db_fetch_array($this->resappform) )
			{
			if (isset( $this->fieldapprob['id_form'] ))
				{
				$this->selected = $this->fieldapprob['id_form'] == $this->appform['id'] ? 'selected' : '';
				}
			elseif (isset($this->arr['id_approb_form']))
				{
				$this->selected = $this->arr['id_approb_form'] == $this->appform['id'] ? 'selected' : '';
				}
			$this->appform['name'] = bab_toHtml($this->appform['name']);
			return true;
			}
		else
			{
			if ($this->db->db_num_rows($this->resappform) > 0)
				$this->db->db_data_seek($this->resappform,0);
			return false;
			}
		}
	
	
	function getnextapprob()
		{
		if ( list(,$this->approb) = each($this->applist) )
			{
			if (isset( $this->fieldapprob['id_sh'] ))
				{
				$this->selected = $this->fieldapprob['id_sh'] == $this->approb['id'] ? 'selected' : '';
				}
			elseif (isset( $this->arr['id_approb_sh'] ))
				{
				$this->selected = $this->arr['id_approb_sh'] == $this->approb['id'] ? 'selected' : '';
				}
			$this->approb['name'] = bab_toHtml($this->approb['name']);
			return true;
			}
		else
			{
			reset($this->applist);
			return false;
			}
		}
		
		
	function getnextgroup()
		{
		if (list($key,$this->group['id']) = each($this->ov_groups['id']))
			{
			$this->group['name'] = bab_toHtml($this->ov_groups['name'][$key]);
			$this->selected = $this->group['id'] == $this->arr['approb_mail_group'];
			return true;
			}
		return false;
		}
		
	
	function getnextfieldapprob(&$skip)
		{
		static $i=0,$j = 0, $deleted=0;
		if(isset($this->arr['nb_approb']) && $i < $this->arr['nb_approb'] + $deleted)
			{
			$this->altbg = !$this->altbg;

			if (isset($_POST['approb_del_fields']) && count($_POST['approb_del_fields']) >0 && in_array($i,$_POST['approb_del_fields']))
				{
				$skip = true;
				$i++;
				$deleted++;
				return true;
				}
			$el_to_init = array('id', 'id_form_field','field_value','id_sh', 'id_form');
			$index = $i;
			
			$default = array();

			if (isset($this->resapprob))
				{
				$this->fieldapprob = $this->db->db_fetch_array($this->resapprob);
				
				$arr = & $this->fieldapprob;
				foreach ($el_to_init as $el)
					$this->fieldapprob[$el] = isset($arr[$el]) ? bab_toHtml($arr[$el]) : (isset($default[$el]) ? $default[$el] : '');
				}
			else
				{
				$this->fieldapprob = $_POST;
				$arr = & $_POST;
				foreach ($el_to_init as $el)
					$this->fieldapprob[$el] = isset($arr['approb_'.$el.'_'.$index]) ? bab_toHtml($arr['approb_'.$el.'_'.$index]) : (isset($default[$el]) ? $default[$el] : '');
				}

			$this->index = $j;
			$i++;
			$j++;
			return true;
			}
		else
			{
			$deleted = 0;
			$i = 0;
			$j =0;
			return false;
			}
		
		}
		
		
	function getnexteditform()
		{
		static $i = 0;
		$this->form = $this->db->db_fetch_array($this->reseditform);
		if (!$this->form)
			{
			if (isset($i)) $i++;
			if ($this->db->db_num_rows($this->reseditform) > 0)
				{
				$this->db->db_data_seek($this->reseditform,0);
				}
			return false;
			}
		else
			{
			$this->form['name'] = bab_toHtml($this->form['name']);
			return true;
			}
		}
		
	function getnextfield()
		{
		if (!isset($this->formfields[$this->form['id']]) || !is_array($this->formfields[$this->form['id']]))
			return false;
		$this->field =  current($this->formfields[$this->form['id']]);
		if (!$this->field)
			{
			reset($this->formfields[$this->form['id']]);
			return false;
			}
		else
			{
			next($this->formfields[$this->form['id']]);
			if (isset($this->cases['id_form_field']))
				{
				$this->selected = $this->field['id'] == $this->cases['id_form_field'] ? 'selected' : '';
				}
			elseif(isset($this->fieldapprob['id_form_field']))
				{
				$this->selected = $this->field['id'] == $this->fieldapprob['id_form_field'] ? 'selected' : '';
				}
			$this->field['name'] = bab_toHtml($this->field['name']);
			return true;
			}
		}

	}







function form_getInstance($idsch, $form_row, $id_step, $id_table, $approb_change)
{
	$db = &$GLOBALS['babDB'];

	$id_user = $GLOBALS['BAB_SESS_USERID'];

	if (list($idschi, $old_id_user) = $db->db_fetch_array($db->db_query("SELECT id_schi, id_user FROM ".FORM_APPROB_SCHI." WHERE id_line='".$db->db_escape_string($form_row)."' AND id_table='".$db->db_escape_string($id_table)."'")))
		{
		// une instance existe d�ja pour cette ligne

		switch ($approb_change)
			{
			case 0: // ne rien faire
				return $idschi;

			case 2: // Cr�er le workflow avec l'utilisateur d'origine
				$id_user = $old_id_user;
			case 1: // Cr�er le workflow avec le nouvel utilisateur
				bab_WFDeleteInstance($idschi);
				$db->db_query("DELETE FROM ".FORM_APPROB_SCHI." WHERE id_schi='".$db->db_escape_string($idschi)."'");
			}
		
		}

	$idschi = bab_WFMakeInstance($idsch, 'tbl:'.$id_table.'-id:'.$form_row);

	$db->db_query("INSERT INTO ".FORM_APPROB_SCHI." 
		(id_line, id_schi, id_step, id_table, id_user) 
	VALUES 
		('".$db->db_escape_string($form_row)."',
		'".$db->db_escape_string($idschi)."',
		'".$db->db_escape_string($id_step)."',
		'".$db->db_escape_string($id_table)."', 
		'".$db->db_escape_string($id_user)."')");

	// mettre la ligne en attente

	$db->db_query("UPDATE ".form_tbl_name($id_table)." SET form_approb='0' WHERE form_id='".$db->db_escape_string($form_row)."'");

	return $idschi;
}




function form_getApprovers($idschi)
	{
	$return = array();

	$arr = & bab_WFGetWaitingApproversInstance($idschi, true);
	foreach ($arr as $id)
		{
		$return[] = bab_getUserEmail($id);
		}

	return $return;
	}




/**
 * Type workflow
 */
class form_stepType_4 extends form_stepType {


	function getEditHtml($id_app, $id_step) {
		
		$tp = new form_stepTypeTemplateTemp_4($id_app, $id_step);
		return bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."steps/4.html", "edit" );
	}
	
	
	
	function record() {
	
		global $babBody;
	
		if (!isset($_POST['id_approb_form'])) {
			$babBody->msgerror = form_translate('The approbation form is mandatory');
			return false;
		}
	
	
	
	
	
		$db = &$GLOBALS['babDB'];
		
		
		$approb_mail_applicant = isset($_POST['approb_mail_applicant']) ? 1 : 0;
		
	
		if (isset($_POST['id_step']) && !empty($_POST['id_step']))
			{
			$id_step = $_POST['id_step'];

			$db->db_query("UPDATE ".FORM_APP_STEPS." SET 
			 
			 	name=".$db->quote($_POST['name']).",
			 	description=".$db->quote($_POST['description']).",
			 	id_approb_next_step=".$db->quote($_POST['id_approb_next_step']).", 
			 	id_approb_sh=".$db->quote($_POST['id_approb_sh']).", 
			 	id_approb_form=".$db->quote($_POST['id_approb_form']).", 
			 	approb_mail_subject=".$db->quote($_POST['approb_mail_subject']).", 
			 	valid_mail_subject=".$db->quote($_POST['valid_mail_subject']).", 
			 	refused_mail_subject=".$db->quote($_POST['refused_mail_subject']).", 
			 	approb_change=".$db->quote($_POST['approb_change']).",
			 	approb_mail_applicant =".$db->quote($approb_mail_applicant).", 
			 	approb_mail =".$db->quote($_POST['approb_mail']).", 
			 	approb_mail_group =".$db->quote($_POST['approb_mail_group'])."  
			 	
			 WHERE 
			 	id='".$db->db_escape_string($id_step)."' 
			 ");
	
			$res = $db->db_query("SELECT id FROM ".FORM_APP_STEP_CASES." WHERE id_step='".$db->db_escape_string($id_step)."'");
			$list_fields_id = array();
			while ($arr = $db->db_fetch_array($res))
				{
				$list_fields_id[] = $arr['id'];
				}
			}
		else
			{

			$res = $db->db_query("INSERT INTO ".FORM_APP_STEPS." 
				(
					id_application,
					id_type,
					name,
					description,
					id_approb_next_step,
					id_approb_sh,
					id_approb_form,
					approb_mail_subject,
					valid_mail_subject,
					refused_mail_subject,
					approb_change,
					approb_mail_applicant,
					approb_mail,
					approb_mail_group 
				) 
			VALUES 
				(
					".$db->quote($_POST['id_app']).",
					'4',
					".$db->quote($_POST['name']).",
					".$db->quote($_POST['description']).",
					".$db->quote($_POST['id_approb_next_step']).",
					".$db->quote($_POST['id_approb_sh']).",
					".$db->quote($_POST['id_approb_form']).",
					".$db->quote($_POST['approb_mail_subject']).",
					".$db->quote($_POST['valid_mail_subject']).",
					".$db->quote($_POST['refused_mail_subject']).",
					".$db->quote($_POST['approb_change']).",
					".$db->quote($approb_mail_applicant).",
					".$db->quote($_POST['approb_mail']).",
					".$db->quote($_POST['approb_mail_group'])."
				)
			");
			$id_step = $db->db_insert_id($res);
			}
			
			
		// approbation

		$worked_id = array();
	
		$prefix = 'approb_id_approb_';
		foreach ($_POST as $field => $value)
			{
			if (substr($field,0,strlen($prefix)) == $prefix)
				{
				$tmp = explode('_',$field);
				$idx = $tmp[3];
	
				$form_el = array('id_form_field', 'field_value','id_sh','id_form');
	
				if ( !empty($value) && is_numeric($value))
					{
					$query = '';
					foreach ($form_el as $field)
						{
							$query = $query == '' ? $query : $query.',';
							$query .= $field."='".(isset($_POST['approb_'.$field.'_'.$idx]) ? $db->db_escape_string($_POST['approb_'.$field.'_'.$idx]) : '')."'";
						}
					
					$db->db_query("UPDATE ".FORM_APP_STEP_APPROB." SET ".$query." WHERE id='".$db->db_escape_string($value)."'");
					$worked_id[] = $value;
					}
				else
					{
					$query = '';
					foreach ($form_el as $field)
						{
							$query = $query == '' ? $query : $query.',';
							$query .= "'".(isset($_POST['approb_'.$field.'_'.$idx]) ? $db->db_escape_string($_POST['approb_'.$field.'_'.$idx]) : '')."'";
						}
	
					$res = $db->db_query("INSERT INTO ".FORM_APP_STEP_APPROB." (id_step,".implode(',',$form_el).") VALUES ('".$db->db_escape_string($id_step)."',".$query.")");
					$worked_id[] = $db->db_insert_id($res);
					}
				}
			}
	
		if (count($worked_id) > 0 )
			$db->db_query("DELETE FROM ".FORM_APP_STEP_APPROB." WHERE id_step='".$db->db_escape_string($id_step)."' AND id NOT IN(".$db->quote($worked_id).")");
		else
			$db->db_query("DELETE FROM ".FORM_APP_STEP_APPROB." WHERE id_step='".$db->db_escape_string($id_step)."'");
	


		return true;
	}



	function fire($stepFlow) {
	
		$row = $stepFlow->getStep();
		
		
		
		$form_row = form_currentDbRow();
		if (NULL === $form_row) {
			$form_row = (int) bab_pp('form_row');
		}
		
		
		
		
		if (empty($form_row) || empty($_POST['id_table']))
			{
			$stepFlow->addError(form_translate('The workflow step must be after a create/modify form'));
			$stepFlow->gotoStep($row['id_approb_next_step']);
			return false;
			}
			
		$id_step = $stepFlow->id_step;
			
		global $babDB;

		$res = $babDB->db_query("SELECT * FROM ".FORM_APP_STEP_APPROB." WHERE id_step='".$babDB->db_escape_string($id_step)."'");

		while($arr = $babDB->db_fetch_array($res))
			{
			if (isset($_POST['form_field_'.$arr['id_form_field']]))
				{
				$val = $_POST['form_field_'.$arr['id_form_field']];
				
				if (!empty($arr['field_value']) && $val == $arr['field_value'])
					{
					if (!empty($arr['id_form']))
						$row['id_approb_form'] = $arr['id_form'];
					if (!empty($arr['id_sh']))
						$row['id_approb_sh'] = $arr['id_sh'];
					continue;
					}
				}
			}

		require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
		
		if (empty($row['id_approb_sh']))
			{
			$GLOBALS['babBody']->msgerror = form_translate('Error : there is no approbation schema selected');
			$stepFlow->gotoStep($row['id_approb_next_step']);
			return false;
			}
			
		// creer une instance
		$idschi = form_getInstance($row['id_approb_sh'], $form_row, $id_step, $_POST['id_table'], $row['approb_change']);

		$mail = form_getApprovers($idschi);
		if (isset($GLOBALS['form_mailmsg']))
			{
			list($table_name, $table_description) = $babDB->db_fetch_array($babDB->db_query("SELECT name,description FROM ".FORM_TABLES." WHERE id='".$babDB->db_escape_string($_POST['id_table'])."'"));


			// envoyer une copie du mail aux approbateurs
			$mailmsg = & $GLOBALS['form_mailmsg'];
			$mailmsg->message = form_translate('Waiting approbation').' : '.$table_description;
			$url = $GLOBALS['babUrlScript'].'?tg=addon/forms/form&idx=7&id_form='.$row['id_approb_form'].'&form_row='.$form_row.'&popup=0';
			$mailmsg->addlink(form_translate('Approbation form'), '', $GLOBALS['babUrlScript'].'?tg=login&cmd=detect&referer='.urlencode($url));
			$url = $GLOBALS['babUrlScript'].'?tg=approb';
			$mailmsg->addlink(form_translate('Approbation list'), '', $GLOBALS['babUrlScript'].'?tg=login&cmd=detect&referer='.urlencode($url));
			$mailmsg->mailDestArray($mail,'mailTo');
			$mailmsg->mailSubject(form_translate('FORM').' : '.form_translate('Approbation').' : '.$table_name, $row['approb_mail_subject']);
			$mailmsg->mailmsgsend();
			}
		else
			{
			trigger_error('mail object is not defined, the form must be create/modify');
			}


		$stepFlow->gotoStep($row['id_approb_next_step']);
		
	}
}


?>