<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';





class form_stepTypeTemplateTemp_5 extends form_stepTypeTemplate
	{ 
	var $altbg = false;
	function form_stepTypeTemplateTemp_5($id_app, $id_step)
		{
		$this->form_stepTypeTemplate($id_app, $id_step);
		
		$this->db = & $GLOBALS['babDB'];
	
			
		if (!empty($this->id_step) && isset($_GET['id_step'])) {
		
			$res = $this->db->db_query("
				SELECT 
					s.name,
					s.description 
				FROM 
					".FORM_APP_STEPS." s 
				WHERE 
					s.id=".$this->db->quote($this->id_step));
			$arr = $this->db->db_fetch_assoc($res);
	
			$this->name 		= bab_toHtml($arr['name']);
			$this->description = bab_toHtml($arr['description']);
	
		} elseif (isset($_POST['name'])) {
		
			$this->name 		= bab_toHtml($_POST['name']);
			$this->description = bab_toHtml($_POST['description']);
		} else {
		
			$this->name 		= '';
			$this->description 	= '';
		}
	}
}




/**
 * Type export csv
 */
class form_stepType_5 extends form_stepType {


	function getEditHtml($id_app, $id_step) {
		
		$tp = new form_stepTypeTemplateTemp_5($id_app, $id_step);
		return bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."steps/5.html", "edit" );
	}
	
	
	
	function record() {
	
		$db = &$GLOBALS['babDB'];
	
		if (isset($_POST['id_step']) && !empty($_POST['id_step']))
			{
			$id_step = $_POST['id_step'];

			$db->db_query("UPDATE ".FORM_APP_STEPS." SET 
			 
			 	name=".$db->quote($_POST['name']).",
			 	description=".$db->quote($_POST['description'])." 
			 
			 WHERE 
			 	id='".$db->db_escape_string($id_step)."' 
			 ");
	
			
			}
		else
			{

			$res = $db->db_query("INSERT INTO ".FORM_APP_STEPS." 
				(
					id_application,
					id_type,
					name,
					description 
				) 
			VALUES 
				(
					".$db->quote($_POST['id_app']).",
					'5',
					".$db->quote($_POST['name']).",
					".$db->quote($_POST['description'])."
				)
			");
			$id_step = $db->db_insert_id($res);
			}
			
		return true;
	}



	function fire($stepFlow) {
		
		if (!isset($_POST['id_form'])) {
			die(form_translate('The export step must be linked with a submit button'));
			break;
		}

		$tp = new form_list($_POST['id_form']);
		$tp->arr['rows_per_page'] = false;
		$tp->list_query();
		$tp->export();
	}
}


?>