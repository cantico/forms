<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';





class form_stepTypeTemplateTemp_10 extends form_stepTypeTemplate
	{ 
	var $altbg = false;
	function form_stepTypeTemplateTemp_10($id_app, $id_step)
		{
		$this->form_stepTypeTemplate($id_app, $id_step);
		$this->t_id_paybox_system = form_translate('Paybox configuration');
		$this->t_id_paybox_total = form_translate('Total amount');
		$this->t_id_paybox_cmd = form_translate('Order reference');
		$this->t_id_paybox_porteur = form_translate('Email adress of purchaser');
		$this->t_id_paybox_effectue = form_translate('Step after payment has been accepted');
		$this->t_id_paybox_annule = form_translate('Step after payment has been canceled');
		$this->t_id_paybox_refuse = form_translate('Step after payment has been refused');
		
		$this->db = & $GLOBALS['babDB'];

		
		$this->resstep = $this->db->db_query("
			SELECT 
				id,
				name 
			FROM 
				".FORM_APP_STEPS." 
			WHERE 
				id_application='".$this->db->db_escape_string($id_app)."' 
			ORDER BY name
		");
	
		$addonpb = bab_functionality::get('LibPaybox');
		$this->payboxtpe = $addonpb->getPayboxSystemList();
		
		if (isset($_POST['name'])) {
		
			$this->name 			= bab_toHtml($_POST['name']);
			$this->description  	= bab_toHtml($_POST['description']);
			$this->arr['id_paybox_system'] = $_POST['id_paybox_system'];
			$this->arr['id_paybox_total'] =  $_POST['id_paybox_total'];
			$this->arr['id_paybox_cmd'] =  $_POST['id_paybox_cmd'];
			$this->arr['id_paybox_porteur'] =  $_POST['id_paybox_porteur'];
			$this->arr['id_paybox_step_effectue'] =  $_POST['id_paybox_step_effectue'];
			$this->arr['id_paybox_step_annule'] =  $_POST['id_paybox_step_annule'];
			$this->arr['id_paybox_step_refuse'] =  $_POST['id_paybox_step_refuse'];
		} elseif (!empty($id_step)) {
		
			$res = $this->db->db_query("
				SELECT 
					s.name,
					s.description, 
					s.id_paybox_system,
					s.id_paybox_total, 
					s.id_paybox_cmd, 
					s.id_paybox_porteur, 
					s.id_paybox_step_effectue, 
					s.id_paybox_step_annule, 
					s.id_paybox_step_refuse 
				FROM 
					".FORM_APP_STEPS." s 
				WHERE 
					s.id=".$this->db->quote($id_step));
					
			$arr = $this->db->db_fetch_assoc($res);
	
			$this->name 		= bab_toHtml($arr['name']);
			$this->description  = bab_toHtml($arr['description']);
			$this->arr['id_paybox_system'] = (int) $arr['id_paybox_system'];
			$this->arr['id_paybox_total'] =  (int) $arr['id_paybox_total'];
			$this->arr['id_paybox_cmd'] =  (int) $arr['id_paybox_cmd'];
			$this->arr['id_paybox_porteur'] =  (int) $arr['id_paybox_porteur'];
			$this->arr['id_paybox_step_effectue'] =  (int) $arr['id_paybox_step_effectue'];
			$this->arr['id_paybox_step_annule'] =  (int) $arr['id_paybox_step_annule'];
			$this->arr['id_paybox_step_refuse'] =  (int) $arr['id_paybox_step_refuse'];
		} else {
		
			$this->name 		= '';
			$this->description 	= '';
			$this->arr['id_paybox_system'] = 0;
			$this->arr['id_paybox_total'] =  0;
			$this->arr['id_paybox_cmd'] =  0;
			$this->arr['id_paybox_porteur'] =  0;
			$this->arr['id_paybox_step_effectue'] = 0;
			$this->arr['id_paybox_step_annule'] = 0;
			$this->arr['id_paybox_step_refuse'] = 0;
					}
		
		$this->reseditform = $this->db->db_query("
			SELECT f.id, f.name FROM 
			
				".FORM_FORMS." f, 
				".FORM_WORKSPACE_ENTRIES." w 
				
			WHERE 
				w.id_object = f.id 
				AND w.type = 'form' 
			 	AND f.id_type='2' 
			 	AND w.id_workspace = ".$this->db->quote(form_getWorkspace())." 
			
			ORDER BY name
		");
		
		$resfield = $this->db->db_query("SELECT id,id_form,name FROM ".FORM_FORMS_FIELDS." ORDER BY name");
		$this->formfields = array();
		while ($arr = $this->db->db_fetch_array($resfield))
			{
			if (!isset($this->formfields[$arr['id_form']])) $this->formfields[$arr['id_form']] = array();
			$this->formfields[$arr['id_form']][] = array('id' => $arr['id'],'name' => $arr['name']);
			}
			
		$this->steps = array('id_paybox_step_effectue', 'id_paybox_step_annule', 'id_paybox_step_refuse');
		$this->fields = array('id_paybox_total', 'id_paybox_cmd', 'id_paybox_porteur');
		}
	
	function getnexteditform()
		{
		static $i = 0;
		$this->form = $this->db->db_fetch_array($this->reseditform);
		if (!$this->form)
			{
			if (isset($i)) $i++;
			if ($this->db->db_num_rows($this->reseditform) > 0)
				{
				$this->db->db_data_seek($this->reseditform,0);
				}
			return false;
			}
		else
			{
			$this->form['name'] = bab_toHtml($this->form['name']);
			$this->fieldsel = $this->fields[$i];
			return true;
			}
		}
		
		
	function getnextfield()
		{
		if (!isset($this->formfields[$this->form['id']]) || !is_array($this->formfields[$this->form['id']]))
			return false;
		$this->field =  current($this->formfields[$this->form['id']]);
		if (!$this->field)
			{
			reset($this->formfields[$this->form['id']]);
			return false;
			}
		else
			{
			next($this->formfields[$this->form['id']]);
			$this->selected = $this->arr[$this->fieldsel] == $this->field['id'] ? 'selected' : '';
			$this->field['name'] = bab_toHtml($this->field['name']);
			return true;
			}
		}
	
	function getnextstep()
		{
		static $i = 0;
		$this->step = $this->db->db_fetch_array($this->resstep);
		if (!$this->step)
			{
			if (isset($i)) $i++;
			if ($this->db->db_num_rows($this->resstep) > 0)
				{
				$this->db->db_data_seek($this->resstep,0);
				}
			return false;
			}
		else
			{
			$this->selected = $this->arr[$this->steps[$i]] == $this->step['id'];
				
			$this->step['name'] = bab_toHtml($this->step['name']);
			return true;
			}
		}

	function getnexttpe()
		{
		static $i = 0;
		if(list($this->tpe_id, $this->tpe_name) = each($this->payboxtpe))
			{
				$this->selected = $this->arr['id_paybox_system'] ==  $this->tpe_id ? 'selected' : '';
				return true;
			}
			else
			{
				return false;
			}
		}
		
}




/**
 * Type Importer un utilisateur dans la table
 */
class form_stepType_10 extends form_stepType {


	function getEditHtml($id_app, $id_step) {
		
		$tp = new form_stepTypeTemplateTemp_10($id_app, $id_step);
		return bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."steps/10.html", "edit" );
	}
	
	
	
	function record() {
	
		$db = &$GLOBALS['babDB'];
		
		
		
		
		if (isset($_POST['id_step']) && !empty($_POST['id_step']))
			{
			$id_step = $_POST['id_step'];

			$db->db_query("UPDATE ".FORM_APP_STEPS." SET 
			 
			 	name=".$db->quote($_POST['name']).",
			 	description=".$db->quote($_POST['description']).", 
			 	id_paybox_system=".$db->quote($_POST['id_paybox_system']).", 
			 	id_paybox_total=".$db->quote($_POST['id_paybox_total']).", 
			 	id_paybox_cmd=".$db->quote($_POST['id_paybox_cmd']).", 
			 	id_paybox_porteur=".$db->quote($_POST['id_paybox_porteur']).", 
			 	id_paybox_step_effectue=".$db->quote($_POST['id_paybox_step_effectue']).", 
			 	id_paybox_step_annule=".$db->quote($_POST['id_paybox_step_annule']).", 
			 	id_paybox_step_refuse=".$db->quote($_POST['id_paybox_step_refuse'])." 
			 WHERE 
			 	id='".$db->db_escape_string($id_step)."' 
			 ");
	
			
			}
		else
			{

			$res = $db->db_query("INSERT INTO ".FORM_APP_STEPS." 
				(
					id_application,
					id_type,
					name,
					description, 
					id_paybox_system, 
					id_paybox_total, 
					id_paybox_cmd, 
					id_paybox_porteur, 
					id_paybox_step_effectue, 
					id_paybox_step_annule, 
					id_paybox_step_refuse 
				) 
			VALUES 
				(
					".$db->quote($_POST['id_app']).",
					'10',
					".$db->quote($_POST['name']).",
					".$db->quote($_POST['description']).", 
					".$db->quote($_POST['id_paybox_system']).", 
					".$db->quote($_POST['id_paybox_total']).", 
					".$db->quote($_POST['id_paybox_cmd']).", 
					".$db->quote($_POST['id_paybox_porteur']).", 
					".$db->quote($_POST['id_paybox_step_effectue']).", 
					".$db->quote($_POST['id_paybox_step_annule']).", 
					".$db->quote($_POST['id_paybox_step_refuse'])." 
				)
			");
			$id_step = $db->db_insert_id($res);
			}
		return true;
	}



	function fire($stepFlow) {
		
		$row = $stepFlow->getStep();
		$id_paybox_system = $row['id_paybox_system'];
		$customer = array();
		$customer['total'] = isset($_POST['form_field_'.$row['id_paybox_total']])? trim($_POST['form_field_'.$row['id_paybox_total']]):0;
		$customer['cmd'] = isset($_POST['form_field_'.$row['id_paybox_cmd']])? trim($_POST['form_field_'.$row['id_paybox_cmd']]):'';
		$customer['porteur'] = isset($_POST['form_field_'.$row['id_paybox_porteur']])? trim($_POST['form_field_'.$row['id_paybox_porteur']]):'';
		
		$customer['effectue'] = urlencode($GLOBALS['babAddonUrl']."form&idx=paybox&paction=effectue&pid_step=".$stepFlow->id_step);
		$customer['refuse'] = urlencode($GLOBALS['babAddonUrl']."form&idx=paybox&paction=refuse&pid_step=".$stepFlow->id_step);
		$customer['annule'] = urlencode($GLOBALS['babAddonUrl']."form&idx=paybox&paction=annule&pid_step=".$stepFlow->id_step);
		
		$pb = bab_functionality::get('LibPaybox');
		$pb->execute($id_paybox_system, $customer);
		exit;
		return true;
	}
}


?>