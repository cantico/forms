<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");


function edit_tabledesc($id_table)
{
global $babBody;
    class temp
        {
		var $altbg = false;
		var $strlimit = 30;

        function temp($id_table)
            {
			$this->id_table = $id_table;
			$this->db = & $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_tablename = bab_toHtml(form_getTableName($this->id_table));
			$this->t_record = form_translate('Record');
			$this->t_default_val = form_translate('Default value');

			$this->res = $this->db->db_query("SELECT f.*, t.sql_type FROM ".FORM_TABLES_FIELDS." f LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t ON t.id=f.id_type WHERE f.id_table='".$this->db->db_escape_string($this->id_table)."' 
			ORDER BY f.name 
			");
			}

		function getnextfield()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				//$this->ifdefaultvalue = empty($this->field['field_function']) && !in_array($this->field['sql_type'],array(17,18,19,20,23,24,25,26));
				
				$this->field['id'] = bab_toHtml($this->field['id']);
				$this->field['name'] = bab_toHtml($this->field['name']);
				$this->field['description'] = bab_toHtml($this->field['description']);

				$this->altbg = !$this->altbg;
				return true;
				}
			else
				{
				return false;
				}
			}

        }
    $tp = new temp($id_table);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."tabledesc.html", "edit" ));
}


// record





// main

$idx = isset($_REQUEST['idx']) ? $_REQUEST['idx'] : 'edit_tabledesc';


$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");

switch($idx)
{
	default:
	case "edit_tabledesc":
		$babBody->addItemMenu("edit_tabledesc", form_translate("Edit descriptions"),$GLOBALS['babAddonUrl']."tabledesc&idx=edit_tabledesc");
		$babBody->title = form_translate("Edit fields descriptions");
		edit_tabledesc($_REQUEST['id_table']);
		break;
}


$babBody->setCurrentItemMenu($idx);

?>