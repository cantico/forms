INSERT INTO `form_tables_fields_types` 
(`id`, `name`, `description`, `sql_type`, `col_type`, `id_forms_elements`, `str_options`, `id_extend`) 
VALUES 
(1, 'varchar', '', 16, 'varchar(255)', '1,17', '', 1),
(2, 'text', '', 18, 'text', '2,1', '', 1),
(3, 'entier', '', 5, 'int(10) unsigned', '1,11', '', 1),
(4, 'date', '', 10, 'date', '8,1', 'a:1:{s:16:"view_date_format";s:1:"2";}', 2),
(5, 'oui-non', '', 21, 'enum(''Oui'',''Non'')', '3,5,10', '', 1),
(6, 'vide-oui-non', '', 21, 'enum('''',''Oui'',''Non'')', '3,5,10', '', 1),
(7, 'date heure', '', 11, 'datetime', '8,1', 'a:1:{s:16:"view_date_format";s:1:"2";}', 3),
(8, 'fichier', '', 25, 'longblob', '7', '', 1)