<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

bab_functionality::includefile('CalendarBackend');

class Func_CalendarBackend_Forms extends Func_CalendarBackend 
{
	
	public function getDescription()
	{
		require_once dirname(__FILE__).'/functions.php';
		return form_translate('Read only calendar visualisation for forms applications');
	}
	
	/**
	 * The backend can be used as a storage backend for the existing calendars (personal only for now)
	 * @return bool
	 */
	public function StorageBackend()
	{
		return false;
	}
	
	
	public function includeEventCalendar()
	{
		parent::includeEventCalendar();
		require_once dirname(__FILE__).'/calendar.class.php';
	}
	
	/**
	 * @return form_EventCalendar
	 */
	public function PublicCalendar()
	{
		$this->includeEventCalendar();
		return new form_EventCalendar;
	}
	
	

	
	
	private static function flattenCriteria(bab_PeriodCriteria $criteria, &$criteriaArray)
	{
		$criteriaArray[] = $criteria;
		$subCriteria = $criteria->getCriterions();
		foreach($subCriteria as $subCriterion) {
			self::flattenCriteria($subCriterion, $criteriaArray);
		}
	}
	
	
	/**
	 * Select periods from criteria
	 * the bab_PeriodCriteriaCollection and bab_PeriodCriteriaCalendar are mandatory
	 * 
	 * 
	 * @param bab_PeriodCriteria $criteria
	 * 
	 * @return iterator <bab_CalendarPeriod>
	 */
	public function selectPeriods(bab_PeriodCriteria $criteria)
	{
		$criteriaArray = array();
		self::flattenCriteria($criteria, $criteriaArray);

		$selectedCalendars = array();
		$filteredProperties = array();
		$uid = null;
		
		$begin = null;
		$end = null;

		foreach ($criteriaArray as $criterion) {
			//			echo get_class($criterion) . "\n";
			if ($criterion instanceof bab_PeriodCriteriaCalendar) {
				$selectedCalendars = array();
				foreach ($criterion->getCalendar() as $calendarId => $calendar) {
					if ($calendar instanceof form_EventCalendar) {
						$selectedCalendars[$calendarId] = $calendar;
					}
				}
			} else if ($criterion instanceof bab_PeriodCritieraBeginDateLessThanOrEqual) {
				$end = $criterion->getDate();
				$end = isset($end) ? $end->getIsoDateTime() : null;
			} else if ($criterion instanceof bab_PeriodCritieraEndDateGreaterThanOrEqual) {
				$begin = $criterion->getDate();
				$begin = isset($begin) ? $begin->getIsoDateTime() : null;
			} else if ($criterion instanceof bab_PeriodCritieraProperty) {

				foreach ($criterion->getValue() as $value) {
					$filteredProperties[] = array(
						'name' => $criterion->getProperty(), 
						'value' => $value,
						'contain' => $criterion->getContain()
					);
				}
			} elseif ($criterion instanceof bab_PeriodCritieraUid) {
				$uid = $criterion->getUidValues();
			}
		}

		$periods = array();

		foreach ($selectedCalendars as $calendar) {
			
			
			$collection = $this->CalendarEventCollection($calendar);
			$events = $this->getTableEvents($calendar, $collection, $periods, $begin, $end, $filteredProperties, $uid);
		}
		

		return $periods;
	}
	
	
	
	
	
	
	/**
	 * Query form table and extract calendar events
	 * @param 	form_EventCalendar 				$calendar
	 * @param 	bab_CalendarEventCollection 	$collection
	 * @param 	array 							&$periods
	 * @param	string							$begin
	 * @param	string							$end
	 * @param 	array 							$filteredProperties
	 * @param	array							$uid
	 * @return unknown_type
	 */
	private function getTableEvents(form_EventCalendar $calendar, bab_CalendarEventCollection $collection, &$periods, $begin, $end, $filteredProperties, $uid)
	{
		global $babDB;
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		$id_table = $calendar->getUid();
		$tbl_name = form_tbl_name($id_table);
		
		// property cols
		
		$cols = array();
		$query_filters = array();
		
		$res = $babDB->db_query("SELECT id, name, field_function, calendar_property FROM form_tables_fields WHERE calendar_property<>'' AND id_table=".$babDB->quote($id_table));
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$col_name = form_col_name($arr['id'], $arr['name'], $arr['field_function']);
			
			$cols[] = $col_name.' '.$arr['calendar_property'];
			
			foreach($filteredProperties as $f)
			{
				if ($f['name'] === $arr['calendar_property'])
				{
					if ($f['contain'])
					{
						$query_filters[] = $col_name." LIKE '%".$babDB->db_escape_like($f['value'])."%'";
					} else {
						$query_filters[] = $col_name." LIKE '".$babDB->db_escape_like($f['value'])."'";
					}
				}
				
				if ('daydate' === $arr['calendar_property'])
				{
					$query_filters[] = $col_name." >= ".$babDB->quote($begin);
					$query_filters[] = $col_name." <= ".$babDB->quote($end);
				}
				
				if ('DTSTART' === $arr['calendar_property'] && null !== $end)
				{
					$query_filters[] = $col_name." < ".$babDB->quote($end);
				}
				
				if ('DTEND' === $arr['calendar_property'] && null !== $begin)
				{
					$query_filters[] = $col_name." > ".$babDB->quote($begin);
				}
			}
		}
		
		
		if (null !== $uid)
		{
			$query_filters[] = "form_id IN(".$babDB->quote($uid).")";
		}
		
		
		// select data

		
		$query = 'SELECT form_id, form_create, form_lastupdate, form_create_user, '.implode(', ', $cols).' FROM '.$babDB->backTick($tbl_name).' ';
		
		if ($query_filters)
		{
			$query .= 'WHERE '.implode(' AND ', $query_filters);
		}
		
		bab_debug($query);
		
		
		$res = $babDB->db_query($query);
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			if (!isset($arr['SUMMARY']))
			{
				continue;
			}
			
			if (isset($arr['daydate']))
			{
				
				$dtstart = BAB_DateTime::fromIsoDateTime($arr['daydate'].' 00:00:00');
				$dtend = BAB_DateTime::fromIsoDateTime($arr['daydate'].' 23:59:59');
				
			} else {
				
				if (!isset($arr['DTSTART']) || !isset($arr['DTEND']))
				{
					continue;
				}
				
				$dtstart = BAB_DateTime::fromIsoDateTime($arr['DTSTART']);
				$dtend = BAB_DateTime::fromIsoDateTime($arr['DTEND']);
			}
			
			
			$created = BAB_DateTime::fromIsoDateTime($arr['form_create']);
			$lastmodified = BAB_DateTime::fromIsoDateTime($arr['form_lastupdate']);
			
			// create calendar period
			
			$period = $this->CalendarPeriod(0,0);
			$period->setDates($dtstart, $dtend);
			
			$period->setProperty('UID', $arr['form_id']);
			$period->setUiIdentifier('forms_'.$id_table.'_'.$arr['form_id']);
			$period->setProperty('SUMMARY', $arr['SUMMARY']);
			
			if (isset($arr['DESCRIPTION']))
			{
				$period->setProperty('DESCRIPTION', $arr['DESCRIPTION']);
			}
			
			if (isset($arr['CATEGORIES']))
			{
				$period->setProperty('CATEGORIES', $arr['CATEGORIES']);
			}
			
			if (isset($arr['LOCATION']))
			{
				$period->setProperty('LOCATION', $arr['LOCATION']);
			}
			
			$period->setProperty('ORGANIZER;CN='.bab_getUserName($arr['form_create_user']), 'MAILTO:'.bab_getUserEmail($arr['form_create_user']));
			$period->setProperty('CREATED', $created->getICal(true));
			$period->setProperty('LAST-MODIFIED', $lastmodified->getICal(true));
			
			$period->setProperty('CLASS', 'PUBLIC');
			$period->addRelation('PARENT', $calendar);
			
			$collection->addPeriod($period);
			$periods[] = $period;
		}
	}
}