<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';


function form_getLinkedValue($col_ref, $col_to_get, $value)
	{
	// deprecated function, does not work with level 3 links, used only for shopping cart form type

	$db = &$GLOBALS['babDB'];
	
	list($name,$id_table,$function) = $db->db_fetch_array($db->db_query("
	
				SELECT 
					name,
					id_table,
					field_function 
				FROM 
					".FORM_TABLES_FIELDS." 
				WHERE 
					id='".$db->db_escape_string($col_ref)."'"
				
				));

	$query_obj = new form_queryObj($id_table);
	
	$test1 = $query_obj->addTableCol($col_to_get,'opt');
	$test2 = $query_obj->addTableCol($col_ref,'val');

	if (false === $test1)
		{
		return form_translate("Error : this link can't be used");
		}
	else
		{
		$query_obj->addWhere('p.'.form_col_name($col_ref,$name, $function).'=\''.$db->db_escape_string($value).'\'');
		}

	$query = &$query_obj->getQuery();

	$arr = $db->db_fetch_array($db->db_query($query));

	return $arr[1];

	}



/**
 * Trouver les options de la liste deoulante
 * @param int 	$col_ref				Column used for value in form
 * @param int 	$col_to_get				Column displayed in option tag
 * @param int 	$col_init				Column used to compare with default value
 * @param int 	$col_optgroup			Column used for optgroup
 * @param int 	$id_filter				
 */
function form_getResOptions( $col_ref, $col_to_get, $col_init, $id_filter, $col_optgroup )
	{
	$db = &$GLOBALS['babDB'];

	list($name,$id_table,$function) = $db->db_fetch_array($db->db_query("SELECT name,id_table,field_function FROM ".FORM_TABLES_FIELDS." WHERE id='".$db->db_escape_string($col_ref)."'"));

	$query_obj = new form_queryObj($id_table);
	if (!$query_obj->addTableCol($col_to_get,'opt'))
		{
		$GLOBALS['babBody']->msgerror = form_translate("Error : this link can't be used");
		return false;
		}
		
	$query_obj->addTableCol($col_ref,'val');
	$query_obj->addTableCol($col_init,'init');
	
	
	
	if ($col_optgroup > 0)
	{
		// $col_optgroup colonne choisie dans filtrer sur 
		// trouver l'id de la colonne d'$id_table qui est relie la table de $col_optgroup
		
		$query = "SELECT lnk.id 
				FROM 
					".FORM_TABLES_FIELDS." opt_disp, 
					".FORM_TABLES_FIELDS." opt_lnk, 
					".FORM_TABLES_FIELDS." lnk 
				WHERE 
					opt_disp.id=".$db->quote($col_optgroup)." 
					AND opt_lnk.id_table=opt_disp.id_table 
					AND opt_lnk.id=lnk.link_table_field_id 
					AND lnk.id_table=".$db->quote($id_table)."
		";
		
		bab_debug($query);
		
		if ($optgroupfield = $db->db_fetch_assoc($db->db_query($query)))
		{
		
			$col = $query_obj->addLink2('optgroup', $optgroupfield['id'], $col_optgroup);
			$query_obj->cols['optgroup'] = $col;
			
			$query_obj->orderby[] = 'optgroup ASC';
		}
	}

	if ($id_filter > 0)
		{
		$query_obj->setFilter($id_filter);
		}
	$query_obj->orderby[] = 'opt ASC';
	$query = &$query_obj->getQuery();
	return $db->db_query($query);
	}

function form_getResLnk( $id_lnk, $col_to_get, $id_filter )
	{
	$db = &$GLOBALS['babDB'];
	
	$lnk = $db->db_fetch_array($db->db_query("SELECT * FROM ".FORM_TABLES_LNK." WHERE id ='".$db->db_escape_string($id_lnk)."'"));

	$query_obj = new form_queryObj($lnk['id_table2']);
	
	if ($lnk['lnk_type'] == 0)
		{
			
		$query_obj->addTableCol($col_to_get,'opt');
		$query_obj->cols['val'] = array(0, 'p.form_id');
		}
	elseif ($lnk['lnk_type'] == 1)
		{
			
		$query_obj->addTableCol($col_to_get, 'opt');
		$query_obj->addTableCol($lnk['id_field2'], 'val');
		}

	if ($id_filter > 0)
		{
		$query_obj->setFilter($id_filter);
		}
	$query_obj->orderby[] = 'opt ASC';
	$query = &$query_obj->getQuery();
	return $GLOBALS['babDB']->db_query($query);
	}



class form_queryObj
{
	function form_queryObj($id_table, $approb = true)
	{
	$this->db = &$GLOBALS['babDB'];
	$this->id_table = $id_table;

	// approbations

	$res = $this->db->db_query("
					SELECT 
						id 
					FROM 
						".FORM_TABLES." 
					WHERE
						approbation = '1' AND created = 'Y'
						");

	$this->approbation = array();

	while ($arr = $this->db->db_fetch_array($res))
		{
		$this->approbation[$arr['id']] = 1;
		}

	// tables de liaison

	$res = $this->db->db_query("
					SELECT 
						l.*,
						f1.name field1, f1.field_function ff1,
						f2.name field2, f2.field_function ff2,
						f3.name lnk_f1, f3.field_function ff3,
						f4.name lnk_f2, f4.field_function ff4
					FROM 
						".FORM_TABLES_LNK." l
					LEFT JOIN 
						".FORM_TABLES_FIELDS." f1
						ON f1.id=l.id_field1
					LEFT JOIN 
						".FORM_TABLES_FIELDS." f2
						ON f2.id=l.id_field2
					LEFT JOIN 
						".FORM_TABLES_FIELDS." f3
						ON f3.id=l.id_lnk_f1
					LEFT JOIN 
						".FORM_TABLES_FIELDS." f4
						ON f4.id=l.id_lnk_f2
					");

	$this->lnk = array();

	while($arr = $this->db->db_fetch_array($res))
		{
		if ($arr['lnk_type'] == 0)
			{
			$this->lnk[$arr['id']] = array(
									'id_table2' => $arr['id_table2'],
									'lnk_type' => $arr['lnk_type']
									);
			}
		else
			{
			$this->lnk[$arr['id']] = array(
									'id_table2' => $arr['id_table2'],
									'id_lnk_table' => $arr['id_lnk_table'],
									'field1' => form_col_name($arr['id_field1'], $arr['field1'], $arr['ff1']),
									'field2' => form_col_name($arr['id_field2'], $arr['field2'], $arr['ff2']),
									'lnk_f1' => form_col_name($arr['id_lnk_f1'], $arr['lnk_f1'], $arr['ff3']),
									'lnk_f2' => form_col_name($arr['id_lnk_f2'], $arr['lnk_f2'], $arr['ff4']),
									'lnk_type' => $arr['lnk_type']
									);
			}

		}

	


	


	// liaisons

	$reslink = $this->db->db_query("
								SELECT 
									t1.id f1,
									t2.id l2,
									t3.id f2,
									t4.id l3,
									t5.id f3,
									t1.id_table t1,
									t3.id_table t2,
									t5.id_table t3,
									t1.name n1,
									t2.name ln2,
									t3.name n2,
									t4.name ln3,
									t5.name n3,
									t1.field_function fn1,
									t2.field_function lfn2,
									t3.field_function fn2,
									t4.field_function lfn3,
									t5.field_function fn3
								FROM 
									".FORM_TABLES_FIELDS." t1
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t2 
									ON t2.id=t1.link_table_field_id 
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t3 
									ON t3.id_table=t2.id_table 
									AND t3.id<>t1.id 
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t4 
									ON t4.id=t3.link_table_field_id 
								LEFT JOIN
									".FORM_TABLES_FIELDS." t5 
									ON t5.id_table=t4.id_table 
									AND t5.id<>t3.id 
									AND t5.id<>t1.id 
								WHERE 
									t1.id_table='".$this->db->db_escape_string($id_table)."' 
								GROUP BY 
									t1.id,
									t3.id,
									t5.id
								
								");

	$this->link1 = array();
	$this->link2 = array();
	$this->tablecols = array();

	while ($arr = $this->db->db_fetch_assoc($reslink))
		{
		

		if (!empty($arr['t1']) && empty($arr['t2']) && empty($arr['t3']))
			{
			$this->tablecols[$arr['f1']] = form_col_name($arr['f1'],$arr['n1']);

			$this->link1[$arr['f1']] = array(
											't1' => $arr['t1'],
											'f1' => $this->col_name($arr['f1'],$arr['n1'],$arr['fn1'])
											);
			}

		if (!empty($arr['t1']) && !empty($arr['t2']) && empty($arr['t3']))
			{
			$this->tablecols[$arr['f2']] = form_col_name($arr['f2'],$arr['n2']);

			$this->link2[$arr['f1'].'-'.$arr['f2']] = array(
											't1' => $arr['t1'],
											'f1' => $this->col_name($arr['f1'],$arr['n1'],$arr['fn1']),
											't2' => $arr['t2'],
											'f2' => $this->col_name($arr['f2'],$arr['n2'],$arr['fn2']),
											'l2' => $this->col_name($arr['l2'],$arr['ln2'],$arr['lfn2'])
											);
			
			}

		if (!empty($arr['t1']) && !empty($arr['t2']) && !empty($arr['t3']))
			{
			$this->tablecols[$arr['f3']] = form_col_name($arr['f3'],$arr['n3']);

			$this->link3[$arr['f1'].'-'.$arr['f3']] = array(
											't1' => $arr['t1'],
											'f1' => $this->col_name($arr['f1'],$arr['n1'],$arr['fn1']),
											't2' => $arr['t2'],
											'f2' => $this->col_name($arr['f2'],$arr['n2'],$arr['fn2']),
											'l2' => $this->col_name($arr['l2'],$arr['ln2'],$arr['lfn2']),
											't3' => $arr['t3'],
											'f3' => $this->col_name($arr['f3'],$arr['n3'],$arr['fn3']),
											'l3' => $this->col_name($arr['l3'],$arr['ln3'],$arr['lfn3'])
											);
			}

		}

	

	$this->used_tables = array();
	$this->where	= array();
	$this->join		= array(form_tbl_name($id_table).' p');
	$this->addtable($id_table,'p',$approb);
	$this->leftjoin = array();
	$this->cols		= array();
	$this->allcols	= array();
	$this->groupby	= array();
	$this->orderby	= array();
	$this->searchCol= array();
	$this->alias	= array();
	$this->filters	= array();
	$this->querylnk = array(); // tables de liaisons utilis�e dans la requete

	}
	
	
	
	function col_name($id_field, $name, $function)
	{
		if ('formula' === $function)
		{
			$res = $this->db->db_query("SELECT id_table, field_formula FROM ".FORM_TABLES_FIELDS." WHERE id=".$this->db->quote($id_field));
			$arr = $this->db->db_fetch_assoc($res);
			
			return $this->formula2query($arr['field_formula'], form_tbl_name($arr['id_table']));
		}
		
		return form_col_name($id_field, $name, $function);
	}
	
	

	function formula2query($formula,$table)
	{
	global $babDB;
	$level = 0;
	$query = '';
	$evaluate = array();
	$arr = explode(' ',$formula);
	$previous_func = false;
	
	foreach ($arr as $el)
		{
		$key = substr($el,0,1);
		$el = substr($el,1);

		if (!isset($evaluate[$level]))
			$evaluate[$level] = array();

		switch ($key)
			{
			case 'V':
				$evaluate[$level][] = "'".$babDB->db_escape_string(str_replace('%20',' ',$el))."'";
				break;
			case 'O':
				
				$char = $previous_func ? ',' : '';

				if ($el == ')')
					{
					$query .= implode($char, $evaluate[$level]).$el;
					$level--;
					$previous_func = false;
					}
				elseif ($el == '(')
					{
					$query .= implode($char, $evaluate[$level]).$el;
					$level++;
					}
				elseif (in_array($el, array('+','-','*','/','%')))
					$evaluate[$level][] = $el;
				break;
			case 'F':
				if (is_numeric($el))
					{
					if (isset($this->tablecols[$el]))
						{
						$evaluate[$level][] = $table.'.'.$this->tablecols[$el];
						}
					else {
						// colonne non trouve dans la table, proablement car c'est une liaison
						
						$res = $babDB->db_query("SELECT name FROM ".FORM_TABLES_FIELDS." WHERE id=".$babDB->quote($el));
						if ($tmp = $babDB->db_fetch_assoc($res))
							{
								$evaluate[$level][] = $table.'.'.form_col_name($el, $tmp['name']);
							}
						}
					}
				elseif (in_array($el,array('CONCAT(','SUM(','MAX(','MIN(','UNIX_TIMESTAMP(','DATE(','TIME(','YEAR(','DAY(','MONTH(')))
					{
					$query .= $el;
					$evaluate[$level] = array();
					$level++;
					$previous_func = true;
					}
				break;

			}
		}


	if ($level != 0)
		{
		$GLOBALS['babBody']->msgerror = form_translate('Problem with parenthesis in a formula');
		return false;
		}


	if (empty($query) && isset($evaluate[$level]))
		{
		$query = implode('', $evaluate[$level]);
		}

	if (!empty($query))
		{
		return $query;
		}
	
	return false;
	}

	function addTableCol($id, $alias = false)
	{
		static $i = 1;
		
		if (!$alias)
			$alias = $i;

		$i++;
		if (!isset($this->link1[$id]))
			return false;

		$this->cols[$alias] = $this->addLink1($alias, $id);
		return true;
	}

	function addTableCol_NamedAlias($id)
	{
		if (!isset($this->link1[$id]))
			return false;

		$this->allcols[$id] = $this->cols[$id] = $this->addLink1('f'.$id, $id);
		return true;
	}

	function registerFilter($id, $col, $id_table_field, $filter_on_dir, $filter_on, $filter_type)
	{
		global $babDB;
		$value = form_getFilterValue($id, $filter_on_dir, $filter_on, $filter_type );

		if ($value !== false)
			{

			$filterType = form_getFilterType();
			if (in_array($filter_type, array(6,7)) && substr_count($value,',') > 0)
				{
				$value = sprintf($filterType[$filter_type][0],implode("','",explode(',',$babDB->db_escape_string($value))));
				}
			else
				{
				$value = sprintf($filterType[$filter_type][0], $babDB->db_escape_string($value));
				}

			
			$this->addWhere($col[1].' '.$value);
			}

	}

	function addCol(&$arr, $print)
	{
	if (isset($arr['search']) && $arr['search'] == 'Y')
		{
		$this->searchCol[$arr['id']] = 1;
		}

	if (isset($this->link1[$arr['id_table_field']])) // liaison niveau 1
		{
		$col = $this->addLink1($arr['id'],$arr['id_table_field']);
		}
	
	if (isset($this->link2[$arr['id_table_field'].'-'.$arr['id_table_field_link']])) // liaison niveau 2
		{
		$col = $this->addLink2($arr['id'],$arr['id_table_field'], $arr['id_table_field_link']);
		}

	if (isset($this->link3[$arr['id_table_field'].'-'.$arr['id_table_field_link']])) // liaison niveau 3
		{
		$col = $this->addLink3($arr['id'],$arr['id_table_field'], $arr['id_table_field_link']);
		}

	if (!empty($arr['id_table_field_lnk']) && empty($arr['id_table_field'])) // table de liaison niveau 1
		{
		$col = $this->addLnk1($arr['id'], $arr['id_lnk'], $arr['id_table_field_lnk']);
		}

	if (!empty($arr['id_table_field_lnk']) && !empty($arr['id_table_field'])) // table de liaison niveau 2
		{
		$col = $this->addLnk2($arr['id'], $arr['id_lnk'], $arr['id_table_field_lnk'], $arr['id_table_field']);
		}

	if (isset($col) && in_array($arr['id_print'],$print))
		{
		$this->cols[$arr['id']] = $col; // affichage
		}

	if (isset($col))
		{
		$this->allcols[$arr['id']] = $col; // collones non affich�es mais utilis�es dans la recherche et les filtres
		}

	if (!empty($arr['filter_on_dir']) || '' !== $arr['filter_on'])
		{
		$this->registerFilter($arr['id'], $col, $arr['id_table_field'], $arr['filter_on_dir'], $arr['filter_on'], $arr['filter_type']);
		}

	}


	function addLink1($id_field, $id_table_field)
	{
		$this->alias[$id_table_field] = 'p';
		return array($id_table_field, 'p.'.$this->link1[$id_table_field]['f1']);
	}


	function addLink2($id_field, $id_table_field,  $id_table_field_link)
	{
		$link = & $this->link2[$id_table_field.'-'.$id_table_field_link];

		if (isset($this->alias[$id_table_field]))
			{
			$alias = &$this->alias[$id_table_field];
			}
		else
			{
			$alias = $this->alias[$id_table_field] = 'lj'.count($this->leftjoin);

			$this->leftjoin[] = 'LEFT JOIN '.form_tbl_name($link['t2']).' '.$alias.' '."\n\t".'ON '.$alias.'.'.$link['l2'].'=p.'.$link['f1'].' AND '.$alias.'.form_filed=\'N\''."\n";

			$this->addtable($link['t2'], $alias);
			}
	
		return array($id_table_field_link, $alias.'.'.$link['f2'], $alias.'.form_id');
	}

	function addLink3($id_field, $id_table_field,  $id_table_field_link)
	{
		$alias = 'lj'.count($this->leftjoin);
		$link = & $this->link3[$id_table_field.'-'.$id_table_field_link];
		$table2 = form_tbl_name($link['t2']);
		$table3 = form_tbl_name($link['t3']);

		$this->leftjoin[] = 'LEFT JOIN '.$table2.' '.$alias.'2 '."\n\t".'ON '.$alias.'2.'.$link['l2'].'=p.'.$link['f1'].' '."\n".'LEFT JOIN '.$table3.' '.$alias.'3 '."\n\t".'ON '.$alias.'3.'.$link['l3'].'='.$alias.'2.'.$link['f2'].' AND '.$alias.'3.form_filed=\'N\''."\n";

		$this->addtable($link['t2'], $alias.'2');
		$this->addtable($link['t3'], $alias.'3');

		return array($id_table_field_link, $alias.'3.'.$link['f3'], $alias.'3.form_id');
	}


	function addLnk1($id_field, $id_lnk, $id_table_field_lnk)
	{

		$lnk = &$this->lnk[$id_lnk];
		$alias = 'lj'.count($this->leftjoin);


		if ($lnk['lnk_type'] == 0)
			{
			$table_lnk = FORM_TABLES_LNK_VALUES;
			$lnk_f1 = 'row1';
			$lnk_f2 = 'row2';
			$field1 = 'form_id';
			$field2 = 'form_id';
			$table2 = form_tbl_name($lnk['id_table2']);
			$auto = $alias.'1.id_lnk=\''.$id_lnk.'\' AND ';
			}
		else
			{
			$table_lnk = form_tbl_name($lnk['id_lnk_table']);
			$lnk_f1 = &$lnk['lnk_f1'];
			$lnk_f2 = &$lnk['lnk_f2'];
			$field1 = &$lnk['field1'];
			$field2 = &$lnk['field2'];
			$table2 = form_tbl_name($lnk['id_table2']);
			$auto = '';
			}

		$approbation = isset($this->approbation[$lnk['id_table2']]) ? ' AND '.$alias.'2.form_approb=\'1\'' : '';

		if (isset($this->querylnk[$id_lnk])) {

			$this->leftjoin[] = 'LEFT JOIN '.$table2.' '.$alias.'2 '."\n\t".'ON '.$this->querylnk[$id_lnk].'.'.$lnk_f2.'='.$alias.'2.'.$field2.' AND '.$alias.'2.form_filed=\'N\''."\n";
			}
		else {
			$this->leftjoin[] = 'LEFT JOIN '.$table_lnk.' '.$alias.'1 '."\n\t".'ON '.$auto.' p.'.$field1.'='.$alias.'1.'.$lnk_f1.' '."\n".'LEFT JOIN '.$table2.' '.$alias.'2 '."\n\t".'ON '.$alias.'1.'.$lnk_f2.'='.$alias.'2.'.$field2.' AND '.$alias.'2.form_filed=\'N\''."\n";

			$this->querylnk[$id_lnk] = $alias.'1';
			}
		
		

		$this->addtable($lnk['id_table2'], $alias.'2', false);
		list($name,$function) = $this->db->db_fetch_array($this->db->db_query("SELECT name,field_function FROM ".FORM_TABLES_FIELDS." WHERE id='".$this->db->db_escape_string($id_table_field_lnk)."'"));

		return array($id_table_field_lnk, $alias.'2.'.form_col_name($id_table_field_lnk, $name, $function), $alias.'2.form_id');
	}

	function addLnk2($id_field, $id_lnk, $id_table_field_lnk, $id_table_field)
	{


		$res = $this->db->db_query("SELECT f1.name,f2.id_table,f2.id,f2.name FROM ".FORM_TABLES_FIELDS." f1, ".FORM_TABLES_FIELDS." f2 WHERE f1.id='".$this->db->db_escape_string($id_table_field)."' AND f2.id=f1.link_table_field_id");
		list($tmp, $id_table1,$id_table_field_link, $name_table_field_link) = $this->db->db_fetch_array($res);

		$name_table_field = form_col_name($id_table_field, $tmp);

		$name_table_field_link = form_col_name($id_table_field_link, $name_table_field_link);


		$lnk = &$this->lnk[$id_lnk];
		$alias = 'lj'.count($this->leftjoin);

		if ($lnk['lnk_type'] == 0)
			{
			$table_lnk = FORM_TABLES_LNK_VALUES;
			$lnk_f1 = 'row1';
			$lnk_f2 = 'row2';
			$field1 = 'form_id';
			$field2 = 'form_id';
			$table2 = form_tbl_name($lnk['id_table2']);
			$auto = $alias.'1.id_lnk=\''.$this->db->db_escape_string($id_lnk).'\' AND ';
			}
		else
			{
			$table_lnk = form_tbl_name($lnk['id_lnk_table']);
			$lnk_f1 = &$lnk['lnk_f1'];
			$lnk_f2 = &$lnk['lnk_f2'];
			$field1 = &$lnk['field1'];
			$field2 = &$lnk['field2'];
			$table2 = form_tbl_name($lnk['id_table2']);
			$auto = '';
			}

		$approbation = isset($this->approbation[$lnk['id_table2']]) ? ' AND '.$alias.'2.form_approb=\'1\'' : '';

		
		if (isset($this->querylnk[$id_lnk])) {

			$this->leftjoin[] = 'LEFT JOIN '.form_tbl_name($id_table1).' '.$alias.'3 '."\n\t".'ON p.'.$name_table_field.'='.$alias.'3.'.$name_table_field_link.' '."\n".' LEFT JOIN '.$table2.' '.$alias.'2 '."\n\t".'ON '.$this->querylnk[$id_lnk].'.'.$lnk_f2.'='.$alias.'2.'.$field2.' AND '.$alias.'2.form_filed=\'N\''."\n";
			}
		else {
			$this->leftjoin[] = 'LEFT JOIN '.form_tbl_name($id_table1).' '.$alias.'3 '."\n\t".'ON p.'.$name_table_field.'='.$alias.'3.'.$name_table_field_link.' '."\n".' LEFT JOIN '.$table_lnk.' '.$alias.'1 '."\n\t".'ON '.$auto.' '.$alias.'3.'.$field1.'='.$alias.'1.'.$lnk_f1.' '."\n".' LEFT JOIN '.$table2.' '.$alias.'2 '."\n\t".'ON '.$alias.'1.'.$lnk_f2.'='.$alias.'2.'.$field2.' AND '.$alias.'2.form_filed=\'N\''."\n";

			$this->querylnk[$id_lnk] = $alias.'1';
			}
		
		
		
		

		$this->addtable($lnk['id_table2'], $alias.'2', false);

		list($name,$function) = $this->db->db_fetch_array($this->db->db_query("SELECT name,field_function FROM ".FORM_TABLES_FIELDS." WHERE id='".$this->db->db_escape_string($id_table_field_lnk)."'"));

		return array($id_table_field_lnk, $alias.'2.'.form_col_name($id_table_field_lnk, $name, $function), $alias.'2.form_id');
	}

	function setFilter($id_filter)
	{
	
	$filters = form_getFilterType();

	$res = $this->db->db_query("
				SELECT 
					ff.*,
					f.name,
					f.field_function 
				FROM 
					".FORM_TABLES_FILTERS_FIELDS." ff,
					".FORM_TABLES_FIELDS." f
				WHERE 
					ff.id_filter='".$this->db->db_escape_string($id_filter)."' 
					AND f.id=ff.id_table_field
					");

	while ($arr = $this->db->db_fetch_array($res))
		{
		$name = form_col_name($arr['id_table_field'],$arr['name'], $arr['field_function']);

		if (in_array($arr['operator'], array(6,7)) && substr_count($arr['comparator'],',') > 0)
			{
			$arr['comparator'] = implode("','", explode(',',$this->db->db_escape_string($arr['comparator'])));
			}
		else
			{
			$arr['comparator'] = $this->db->db_escape_string($arr['comparator']);
			}

		if ($arr['id_lnk'] == 0) 
			{
			$this->addWhere('p.'.$name.' '.sprintf($filters[$arr['operator']][0],$arr['comparator']) );
			}
		else
			{
			static $i = 0;
			$lnk = $this->db->db_fetch_array($this->db->db_query("
						
						SELECT 
							lnk.* ,
							f1.name name_lnk_f1,
							f1.field_function ff_lnk_f1,
							f3.name name_field1,
							f3.field_function ff_field1

						FROM 
							".FORM_TABLES_LNK." lnk
						LEFT JOIN 
							".FORM_TABLES_FIELDS." f1 ON f1.id = lnk.id_lnk_f1 

						LEFT JOIN 
							".FORM_TABLES_FIELDS." f3 ON f3.id = lnk.id_field1 

						WHERE 
							lnk.id='".$this->db->db_escape_string($arr['id_lnk'])."'"));

			if ($lnk['lnk_type'] == 0)
				{
				$this->addJoin(FORM_TABLES_LNK_VALUES,'jlnk'.$i);
				$this->addWhere('jlnk.id_lnk=\''.$lnk['id'].'\'');

				$lnk_f1 = 'row1';
				$field1 = 'form_id';
				}
			else
				{
				$this->addJoin(form_tbl_name($lnk['id_lnk_table']),'jlnk'.$i);
				
				$lnk_f1 = form_col_name($lnk['id_lnk_f1'],$lnk['name_lnk_f1'], $lnk['ff_lnk_f1']);
				$field1 = form_col_name($lnk['id_field1'],$lnk['name_field1'], $lnk['ff_field1']);
				}

			$this->addWhere('p.'.$field1.'=jlnk'.$i.'.'.$lnk_f1);
			$this->addWhere('jlnk'.$i.'.'.$name.' '.sprintf($filters[$arr['operator']][0],$arr['comparator']));

			$i++;
			}
		}

	$res = $this->db->db_query("
				SELECT 
					ff.ordercol
				FROM 
					".FORM_TABLES_FILTERS." ff 
				WHERE 
					ff.id='".$this->db->db_escape_string($id_filter)."'
				");

	$arr = $this->db->db_fetch_array($res);
	$this->addTableCol_NamedAlias($arr['ordercol']);
	$this->addOrderBy($arr['ordercol'], 'ASC');
	}

	function addOrderBy($field, $type)
	{
	if (isset($this->allcols[$field]))
		$this->orderby[] = 'f'.$field.' '.$type;
	}

	function addGroupBy($field)
	{
	if (isset($this->allcols[$field]))
		$this->groupby[] = 'f'.$field;
	}

	function addWhere($txt)
	{
	$this->where[] = $txt;
	}

	function addJoin($table,$alias)
	{
	$this->join[] = $table.' '.$alias;
	}

	function search($word)
	{
	$cols = array();
	foreach ($this->allcols as $id => $arr)
		{
		if (isset($this->searchCol[$id]))
			{
			$cols[] = $arr[1]." LIKE '%".$this->db->db_escape_like($word)."%'\n";
			}
		}
	$this->addWhere('('.implode("\t OR ",$cols)."\t)\n");
	}

	function searchOnField($id_field, $word)
	{
	if (isset($this->allcols[$id_field][1]))
		{
		$this->addWhere($this->allcols[$id_field][1].' LIKE \'%'.$this->db->db_escape_like($word).'%\'');	
		}
	}
	
	
	function searchOnEnumSet($id_field, $word)
	{
	if (isset($this->allcols[$id_field][1]))
		{
		if (is_array($word))
			{
			$this->addWhere($this->allcols[$id_field][1].' IN('.$this->db->quote($word).')');
			}
		else 
			{
			$this->addWhere($this->allcols[$id_field][1].' = '.$this->db->quote($word).'');	
			}
		}	
	}

	function searchOnLink($id, $id_table_field_link, $id_table_field_lnk, $word)
	{
	$col = & $this->allcols[$id];
	
	if ((!empty($id_table_field_link) && $col[0] == $id_table_field_link) || (!empty($id_table_field_lnk) && $col[0] == $id_table_field_lnk))
		{
		if (is_array($word))
			{
			$this->addWhere($col[2].' IN('.$this->db->quote($word).')');
			}
		else
			{
			$this->addWhere($col[2].' = \''.$this->db->db_escape_string($word).'\'');
			}
		}
	}

	function searchOnInterval($id_field, $begin, $end)
	{
	if (isset($this->allcols[$id_field][1]) && !empty($begin) && !empty($end))
		{
		
		$ts = bab_mktime($end);
		$end = date('Y-m-d', mktime(0, 0, 0, date('m', $ts), 1+date('d', $ts), date('Y', $ts)));
		
		$this->addWhere(
					$this->allcols[$id_field][1].' >= '.$this->db->quote($begin).' AND '.	$this->allcols[$id_field][1].' < '.$this->db->quote($end)
			);
		}
	}

	function addtable($id, $alias, $approb = true)
	{	
	$this->used_tables[$id] = $alias;
	if (isset($this->approbation[$id]))
		{
		if (true === $approb)
			$this->addWhere($alias.'.form_approb=\''.FORM_APPROVED.'\'');
		elseif (false === $approb)
			$this->addWhere($alias.'.form_approb=\''.FORM_WAITING.'\'');
		elseif (0 == strlen($approb))
			$this->addWhere($alias.'.form_approb=\'-1\'');
		elseif (FORM_CREATOR_OR_APPROVED === $approb)
			$this->addWhere('('.$alias.'.form_approb=\''.FORM_APPROVED.'\' OR p.form_create_user=\''.$GLOBALS['BAB_SESS_USERID'].'\')');
		else
			$this->addWhere($alias.'.form_approb IN('.$approb.')');
		}
	}

	/**
	 * 
	 * @return string
	 */
	function getQuery()
	{
	$res = $this->db->db_query("SELECT id, id_table, field_function, field_formula FROM ".FORM_TABLES_FIELDS." WHERE id_table IN(".$this->db->quote(array_keys($this->used_tables)).") AND field_function<>''");

	$field_function = array();

	while ($arr = $this->db->db_fetch_array($res))
		{
		$field_function[$arr['id']] = $arr;
		}

	$cols = array('p.form_id');
	foreach ($this->cols as $id => $arr)
		{
		$num_prefix = is_numeric($id) ? 'f' : '';
		$id_field = &$arr[0];
		$name = &$arr[1];
		if (isset($field_function[$id_field]))
			{

			if ($field_function[$id_field]['field_function'] == 'index')
				{
				$cols[] = $this->used_tables[$field_function[$id_field]['id_table']].'.form_id '.$num_prefix.$id;
				}
			elseif ($field_function[$id_field]['field_function'] == 'approb')
				{
				$cols[] = 'CASE '.$this->used_tables[$field_function[$id_field]['id_table']].".form_approb 
	WHEN '0' THEN '".form_translate('Waiting')."' 
	WHEN '1' THEN '".form_translate('Validated')."' 
	WHEN '2' THEN '".form_translate('Refused')."' 
END ".$num_prefix.$id;
				}
			elseif ($field_function[$id_field]['field_function'] == 'num_approb')
				{
				$cols[] = ''.$this->used_tables[$field_function[$id_field]['id_table']].".form_approb ".$num_prefix.$id;
				}
			elseif ($field_function[$id_field]['field_function'] == 'formula')
				{

				$formula = $this->formula2query($field_function[$id_field]['field_formula'], $this->used_tables[$field_function[$id_field]['id_table']]);
				$cols[] = $formula.' '.$num_prefix.$id;
				}
			}
		elseif (!empty($name))
			{
			$cols[] = $name.' '.$num_prefix.$id;
			}
		}

	

	$query = 'SELECT '.implode(", \n",$cols)."\n".' FROM '.implode(', ',$this->join)."\n".' '.implode(' ',$this->leftjoin);
	
	if (count($this->where) > 0)
		{
		$query .= ' WHERE '.implode(' AND ',$this->where);
		}

	if (count($this->groupby) > 0)
		{
		$query .= ' GROUP BY '.implode(', ',$this->groupby);
		}

	if (count($this->orderby) > 0)
		{
		$query .= ' ORDER BY '.implode(', ',$this->orderby);
		}

	$this->viewQuery($query);

	return $query;
	}


function getValueFromOption($id_table_field, $id_table_field_link, $value)
	{
	if (isset($this->link2[$id_table_field.'-'.$id_table_field_link])) // liaison niveau 2
		{
		$link = & $this->link2[$id_table_field.'-'.$id_table_field_link];
		
		$req = "SELECT ".$link['f2']." FROM ".form_tbl_name($link['t2'])." WHERE ".$link['l2']."='".$this->db->db_escape_string($value)."'";
		if (isset($this->approbation[$link['t2']]))
			{
			$req .= " AND form_approb='1'";
			}
		$res = $this->db->db_query($req);
		}

	if (isset($this->link3[$id_table_field.'-'.$id_table_field_link])) // liaison niveau 3
		{
		$link = & $this->link3[$id_table_field.'-'.$id_table_field_link];
		$table2 = form_tbl_name($link['t2']);
		$table3 = form_tbl_name($link['t3']);

		$req = "SELECT t3.".$link['f3']." FROM ".$table2." t2, ".$table3." t3 WHERE t3.".$link['l3']."=t2.".$link['f2']." AND t2.".$link['l2']."='".$this->db->db_escape_string($value)."'";

		if (isset($this->approbation[$link['t3']]))
			{
			$req .= " AND t3.form_approb='1'";
			}
		$res = $this->db->db_query($req);

		}

	if (!isset($res))
		{
		return form_translate("Error : this link can't be used");
		}

	list($value) = $this->db->db_fetch_array($res);
	return $value;
	}


function viewQuery($str)
	{
	bab_debug($str);
	}
}
?>
