<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

class form_appCreateCls
{
	
	private $tables;
	
	/**
	 * 
	 * @param array $tables
	 * @param array $description
	 * @param array $options
	 * @param array $use_in_list
	 * @return unknown_type
	 */
	public function __construct(Array $tables, Array $descriptions, Array $options, Array $use_in_list)
	{
		foreach($tables as $id_table)
		{
			$this->tables[$id_table] = new form_appCreateTable($id_table);
			$this->tables[$id_table]->updateDescriptions($descriptions[$id_table]);
			$this->tables[$id_table]->createListForm($use_in_list[$id_table]);
			
			if (isset($options[$id_table]['edit']))
			{
				$this->tables[$id_table]->createEditForm();
			}
			
			if (isset($options[$id_table]['visualisation']))
			{
				$this->tables[$id_table]->createViewForm();
			}
		}
	}
	
	
	public static function insertStep($id_application, $id_type, $name, $id_form)
	{
		global $babDB;
		
		$babDB->db_query("INSERT INTO ".FORM_APP_STEPS." 
			(
				id_application,
				id_type,
				name,
				id_form
			) 
			VALUES 
			(
				".$babDB->quote($id_application).",
				".$babDB->quote($id_type).",
				".$babDB->quote($name).",
				".$babDB->quote($id_form)."
			)
		");
		
		return $babDB->db_insert_id();
	}
	
	
	public static function insertButton($id_step, $id_type, $name, $id_goto_step)
	{
		global $babDB;
		
		$babDB->db_query("INSERT INTO ".FORM_APP_STEP_BTN." 
			(
				id_step, 
				btn_name, 
				btn_id_type, 
				btn_id_goto_step 
			) 
			VALUES 
			(
				".$babDB->quote($id_step).",
				".$babDB->quote($name).",
				".$babDB->quote($id_type).",
				".$babDB->quote($id_goto_step)."
			)
		");
		
		return $babDB->db_insert_id();
	}
	
	
	
	public function createApplication()
	{
		global $babDB;
		
		if (empty($this->tables))
		{
			throw new ErrorException('No tables');
		}
		
		
		// build application name
		
		$t_arr = array();
		foreach($this->tables as $T)
		{
			$t_arr[] = $T->name;
		}
		
		
		$name = implode(', ', $t_arr);
		
		
		$babDB->db_query("INSERT INTO ".FORM_APP_APPLICATIONS." 
			(
				name
			) 
			VALUES 
			(
				".$babDB->quote($name)."
			)
		");
		
		
		$id_application = $babDB->db_insert_id();
		form_ieO::inserted('application', $id_application);
		
		// create contextual menu
		$id_menu_step = self::insertStep($id_application, 6, form_translate('Menu'), 0);
		
		// set as first step
		
		$babDB->db_query("UPDATE ".FORM_APP_APPLICATIONS." 
			SET id_first_step=".$babDB->quote($id_menu_step)." WHERE id=".$babDB->quote($id_application)." 
		");
		
		$selmenu = 1;
		
		foreach($this->tables as $T)
		{
			/*@var $T form_appCreateTable */
			
			$id_list_step = $T->createListStep($id_application, $id_menu_step);
			
			if (null !== $T->id_edit_form)
			{
				$id_edit_step = $T->createEditStep($id_application, $id_list_step);
				form_appCreateCls::insertButton($id_list_step, 2, form_translate('Create'), $id_edit_step);
			}
			
			if (null !== $T->id_view_form)
			{
				$id_view_step = $T->createViewStep($id_application, $id_list_step);
			}
			
			// insert list tab
			
			$babDB->db_query("INSERT INTO ".FORM_APP_STEP_MENU." 
				(
					id_step, 
					name, 
					id_menustep, 
					selmenu
				) 
			VALUES 
				(
					".$babDB->quote($id_menu_step).",
					".$babDB->quote($T->name).",
					".$babDB->quote($id_list_step).",
					".$babDB->quote($selmenu)."
				)
			");
			
			$selmenu = 0;
		}
	}
	
	
	
	
}




class form_appCreateTable
{
	public $id_table;
	public $id_list_form;
	public $id_edit_form;
	public $id_view_form;
	public $id_step;
	
	public $name;
	public $description;
	
	/**
	 * column in list form
	 * @var int
	 */
	public $id_view_field;
	
	/**
	 * column in list form
	 * @var int
	 */
	public $id_edit_field;
	
	public function __construct($id_table)
	{
		$this->id_table = $id_table;
		
		global $babDB;
		
		$res = $babDB->db_query('SELECT name, description FROM '.FORM_TABLES.' WHERE id='.$babDB->quote($this->id_table));
		$arr = $babDB->db_fetch_assoc($res);
		
		$this->name = $arr['name'];
		$this->description = $arr['description'];
	}
	
	
	/**
	 * 
	 * @param ressource $res	list of table fields
	 * @return unknown_type
	 */
	private function createFields($res, $id_form)
	{
		global $babDB;
		
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$babDB->db_query("INSERT INTO ".FORM_FORMS_FIELDS." 
					(
						id_form,
						id_table_field,
						id_form_element,
						name,
						description,
						id_print,
						ordering
					) 
				VALUES 
					(
						".$babDB->quote($id_form).",
						".$babDB->quote($arr['id']).",
						".$babDB->quote(form_getDefaultFormElement($arr['id'])).",
						".$babDB->quote($arr['name']).",
						".$babDB->quote($arr['description'])." ,
						'1',
						".$babDB->quote($arr['ordering'])."
					)
			");
		}
	}
	
	/**
	 * 
	 * @param int $id_form
	 * @param int $id_print
	 * @param string $name
	 * @return int		ID form field
	 */
	private function addSpecialField($id_form, $id_print, $name)
	{
		global $babDB;
		
		// get max ordering in table
		
		$ordering = 1;
		$res = $babDB->db_query("SELECT ordering FROM ".FORM_FORMS_FIELDS." WHERE id_form=".$babDB->quote($id_form)." ORDER BY ordering DESC");
		if (0 < $babDB->db_num_rows($res))
		{
			$arr = $babDB->db_fetch_assoc($res);
			$ordering = 1 + ((int) $arr['ordering']);
		}
		
		$babDB->db_query("INSERT INTO ".FORM_FORMS_FIELDS." 
					(
						id_form,
						id_print,
						name,
						ordering
					) 
				VALUES 
					(
						".$babDB->quote($id_form).",
						".$babDB->quote($id_print).",
						".$babDB->quote($name).",
						".$babDB->quote($ordering)."
					)
			");
		
		return $babDB->db_insert_id();
	}
	
	
	
	private function checkDuplicate($name)
	{
		global $babDB;
		
		$query = 'SELECT * FROM 
			'.FORM_FORMS." f 
		WHERE 
			f.name LIKE '".$babDB->db_escape_like($name)."'";
		
		$res = $babDB->db_query($query);
		
		if (0 !== $babDB->db_num_rows($res))
		{
			throw new ErrorException(sprintf(form_translate('A form with the name %s allready exists'), $name));
			return false;
		}
		
		return true;
	}
	
	
	private function insertForm($id_type, $name, $rows_per_page = 0)
	{
		global $babDB;
		
		if (empty($name))
		{
			throw new ErrorException('missing name');
		}
		
		
		$babDB->db_query('INSERT INTO '.FORM_FORMS.' 
			(
				id_table,
				id_type,
				name,
				description,
				rows_per_page
			) 
			VALUES 
			(
				'.$babDB->quote($this->id_table).',
				'.$babDB->quote($id_type).',
				'.$babDB->quote($name).',
				'.$babDB->quote($this->description).',
				'.$babDB->quote($rows_per_page).'
			)
		');
		
		
		$id_form = $babDB->db_insert_id();
		
		// add form to current workspace
		
		form_ieO::inserted('form', $id_form);
		
		
		return $id_form;
	}
	
	
	
	private function insertLink($id_application, $id_field, $id_step)
	{

		global $babDB;
	
		$babDB->db_query("INSERT INTO ".FORM_APP_LINKS." 
			(
				id_application, 
				id_field, 
				id_step
			) 
		VALUES 
			(
				".$babDB->quote($id_application).",
				".$babDB->quote($id_field).",
				".$babDB->quote($id_step)."
			)
		");
	}
	
	
	
	/**
	 * Update table fields
	 * @param array $descriptions
	 * @return form_appCreateTable
	 */
	public function updateDescriptions($descriptions)
	{
		global $babDB;
		
		foreach($descriptions as $id_field => $description)
		{
			$babDB->db_query('
				UPDATE '.FORM_TABLES_FIELDS.' 
				SET 
					description='.$babDB->quote($description).' 
				WHERE 
					id='.$babDB->quote($id_field).' AND 
					id_table='.$babDB->quote($this->id_table)
			);
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param array $fields
	 * @return form_appCreateTable
	 */
	public function createListForm($fields)
	{
		if (!is_array($fields) || 0 === count($fields))
		{
			throw new ErrorException(form_translate('Missing field for list form'));
			return false;
		}
		
		global $babDB;
		
		
		$name = sprintf(form_translate('%s list'), $this->name);
		
		if (!$this->checkDuplicate($name))
		{
			return false;
		}

		$this->id_list_form = $this->insertForm(1, $name, 20);
		
		$res = $babDB->db_query("
			SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table=".$babDB->quote($this->id_table)." AND id IN(".$babDB->quote($fields).")
		");
		
		$this->createFields($res, $this->id_list_form);
		
		$this->id_edit_field = $this->addSpecialField($this->id_list_form, 5, form_translate('Edit')); // link
		$this->id_view_field = $this->addSpecialField($this->id_list_form, 5, form_translate('View')); // link
		
		return true;
	}
	
	
	
	
	public function createEditForm()
	{
		global $babDB;
		
		$name = sprintf(form_translate('%s edit'), $this->name);
		$this->checkDuplicate($name);
		
		$this->id_edit_form = $this->insertForm(2, $name);
		
		$res = $babDB->db_query("
			SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table=".$babDB->quote($this->id_table)." 
		");
		
		$this->createFields($res, $this->id_edit_form);
	}
	
	
	
	public function createViewForm()
	{
		global $babDB;
		
		$name = sprintf(form_translate('%s view'), $this->name);
		$this->checkDuplicate($name);
		
		$this->id_view_form = $this->insertForm(3, $name);
		
		$res = $babDB->db_query("
			SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table=".$babDB->quote($this->id_table)." 
		");
		
		$this->createFields($res, $this->id_view_form);
	}
	
	
	
	/**
	 * 
	 * @param int $id_application
	 * @param int $id_menu_step
	 * @return int
	 */
	public function createListStep($id_application, $id_menu_step)
	{
		$id_step = form_appCreateCls::insertStep($id_application, 1, form_translate('List'), $this->id_list_form);
		return $id_step;
	}
	
	/**
	 * 
	 * @param int $id_application
	 * @param int $id_list_step
	 * @return int
	 */
	public function createEditStep($id_application, $id_list_step)
	{
		$id_step = form_appCreateCls::insertStep($id_application, 1, form_translate('Create / Modify'), $this->id_edit_form);
		
		form_appCreateCls::insertButton($id_step, 1, form_translate('Save'), $id_list_step);
		form_appCreateCls::insertButton($id_step, 2, form_translate('Cancel'), $id_list_step);
		
		$this->insertLink($id_application, $this->id_edit_field, $id_step);
		
		return $id_step;
	}
	
	
	/**
	 * 
	 * @param int $id_application
	 * @param int $id_list_step
	 * @return int
	 */
	public function createViewStep($id_application, $id_list_step)
	{
		$id_step = form_appCreateCls::insertStep($id_application, 1, form_translate('Visualisation'), $this->id_view_form);
		
		form_appCreateCls::insertButton($id_step, 2, form_translate('Ok'), $id_list_step);
		
		$this->insertLink($id_application, $this->id_view_field, $id_step);
		
		return $id_step;
	}
}



function choosetables()
{
	global $babBody;
    class temp
        {
		var $altbg = false;

        function temp()
            {
			global $babDB;

			$this->t_name = form_translate('Name');
			$this->t_next = form_translate('Next');
			$this->t_use_in_app = form_translate('Use in application');
			$this->t_options = form_translate('Options');
			
			$this->t_create_edit = form_translate('Create the creation/modification form');
			$this->t_create_visualisation = form_translate('Create the visualisation form');
			$this->t_create_app = form_translate('Create the application and steps');

			$this->res = $babDB->db_query("
				SELECT t.id, t.name 
					FROM ".FORM_TABLES." t,
					".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					t.id = w.id_object 
					AND w.type='table' 
					AND w.id_workspace=".$babDB->quote(form_getWorkspace())." 
				ORDER BY t.name 
			");
			}

		function getnexttable()
			{
			global $babDB;
				
			if ($arr = $babDB->db_fetch_array($this->res))
				{
				$this->id = bab_toHtml($arr['id']);
				$this->name = bab_toHtml($arr['name']);
				$this->altbg = !$this->altbg;
				return true;
				}
			else
				{
				return false;
				}
			}

        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."appcreate.html", "choosetables" ));
}



function edit_appcreate($tables)
{
global $babBody;
    class temp
    {
		var $altbg = false;

        function temp($tables)
        {
			$this->tables = $tables;
			global $babDB, $babBody;

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			
			$this->t_record = form_translate('Create');
			$this->t_use_in_list = form_translate('Use in list form');
			$this->t_options = form_translate('Options');
			
			$this->t_create_edit = form_translate('Create the creation/modification form');
			$this->t_create_visualisation = form_translate('Create the visualisation form');
			$this->t_create_app = form_translate('Create the application and steps');

			$this->rest = $babDB->db_query("
				SELECT t.* 
				FROM 
					".FORM_TABLES." t,
					".FORM_WORKSPACE_ENTRIES." w 
				
				WHERE 
					t.id = w.id_object 
					AND w.type='table' 
					AND w.id_workspace=".$babDB->quote(form_getWorkspace())." 
					AND t.id IN(".$babDB->quote($tables).")
				
				GROUP BY t.id 
				ORDER BY t.name
			");
			
			
			// afficher un message si des forms du meme espace utilisent une des tables
			
			$res = $babDB->db_query("
				SELECT f.id  
				FROM 
					".FORM_FORMS." f,
					".FORM_WORKSPACE_ENTRIES." w
				WHERE 
					f.id = w.id_object 
					AND w.type='form'
					AND w.id_workspace=".$babDB->quote(form_getWorkspace())."
					AND f.id_table IN(".$babDB->quote($tables).") 
					
				GROUP BY f.id 
			");
			
			if (0 !== $babDB->db_num_rows($res))
			{
				if (1 === $babDB->db_num_rows($res))
				{
					$babBody->addError(form_translate('Warning, there is one form in this workspace associated to the selected table(s)'));
				} else {
					$babBody->addError(sprintf(form_translate('Warning, there are %d forms in this workspace associated to the selected table(s)'), $babDB->db_num_rows($res)));
				}
			} 
			
		}

		function getnexttable()
		{
			global $babDB;
			
			if ($table = $babDB->db_fetch_assoc($this->rest))
			{
				$this->id_table = (int) $table['id'];
				$this->t_tablename = bab_toHtml(sprintf(form_translate('Table : %s'), $table['name']));
				
				$this->resf = $babDB->db_query("
					SELECT f.*, t.sql_type 
						FROM ".FORM_TABLES_FIELDS." f 
							LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t ON t.id=f.id_type 
					WHERE f.id_table='".$babDB->db_escape_string($this->id_table)."' 
						ORDER BY f.name 
				");
				
				return true;
			}
			
			return false;
		}
			
		function getnextfield()
		{
			global $babDB;
				
			if ($this->field = $babDB->db_fetch_array($this->resf))
				{
				$this->field['id'] = bab_toHtml($this->field['id']);
				$this->field['name'] = bab_toHtml($this->field['name']);
				$this->field['description'] = bab_toHtml($this->field['description']);

				$this->altbg = !$this->altbg;
				return true;
			}
			else
				{
				return false;
			}
		}
    }
    
    
    if (is_array(bab_pp('description')))
    {
    	try {
    		
    		$option = bab_pp('option');
    		
    		$save = new form_appCreateCls(bab_pp('id_table'), bab_pp('description'), $option, bab_pp('use_in_list'));
    		
    		if (isset($option['application']))
    		{
    			$save->createApplication();
    		}
    		
    		$url = $GLOBALS['babAddonUrl']."main&idx=list_tables";
			header("location:".$url);
			exit;
    	} catch(ErrorException $e) {
    		$babBody->addError($e->getMessage());
    	}
    }
        
        
        
    $tp = new temp($tables);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."appcreate.html", "edit" ));
}




// main

$idx = bab_rp('idx', 'tables');


$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");

switch($idx)
{
	
	case "edit":
		$babBody->addItemMenu("edit", form_translate("Create Application"),$GLOBALS['babAddonUrl']."appcreate&idx=edit");
		$babBody->title = form_translate("Create an application");
		edit_appcreate(bab_rp('id_table'));
		break;
		
	default:
	case 'tables':
		$babBody->addItemMenu("tables", form_translate("Create Application"),$GLOBALS['babAddonUrl']."appcreate&idx=tables");
		$babBody->title = form_translate("Select tables");
		choosetables();
		break;
}


$babBody->setCurrentItemMenu($idx);

?>