<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
include_once $GLOBALS['babInstallPath']."utilit/devtools.php";
include_once dirname(__FILE__).'/define.php';
include_once dirname(__FILE__).'/functions.php';



function forms_onSectionCreate(&$title, &$content)
{
	if (!defined('FORM_GEST_GROUPS')) 
		return false;

	$user = form_getUserFormAcces();

	$section = new form_app_list($user['SECTION']);
	if ($section->countrows() > 0)
		{
		$title = form_getConfigValue('section_title');
		$content = $section->printout_section();
		return true;
		}
	else
		return false;
}

function forms_searchinfos()
	{
	include_once $GLOBALS['babAddonPhpPath']."search.php";
	$db = &$GLOBALS['babDB'];

	$res = $db->db_query("
			SELECT 
				l.id_application,
				t.id id_table_search,
				fo2.id id_form_view
			FROM 
				".FORM_APP_LINKS." l,
				".FORM_FORMS_FIELDS." fi,
				".FORM_FORMS." fo1,
				".FORM_TABLES." t,
				".FORM_APP_STEPS." s,
				".FORM_FORMS." fo2 
			WHERE
				l.id_field = fi.id 
				AND fi.id_form = fo1.id 
				AND fo1.id_table = t.id 
				AND l.id_step = s.id 
				AND s.id_form = fo2.id 
				AND fo2.id_type = '3'
			");

	while ( $arr = $db->db_fetch_array($res))
			{
			if ( bab_isAccessValid(FORM_APP_GROUPS,$arr['id_application']) && bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$arr['id_table_search']) && bab_isAccessValid(FORM_FORMS_GROUPS,$arr['id_form_view']))
				{
				return form_getConfigValue('section_title');
				}
			}

	return false;
	}

function forms_searchresults($q1, $q2, $option, $pos, $nb_result)
	{
	if (!isset($GLOBALS['form_searchObj']))
		$GLOBALS['form_searchObj'] = new form_search($q1, $q2, $option, $pos, $nb_result);
	return $GLOBALS['form_searchObj']->setnextsearch($pos);
	}



function forms_upgrade($version_base,$version_ini)
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	

	$functionalities = new bab_functionalities();
	$functionalities->registerClass('Func_CalendarBackend_Forms', dirname(__FILE__) . '/calendar.backend.class.php');
	
	// attach events
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
	bab_addEventListener('bab_eventUserDeleted'						,'forms_onUserDeleteEvt'				,'addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventUserAttachedToGroup'				,'forms_onUserAttachToGroup'			,'addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventUserDetachedFromGroup'			,'forms_onUserDetachFromGroup'			,'addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventGroupDeleted'					,'forms_onGroupDeleteEvt'				,'addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventBeforeWaitingItemsDisplayed'		,'forms_getWaitingItemsDisplay'			,'addons/forms/approbincl.php'	, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventBeforeSiteMapCreated'			,'forms_onBeforeSiteMapCreated'			,'addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventEditorFunctions'					,'forms_onEditorFunctions'				,'addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventBeforePeriodsCreated'			,'forms_onBeforePeriodsCreated'			,'addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	bab_addEventListener('bab_eventCollectCalendarsBeforeDisplay'	,'forms_onCollectCalendarsBeforeDisplay','addons/forms/events.php'		, $GLOBALS['babAddonFolder']);
	
	
	require( $GLOBALS['babAddonPhpPath']."upgrade.php" );
	return form_upgrade();
}


function forms_onDeleteAddon()
{
	if (is_dir($GLOBALS['babAddonUpload']))
		{
		form_deldir($GLOBALS['babAddonUpload']);
		}
		
	// detach events
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
	bab_removeEventListener('bab_eventUserDeleted'					,'forms_onUserDeleteEvt'				,'addons/forms/events.php');
	bab_removeEventListener('bab_eventUserAttachedToGroup'			,'forms_onUserAttachToGroup'			,'addons/forms/events.php');
	bab_removeEventListener('bab_eventUserDetachedFromGroup'		,'forms_onUserDetachFromGroup'			,'addons/forms/events.php');
	bab_removeEventListener('bab_eventGroupDeleted'					,'forms_onGroupDeleteEvt'				,'addons/forms/events.php');
	bab_removeEventListener('bab_eventBeforeWaitingItemsDisplayed'	,'forms_getWaitingItemsDisplay'			,'addons/forms/approbincl.php');
	bab_removeEventListener('bab_eventBeforeSiteMapCreated'			,'forms_onBeforeSiteMapCreated'			,'addons/forms/events.php');
	bab_removeEventListener('bab_eventEditorFunctions'				,'forms_onEditorFunctions'				,'addons/forms/events.php');
	bab_removeEventListener('bab_eventBeforePeriodsCreated'			,'forms_onBeforePeriodsCreated'			,'addons/forms/events.php');
	bab_removeEventListener('bab_eventCollectCalendarsBeforeDisplay','forms_onCollectCalendarsBeforeDisplay','addons/forms/events.php');
	
	
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	
	$functionalities = new bab_functionalities();
	$functionalities->unregister('CalendarBackend/Forms');
	
	return true;
}
?>
