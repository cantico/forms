<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';


class data_table_editor
	{
		
	function data_table_editor($id,$approbation)
		{
		$this->db = $GLOBALS['babDB'];
		$this->id_table = $id;
		$this->table_name = form_tbl_name($id);

		if (!data_table_editor::tableCreated($this->id_table) )
			{
			$this->db->db_query("CREATE TABLE `".$this->db->db_escape_string($this->table_name)."` (
				`form_id` int(10) unsigned NOT NULL auto_increment,
				`form_lastupdate` datetime NOT NULL default '0000-00-00 00:00:00',
				`form_lastupdate_user` int(10) unsigned NOT NULL default '0',
				`form_create` datetime NOT NULL default '0000-00-00 00:00:00',
				`form_create_user` int(10) unsigned NOT NULL default '0',
				`form_locked` enum('Y','N') NOT NULL default 'N',
				`form_filed` enum('Y','N') NOT NULL default 'N',".
				($approbation ? '`form_approb` TINYINT UNSIGNED DEFAULT \'0\' NOT NULL, ' : '')
				."PRIMARY KEY  (`form_id`))");

			$this->db->db_query("ALTER TABLE `".$this->db->db_escape_string($this->table_name)."` ADD INDEX ( `form_locked` )");
			$this->db->db_query("ALTER TABLE `".$this->db->db_escape_string($this->table_name)."` ADD INDEX ( `form_filed` )");

			$this->describe = array();
			if ($approbation)
				$this->describe[] = 'form_approb';
			}
		else
			{
			$this->describe = array();
			$res = $this->db->db_query("DESCRIBE `".$this->db->db_escape_string($this->table_name)."`");
			while ($arr = $this->db->db_fetch_array($res))
				{
				if (!in_array($arr['Field'],array('form_lastupdate')))
					{
					$this->describe[] = $arr['Field'];
					}
				}
			}
			
		$this->db->db_query("UPDATE ".FORM_TABLES." SET created='Y' WHERE id='".$this->db->db_escape_string($id)."'");
		$this->save_approbation_state($approbation);
		}
		
	/**
	 * @static
	 */
	function tableCreated($id) {
		global $babDB;
		$table_name = form_tbl_name($id);
		$arr = $babDB->db_fetch_array($babDB->db_query("SHOW TABLES LIKE '".$babDB->db_escape_like($table_name)."'"));
		if (!isset($arr[0]) || $arr[0] != $table_name ) {
			return false;
		}
		return true;
	}
		
	
	function get_type($id,$link)
		{
		if (is_numeric($link) && $link > 0)
			{
			
			$req = "SELECT  t.col_type, f.field_function FROM ".FORM_TABLES_FIELDS." f LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t ON  f.id_type = t.id WHERE f.id='".$this->db->db_escape_string($link)."'";

			list($type,$func) = $arr = $this->db->db_fetch_array($this->db->db_query($req));

			if ($func == 'index')
				return 'int(10) unsigned';
			else
				return $type;

			}
		else
			{
			list($type) = $this->db->db_fetch_array($this->db->db_query("SELECT  col_type FROM ".FORM_TABLES_FIELDS_TYPES." WHERE id='".$this->db->db_escape_string($id)."'"));

			return $type;
			}
		
		
		}

	function ctrl_default($tp,$default)
		{
		$enum = substr_count($tp,'enum') > 0 ? true : false;
		$set = substr_count($tp,'set') > 0 ? true : false;
		if ($enum || $set)
			{
			$allvalues = enumToArray($tp);
			if ($enum && !in_array($default,$allvalues))
				{
				return '';
				}
			if ($set)
				{
				$selected = explode(',',$default);
				foreach($selected as $val)
					{
					if (!in_array($val,$allvalues))
						return '';
					}
				}
			}
		return $default;
		}


	function save_approbation_state($state)
		{
		if ($state && !in_array('form_approb',$this->describe))
			{
			$this->db->db_query('ALTER TABLE `'.$this->db->db_escape_string($this->table_name).'` ADD `form_approb` TINYINT UNSIGNED DEFAULT \'0\' NOT NULL');
			$this->db->db_query('UPDATE `'.$this->db->db_escape_string($this->table_name).'` SET `form_approb`=\'1\'');
			}
		elseif (!$state)
			{
			require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
			$res = $this->db->db_query("SELECT * FROM `".FORM_APPROB_SCHI."` WHERE id_table='".$this->db->db_escape_string($this->id_table)."'");
			while ($arr = $this->db->db_fetch_array($res))
				{
				bab_WFDeleteInstance($arr['idschi']);
				}
			$this->db->db_query("DELETE FROM `".FORM_APPROB_SCHI."` WHERE id_table='".$this->db->db_escape_string($this->id_table)."'");
			$this->remove_field('form_approb');
			}

		}
		
	function add_field($name, $type, $link, $default, $auto_increment)
		{
		if (empty($name)) {
			return false;
			}
			
		if (empty($type) && empty($link) ) 
			{
			$this->remove_field($name);
			return false;
			}
			

		if (!in_array($name,$this->describe))
			{
			$tp = $this->get_type($type,$link);
			$default = $this->ctrl_default($tp,$default);
			if (!empty($tp))
				{
				$req = "ALTER TABLE `".$this->db->db_escape_string($this->table_name)."` ADD `".$this->db->db_escape_string($name)."` ".$tp."";
				if ($default != '')
					$req .= " DEFAULT '".$this->db->db_escape_string($default)."'";
				$req .= " NOT NULL";
				if ($auto_increment) {
					$req .= " AUTO_INCREMENT";
				}
				$this->db->db_query($req);
				}
			}
		else {
			$this->modify_field($name, $type, $link, $default, $auto_increment);
			}

		}
		
	function get_old_col_name($id)
		{
		$req = "SELECT name FROM ".FORM_TABLES_FIELDS." WHERE id='".$this->db->db_escape_string($id)."'";
		list($name) = $this->db->db_fetch_array($this->db->db_query($req));
		$this->old_col_name = form_col_name($id,$name);
		}
		
	function modify_field($name, $type, $link, $default, $auto_increment)
		{
		if (empty($name)) 
			return false;
		if (!isset($this->old_col_name)) $this->old_col_name = $name;
		if (!in_array($this->old_col_name,$this->describe))
			{
			$this->add_field($name, $type, $link, $default, $auto_increment);
			}
		elseif (empty($type) && empty($link))
			{
			$this->remove_field($name);
			}
		elseif (is_numeric($type))
			{
			$tp = $this->get_type($type,$link);
			$default = $this->ctrl_default($tp,$default);

			if (!empty($tp))
				{
				$req = "ALTER TABLE `".$this->db->db_escape_string($this->table_name)."` CHANGE `".$this->db->db_escape_string($this->old_col_name)."` `".$this->db->db_escape_string($name)."` ".$tp."";
				if ($default != '')
					$req .= " DEFAULT '".$this->db->db_escape_string($default)."'";
				$req .= " NOT NULL";
				if ($auto_increment) {
					$req .= " AUTO_INCREMENT";
					}
				$this->db->db_query($req);
				}
			}
		}
	function remove_field($name)
		{
		if (in_array($name,$this->describe))
			$this->db->db_query("ALTER TABLE `".$this->db->db_escape_string($this->table_name)."` DROP `".$this->db->db_escape_string($name)."`");
		}
	function remove_table()
		{
		$this->db->db_query("DROP TABLE ".$this->db->db_escape_string($this->table_name)."");
		}
	}




?>