<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

function form_staticContext($param = NULL) {
	static $memory = NULL;
	if (NULL === $memory) {
		$memory = $param;
	}
	return $memory;
}



function form_isError($id_field, $status = NULL) {
	
	static $arr = array();
	if (isset($arr[$id_field])) {
		return $arr[$id_field];
	} elseif (NULL != $status) {
		$arr[$id_field] = $status;
	}
	return false;
}


/**
 * Application manager
 * display forms
 */
class form_application
{

var $next_row = '';
var $form_row;

function form_application()
	{
	$this->html_file = form_getConfigValue('html_file');
	if (substr_count($this->html_file,'/') == 0)
		{
		$this->html_file = $GLOBALS['babAddonHtmlPath'].$this->html_file;
		}

	$this->id_app = bab_rp('id_app');
	$this->id_step = form_currentIdStep();
	$this->id_form = bab_rp('id_form');
	$this->form_row = form_currentDbRow();
	if (NULL === $this->form_row) {
		$this->form_row = bab_rp('form_row');
	}
	
	$this->form_menu = isset($GLOBALS['form_menu_enter_menu']) ? $GLOBALS['form_menu_enter_menu'] : bab_rp('form_menu');
	$this->url_popup = bab_rp('popup',0);
	$this->form_links = array();
	$this->access = false;
	$this->filters = '';
	$this->format_date = form_getConfigValue('date_format');

	$this->t_download = form_translate('Download');
	$this->t_form = form_translate('Form');


	$this->db = &$GLOBALS['babDB'];	
	$this->formAddonUrl = $GLOBALS['formAddonUrl'];

	if (is_numeric($this->id_app))
		{
		if (bab_isAccessValid(FORM_APP_GROUPS,$this->id_app))
			$this->access = true;

		$query = "SELECT * FROM ".FORM_APP_APPLICATIONS." WHERE id='".$this->db->db_escape_string($this->id_app)."'";
		$this->app = $this->db->db_fetch_array($this->db->db_query($query));

		if ($this->app['https'] == 'Y')
			{
			form_urlToHttps($GLOBALS['formAddonUrl']);
			}
		
		if (is_numeric($this->id_step))
			{
			$query = "SELECT * FROM ".FORM_APP_STEPS." WHERE id_application='".$this->db->db_escape_string($this->id_app)."' AND id='".$this->db->db_escape_string($this->id_step)."'";
			$this->step = $this->db->db_fetch_assoc($this->db->db_query($query));
			}


		if (empty($this->id_step) && isset($this->step['id'])) {
			$this->id_step = $this->step['id'];
		}

		if (!empty($this->step['id_form']))
			{
			$this->resbtn = $this->db->db_query("
			
				SELECT 
					b.*, 
					s.id_form, 
					f.id_type, 
					f.waiting_modify, 
					f.id_table, 
					t.approbation 
				FROM 
					".FORM_APP_STEP_BTN." b 
					LEFT JOIN ".FORM_APP_STEPS." s ON s.id=b.btn_id_goto_step 
					LEFT JOIN ".FORM_FORMS." f ON f.id = s.id_form 
					LEFT JOIN ".FORM_TABLES." t ON t.id = f.id_table 
				WHERE 
					btn_id_type<>'6' AND id_step='".$this->db->db_escape_string($this->id_step)."'
			");

			$this->countbtn = $this->db->db_num_rows($this->resbtn);
			}

		$this->app_message = isset($this->step['description']) ? $this->step['description'] : ''; // default

		if (!empty($GLOBALS['trt_step_id_form']))
			{
			$this->id_form = $this->id = $GLOBALS['trt_step_id_form'];
			}
		elseif (!empty($this->step['id_form']))
			{
			$this->id_form = $this->id = $this->step['id_form'];
			}
		elseif (!isset($this->id))
			{
			$GLOBALS['babBody']->msgerror = form_translate('The application cannot reach a valid form');
			return false;
			}

		

		if (!empty($this->id))
			{
			if (!bab_isAccessValid(FORM_FORMS_GROUPS,$this->id))
				{
				$this->msgerror = 'No access right on the form';
				$this->access = false;
				}

			$res = $this->db->db_query("
									SELECT 
										f.id, 
										s.id_form, 
										l.id_step, 
										fo.id_type, 
										f.id_print, 
										l.id_index, 
										l.id_if_field, 
										l.if_operator, 
										l.if_value 
									FROM 
										".FORM_APP_LINKS." l,
										".FORM_FORMS_FIELDS." f,
										".FORM_APP_STEPS." s
									LEFT JOIN
										".FORM_FORMS." fo ON fo.id=s.id_form 
									WHERE 
										l.id_application='".$this->db->db_escape_string($this->id_app)."'
									AND f.id_form = '".$this->db->db_escape_string($this->id)."'
									AND f.id=l.id_field 
									AND s.id=l.id_step 
									");
			
			$operators = form_getFilterType();
			while($arr = $this->db->db_fetch_array($res))
				{
				$operator = '';
				if (!empty($arr['id_if_field']))
					{
					$operator = $operators[$arr['if_operator']][2];
					}
				else
					{
					$arr['if_value'] = '';
					}

				$this->form_links[$arr['id']] = array(
													'id_form'	=>	$arr['id_form'],
													'id_type'	=>	isset($arr['id_type']) ? $arr['id_type'] : 0,
													'id_step'	=>	$arr['id_step'],
													'id_index'	=>  $arr['id_index'],
													'popup'		=>	$arr['id_print'] == 8 ? 1 : 0,
													'operator'	=>	$operator,
													'id_field'	=>	$arr['id_if_field'],
													'value'		=>	$arr['if_value']
													);
				}
			
			$this->viewCtxMenu($this->app['name']);
			$this->keyCtrl();
			}
		}
	elseif (bab_isAccessValid(FORM_FORMS_GROUPS,$this->id))
		$this->access = true;


	// gestion form_row

	}

function start()
	{
	if (!empty($this->id))
		{
		list($type) = $this->db->db_fetch_array($this->db->db_query("SELECT id_type FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->id)."'"));
		$url = $GLOBALS['formAddonUrl']."form&idx=".urlencode($type)."&id_app=".urlencode($this->id_app)."&id_form=".urlencode($this->id);
		
		header("location:".$url);
		}
	}

function nextpreviousrow($tablename,$order)
	{
	$id_index = array();

	$query = "SELECT form_id FROM ".$this->db->db_escape_string($tablename)." ";

	$arr =  $this->db->db_fetch_array( $this->db->db_query("DESCRIBE ".$this->db->db_escape_string($tablename)." form_approb"));
	if ( $arr[0] == 'form_approb' )
		{
		$query .= "WHERE form_approb='1'";
		}

	$res = $this->db->db_query($query." ORDER BY ".$this->db->db_escape_string($order));
	while (list($form_id) = $this->db->db_fetch_array($res))
		{
		$id_index[] = $form_id;
		}

	if (!empty($this->form_row))
		{
		$k = array_search($this->form_row,$id_index);
		$this->prev_row = isset($id_index[$k-1]) ? $id_index[$k-1] : '';
		$this->next_row = isset($id_index[$k+1]) ? $id_index[$k+1] : '';
		}

	$this->first_row = isset($id_index[0]) ? $id_index[0] : 0;
	}

function getnextbutton(&$skip)
	{
	$this->btn = $this->db->db_fetch_array($this->resbtn);
	if (!$this->btn) {
		return false;
	}

	if ($this->btn['btn_id_type'] == 4 && empty($this->prev_row)) {
		$skip = true;
		return true;
	}

	if ($this->btn['btn_id_type'] == 5 && empty($this->next_row)) {
		$skip = true;
		return true;
	}

	if (!empty($this->btn['id_form']) && !bab_isAccessValid(FORM_FORMS_GROUPS,$this->btn['id_form'])) {
		form_debug(sprintf(form_translate("The button %s is disabled because there is no access rights to view the target form"),$this->btn['btn_name']));
		$skip = true;
		return true;
	}

	if (
		2 == $this->btn['id_type'] 
		&& 2 > ((int) $this->btn['waiting_modify']) 
		&& !empty($this->form_row) 
		&& !empty($this->btn['id_table']) 
		&& 1 == $this->btn['approbation'] 
		&& $this->btn['id_table'] == $this->arr['id_table']
		) {
		$table = form_tbl_name($this->btn['id_table']);
		$res = $this->db->db_query("SELECT form_approb, form_create_user FROM ".$this->db->db_escape_string($table)." WHERE form_id='".$this->db->db_escape_string($this->form_row)."'");
		list($form_approb, $form_create_user) = $this->db->db_fetch_array($res);
		if (FORM_APPROVED != $form_approb) {
			if (0 == ((int) $this->btn['waiting_modify'])) {
				form_debug(sprintf(form_translate("The button %s is disabled because the row is in waiting or refused state"),$this->btn['btn_name']));
				$skip = true;
				return true;
			}

			if (1 == ((int) $this->btn['waiting_modify']) && $form_create_user != $GLOBALS['BAB_SESS_USERID']) {
				form_debug(sprintf(form_translate("The button %s is disabled because the current user is not the creator of the row and the row is in waiting or refused state"),$this->btn['btn_name']));
				$skip = true;
				return true;
			}
		}
	}

	return true;
	}


function removeCtxMenu()
	{
	unset($_SESSION['form_ctxMenu_'.$this->id_app]);
	$this->form_menu = '';
	}

function viewCtxMenu($app)
	{
	if (isset($_SESSION['form_ctxMenu_'.$this->id_app]))
		{
		$menu = unserialize($_SESSION['form_ctxMenu_'.$this->id_app]);

		$forms = array();
		foreach($menu as $arr)
			{
			$forms[$arr['id_form']] = 1;
			}
		
		if (isset($forms[$this->id]))
			{
			foreach($menu as $arr)
				{
				if (bab_isAccessValid(FORM_FORMS_GROUPS, $arr['id_form']))
					{
					$form_row = empty($this->form_row) && !empty($this->form_menu) ? $this->form_menu : $this->form_row;
					
					$GLOBALS['babBody']->addItemMenu( $arr['id_form'], $arr['name'], $GLOBALS['formAddonUrl']."form&idx=".$arr['idx']."&id_app=".$this->id_app."&id_step=".$arr['id_step']."&id_form=".$arr['id_form']."&form_row=".$form_row."&form_menu=".$this->form_menu);
					}
				}

			return;
			}
		}
	//$GLOBALS['babBody']->addItemMenu($this->id, $app,'');
	}


function keyCtrl()
	{
	$res = $this->db->db_query("
						SELECT 
							t.id, 
							t.name, 
							t.field_function,
							t.id_table,
							f.field_use_key_val
						FROM 
							".FORM_FORMS_FIELDS." f,
							".FORM_TABLES_FIELDS." t 
						WHERE 
							id_form='".$this->db->db_escape_string($this->id)."' 
						AND t.id=f.id_table_field
						AND f.field_use_key_val > 0
						");

	if ($this->db->db_num_rows($res) == 0)
		return;

	if ($this->db->db_num_rows($res) > 1)
		$GLOBALS['babBody']->msgerror = form_translate('Warning, there is more than one custom key defined on this form');

	$arr = $this->db->db_fetch_array($res);

	$col = form_col_name($arr['id'],$arr['name'], $arr['field_function']);

	$query = "SELECT form_id FROM ".form_tbl_name($arr['id_table'])." WHERE ".$col."='".$this->db->db_escape_string(form_getMetaSelectValue($arr['field_use_key_val']))."'";
	
	$res = $this->db->db_query($query);
	if ($this->db->db_num_rows($res) == 0)
		$this->form_row = '';
	else
		list($this->form_row) = $this->db->db_fetch_array($res);
	}

function dataTypeCtrl()
	{

	/* ca ne fonctionne pas dans le cas d'1 lien ou d'1 type direct ! */

	$this->data_type = 'varchar';
	if (isset($this->col['sql_type']))
		{
		switch($this->col['sql_type'])
			{
			case 23:
			case 24:
			case 25:
			case 26:
				$this->data_type = 'blob';
				break;
			case 18:
			case 19:
			case 20:
				$this->data_type = 'text';	
				break;
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
				$this->data_type = 'date';
				break;
			case 21:
			case 22:
				$this->data_type = 'list';
				break;
			case 1:
			case 3:
			case 4:
			case 5:
				$this->data_type = 'integer';
				break;
			}
		}

	if (!empty($this->col['id_lnk']))
		{
		$this->data_type = 'list';
		}
	}

function styleCtrl()
	{
	$file = & $this->arr['css_file'];
	$path = $GLOBALS['babInstallPath'].'skins/ovidentia/templates/addons/forms/styles/';
	if (is_file($path.$file))
		$this->css_file = $path.$file;
	else
		$this->css_file = $path.FORM_CSS;
	}

function listCtrl()
	{
	$this->js_listCtrl = true;

	$this->t_rows_per_page = form_translate('Rows/page');
	$this->t_opt = form_translate('Default');
	$this->t_opt_hide = form_translate('Hide optional columns');
	$this->t_opt_view = form_translate('Show optional columns');
	$this->t_allow_delete = form_translate('Del.');
	$this->js_confirm_delete = form_translate('Do you really want to delete these items?');

	// Propri�t�s descripteur table
	
	$this->arr = $this->db->db_fetch_assoc($this->db->db_query("
					SELECT 
						f.*,
						t.name name_order_by
					FROM 
						".FORM_FORMS." f 
					LEFT JOIN 
						".FORM_TABLES_FIELDS." t 
					ON t.id=f.id_order_by 
					WHERE f.id='".$this->db->db_escape_string($this->id)."'
					"));
					
	if (!$this->arr) {
		return false;
	}

	$this->arr['buttons_next_previous'] = $this->arr['buttons_next_previous'] == 'Y';

	if (!empty($this->arr['id']) && !bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$this->arr['id_table']))
		{
		$this->msgerror = 'No access right on the table to view';
		$this->access = false;
		}

	form_auto_filing($this->arr['id_table']);
	
	$this->arr['rows_per_page'] = $this->arr['rows_per_page'] > 0 ? $this->arr['rows_per_page'] : 30;
	$this->pos = !empty($_REQUEST['pos']) ? $_REQUEST['pos'] : 0;
	
	// Propri�t�s colonnes

	$this->opt_hide = bab_rp('opt_hide');
	$this->opt_view = bab_rp('opt_view');

	$this->col_print = array(3,1,5,6,7,8);

	if ($this->opt_hide) array_shift($this->col_print);
	if ($this->opt_view) array_push($this->col_print,4);

	$this->list_links = false;

	$res = $this->db->db_query("SELECT DISTINCT id_print FROM ".FORM_FORMS_FIELDS." WHERE id_print IN (3,4) AND (id_type <> '0' || id_table_field <> '0') AND name <> '' AND id_form='".$this->db->db_escape_string($this->id)."'");

	while($opt = $this->db->db_fetch_array($res))
		{
		if ($opt['id_print'] == 3 && !$this->opt_hide)
			$this->url_opt_hide =  true;
		if ($opt['id_print'] == 4 && !$this->opt_view)
			$this->url_opt_view = true;
		if ($opt['id_print'] == 3 || $opt['id_print'] == 4)
			$this->url_opt = true;

		$this->list_links = true;
		}

	$this->rescol = $this->db->db_query(
				"SELECT f.*,
					t.name name_table_field,
					t.field_function,
					t.field_formula,
					t.link_table_field_id,
					h.id help_id,
					h.name help_name,
					h.help_title 
				FROM 
					".FORM_FORMS_FIELDS." f
				LEFT JOIN 
					".FORM_TABLES_FIELDS." t
					ON t.id=f.id_table_field 
				LEFT JOIN ".FORM_HELP." h 
					ON h.id=f.field_help 
				WHERE 
					f.id_form='".$this->db->db_escape_string($this->id)."' 
				ORDER BY f.ordering");


	$this->queryObj = new form_queryObj($this->arr['id_table'],$this->arr['approb_list_status']);

	while($arr = $this->db->db_fetch_assoc($this->rescol))
		{
		$this->queryObj->addCol($arr, $this->col_print);
		}
	$this->db->db_data_seek($this->rescol,0);

	$this->filters = isset($GLOBALS['FORMS_filters']) ? $GLOBALS['FORMS_filters'] : (isset($_POST['filters']) ? $_POST['filters'] : '');
	$this->filters = bab_toHtml($this->filters);
	
	return true;
	}


function orderCtrl()
	{
	if (!empty($_POST['orderby']) && !empty($_POST['ordertype']))
		{
		$this->arr['id_order_by'] = $_POST['orderby'];
		$this->arr['order_type'] = $_POST['ordertype'];
		}

	if (!empty($this->arr['id_order_by']))
		{
		$this->queryObj->addOrderBy($this->arr['id_order_by'],$this->arr['order_type']);
		$this->arr['orderby'] = $this->arr['id_order_by'];
		}

	if (!empty($this->arr['id_order_by2']))
		{
		$this->queryObj->addOrderBy($this->arr['id_order_by2'],$this->arr['order_type2']);
		}

	if (!empty($this->arr['id_group_by']))
		{
		$this->queryObj->addGroupBy($this->arr['id_group_by']);
		}

	if (!empty($this->arr['id_group_by2']))
		{
		$this->queryObj->addGroupBy($this->arr['id_group_by2']);
		}
	}


function approbCtrl($id_table)
	{
	if (!empty($id_table))
		{
		$req = "SELECT approbation FROM ".FORM_TABLES." WHERE id='".$this->db->db_escape_string($id_table)."'";
		list($approbation) = $this->db->db_fetch_array($this->db->db_query($req));
		return $approbation ? true : false;
		}
	return false;
	}







function filterCtrl()
	{
	// filters

	if ($this->arr['list_filed_rows'] == 'HIDE')
		{
		$this->queryObj->addWhere('p.form_filed = \'N\'');
		}
	}

function searchCtrl()
	{

	$this->t_search = form_translate('Search');
	$this->form_search = '';

	if (!empty($_REQUEST['form_search']))
		{
		$words = explode(' ',$_REQUEST['form_search']);

		foreach($words as $word)
			{
			form_formatDate($word);
			$this->queryObj->search($word);
			
			}
		$this->form_search = $_REQUEST['form_search'];
		}

	if ($this->arr['search_box'] == 1 && isset($_POST))
		{
		
		$res = $this->db->db_query("SELECT f.search_type, f.id, f.id_table_field_link, f.id_table_field_lnk FROM ".FORM_FORMS_FIELDS." f WHERE f.search_crit = '1' AND f.search='Y'");

		while ($arr = $this->db->db_fetch_array($res))
			{
			
			if (isset($_POST['form_crit_'.$arr['id']]) || isset($_POST['form_crit_'.$arr['id'].'_b']) || isset($_POST['form_crit_'.$arr['id'].'_e']))
				{
				switch ($arr['search_type'])
					{
					case 1:
					case 4:
						if ('' === $_POST['form_crit_'.$arr['id']]) {
							break;
						}
					
						$this->queryObj->searchOnLink($arr['id'], $arr['id_table_field_link'], $arr['id_table_field_lnk'], $_POST['form_crit_'.$arr['id']]);
						break;
					case 2:
					case 3:
						$this->queryObj->searchOnInterval($arr['id'], $_POST['form_crit_'.$arr['id'].'_b'], $_POST['form_crit_'.$arr['id'].'_e'] );
						break;
						
					case 5:
					case 6:
						
						if ('' === $_POST['form_crit_'.$arr['id']]) {
							break;
						}
						
						$this->queryObj->searchOnEnumSet($arr['id'], $_POST['form_crit_'.$arr['id']] );
						break;
					
					default:
					case 0:
						if ('' === $_POST['form_crit_'.$arr['id']]) {
							break;
						}
						
						$this->queryObj->searchOnField($arr['id'], $_POST['form_crit_'.$arr['id']] );
						break;
					}
				}
			}
		}

	}


function cardCtrl($table_id)
	{
	require_once( $GLOBALS['babInstallPath']."addons/forms/card.php");
	$this->form_card = new form_card($this->id_app);
	$tmp = & $this->form_card->get($table_id);
	return is_array($tmp) ? array_filter($tmp) : false;
	}

function incrementCtrl($appli_only = true) {
	
	if ($appli_only && empty($this->id_app))
		return;
	
	if (empty($this->form_row))
		return;
	
	$res = $this->db->db_query("SELECT t.id,t.name,t.id_table FROM ".FORM_FORMS_FIELDS." f, ".FORM_TABLES_FIELDS." t WHERE f.id_form='".$this->db->db_escape_string($this->arr['id'])."' AND f.id_table_field=t.id AND f.int_increment='1'");

	while ($arr = $this->db->db_fetch_array($res)) {
		$colname = form_col_name($arr['id'], $arr['name']);
		$this->db->db_query("UPDATE ".form_tbl_name($arr['id_table'])." SET ".$colname."=".$colname."+1 WHERE form_id='".$this->db->db_escape_string($this->form_row)."'");
		}
	}


function displayManager($html) {

	$this->js_callback_alert = form_translate("This value is allready in the list");
	$javascript = bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'javascript.html');

	$html = $javascript.$html;
	
	if (empty($this->arr['description'])) {
		$GLOBALS['babBody']->setTitle($this->arr['name']);
	} else {
		$GLOBALS['babBody']->setTitle($this->arr['description']);
	}

	if (!empty($_REQUEST['popup']))
		{
		$GLOBALS['babBody']->babPopup($html);
		}
	else
		{
		$GLOBALS['babBody']->babecho($html);
		}

}


function echohtml($file,$template)
	{
	if (!$this->access)
		{
		$this->access_denied();
		return;
		}

	$this->styleCtrl();
	$this->incrementCtrl();

	$this->buttonbar = bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'buttonbar.html');
	$html = bab_printTemplate($this,$file,$template);

	$this->displayManager($html);
	}



function echoOvml($file) {

	if (!$this->access)
		{
		$this->access_denied();
		return;
		}

	$this->styleCtrl();
	$this->incrementCtrl();

	$this->buttonbar = bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'buttonbar.html');

	if (!empty($this->step['ovml_file'])) {
		$file = $this->step['ovml_file'];
	} else {
		$file = 'addons/forms/'.$file;
	}

	form_staticContext($this);
	$html = bab_printOvmlTemplate($file, $_REQUEST);
	$this->displayManager($html);
}



function access_denied()
	{
	if (isset($this->msgerror))
		{
		$GLOBALS['babBody']->msgerror = & form_translate($this->msgerror);
		}
	else
		{
		$GLOBALS['babBody']->msgerror = & form_translate('Form access denied');
		}
	}

function export()
	{
	if (!$this->access)
		{
		$this->access_denied();
		return;
		}
	header('content-type:text/csv');
	header('Content-Disposition: attachment; filename="'.$this->arr['name'].'.csv"');

	$skip = false;
	$first = true;

	while ($this->getnextcol($skip)) {

		if ($this->isColVisible()) {

			if (!$first) echo ',';
			echo '"'.$this->col['name'].'"';
			$first = false;

			}
		}

	echo "\n";
	$first = true;

	while ($this->getnextrow()) {
		while ($this->getnextcol($skip)) {

			if ($this->isColVisible()) {

				if (!$first) echo ',';
				echo '"'.$this->export.'"';
				$first = false;

				}
			}
		echo "\n";
		$first = true;
		}
	die();
	}
}


function pop_closer($msg, $refresh_parent)
	{
	global $babBody;
	class temp
		{
		function temp($msg, $refresh_parent)
			{
			global $tg;
			$this->refresh_parent = $refresh_parent;
			$this->t_close = form_translate("Close");
			$this->msg = $msg;
			$this->parentUrl = $GLOBALS['babUrlScript'].'?tg='.$tg.'&idx='.$_POST['parent_idx'].'&id_form='.$_POST['id_form'].'&pos='.$_POST['pos'].'';
			}
		}
	$temp = new temp($msg, $refresh_parent);
	die( bab_printTemplate($temp,$GLOBALS['babAddonHtmlPath'].'main.html', 'pop_closer') );
	}

// evaluation php des formules
function form_formula($formula,$functions)
	{
	$evaluate = '';
	$arr = explode(' ',$formula);
	
	foreach ($arr as $el)
		{
		$key = substr($el,0,1);
		$el = substr($el,1);

		switch ($key)
			{
			case 'V':
				if (is_numeric($el))
					$evaluate.= $el;
				break;
			case 'O':
				if (in_array($el, array('+','-','*','/','(',')','%')))
					$evaluate.= $el;
				break;
			case 'F':
				if (in_array($el, array_keys($functions)))
					$evaluate.= $functions[$el];
				break;

			}
		}
	
	$result = null;
	if (!empty($evaluate) && false !== @eval('$result = '.$evaluate.';')) {
		return $result;
		}
	trigger_error('Formula : $result = '.$evaluate.";\n <br />");
	$GLOBALS['babBody']->msgerror = form_translate('Formula is incorrect');
	return false;
	}


function form_formula_sc($formula,$product_cols,$qte)
	{
	if (empty($qte))
		{
		trigger_error('Formula : no quantity for product '.implode(',',$product_cols).";\n <br />");
		return false;
		}
	$product_cols['qte'] = $qte;
	return form_formula($formula,$product_cols);
	}


function form_formula_trans($formula,$total,$qte)
	{
	// utilise F1 pour le total
	return form_formula($formula,array(1 => $total,'qte' => $qte));
	}


class form_sheet extends form_application
{
function form_sheet()
	{
	$this->form_application();
	
	$this->t_filename = form_translate('File');
	$this->t_filesize = form_translate('Size');
	$this->t_nofiles = form_translate('No loaded files');
	}


function editColCtrl()
	{
	$req = "SELECT 
				f.*,
				t.name name_table_field, 
				t.link_table_field_id, 
				t.field_function, 
				lnk.id_table, 
				e.name form_element, 
				ty.sql_type,
				h.id help_id,
				h.name help_name,
				h.help_title 
			FROM 
				".FORM_FORMS_FIELDS." f 
			LEFT JOIN ".FORM_TABLES_FIELDS." t
				ON t.id=f.id_table_field 
			LEFT JOIN ".FORM_TABLES_FIELDS." lnk 
				ON lnk.id=t.link_table_field_id 
			LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." ty 
				ON ty.id = t.id_type 
			LEFT JOIN ".FORM_HELP." h 
				ON h.id=f.field_help ,
				".FORM_FORMS_ELEMENTS." e 
			WHERE 
				e.id=f.id_form_element 
				AND f.id_form='".$this->db->db_escape_string($this->id)."' 
				AND f.id_print<>'2' 
			GROUP BY f.id 
			ORDER BY f.ordering ";

	$this->rescol = $this->db->db_query($req);
	$this->countcol = $this->db->db_num_rows($this->rescol);


	$this->col_print = array(1,3,4,6,7,9);

	if (!empty($this->arr['id_table']))
		{
		if ($this->arr['id_type'] == 7)
			$approb = FORM_WAITING;
		elseif($this->arr['id_type'] == 2 || $this->arr['id_type'] == 4)
			{
			if (2 == $this->arr['waiting_modify'])
				$approb = FORM_WAITING.','.FORM_APPROVED.','.FORM_REFUSED;
			elseif (1 == $this->arr['waiting_modify'])
				$approb = FORM_CREATOR_OR_APPROVED;
			else
				$approb = FORM_APPROVED;
			}
		else
			$approb = FORM_WAITING.','.FORM_APPROVED.','.FORM_REFUSED;

		$this->queryObj = new form_queryObj($this->arr['id_table'], $approb);

		while($arr = $this->db->db_fetch_assoc($this->rescol)) {
			if ($arr['id_lnk'] == 0) {  //  && !in_array($arr['sql_type'],array(23,24,25,26))
				$this->queryObj->addCol($arr, $this->col_print);
			}
		}
		$this->db->db_data_seek($this->rescol,0);

		$this->queryObj->addWhere('p.form_id = \''.$this->db->db_escape_string($this->form_row).'\'');
		$req = $this->queryObj->getQuery();
		$this->sheet = $this->db->db_fetch_assoc($this->db->db_query($req));

		if (!empty($this->form_row) && !$this->sheet)
			{
			$this->access = false;
			$this->msgerror = form_translate("Access denied, the row does not exists");
			return;
			}


		$req = "SELECT * FROM ".form_tbl_name($this->arr['id_table'])." WHERE form_id='".$this->db->db_escape_string($this->form_row)."'";
		$this->line = $this->db->db_fetch_assoc($this->db->db_query($req));

		// transformation de la description du formulaire avec les valeures de la requette
		form_setMacro($this->arr['description'], $this->sheet );
		}

		
	}


function getLnkValues($col_value)
	{
	$row_value = array();
	$res = $this->db->db_query("SELECT row2 FROM ".FORM_TABLES_LNK_VALUES." WHERE id_lnk='".$this->db->db_escape_string($this->col['id_lnk'])."' AND row1='".$this->db->db_escape_string($col_value)."'");

	while ($arr = $this->db->db_fetch_array($res))
		{
		$row_value[] = $arr['row2'];
		}
	return $row_value;
	}


function dirfiles_fill()
	{
	$this->row_directory = $GLOBALS['babAddonUpload'].'directories/'.$this->tablename.'/'.$this->form_row.'/';

	if (is_dir($this->row_directory))
		{
		if ($dh = opendir($this->row_directory)) {
			   while (($file = readdir($dh)) !== false) {
				   if (is_file($this->row_directory . $file))
						{
						$this->dirfiles[] = $file;
						}
			   }
			   closedir($dh);
		   }
		natcasesort($this->dirfiles);
		}
	}

function getnextdirfile()
	{
	if (list(,$this->filename) = each($this->dirfiles))
		{
		$this->filesize = filesize($this->row_directory.$this->filename);
		$this->filesize = sprintf("%01.2f %s",($this->filesize/1024),form_translate('Ko'));
		$this->fileurl = $GLOBALS['babAddonUrl'].'directory&amp;idx=getfile&amp;id_table='.$this->arr['id_table'].'&amp;form_row='.$this->form_row.'&amp;filename='.urlencode($this->filename);
		$mime = form_getFileMimeType($this->filename);
		$this->classname = str_replace('/','-',$mime);
		$this->filename = bab_toHtml($this->filename);
		
		
		return true;
		}
	return false;
	}


function getnextcol()
	{
	global $babDB;
	static $i=0;
	if( $i < $this->countcol)
		{
		if (isset($this->altbg))
			$this->altbg = !$this->altbg;
		$this->col = $this->db->db_fetch_assoc($this->rescol);

		

		$this->col['form_element_param'] = bab_toHtml(sprintf($this->col['form_element_param'],$this->col['id']));

		$this->dataTypeCtrl();

		$col = !empty($this->col['id_table_field']) ? form_col_name($this->col['id_table_field'],$this->col['name_table_field']) : '';
		$this->loopoption = false;
		$this->notempty = false;
		if (isset($this->col['help_id']))
			$this->help_url = $GLOBALS['babAddonUrl']."help&idx=view&help_id=".$this->col['help_id'];

		$this->field_classname = false;

		
		
		switch ($this->col['id_print'])
			{
			case 6:
				$this->typefield = 'readonly';
				break;
			case 7:
				$this->typefield = 'hidden';
				break;
			case 10:
				$this->typefield = 'directory';
				break;
			case 11:
				$this->typefield = 'rodirectory';
				$this->field_classname = 'directory';
				break;
			case 12:
				$this->typefield = 'captcha';
				
				$oCaptcha = @bab_functionality::get('Captcha');
				if(false !== $oCaptcha) {
					$this->sCaptchaSecurityData = $oCaptcha->getGetSecurityHtmlData();
				} else {
					$this->sCaptchaSecurityData = form_translate('The captcha functionality is not installed');
				}
				
				$this->notempty = true;

				break;
			
			default:
				$this->notempty = $this->arr['view_notempty'] == 1 && $this->col['notempty'] == 'Y' ? true : false;
				$this->typefield = $this->col['form_element'];
				break;
			}

		if (!$this->field_classname) $this->field_classname = $this->typefield;

		if (!empty($this->col['classname']))
			$this->field_classname .= ' '.$this->col['classname'];

		$this->field_classname .= ' f'.$this->col['id'];

		if (form_isError($this->col['id'])) {
			$this->field_classname .= ' post_submit_error';
		}


		if (3 == $this->arr['id_type'] 
			&& 'image' != $this->typefield 
			&& 'rodirectory' != $this->typefield)
			{
			// lecture seule obligatoire pour les formulaire de visualisation
			$this->typefield = 'readonly';
			}

		
		// valeur recuperee dans la ligne de la table
		if (isset($this->line[$col]) && $this->col['init_on_modify'] != 'Y') {
			$this->line_value = $this->line[$col];
		} else {
			$this->line_value = isset($this->default_values[$this->col['id']]) ? $this->default_values[$this->col['id']] : '';
		}
		
		// valeur recuperee dans la ligne de la requete ou dans le POST si il y a eu une erreur
		if (isset($_POST['form_field_'.$this->col['id']])) {
			$this->row_value = $_POST['form_field_'.$this->col['id']];
		} else {
			$this->row_value = isset($this->sheet['f'.$this->col['id']]) ? $this->sheet['f'.$this->col['id']] : '';
		}
		

		
		if (!empty($this->col['id_table_field_link']))
			{
			if (isset($this->default_values[$this->col['id']]))
				{
				if ($this->row_value == '' || $this->col['init_on_modify'] == 'Y')
					$this->row_value = $this->default_values[$this->col['id']];
				}
				
			$this->resoption = form_getResOptions(
					$this->col['link_table_field_id'], 
					$this->col['id_table_field_link'], 
					$this->col['id_table_field_init'], 
					$this->col['id_list_filter'],
					$this->col['id_table_field_optgroup']
			);
			
			if ($this->col['id_table_field_optgroup'] && $babDB->db_num_rows($this->resoption) > 0)
			{
				$this->optgroups = array();
				while ($arr = $babDB->db_fetch_assoc($this->resoption))
				{
					$this->optgroups[$arr['optgroup']] = $arr['optgroup'];
				}
				$babDB->db_data_seek($this->resoption, 0);
			}
			
			$this->option_link = true;
			}
		elseif (!empty($this->col['id_table_field_lnk']))
			{
			$this->resoption = form_getResLnk( $this->col['id_lnk'], $this->col['id_table_field_lnk'], $this->col['id_list_filter'] );

			if (!empty($this->form_row))
				{
				$this->row_value = $this->getLnkValues($this->form_row);
				$this->line_value = $this->row_value;
				}

			$this->option_link = true;
			$this->loopoption = true;
			}
		else
			{
			$arr = form_getInputTypeFormField($this->col['id']);
			$this->options = &$arr['options'];
			if (isset($arr['col_type']) && (substr_count($arr['col_type'],'enum')>0 || substr_count($arr['col_type'],'set')>0))
				{
				$this->tab_values = enumToArray($arr['col_type']);
				}
			else
				$this->tab_values = array();
			
			if (substr_count($arr['col_type'],'enum')>0)
				{
				if (isset($this->row[$col]) && $this->col['init_on_modify'] != 'Y')
					{
					$this->row_value = $this->row[$col];
					}
				elseif (isset($this->default_values[$this->col['id']]))
					{
					$this->row_value = $this->default_values[$this->col['id']];
					}
				else
					{
					$this->row_value = $this->tab_values[0];
					}
				}
			elseif (substr_count($arr['col_type'],'set')>0)
				{
				$this->t_value = '';
				$this->loopoption = true;
				$this->row_value = explode(',', $this->line_value);
				}
			else
				{
				if (!empty($this->row[$col]) && $this->col['init_on_modify'] != 'Y')
					{
					$this->row_value = $this->row[$col];
					}
				elseif (isset($this->default_values[$this->col['id']]))
					{
					if ($this->row_value == '' || $this->col['init_on_modify'] == 'Y')
						$this->row_value = $this->default_values[$this->col['id']];
					}
				elseif (!isset($this->row_value))
					{
					$this->row_value = '';
					}
				}
			$this->option_link = false;
			}


		$this->not_blob = 'blob' !== $this->data_type;
		
		
		if (is_array($this->row_value))
			{
			$this->value = $this->row_value;
			$this->t_value = implode(', ',$this->value);
			}
		else
			{
			$this->value = form_getTransformedValue($this->col['id'], $this->form_row, $this->row_value, FORM_TRANS_DB_HTML);
			}

		$this->html_value = '';
		if (false == $this->loopoption) {
			$format = $this->typefield == 'readonly' ? FORM_TRANS_RICH_HTML : FORM_TRANS_RICH_HTML_SMALL;
			$this->html_value = form_getTransformedValue($this->col['id'], $this->form_row, $this->row_value, $format);
			$this->t_value = form_getTransformedValue($this->col['id'], $this->form_row, $this->row_value, FORM_TRANS_HTML);
			}

		

		if ('hidden' === $this->typefield) {
			$this->html_label = '';
		} else {
			$this->html_label = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath']."html_utilities.html", 'editlabel');
		}
		$this->html_field = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath']."fields_edit.html", $this->typefield);


		$i++;
		return true;
		}
	else
		{
		$i=0;
		if ($this->countcol > 0)
			$this->db->db_data_seek($this->rescol,0);
		return false;
		}
	
	}


	public function getnextoptgroup()
	{
		if (list($optgroup) = each($this->optgroups))
		{
			$this->optgroup = bab_toHtml($optgroup);
			return true;
		}
		return false;
	}
	
	public function getnextoptgroupoption(&$skip = false)
	{
		if( $option = $this->db->db_fetch_assoc($this->resoption) )
		{
			if ($this->optgroup !== bab_toHtml($option['optgroup']))
			{
				$skip = true;
				return true;
			}
			
			if (empty($this->form_row) || $this->col['init_on_modify'] == 'Y')
				$init = isset($option['init']) ? $option['init'] : false;
			elseif (!empty($this->line_value)) {
				$init = $option['val'];
			} else
				$init = false;
			
			if (false !== $init)
			{
				if (is_array($this->line_value))
				{
					$this->selected = in_array($init,$this->line_value);
				}
				else
				{
					$this->selected = ($init == $this->line_value);
				}
			}
			
			$this->option['opt'] = bab_toHtml($option['opt']);
			$this->option['val'] = bab_toHtml($option['val']);
			
			return true;
		}
		if ($this->db->db_num_rows($this->resoption) > 0)
			$this->db->db_data_seek($this->resoption,0);
		return false;
	}
	
	

	public function getnextoption()
	{
	static $i=0;


	//if (337 == $this->col['id'])
	//	echo $this->col['id'].' '.$this->line_value."\n";

	if ($this->option_link && $this->resoption)
		{
		if( $this->option = $this->db->db_fetch_assoc($this->resoption) )
			{
			if (empty($this->form_row) || $this->col['init_on_modify'] == 'Y')
				$init = isset($this->option['init']) ? $this->option['init'] : false;
			elseif (!empty($this->line_value)) {
				$init = $this->option['val'];
			} else
				$init = false;

			if (false !== $init)
				{
				if (is_array($this->line_value))
					{
					$this->selected = in_array($init,$this->line_value);
					}
				else
					{
					$this->selected = ($init == $this->line_value);
					}
				}
				
			$this->option['opt'] = bab_toHtml($this->option['opt']);
			$this->option['val'] = bab_toHtml($this->option['val']);
			return true;
			}
		else
			{
			if ($this->db->db_num_rows($this->resoption) > 0)
				$this->db->db_data_seek($this->resoption,0);
			return false;
			}
		}
	else
		{
		if( $i < count($this->tab_values)) // Enum / Set
			{
			$this->option['opt'] = $this->tab_values[$i];
			$this->option['val'] = $this->tab_values[$i];
			if (isset($this->row_value) && is_array($this->row_value))
				{
				$this->selected = in_array($this->option['val'],$this->row_value);
				}
			else
				{
				$this->selected = isset($this->row_value) && ($this->option['val'] == $this->row_value) ? true : false;
				}

			$this->option['opt'] = bab_toHtml($this->option['opt']);
			$this->option['val'] = bab_toHtml($this->option['val']);

			$i++;
			return true;
			}
		else
			{
			$i=0;
			return false;
			}
		}
	}
	
	
	
	private function getProposal()
	{
		$id_proposal = (int) $this->col['id_proposal'];
		
		if (0 === $id_proposal)
		{
			return null;
		}
		
		global $babDB;
		
		$this->specify_value = '';
		
		$res = $babDB->db_query('SELECT * FROM '.FORM_PROPOSAL.' WHERE id='.$babDB->quote($id_proposal));
		if ($arr = $babDB->db_fetch_assoc($res))
		{
			$arr['res'] = $babDB->db_query('SELECT * FROM '.FORM_PROPOSAL_ITEM.' WHERE id_proposal='.$babDB->quote($id_proposal));
			
			return $arr;
		}
		
		return null;
	}
	
	
	public function getnextproposal()
	{
		if (!isset($this->col['proposal']))
		{
			$this->col['proposal'] = $this->getProposal();
		}
		
		$p = $this->col['proposal'];
		
		$this->checkbox = (bool) $p['checkbox'];
		$this->specify = (bool) $p['specify'];
		
		if ($pos = mb_strrpos($this->row_value, ':'))
		{
			$specify = trim(mb_substr($this->row_value, 1+$pos));
			$this->row_value = trim(mb_substr($this->row_value, 0, $pos));
			$this->specify_value = bab_toHtml($specify);
		}
		
		if ($this->checkbox)
		{
			$v = explode(',', $this->row_value);
			foreach($v as $k => $x)
			{
				$v[$k] = trim($x);
			}
		}
		
		$this->t_specify = form_translate('specify');
		
		global $babDB;
		
		if ($arr = $babDB->db_fetch_assoc($p['res']))
		{
			$this->name = bab_toHtml($arr['name']);
			if ($this->checkbox)
			{
				$this->checked = in_array($this->name, $v);
			} else {
				$this->checked = $this->row_value === $this->name;
			}
			
			return true;
		}
		
		return false;
	}

}


class form_view extends form_sheet
{
var $altbg = false;
function form_view($id)
	{
	$this->id = $id;
	$this->form_sheet();

	$this->t_previous = form_translate('previous');
	$this->t_next = form_translate('next');
	$this->t_close = form_translate('Close');

	$this->next_idx = $this->url_popup ? 'pop_closer' : 3;
	$this->next_step = $this->id_step;

	$this->arr = $this->db->db_fetch_assoc($this->db->db_query("
	
			SELECT 
				f.*,
				f.id_order_by,
				t.name 
				name_order_by 
			FROM 
				".FORM_FORMS." f
			LEFT JOIN ".FORM_TABLES_FIELDS." t
				ON t.id=f.id_order_by 
			WHERE 
				f.id='".$this->db->db_escape_string($id)."'"
	));

	$this->arr['buttons_next_previous'] = $this->arr['buttons_next_previous'] == 'Y';

	if (!empty($this->arr['id_table']))
		{
		if (!bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$this->arr['id_table']))
			{
			$this->msgerror = form_translate('Table access denied');
			$this->access = false;
			}

		form_auto_filing($this->arr['id_table']);

		$this->tablename = form_tbl_name($this->arr['id_table']);

		if (!empty($this->arr['name_order_by']))
			{
			$order = form_col_name($this->arr['id_order_by'],$this->arr['name_order_by']).' '.$this->arr['order_type'];
			}
		else
			$order = 'form_create ASC';

		$this->nextpreviousrow($this->tablename,$order);

		if (empty($this->form_row) && empty($this->id_app)) {
			$this->form_row = $this->first_row;
		}

		$req = "SELECT * FROM ".$this->tablename." WHERE form_id='".$this->db->db_escape_string($this->form_row)."'";
		$this->row = $this->db->db_fetch_assoc($this->db->db_query($req));

		$this->dirfiles = array();
		$this->dirfiles_fill();


		$this->editColCtrl();
		}
	if (!isset($this->countcol)) $this->countcol = 0;
	}
	
	
	function getFormHead() {
	
		return bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'html_utilities.html', 'FormHtmlHead_view');
	}

	function getFormFoot() {
	
		return bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'html_utilities.html', 'FormHtmlFoot_view');
	}

}


class form_edit extends form_sheet
{
var $altbg = false;
function form_edit()
	{
	$this->form_sheet();

	$this->js_submitCtrl = true;

	$this->t_view_notempty = form_translate('Fields must be filled');
	$this->js_empty_alert = form_translate('must be filled');
	$this->t_submit = form_translate('Submit');
	$this->t_delete = form_translate('Del.');
	$this->t_add_field = form_translate('Load more files');
	$this->t_remove_field = form_translate('Remove');
	$this->t_edit = form_translate('Edit');
	$this->t_remove = form_translate('Remove');

	$this->calendar_url = $GLOBALS['babUrlScript']."?tg=month&ymin=100&ymax=100&month=".date('m')."&year=".date('Y');
	
	$this->parent_idx = bab_rp('idx');
	$this->pos = 0;
	
	

	if (!isset($this->next_idx))
		$this->next_idx = $this->url_popup ? 'pop_closer' : $this->parent_idx;

	$this->arr = $this->db->db_fetch_assoc($this->db->db_query("
	
		SELECT 
			f1.*,
			NULLIF(f1.buttons_next_previous,'N') buttons_next_previous, 
			IF(COUNT(f2.id) > 0 AND f1.view_notempty,1,0) view_notempty 
		FROM 
			".FORM_FORMS." f1 
			LEFT JOIN ".FORM_FORMS_FIELDS." f2 
			ON f2.id_form='".$this->db->db_escape_string($this->id)."' AND f2.notempty='Y' 
		WHERE 
			f1.id='".$this->db->db_escape_string($this->id)."' GROUP BY f1.id
		
		"));


	if (!empty($this->arr['id_table']) && !bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$this->arr['id_table']))
		$this->access = false;

	if (isset($_POST['id_table']) && $this->arr['id_table'] != $_POST['id_table'])
		{
		$this->form_row = '';
		}

	// selectionner la permiere ligne de la table pour la pr�visualisation
	if ( empty($this->form_row) && !empty($this->arr['id_table']) && empty($this->id_app) )
		{
		if (!bab_isAccessValid(FORM_TABLES_INSERT_GROUPS,$this->arr['id_table']))
			$this->access = false;

		$req = "SELECT MIN(form_id) FROM ".form_tbl_name($this->arr['id_table'])."";
		$arr = $this->db->db_fetch_array($this->db->db_query($req));
		if ($arr[0]) {
			$this->form_row = $arr[0];
			}
		
		}

	if ($this->approbCtrl($this->arr['id_table']))
		{
		$this->access = false;
		if ($this->arr['waiting_modify'] == 2 || empty($this->app_id) || $this->parent_idx == 7)
			{
			$this->access = true;
			}
		elseif ($this->arr['waiting_modify'] == 1 && $this->form_row > 0 )
			{
			$req = "SELECT form_lastupdate_user FROM ".form_tbl_name($this->arr['id_table'])." WHERE form_id='".$this->db->db_escape_string($this->form_row)."'";
			list($id_user_last_modification) = $this->db->db_fetch_array($this->db->db_query($req));
			if ($id_user_last_modification == $GLOBALS['BAB_SESS_USERID'])
				$this->access = true;
			}
		}


	// insert : default values
	list($this->row, $this->default_values) = form_getInit($this->id);
		
	$this->dirfiles = array();
	
	if (!empty($this->form_row))
		{
		if (!bab_isAccessValid(FORM_TABLES_CHANGE_GROUPS,$this->arr['id_table']))
			$this->access = false;

		$this->tablename = form_tbl_name($this->arr['id_table']);

		$req = "SELECT * FROM ".$this->tablename." WHERE form_id='".$this->db->db_escape_string($this->form_row)."'";
		$this->row = $this->db->db_fetch_assoc($this->db->db_query($req));

		$this->nextpreviousrow($this->tablename,'form_create');
		$this->dirfiles_fill();
		
		}
		
	
	$this->editColCtrl();
	

	$this->resvalid = $this->db->db_query("
						SELECT 
							f.id,
							f.name,
							f.notempty ,
							COUNT(c.id) validations 
						FROM 
							".FORM_FORMS_FIELDS." f 
						LEFT JOIN 
							".FORM_FORMS_FIELDS_CTRL." c 
							ON c.id_field = f.id
						WHERE 
							f.id_form='".$this->db->db_escape_string($this->id)."' 
							AND (f.notempty = 'Y' OR c.id > 0)
							GROUP BY f.id
						");

	// pour revenir au formulaire precedent.
	$parent_id_form = bab_rp('parent_id_form');

	if (!empty($parent_id_form))
		{
		$this->next_form =$parent_id_form;
		$this->next_step = bab_rp('parent_id_step');
		$this->next_idx = $this->url_popup ? 'pop_closer' : 1;
		}
	else
		{
		$this->next_form = $this->id;
		$this->next_step = $this->id_step;
		}

	}





function getnextvalid()
	{
	$out = list($this->id_field, $this->fieldname, $notempty, $validations) = $this->db->db_fetch_array($this->resvalid);
	
	if ($validations > 0)
		{
		$this->resctrl = $this->db->db_query("SELECT c.id,v.javascript, c.parameter, c.alert FROM ".FORM_FORMS_FIELDS_CTRL." c, ".FORM_FORMS_FIELDS_VALIDATION." v WHERE c.id_field='".$this->db->db_escape_string($this->id_field)."' AND v.id = c.id_validation");
		}

	$this->notempty = $notempty == 'Y';
	$this->alert2 = $this->fieldname.' '.$this->js_empty_alert;

	$this->alert2 = str_replace("'", "\'", $this->alert2);
	$this->alert2 = str_replace('"', "'+String.fromCharCode(34)+'",$this->alert2);

	if (!$out)
		{
		if ($this->db->db_num_rows($this->resvalid))
			$this->db->db_data_seek($this->resvalid,0);

		return false;
		}
	return true;
	}


function getnextctrl()
	{
	if (!isset($this->resctrl)) return false;

	if (list($this->id_ctrl,$this->js_code, $param, $alert) = $this->db->db_fetch_array($this->resctrl))
		{
		$this->js_code = sprintf($this->js_code,$param);
		$alert = str_replace("'", "\'", $alert);
		$alert = str_replace('"', "'+String.fromCharCode(34)+'",$alert);
		$this->fieldname = str_replace("'", "\'", $this->fieldname);
		$this->fieldname = str_replace('"', "'+String.fromCharCode(34)+'",$this->fieldname);
		$this->alert = form_msgError($alert,$this->fieldname);

		return true;
		}
	else
		{
		return false;
		}
	}

function getFormHead() 
	{
	return bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'html_utilities.html', 'FormHtmlHead_edit');
	}

function getFormFoot() 
	{
	return bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'html_utilities.html', 'FormHtmlFoot_edit');
	}

}


class form_list extends form_application
	{ 
	var $altbg = false;
	function form_list($id)
		{
		
		
		$this->id = $id;
		$this->form_application();
		$this->listCtrl();


		$this->t_total = form_translate('Total');
		$this->t_between = form_translate('between');
		$this->t_and = form_translate('and');
		$this->calendar_url = $GLOBALS['babUrlScript']."?tg=month&ymin=100&ymax=100&month=".date('m')."&year=".date('Y');

		if (empty($this->arr['id_table']))
			{
			return;
			}

		$this->orderCtrl();
		$this->filterCtrl();
		$this->searchCtrl();

		$this->rescrit = $this->db->db_query("
		
			SELECT 
				f.id,
				f.name,
				f.description,
				f.search_type,
				f.id_table_field,
				f.id_table_field_link,
				t1.id_table t1_id_table,
				f.id_table_field_lnk,
				t2.id_table t2_id_table, 
				t3t.col_type,
				t3t.str_options  
			FROM 
				".FORM_FORMS_FIELDS." f 
			LEFT JOIN 
				".FORM_TABLES_FIELDS." t1
			ON  
				t1.id = f.id_table_field_link 
			LEFT JOIN 
				".FORM_TABLES_FIELDS." t2
			ON  
				t2.id = f.id_table_field_lnk 
			LEFT JOIN 
				".FORM_TABLES_FIELDS." t3 
			ON
				t3.id = f.id_table_field 
			LEFT JOIN 
				".FORM_TABLES_FIELDS_TYPES." t3t 
			ON 
				t3t.id = t3.id_type 
			WHERE 
				f.id_form='".$this->db->db_escape_string($this->id)."' 
				AND f.search_crit='1' 
				AND f.search='Y' 
			ORDER BY f.ordering
			");

		$this->colspan = 0;
		$this->lb = "\n";
		}

	function list_query()
		{
		if (!$this->arr || !$this->arr['id_table']) {
			return false;
		}
		$this->arr['allow_delete'] = bab_isAccessValid(FORM_TABLES_DELETE_GROUPS,$this->arr['id_table']) ? $this->arr['allow_delete'] : 'N';
		
		$req = $this->queryObj->getQuery();

		$this->max = $this->db->db_num_rows($this->db->db_query($req));

		if ($this->pos >= $this->max)
			{
			$this->pos = 0;
			}

		if (false !== $this->arr['rows_per_page'])
			{
			$req .= ' LIMIT '.$this->db->db_escape_string($this->pos).','.$this->db->db_escape_string($this->arr['rows_per_page']);
			}

		$this->resrow = $this->db->db_query($req);
		$this->countrow = $this->db->db_num_rows($this->resrow);

		$this->countcol = $this->db->db_num_rows($this->rescol);
		return true;
		}

	function getnextrow()
		{
		static $i=0;
		if( $i < $this->countrow)
			{
			$this->altbg = !$this->altbg;
			$this->row = $this->db->db_fetch_assoc($this->resrow);
			if ($i == 0)
				{
				// transformation de la description du formulaire avec les valeures de la requette
				form_setMacro($this->arr['description'], $this->row );
				}
			$this->row['qte'] = isset($this->quantities[$this->row['form_id']]) ? $this->quantities[$this->row['form_id']] : '';

			$i++;
			return true;
			}
		else
			{
			$i=0;
			return false;
			}
		}


	function isColVisible()
		{
			if (!in_array($this->col['id_print'],$this->col_print) || $this->col['id_print'] == 7)
				{
				return false;
				}

			if (isset($this->form_links[$this->col['id']]) && 0 < $this->form_links[$this->col['id']]['id_form'] &&  !bab_isAccessValid(FORM_FORMS_GROUPS,$this->form_links[$this->col['id']]['id_form']))
				{
				return false;
				}

			return true;
		}

		


	function getnextcol(&$skip)
		{
		static $i=0;
		static $first = 0;
		if( $i < $this->countcol)
			{
			$this->col = $this->db->db_fetch_assoc($this->rescol);

			if (!$this->isColVisible()) {
				$i++;
				$skip = true;
				return true;
			}

			if (empty($this->col['id_table_field']) || !empty($this->col['transform_to']))
				{
				$this->orderby = 'FIXED';
				}
			else
				{
				$this->orderby = !empty($this->arr['id_order_by']) && $this->arr['id_order_by'] == $this->col['id'] ? $this->arr['order_type'] : 'NO';
				}
			
			if (isset($this->col['help_id']))
				$this->help_url = $GLOBALS['formAddonUrl']."help&idx=view&help_id=".$this->col['help_id'];

			
			
			

			if (!isset($this->row))
				$this->colspan++;


			if ($this->col['search'] == 'Y' && $this->arr['search_box'] == 0)
				$this->find = form_translate('Find');

			$this->value = '';
			$this->full = false;
			$this->short = false;

			$col = 'f'.$this->col['id'];
			if (isset($this->row[$col]))
				{
				$this->value = $this->row[$col];
				}

			if (
			!empty($this->id_app) 
			&& isset($this->row['form_id']) 
			&& ($this->col['id_print'] == 8 || $this->col['id_print'] == 5 ) 
			&& isset($this->form_links[$this->col['id']]))
				{
				if (!empty($this->form_links[$this->col['id']]['id_field']))
					{
					$test_value = $this->row['f'.$this->form_links[$this->col['id']]['id_field']];
					$if_value = $this->form_links[$this->col['id']]['value'];
					$view_link = null;
					eval(sprintf('$view_link = '.$this->form_links[$this->col['id']]['operator'].';', '$test_value' , '$if_value'));
					$this->link = $view_link;
					}
				else
					{
					$this->link = true;
					}

				$this->popup = & $this->form_links[$this->col['id']]['popup'];
				$this->type = & $this->form_links[$this->col['id']]['id_type'];
				$form_row = empty($this->form_links[$this->col['id']]['id_index']) ? $this->row['form_id'] : $this->row['f'.$this->form_links[$this->col['id']]['id_index']];
				$this->link_url = $GLOBALS['formAddonUrl']. "form&idx=". $this->type ."&id_app=".$this->id_app."&id_step=".$this->form_links[$this->col['id']]['id_step']."&id_form=". $this->form_links[$this->col['id']]['id_form'] ."&form_row=".$form_row."&popup=". $this->form_links[$this->col['id']]['popup']."&parent_id_form=".$this->id."&parent_id_step=".$this->id_step."&form_menu=".$this->form_menu."&trt_step=1&form_value=";
				
				if (!empty($this->value) && strlen($this->value) < FORM_VIEW_MAX_CHAR) 
					{
					$this->link_url .= urlencode($this->value);
					}	
				}
			else
				{
				$this->link = false;
				}

			if (isset($this->row['form_id'])) {
				$this->export = form_getTransformedValue($this->col['id'], $this->row['form_id'], $this->value, FORM_TRANS_CSV);
			} else {
				$this->export = $this->value;
			}


			if (strlen($this->value) > FORM_VIEW_MAX_CHAR) {
				$this->short 	= bab_abbr($this->value, BAB_ABBR_FULL_WORDS, FORM_VIEW_MAX_CHAR);
				$this->full 	= $this->value;
				}
			else
				{
				$this->short 	= $this->value;
				$this->full 	= '';
				}

			
			if (isset($this->row['form_id'])) {
				$this->full = form_getTransformedValue($this->col['id'], $this->row['form_id'], $this->full, FORM_TRANS_HTML);
				if ($this->link) {
					// si lien, pas de html riche
					$this->short = form_getTransformedValue($this->col['id'], $this->row['form_id'], $this->short, FORM_TRANS_HTML);
				} else {
					$this->short = form_getTransformedValue($this->col['id'], $this->row['form_id'], $this->short, FORM_TRANS_RICH_HTML_SMALL);
				}
			}
			
			$i++;
			
			return true;
			}
		else
			{
			$i=0;
			$first = 0;
			if ($this->countcol > 0) 
				$this->db->db_data_seek($this->rescol,0);
			return false;
			}
		}

	function getnextcrit()
		{
		if ($this->crit = $this->db->db_fetch_assoc($this->rescrit))
			{
			$this->crit_value = isset($_POST['form_crit_'.$this->crit['id']]) ? $_POST['form_crit_'.$this->crit['id']] : '';
			$this->crit_value_b = isset($_POST['form_crit_'.$this->crit['id'].'_b']) ? $_POST['form_crit_'.$this->crit['id'].'_b'] : '';
			$this->crit_value_e = isset($_POST['form_crit_'.$this->crit['id'].'_e']) ? $_POST['form_crit_'.$this->crit['id'].'_e'] : '';

			if (!empty($this->crit['id_table_field_link']))
				{
				$this->qo = new form_queryObj($this->crit['t1_id_table']);
				$this->qo->addTableCol_NamedAlias($this->crit['id_table_field_link']);
				$this->qo->addOrderBy($this->crit['id_table_field_link'], 'ASC');
				$this->rescritval = $this->db->db_query($this->qo->getQuery());
				
				}
			elseif (!empty($this->crit['id_table_field_lnk']))
				{
				$this->qo = new form_queryObj($this->crit['t2_id_table']);
				$this->qo->addTableCol_NamedAlias($this->crit['id_table_field_lnk']);
				$this->qo->addOrderBy($this->crit['id_table_field_lnk'], 'ASC');
				$this->rescritval = $this->db->db_query($this->qo->getQuery());
				}
			elseif (5 === (int) $this->crit['search_type'] || 6 === (int) $this->crit['search_type'] ) 
				{
				// enum or set
				$this->rescritval = enumToArray($this->crit['col_type']);
				reset($this->rescritval);
				}
			elseif (7 === (int) $this->crit['search_type'] || 8 === (int) $this->crit['search_type'] )
				{
				// ovuser ovdirectory
				$this->t_edit = form_translate('Edit');
				$this->t_remove = form_translate('Remove');
				
				$options = unserialize($this->crit['str_options']);
				$this->id_directory = (int) $options['id_directory'];
				
				if (7 === (int) $this->crit['search_type'])
					{
					// ovuser
					$this->t_value = bab_getUserName($this->crit_value);
					
					}
				
				if (8 === (int) $this->crit['search_type'])
					{
					// ovdirectory
					$sheet = bab_getDirEntry($this->crit_value, BAB_DIR_ENTRY_ID, $this->id_directory);
					$this->t_value = $sheet['sn']['value'].' '.$sheet['givenname']['value'];
					}
				}

			$this->html_criterion = bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'html_utilities.html', 'search_type_'.$this->crit['search_type']);
			
			$this->label = !empty($this->crit['description']) ? bab_toHtml($this->crit['description']) : $this->crit['name'];

			$this->html_criterion_label = bab_printTemplate($this,$GLOBALS['babAddonHtmlPath'].'html_utilities.html', 'search_label');


			return true;
			}
		else
			{
			return false;
			}
		}

	function getnextcritval()
		{
		if ($arr = $this->db->db_fetch_array($this->rescritval))
			{
			
			list($this->key, $this->value) = $arr;
			if (strlen($this->value) > FORM_DROPDOWN_MAX_CHAR)
				$this->value = substr($this->value,0,FORM_DROPDOWN_MAX_CHAR).'...';
				
				$this->value = bab_toHtml($this->value);
				
			if ((!is_array($this->crit_value) && $this->key == $this->crit_value) || (is_array($this->crit_value) && in_array($this->key,$this->crit_value)))
				{
				$this->selected = true;
				}
			else
				{
				$this->selected = false;
				}
			return true;
			}
		else
			{
			return false;
			}
		}
		
		
		
	function getnextsetorenum()
		{
			if (list(,$value) = each($this->rescritval)) {
			    
			   
				
				$this->value = bab_toHtml($value);
				
				if ((!is_array($this->crit_value) && $value == $this->crit_value) || (is_array($this->crit_value) && in_array($value,$this->crit_value)))
					{
					$this->selected = true;
					}
				else
					{
					$this->selected = false;
					}
				return true;
			}
			
			reset($this->rescritval);
			return false;
		}
	}
