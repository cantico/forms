<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function form_proposal_list()
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = & $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_specify = form_translate('Specify field');
			$this->t_checkbox = form_translate('Use checkboxes');
			$this->t_delete = form_translate('Delete');
			$this->js_alert = form_translate('Do you really want to delete the selected items').'?';

			$this->res = $this->db->db_query("
				SELECT p.* 
				FROM 
					".FORM_PROPOSAL." p,
					".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					p.id=w.id_object 
					AND w.type='form_proposal' 
					AND w.id_workspace=".$this->db->quote(form_getWorkspace())."
			");
			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);

				$this->arr['name'] = bab_toHtml($this->arr['name']);
				if ($this->arr['specify'])
				{
					$this->arr['specify'] = bab_toHtml(form_translate('Enabled'));
				} else {
					$this->arr['specify'] = bab_toHtml(form_translate('Disabled'));
				}
				
				if ($this->arr['checkbox'])
				{
					$this->arr['checkbox'] = bab_toHtml(form_translate('Enabled'));
				} else {
					$this->arr['checkbox'] = bab_toHtml(form_translate('Disabled'));
				}

				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."proposal.html", "list"));
}





function form_proposal_edit()
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = & $GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_specify = form_translate('Use the specify field associated to the last proposal item');
			$this->t_checkbox = form_translate('Use checkboxes');
			$this->t_items = form_translate('Proposals, coma separated');
			$this->t_delete = form_translate('Delete');
			$this->t_submit = form_translate('Submit');
			
			if (isset($_POST) && count($_POST) > 0)
				{
				$this->arr = $_POST;
				}
			elseif (isset($_GET['id_proposal']))
				{
				$this->arr = $this->db->db_fetch_assoc($this->db->db_query("SELECT * FROM ".FORM_PROPOSAL." WHERE id='".$this->db->db_escape_string($_GET['id_proposal'])."'"));
				$res = $this->db->db_query('SELECT name FROM '.FORM_PROPOSAL_ITEM.' WHERE id_proposal='.$this->db->quote($_GET['id_proposal']));
				$items = array();
				while ($item = $this->db->db_fetch_assoc($res))
					{
					$items[] = $item['name'];	
					}
				$this->arr['items'] = implode(', ', $items);
				}
			else
				{
				$el_to_init = array('id','name','specify', 'checkbox', 'items');
				foreach($el_to_init as $field)
					{
					$this->arr[$field] = '';
					}
				}
				
			
           
			$this->arr['name'] = bab_toHtml($this->arr['name']);
			$this->arr['specify'] = bab_toHtml($this->arr['specify']);
			$this->arr['checkbox'] = bab_toHtml($this->arr['checkbox']);
			$this->arr['items'] = bab_toHtml($this->arr['items']);
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."proposal.html", "edit"));
}



function form_proposal_delete()
	{
	if (isset($_POST['id_help']) && count($_POST['id_help']) > 0)
		{
		$db = & $GLOBALS['babDB'];
		$db->db_query("DELETE FROM ".FORM_HELP." WHERE id IN (".$db->quote($_POST['id_help']).")");
		}
	}

function form_proposal_record()
	{
	$db = & $GLOBALS['babDB'];
	
	$specify = bab_pp('specify') ? '1' : '0';
	$checkbox =  bab_pp('checkbox') ? '1' : '0';
	
	$id_proposal = bab_pp('id_proposal');

	if (empty($id_proposal))
		{
		
		$db->db_query("INSERT INTO ".FORM_PROPOSAL." (name, specify, checkbox) VALUES (
				'".$db->db_escape_string($_POST['name'])."',
				'".$db->db_escape_string($specify)."',
				'".$db->db_escape_string($checkbox)."')");
		
		$id_proposal = $db->db_insert_id();
		form_ieO::inserted('form_proposal', $id_proposal);
		}
	else
		{
		$db->db_query("UPDATE ".FORM_PROPOSAL." 
		SET 
			name = '".$db->db_escape_string($_POST['name'])."', 
			specify = '".$db->db_escape_string($specify)."', 
			checkbox = '".$db->db_escape_string($checkbox)."' 
		WHERE 
			id = '".$db->db_escape_string($id_proposal)."'");
		}
		
		
	// update items
	
	$db->db_query('DELETE FROM '.FORM_PROPOSAL_ITEM.' WHERE id_proposal='.$db->quote($id_proposal));
	$items = explode(',', bab_pp('items'));
	$s = 1;
	foreach($items as $item )
		{
		$item = trim($item);
		$db->db_query('INSERT INTO '.FORM_PROPOSAL_ITEM.' (id_proposal, name, sortkey) VALUES ('.$db->quote($id_proposal).', '.$db->quote($item).', '.$db->quote($s).')');
		$s++;
		}
		
	}

// main

$idx = bab_rp('idx','list');

if (isset($_POST['action']))
{

switch ($_POST['action'])
	{
	case 'edit':
		form_proposal_record();
		break;

	case 'delete':
		form_proposal_delete();
		break;
	}
}

$babBody->addItemMenu("forms", form_translate("Forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
$babBody->addItemMenu("list", form_translate("List"),$GLOBALS['babAddonUrl']."proposal&idx=list");



switch ($idx)
	{

	case 'edit':
		$babBody->title = form_translate("Edit proposal");
		$babBody->addItemMenu("edit", form_translate("Edit"), $GLOBALS['babAddonUrl']."proposal&idx=edit");
		form_proposal_edit();

		break;

	default:
	case 'list':
		$babBody->addItemMenu("edit", form_translate("Add"), $GLOBALS['babAddonUrl']."proposal&idx=edit");
		$babBody->title = form_translate("Proposals");
		form_proposal_list();
		break;
	}

$babBody->setCurrentItemMenu($idx);
