<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

function form_getStyles()
{
	$return = array();
	$dir = $GLOBALS['babInstallPath'].'skins/ovidentia/templates/addons/forms/styles/';
	if (is_dir($dir)) 
		{
		if ($dh = opendir($dir)) 
			{
			while (($file = readdir($dh)) !== false) 
				{
				if (strrchr($file,'.') == '.css')
					{
					$return[] = $file;
					}
				}
			closedir($dh);
			
			}
		}
	else
		trigger_error('missing styles directory');

	return $return;
}

function list_tables()
{
global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_nb_rec = form_translate('Records');
			$this->t_browse = form_translate('Browse');
			$this->t_edit = form_translate('Properties');
			$this->t_acces = form_translate('Rights');
			$this->t_edit_alertJs = form_translate('Warning, this table is not empty, you may lose some data');
			$this->t_help = form_translate('Help');
			$this->t_tables_content = form_translate('Browse tables');
			$this->t_tables_lnk = form_translate('Tables links');
			$this->t_appcreate_lnk = form_translate('Create application from tables');
			$this->t_approb = form_translate('Approbation');
			$this->t_funccol = form_translate('Functions');
			$this->t_filters = form_translate('Filters');
			$this->t_locked = form_translate('Locked');
			$this->t_descriptions = form_translate('Fields description');
			$this->t_access_title = form_translate('Set access rights on all tables of workspace');
			
			$this->res = $this->db->db_query("
				SELECT t.*,
					COUNT(DISTINCT f1.id) functions, 
					l.message messagelock 
				FROM 
					".FORM_TABLES." t
				LEFT JOIN 
					".FORM_TABLES_FIELDS." f1
					ON f1.field_function <> '' AND f1.id_table=t.id 
				LEFT JOIN 
					".FORM_TABLES_LOCKS." l 
					ON l.id_table=t.id,
				
				".FORM_WORKSPACE_ENTRIES." w 
				
				WHERE 
					t.id = w.id_object 
					AND w.type='table' 
					AND w.id_workspace=".$this->db->quote(form_getWorkspace())." 
				
				GROUP BY t.id 
				ORDER BY t.name
			");
			
			$this->count = $this->db->db_num_rows($this->res);
			
			$this->access_all_url = bab_toHtml($GLOBALS['babAddonUrl']."rights&idx=acces_table");
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);
				
				$this->arr['name'] = bab_toHtml($this->arr['name']);

				if ($this->arr['created'] == 'Y')
					{
					list($this->nb_rec) = $this->db->db_fetch_array($this->db->db_query("SELECT COUNT(*) FROM ".form_tbl_name($this->arr['id']).""));
					}
				else
					$this->nb_rec = 0;
					
				$this->arr['messagelock'] = isset($this->arr['messagelock']) ? $this->arr['messagelock'] : '';


				$this->browse_url 		= bab_toHtml($GLOBALS['babAddonUrl']."user&idx=browse_rows&id_table=".$this->arr['id']);
				$this->edit_url 		= bab_toHtml($GLOBALS['babAddonUrl']."main&idx=edit_table&id_table=".$this->arr['id']);
				$this->tabledesc_url 	= bab_toHtml($GLOBALS['babAddonUrl']."tabledesc&id_table=".$this->arr['id']);
				$this->acces_url 		= bab_toHtml($GLOBALS['babAddonUrl']."rights&idx=acces_table&id_table=".$this->arr['id']);
				$this->funccol_url 		= bab_toHtml($GLOBALS['babAddonUrl']."funccol&idx=list&id_table=".$this->arr['id']);
				$this->filters_url 		= bab_toHtml($GLOBALS['babAddonUrl']."filters&idx=list&id_table=".$this->arr['id']);

				$this->browse_btn = bab_isAccessValid(FORM_TABLES_VIEW_GROUPS, $this->arr['id']);

				list($this->arr['filters']) = $this->db->db_fetch_array($this->db->db_query("SELECT COUNT(*) FROM ".FORM_TABLES_FILTERS." WHERE id_table='".$this->db->db_escape_string($this->arr['id'])."'"));

				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "list_tables"));
}

function edit_table($id = '')
{
global $babBody;
    class temp
        {
		var $altbg = false;
		var $strlimit = 30;

        function temp($id)
            {
			$this->id = $id;
			$this->t_name = form_translate('Short name');
			$this->t_description = form_translate('Long name');
			$this->t_type = form_translate('Type');
			$this->t_types = form_translate('Types');
			$this->t_default_val = form_translate('Default value');
			$this->t_enabled = form_translate('Enabled');
			$this->t_submit = form_translate('Record');
			$this->t_add = form_translate('Add');
			$this->t_fields = form_translate('field(s)');
			$this->t_ok = form_translate('Ok');
			$this->t_delete_field = form_translate('Delete field');
			$this-> t_delete = form_translate('Del.');
			$this->t_filing_delay = form_translate('Days before archiving');
			$this->t_data = form_translate('Data');
			$this->t_alias = form_translate('Alias');
			$this->t_disable = form_translate('Disable');
			$this->t_query_mode = form_translate('Query mode');
			$this->t_edit_type = form_translate('Edit type');
			$this->t_link_table_field_id = form_translate('Link to table');
			$this->t_add_type = form_translate('Add type');
			//$this->t_delete = form_translate('Delete');
			$this->js_delete = form_translate('Do you really want to delete this table').'?';
			$this->js_alert = form_translate('Do you really want to delete these items?');
			$this->t_help = form_translate('Help');
			$this->t_approbation = form_translate('Approbation');
			$this->t_yes = form_translate('Yes');
			$this->t_no = form_translate('No');
			$this->t_functions = form_translate('Functions');
			$this->t_index = form_translate('Index');
		

			$this->db = & $GLOBALS['babDB'];

			if (isset($_POST['action']) && $_POST['action'] == 'edit_table')
				{
				$this->arr = $_POST;
				$this->arr['id'] = $this->arr['id_table'];
				}
			elseif (is_numeric($id))
				{
				$res = $this->db->db_query("SELECT * FROM ".FORM_TABLES." WHERE id='".$this->id."'");
				$this->arr = $this->db->db_fetch_array($res);

				$this->resfields = $this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($this->id)."' AND field_function='' ORDER BY ordering");
				$this->arr['nb_fields'] = $this->db->db_num_rows($this->resfields);
				}
			else
				{
				$this->arr = array();
				}

			if (empty($this->arr['nb_fields'])) $this->arr['nb_fields'] = 0;

			$el_to_init = array('id','name','description','filing_delay','nb_fields');
			foreach ($el_to_init as $field)
				{
				$this->arr[$field] = isset($this->arr[$field]) ? $this->arr[$field] : '';
				}

			$this->restype = $this->db->db_query("
				SELECT t.* FROM 
					".FORM_TABLES_FIELDS_TYPES." t, 
					".FORM_WORKSPACE_ENTRIES." e 
				WHERE 
					e.type='type' 
					AND e.id_object=t.id 
					AND e.id_workspace=".$this->db->quote(form_getWorkspace())."
			");
			
			$this->counttype = $this->db->db_num_rows($this->restype);


			$this->restable = $this->db->db_query("
				SELECT t.id, t.name 
				FROM 
					".FORM_TABLES_FIELDS." f, 
					".FORM_TABLES." t, 
					".FORM_WORKSPACE_ENTRIES." e 
				WHERE 
					t.id<>'".$this->db->db_escape_string($this->arr['id'])."' 
					AND t.id=f.id_table 
					AND e.type='table' 
					AND e.id_object=t.id 
					AND e.id_workspace=".$this->db->quote(form_getWorkspace())."
				GROUP BY t.id 
				ORDER BY name
			");

			$this->fieldslinks = array();

			$this->reslink = $this->db->db_query("
				SELECT 
					f.id, 
					t.id id_table,
					f.name 
				FROM 
					".FORM_TABLES_FIELDS." f, 
					".FORM_TABLES." t 
				WHERE 
					t.id<>'".$this->db->db_escape_string($this->arr['id'])."' 
					AND f.id_table = t.id 
					AND f.field_function IN('','index') 
				GROUP BY t.id,f.id 
				ORDER BY t.name,f.name
			");
			
			while ( $arr = $this->db->db_fetch_array($this->reslink))
				{
				$this->fieldslinks[$arr['id_table']][$arr['id']] = $arr['name'];
				}
				
				
			$this->arr['name'] = bab_toHtml($this->arr['name']);
			$this->arr['description'] = bab_toHtml($this->arr['description']);
			$this->arr['filing_delay'] = bab_toHtml($this->arr['filing_delay']);
				
            }

		function getnextfield(&$skip)
			{
			static $i=0,$j = 0, $deleted=0;
			if( $i < $this->arr['nb_fields'] + $deleted)
				{
				

				if (isset($_POST['del_fields']) && count($_POST['del_fields']) > 0 && in_array($i,$_POST['del_fields']))
					{
					$skip = true;
					$i++;
					$deleted++;
					return true;
					}

				$this->altbg = !$this->altbg;
				$index = $i;
				$el_to_init = array('id','name','description','id_type','type','default_val','link_table_field_id','field_function');
				
				$default = array();
				$default['query_mode'] = 'DATA';

				if (isset($this->resfields))
					{
					$arr = $this->db->db_fetch_array($this->resfields);
					foreach ($el_to_init as $el)
						$this->field[$el] = isset($arr[$el]) ? $arr[$el] : (isset($default[$el]) ? $default[$el] : '');
					}
				else
					{
					$arr = $_POST;
					foreach ($el_to_init as $el)
						$this->field[$el] = isset($arr['field_'.$el.'_'.$index]) ? $arr['field_'.$el.'_'.$index] : (isset($default[$el]) ? $default[$el] : '');
					$this->field['id_type'] = $this->field['type'];
					if (isset($this->field['type']) && !is_numeric($this->field['type']))
						{
						$this->field['field_function'] = $this->field['type'];
						}
					}
					
				$this->field['id'] = bab_toHtml($this->field['id']);
				$this->field['name'] = bab_toHtml($this->field['name']);
				$this->field['default_val'] = bab_toHtml($this->field['default_val']);

				$this->index = $j;
				$i++;
				$j++;
				return true;
				}
			else
				{
				$deleted = 0;
				$i = 0;
				$j =0;
				return false;
				}
			}

		function getnexttype()
			{
			static $i=0;
			if( $i < $this->counttype)
				{
				$this->type = $this->db->db_fetch_array($this->restype);
				$this->selected = $this->type['id'] == $this->field['id_type'] ? 'selected' : '';
				$this->type['name'] = bab_toHtml($this->type['name']);
				$i++;
				return true;
				}
			else
				{
				if ($this->restype && $this->counttype >0)
					$this->db->db_data_seek($this->restype,0);
				$i=0;
				return false;
				}
			}

		function getnexttable()
			{
			$this->table = $this->db->db_fetch_array($this->restable);
			if (!$this->table)
				{
				if ($this->restype && $this->db->db_num_rows($this->restable) > 0)
					$this->db->db_data_seek($this->restable,0);
				return false;
				}
			$this->table['name'] = bab_toHtml($this->table['name']);
			return true;
			}
			
		function getnextlink()
			{
			if (list($this->linkid,$this->linkname) = each($this->fieldslinks[$this->table['id']]))
				{
				$this->selected = ($this->field['link_table_field_id'] == $this->linkid) ? 'selected' : '';
				$this->linkname = bab_toHtml($this->linkname);
				return true;
				}
			else
				{
				reset($this->fieldslinks[$this->table['id']]);
				return false;
				}
			}

        }
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "edit_table" ));
}


function list_types()
{
global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_nb_rec = form_translate('Records');
			$this->t_edit = form_translate('Edit');
			$this->t_used = form_translate('In use');
			$this->t_sql_type = form_translate('Sql type');
			$this->t_help = form_translate('Help');
			$this->t_extend = form_translate('Extended format');

			$FORM_USER = form_getUserFormAcces();
			
			$this->manager = $FORM_USER['ADMIN'];
			$this->res = $this->db->db_query("SELECT 
					t.*,
					IF(COUNT(f.id)>0,NULL,'Y') notused, 
					IFNULL(s.name,'') sql_type, 
					e.name extend 
				FROM 
					".FORM_TABLES_FIELDS_TYPES." t
				LEFT JOIN 
					".FORM_TABLES_FIELDS." f 
					ON f.id_type = t.id 
				LEFT JOIN 
					".FORM_SQL_TYPES." s 
					ON t.sql_type = s.id , 
				".FORM_TABLES_FIELDS_TYPES_EXTEND." e,
				".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					w.id_object = t.id 
					AND w.type = 'type' 
					AND w.id_workspace=".$this->db->quote(form_getWorkspace())."
					AND e.id = t.id_extend 
				GROUP BY t.id 
				ORDER BY t.name
			");

			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);
				$this->edit_url = bab_toHtml($GLOBALS['babAddonUrl']."main&idx=edit_type&id_type=".$this->arr['id']);
				$this->arr['extend'] 		= bab_toHtml(form_translate($this->arr['extend']));
				$this->arr['name'] 			= bab_toHtml($this->arr['name']);
				$this->arr['description'] 	= bab_toHtml($this->arr['description']);
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "list_types" ));
}

function decompose_sql_format($str)
	{
	$out = array('sql_type'=>'','sql_size'=>'','sql_decimal'=>'','sql_attrib'=>'','sql_values'=>array());

	$str = trim($str);
	$tmp = explode(' ',$str);
	if (count($tmp) == 2)
		{
		$out['sql_attrib'] = $tmp[1];
		}

	if (substr_count($str,'(') == 1)
		{
		$pos = strpos ( $str, '(');
		$out['sql_type'] = substr($str, 0, $pos);
		$end = substr($str, $pos+1,-1);
		if (in_array(strtoupper($out['sql_type']),array('ENUM','SET')))
			{
			function trimquotes($str)
				{
				return trim($str,"'");
				}
			$out['sql_values'] = array_map('trimquotes',explode(',',$end));
			}
		elseif (substr_count($end,',') == 1)
			{
			$tmp = explode(',',$end);
			$out['sql_size'] = $tmp[0];
			$out['sql_decimal'] = $tmp[1];
			}
		else
			{
			$out['sql_size'] = $end;
			settype($out['sql_size'],'integer');
			}
		}
	else
		{
		$out['sql_type'] = $tmp[0];
		}

	return $out;
	}



function extend_type() {

global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = &$GLOBALS['babDB'];
			$this->t_next = form_translate('Next');
			$this->id_table = isset($_REQUEST['id_table']) ? $_REQUEST['id_table'] : '';
			$this->res = $this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS_TYPES_EXTEND."");
            }

		function getnext()
			{
			if ($this->ext = $this->db->db_fetch_assoc($this->res)) {
				$this->ext['name'] = form_translate($this->ext['name']);
				$this->selected = $this->ext['id'] == 1;
				$this->altbg = !$this->altbg;
				return true;
				}
			return false;
			}
			
        }
    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "extend_type" ));
}



function edit_type_sql($id = '') {
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			$this->t_new_type = form_translate('New type');
			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_sql_type = form_translate('SQL type');
			$this->t_sql_size = form_translate('Length');
			$this->t_sql_decimal = form_translate('Decimal');
			$this->t_sql_vals = form_translate('Values');
			$this->t_sql_auto_increment = form_translate('Auto increment');
			$this->t_sql_attrib = form_translate('Attribut');
			$this->t_unsigned = form_translate('unsigned');
			$this->t_button = form_translate('Record');
			$this->delete_type_confirmJs = form_translate('Do you really want to delete this type?');
			$this->t_delete = form_translate('Delete');
			$this->t_add = form_translate('Add');
			$this->t_del = form_translate('Del');
			$this->t_help = form_translate('Help');
			
			$this->db = &$GLOBALS['babDB'];

			$FORM_USER = form_getUserFormAcces();

			$this->manager = $FORM_USER['ADMIN'];

			if (is_numeric($this->id))
				{
				$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT IF(COUNT(f.id)>0,NULL,'Y') notused,t.* FROM ".FORM_TABLES_FIELDS_TYPES." t LEFT JOIN ".FORM_TABLES_FIELDS." f ON f.id_type = t.id WHERE t.id='".$this->db->db_escape_string($this->id)."' GROUP BY t.id"));
				}
			else
				{
				$el_to_init = array('name','description','sql_type');
				foreach($el_to_init as $field)
					{
					$this->arr[$field] = '';
					}
				}
			if (!isset($this->arr['col_type'])) $this->arr['col_type'] = '';
			$this->sql = decompose_sql_format($this->arr['col_type']);
			$this->unsigned_selected = substr_count($this->sql['sql_attrib'],'unsigned') == 1 ? 'selected' : '';

			$this->id_table = '';
			$this->id_form = '';
				
			if (isset($_POST['id_table']) && $_POST['id_table'] > 0)
				{
				$this->nextidx = 'edit_table';
				$this->id_table = $_POST['id_table'];
				}
			elseif (isset($_POST['id_form']))
				{
				$this->nextidx = 'edit_form';
				$this->id_form = $_POST['id_form'];
				}
			else
				{
				$this->nextidx = 'list_types';
				}

			$this->ressql = $this->db->db_query("SELECT * FROM ".FORM_SQL_TYPES."");
			$this->countsql = $this->db->db_num_rows($this->ressql);
            }

		function getnextsql_type()
			{
			static $i=0;
			if( $i < $this->countsql)
				{
				$this->sql_type = $this->db->db_fetch_array($this->ressql);
				$this->selected = ($this->arr['sql_type'] == $this->sql_type['id']) ? 'selected' : '';
				$i++;
				return true;
				}
			else
				{
				if ($this->countsql > 0)
					$this->db->db_data_seek($this->ressql,0);
				$i=0;
				return false;
				}
			}
			
		function getnextsqlvalue()
			{
			static $i=0;
			if( $i < count($this->sql['sql_values']))
				{
				// les valeur enumerees on 1 echappement supplementaire
				$this->value = bab_toHtml(stripslashes($this->sql['sql_values'][$i]));
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "edit_type" ));
}


function edit_type($id = '')
{
	$db = &$GLOBALS['babDB'];

	list($id_extend) = $db->db_fetch_array($db->db_query("SELECT id_extend FROM ".FORM_TABLES_FIELDS_TYPES." WHERE id='".$db->db_escape_string($id)."'"));
	
	if (1 == $id_extend) {
		edit_type_sql($id);
		}
	else
		{
		edit_extended_type($id_extend, $id);
		}
}



function edit_extended_type($id_extend, $id = '') {

	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id_extend, $id)
            {
			$this->db = &$GLOBALS['babDB'];
			$this->id_extend = $id_extend;
			$this->id = $id;
			$this->id_table = bab_rp('id_table');
			
			$this->t_name = form_translate('Name');
			$this->t_delete = form_translate('Delete');
			$this->t_description = form_translate('Description');
			$this->t_forms_elements = form_translate('Form elements');
			$this->t_record = form_translate('Record');
			$this->t_directory = form_translate('Directory');
			$this->t_view_date_format = form_translate('View date format');
			$this->t_date_forms = form_translate('Fixed format');
			$this->t_date_ovidentia = form_translate('Format defined by ovidentia options');
			$this->delete_type_confirmJs = form_translate('Do you really want to delete this type?');
			$this->t_on_delete_user = form_translate('Delete rows when the user is deleted');

			

			$this->extend = $this->db->db_fetch_assoc($this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS_TYPES_EXTEND." WHERE id ='".$this->db->db_escape_string($id_extend)."'"));

			$this->on_delete_user = 4 == $id_extend;

			$this->extend['name'] = form_translate($this->extend['name']);

			$this->resfe = $this->db->db_query("SELECT * FROM ".FORM_FORMS_ELEMENTS." WHERE id IN(".$this->extend['id_forms_elements'].")");

			if (empty($this->id)) {
				// nouveau
				$this->arr = array(
					'name' => '',
					'description' => '',
					'id_forms_elements' => $this->extend['id_forms_elements']
					);

				$this->options = array(
					
					'view_date_format' => 2
					);
				}
			else {
				// modification
				$req = "SELECT IF(COUNT(f.id)>0,NULL,'Y') notused, t.* FROM ".FORM_TABLES_FIELDS_TYPES." t LEFT JOIN ".FORM_TABLES_FIELDS." f ON f.id_type = t.id WHERE t.id='".$this->db->db_escape_string($this->id)."' GROUP BY t.id";
				$this->arr = $this->db->db_fetch_assoc($this->db->db_query($req));

				$this->options = unserialize($this->arr['str_options']);

				$this->on_delete_user_checked = $this->arr['on_delete_user'];
				}

			$this->checked_fe = array_flip(explode(',',$this->arr['id_forms_elements']));


			// formatage des dates, 2 choix possibles (module formulaire ou ovidentia)

			if (2 == $id_extend || 3 == $id_extend)
				$this->formatdate = true;
			

			// annuaires

			if (5 == $id_extend || 4 == $id_extend ) {
				$this->resdir = bab_getUserDirectories();
				if (empty($this->resdir)) {
					global $babBody;
					$babBody->addError(form_translate('You need at least one directory with reading access'));
				}
			}
				
				
			$this->arr['name'] 			= bab_toHtml($this->arr['name']);
			$this->arr['description'] 	= bab_toHtml($this->arr['description']);
            }

		function getnextformelement() {
			if ($this->fe = $this->db->db_fetch_assoc($this->resfe)) {
				$this->fe['description'] = form_translate($this->fe['description']);
				$this->checked = isset($this->checked_fe[$this->fe['id']]);
				return true;
				}
			return false;
			}

		function getnextdir(&$skip) {
			
			if (list($this->value, $arr) = each($this->resdir)) {
				
				$valid = 5 == $this->id_extend ? $arr['entry_id_directory'] : $arr['id_group'];

				if ($valid == 0) {
					$skip = true;
					return true;
					}

				if (isset($this->options['id_directory']))
					$this->selected = $this->options['id_directory'] == $this->value;

				$this->option = $arr['name'];
				return true;
				}
			return false;
			}
        }
    $tp = new temp($id_extend, $id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "edit_extended_type" ));
}


function list_forms()
{
global $babBody;
    class list_forms_temp
        { 
		var $altbg = false;
        function list_forms_temp()
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_type = form_translate('Type');
			$this->t_acces = form_translate('Rights');
			$this->t_access_title = form_translate('Set access rights on all forms of workspace');
			$this->t_edit = form_translate('Edit');
			$this->t_ordering = form_translate('Order');
			$this->t_form = form_translate('Form');
			$this->t_help = form_translate('Help');
			$this->t_help_management = form_translate('Help management');
			$this->t_proposal_management = form_translate('Proposals management');
			$this->t_mail = form_translate('Mail tools');
			$this->t_curl = form_translate('Transaction tools');
			$this->t_curl_required = form_translate('CURL is required');
			$this->t_search = form_translate('Search tools');
			$this->t_options = form_translate('Options');
			$this->t_elements = form_translate('Form elements');
			$this->t_filter_by_table = form_translate('Filter by table');
			$this->t_styles = form_translate('Styles');
			$this->t_init = form_translate('Fields initialisation');
			$this->t_approbation_list = form_translate('Approbation list options');
			$this->t_transform = form_translate('Transform the read-only values');
			$this->t_form_approb = form_translate('Approbation form options');
			$this->t_ordgrp = form_translate('Order by and group by');
			$this->t_copy_form = form_translate('Copy form');
			$this->t_increment = form_translate('Increment');

			$this->access_all_url = bab_toHtml($GLOBALS['babAddonUrl']."rights&idx=acces_form");
			$this->urlorderby =  $GLOBALS['babAddonUrl']."main&idx=list_forms";
			$id_workspace = form_getWorkspace();

			if (isset($_GET['id_table']) && !empty($_GET['id_table']))
				{
				$_SESSION['FORM_WORK_ID_TABLE'][$id_workspace] = $_GET['id_table'];
				}
			elseif (isset($_GET['id_table']) && $_GET['id_table'] == 0)
				{
				$_SESSION['FORM_WORK_ID_TABLE'][$id_workspace] = '';
				}

			if (!empty($_SESSION['FORM_WORK_ID_TABLE'][$id_workspace]))
				{
				
				$this->urlorderby .= '&id_table='.$_SESSION['FORM_WORK_ID_TABLE'][$id_workspace];
				$filter = "AND id_table='".$this->db->db_escape_string($_SESSION['FORM_WORK_ID_TABLE'][$id_workspace])."'";
				}
			else
				{
				$filter = '';
				}

			$orderby = isset($_GET['orderby']) ? "ORDER BY ".$_GET['orderby'] : "ORDER BY f.name";

			
			$this->res = $this->db->db_query("
			
				SELECT 
					f.*,
					t.id id_type,
					t.description type, 
					t.curl_required, 
					ta.approbation 
				FROM 
					".FORM_FORMS." f
				LEFT JOIN 
					".FORM_TABLES." ta 
					ON ta.id=f.id_table , 
					
				".FORM_FORMS_TYPES." t,
				".FORM_WORKSPACE_ENTRIES." w 
					
				WHERE 
					w.id_object = f.id 
					AND w.type = 'form' 
					AND w.id_workspace = ".$this->db->quote(form_getWorkspace())."
					AND t.id = f.id_type ".$filter." 
				GROUP BY f.id ".$this->db->db_escape_string($orderby)."
				
				");

			$this->count = $this->db->db_num_rows($this->res);

			$this->restable = $this->db->db_query("
				SELECT 
					t.id, t.name 
				FROM 
					".FORM_TABLES." t,
					".FORM_WORKSPACE_ENTRIES." w
				WHERE 
					t.created='Y' 
					AND w.id_object = t.id 
					AND w.type = 'table' 
					AND w.id_workspace = ".$this->db->quote(form_getWorkspace())."
					ORDER BY name
			");
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);
				$this->arr['name'] = bab_toHtml($this->arr['name']);
				$this->arr['description'] = bab_toHtml($this->arr['description']);
				$this->arr['type'] = form_translate($this->arr['type']);
				$this->create_mod = in_array($this->arr['id_type'],array(2,4,7));
				$this->mod_view = in_array($this->arr['id_type'],array(2,3,4,7));
				$this->search = in_array($this->arr['id_type'],array(1,6,8));
				$this->approbation_list = 1 == $this->arr['id_type'] && $this->arr['approbation'];
				$this->transform = in_array($this->arr['id_type'],array(1,2,3,4,7));
				$this->form_approb = $this->arr['id_type'] == 7;

				$this->form_url = $GLOBALS['babAddonUrl']."form&amp;idx=".$this->arr['id_type']."&amp;id_form=".$this->arr['id'];
				$this->edit_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form&amp;id_form=".$this->arr['id'];
				$this->ordering_url = $GLOBALS['babAddonUrl']."main&amp;idx=order_form&amp;id_form=".$this->arr['id'];
				$this->acces_url = $GLOBALS['babAddonUrl']."rights&amp;idx=acces_form&amp;id_form=".$this->arr['id'];
				$this->mail_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_mail&amp;id_form=".$this->arr['id'];
				$this->curl_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_curl&amp;id_form=".$this->arr['id'];
				$this->search_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_search&amp;id_form=".$this->arr['id'];
				$this->ordgrp_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_ordgrp&amp;id_form=".$this->arr['id'];
				$this->elements_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_elements&amp;id_form=".$this->arr['id'];
				$this->init_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_init&amp;id_form=".$this->arr['id'];
				$this->styles_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_styles&amp;id_form=".$this->arr['id'];
				$this->approbation_list_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_approbation_list&amp;id_form=".$this->arr['id'];
				$this->transform_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_transform&amp;id_form=".$this->arr['id'];
				$this->form_approb_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_validation&amp;id_form=".$this->arr['id'];
				$this->form_increment_url = $GLOBALS['babAddonUrl']."main&amp;idx=edit_form_increment&amp;id_form=".$this->arr['id'];

				$this->allow_form = bab_isAccessValid(FORM_FORMS_GROUPS,$this->arr['id']);
				$this->nocurl = !FORM_CURL && $this->arr['curl_required'] == 'Y';
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}


		function getnexttable()
			{
			if ($this->table = $this->db->db_fetch_array($this->restable))
				{
				$id_workspace = form_getWorkspace();
				$this->selected = isset($_SESSION['FORM_WORK_ID_TABLE'][$id_workspace]) && $_SESSION['FORM_WORK_ID_TABLE'][$id_workspace] == $this->table['id']; 
				return true;
				}
			else
				{
				return false;
				}
			}
        }
    $tp = new list_forms_temp();
    $babBody->addStyleSheet($GLOBALS['babAddonHtmlPath'].'forms.css');
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "list_forms"));
}


function edit_form($id = '')
{
global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->db = &$GLOBALS['babDB'];
			$this->id = $id;
			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_type = form_translate('Type');
			$this->t_acces = form_translate('Rights');
			$this->t_edit = form_translate('Edit');
			$this->t_record = form_translate('Record');
			$this->t_table = form_translate('Table');
			$this->t_field = form_translate('Field link');			
			$this->t_search = form_translate('Search');
			$this->t_more = form_translate('More options');
			$this->t_delete = form_translate('Del.');
			$this->t_add = form_translate('Add');
			$this->t_fields = form_translate('Fields');
			$this->t_ok = form_translate('Ok');
			$this->t_add_type = form_translate('Add type');
			$this->t_init_fields = form_translate('Init fields with table structure');
			$this->t_delete_field = form_translate('Delete field');
			$this->t_longdesc = form_translate('Long description');
			$this->t_list_filed_rows = form_translate('List archive rows');
			$this->t_buttons_next_previous = form_translate('Page next previous');
			$this->t_hide = form_translate('Hide');
			$this->t_viewable = form_translate('Viewable');
			$this->t_always = form_translate('Always');
			$this->t_alert = form_translate('Error message');
			$this->t_rows_per_page = form_translate('Rows per page');
			$this->js_alert_print = form_translate('Incorrect print mode in field');
			$this->js_confirmdelete = form_translate('Do you really want to delete this form').' ?';
			$this->t_delete = form_translate('Del.');
			$this->t_order_by = form_translate('Order');
			$this->t_order_type = form_translate('Ordering');
			$this->t_asc = form_translate('Asc');
			$this->t_desc = form_translate('Desc');
			$this->t_view_notempty = form_translate('Indicate not empty fields');
			$this->t_yes = form_translate('Yes');
			$this->t_no = form_translate('No');
			$this->t_allow_delete = form_translate('Allow delete');
			$this->t_help = form_translate('Help');
			$this->t_id_qte_table_col = form_translate('Shopping list quantities initialisation');
			$this->t_formula = form_translate('Formula');
			$this->t_sc_formula = form_translate('Shopping cart formula');
			$this->t_trans_formula = form_translate('Total formula');
			$this->t_subtotal = form_translate('Sub-total');
			$this->t_quantity = form_translate('Quantity');
			$this->t_unit_concat = form_translate('Formula result unit');
			$this->t_total_qte = form_translate('Total quantities');
			$this->t_trans_formula_msg = form_translate('Total formula message');
			$this->t_id_form_card = form_translate('Shopping cart form');
			$this->t_card_table_fields = form_translate('Transaction table fields');
			$this->t_css_file = form_translate('Stylesheet');
			$this->t_id_table_copy = form_translate('Insert modification copy into table');
			$this->t_waiting_modify = form_translate('Authorize the modification of the waiting or refused lines');
			$this->t_creator_only = form_translate('Creator only');
			$this->t_all = form_translate('All');
			$this->js_alert = form_translate('Do you really want to delete these items?');
			$this->t_delete_refused = form_translate('Delete refused lines');
			$this->t_copy = form_translate('Copy form');
			$this->t_create_form_copy = form_translate('Create a copy of the form').'?';
			$this->curl = FORM_CURL;

			$this->del_fields = isset($_POST['del_fields']) ? $_POST['del_fields'] : array();

			$req = "SELECT * FROM ".FORM_FORMS_TYPES."";

			if (!$this->curl)
				$req .= " WHERE curl_required='N'";

			$this->resformtype = $this->db->db_query($req);
			$this->countformtype = $this->db->db_num_rows($this->resformtype);

			$this->restable = $this->db->db_query("
				SELECT t.* FROM 
					".FORM_TABLES." t,
					".FORM_WORKSPACE_ENTRIES." e 
				WHERE 
					t.created='Y' 
					AND e.type='table' 
					AND e.id_object=t.id 
					AND e.id_workspace=".$this->db->quote(form_getWorkspace())." 
				
				ORDER BY t.name
				
			");
			$this->counttable = $this->db->db_num_rows($this->restable);

			if (isset($_POST['action']) && $_POST['action'] == 'edit_form')
				{
				$this->arr = $_POST;
				$this->arr['buttons_next_previous'] = isset($_POST['buttons_next_previous']) ? 'Y' : 'N';
				$this->arr['allow_delete'] = isset($_POST['allow_delete']) ? 'Y' : 'N';
				}
			elseif (isset($_POST['action']) && $_POST['action'] == 'init_fields' && is_numeric($_POST['id_table']))
				{
				$this->arr = $_POST;
				$this->arr['buttons_next_previous'] = isset($_POST['buttons_next_previous']) ? 'Y' : 'N';
				$this->arr['allow_delete'] = isset($_POST['allow_delete']) ? 'Y' : 'N';
				$this->resfields = $this->db->db_query("SELECT name,description,id_type,id id_table_field FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($_POST['id_table'])."' ORDER BY ordering");
				$this->arr['nb_fields'] = $this->db->db_num_rows($this->resfields);
				}
			elseif (is_numeric($this->id))
				{
				$res = $this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->id)."'");
				$this->arr = $this->db->db_fetch_array($res);

				
				$this->resfields = $this->db->db_query("SELECT * FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$this->db->db_escape_string($this->id)."' ORDER BY ordering");
				$this->arr['nb_fields'] = $this->db->db_num_rows($this->resfields);
				}
			else
				{
				$this->arr = array();
				}
			
			$default = array();
			$default['list_filed_rows'] = 'VIEWABLE';
			$default['buttons_next_previous'] = 'Y';
			$default['allow_delete'] = 'N';
			$default['init_on_modify'] = 'N';
			$default['waiting_modify'] = 0;
			$default['delete_refused'] = 0;
			$default['rows_per_page'] = 30;

			$el_to_init = array('nb_fields');

			$res = $this->db->db_query("DESCRIBE ".FORM_FORMS);
			while (list($field) = $this->db->db_fetch_array($res))
				{
				$el_to_init[] = $field;
				}
			
			foreach ($el_to_init as $field)
				{
				$this->arr[$field] = isset($this->arr[$field]) ? $this->arr[$field] : (isset($_POST[$field]) ? $_POST[$field] : (isset($default[$field]) ? $default[$field] : ''));
				}
			
			if (empty($this->arr['list_filed_rows'])) $this->arr['list_filed_rows'] = 'VIEWABLE';

			$this->countlink = 0;
			if ($this->arr['nb_fields'] > 0)
				{
				$this->restype = $this->db->db_query("
					SELECT t.* FROM 
						".FORM_TABLES_FIELDS_TYPES." t,
						".FORM_WORKSPACE_ENTRIES." w 
					WHERE 
						w.id_object=t.id 
						AND w.id_workspace=".$this->db->quote(form_getWorkspace())." 
						AND w.type='type'
				");
				$this->counttype = $this->db->db_num_rows($this->restype);



				if (is_numeric($this->arr['id_table']))
					{
					$this->reslink = $this->db->db_query("
								SELECT 
									t1.id, 
									t1.name,
									IF(t5.id IS NULL,t3.id,t5.id) linkid,
									IF(t5.name IS NULL,CONCAT(t2bis.name,'.',t3.name),CONCAT(t5bis.name,'.',t5.name)) linkname,
									t2.id_table lvl2table 
									
								FROM 
									".FORM_TABLES_FIELDS." t1
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t2 
									ON t2.id=t1.link_table_field_id 
								LEFT JOIN 
									".FORM_TABLES." t2bis
									ON t2bis.id = t2.id_table 
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t3 
									ON t3.id_table=t2.id_table 
									AND t3.id<>t1.id 
								LEFT JOIN 
									".FORM_TABLES." t3bis
									ON t3bis.id = t3.id_table 
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t4 
									ON t4.id=t3.link_table_field_id 
								LEFT JOIN
									".FORM_TABLES_FIELDS." t5 
									ON t5.id_table=t4.id_table 
									AND t5.id<>t3.id
									AND t5.id<>t1.id 
								LEFT JOIN 
									".FORM_TABLES." t5bis 
									ON t5bis.id=t5.id_table 
								WHERE 
									t1.id_table='".$this->db->db_escape_string($this->arr['id_table'])."' 
								GROUP BY t1.id,t3.id,t5.id
								
								");
					$this->countlink = $this->db->db_num_rows($this->reslink);
					}

				}

			

			$this->resform = $this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id_type='5'");

			$this->create_link_arr = true;
			$this->styles = form_getStyles();

			$this->lnk = array();
			$res = $this->db->db_query("SELECT id,name,id_table1 FROM ".FORM_TABLES_LNK."");
			while($arr = $this->db->db_fetch_array($res))
				{
				if (!isset($this->lnk[$arr['id_table1']]))
					$this->lnk[$arr['id_table1']] = array();

				$this->lnk[$arr['id_table1']][$arr['id']] = $arr['name'];
				}

			$this->arr['id_order_by'] 	= bab_toHtml($this->arr['id_order_by']);
			$this->arr['nb_fields'] 	= bab_toHtml($this->arr['nb_fields']);
			$this->arr['name'] 			= bab_toHtml($this->arr['name']);
			$this->arr['description'] 	= bab_toHtml($this->arr['description']);
			$this->arr['longdesc'] 		= bab_toHtml($this->arr['longdesc']);

            }

		function getnextformtype()
			{
			static $i=0;
			if( $i < $this->countformtype)
				{
				$this->type = $this->db->db_fetch_array($this->resformtype);
				$this->type['name'] = bab_toHtml(form_translate($this->type['description']));
				$this->selected = isset($this->arr['id_type']) && $this->type['id'] == $this->arr['id_type'] ? 'selected' : '';
				$i++;
				return true;
				}
			else
				{
				return false;
				}
			}

		function getnexttable()
			{
			static $i=0;
			static $j=0;
			if( $i < $this->counttable)
				{
				$this->table = $this->db->db_fetch_array($this->restable);
				if (!empty($this->arr['id_table']) && $this->table['id'] == $this->arr['id_table'] && $j == 0)
					{
					$this->selected = 'selected';
					
					}
				elseif ($j == 1 && !empty($this->arr['id_table_copy']) && $this->table['id'] == $this->arr['id_table_copy'])
					{
					$this->selected = 'selected';
					}
				else
					{
					$this->selected = '';
					}
					
				$this->table['id'] = bab_toHtml($this->table['id']);
				$this->table['name'] = bab_toHtml($this->table['name']);
					
				$i++;
				return true;
				}
			else
				{
				$i = 0;
				$j = 1;
				if ($this->restable && $this->counttable >0)
					$this->db->db_data_seek($this->restable,0);
				return false;
				}
			}

		function getnextstyle()
			{
			$bool = (false !== $this->style = current($this->styles));
			$this->selected = $this->style == $this->arr['css_file'] ? 'selected' : '';
			$this->styleDisplay = bab_toHtml(form_translate(substr($this->style,0,-4)));
			next($this->styles);
			return $bool;
			}

		function getnextfield(&$skip)
			{
			static $i=0,$j = 0, $deleted=0;
			if( $i < $this->arr['nb_fields'] + $deleted)
				{
				
				if (in_array($i,$this->del_fields))
					{
					$skip = true;
					$i++;
					$deleted++;
					return true;
					}

				$this->altbg = !$this->altbg;

				$el_to_init = array('id','name','description','notempty','id_type','type','id_table_field','table_field','id_table_field_link','table_field_link','mail','search','id_print','print','init_dir','init','filter_on_dir','filter_on','id_form_element','form_element','id_qte_table_col','init_on_modify', 'field_transaction', 'field_copy_table_field', 'field_help','field_copy_on_change','field_link_to_fill', 'id_table_field_lnk','id_lnk','field_use_key_val','filter_type');
				$index = $i;
				$default = array();

				if (isset($this->resfields))
					{
					$arr = $this->db->db_fetch_array($this->resfields);
					$this->field = $arr;
					foreach ($el_to_init as $el)
						$this->field[$el] = isset($arr[$el]) ? $arr[$el] : (isset($default[$el]) ? $default[$el] : '');

					$this->field['table_field'] = $arr['id_table_field'];

					if (isset($_POST['action']) && $_POST['action'] == 'init_fields' && is_numeric($_POST['id_table']))
						{
						$this->field['id_forms_elements'] = form_getElementByType($this->field['id_type']);
						}
					}
				else
					{
					$arr = $_POST;
					foreach ($el_to_init as $el)
						{
						if (isset($arr['field_'.$el.'_'.$index]))
							$this->field[$el] = $arr['field_'.$el.'_'.$index];
						elseif (isset($arr[$el.'_'.$index]))
							$this->field[$el] = $arr[$el.'_'.$index];
						elseif (isset($default[$el]))
							$this->field[$el] = $default[$el];
						else
							$this->field[$el] = '';
						}
						
					$el_to_equal = array('type','table_field','table_field_link','validation','print','form_element');
					foreach ($el_to_equal as $el)
						$this->field['id_'.$el] = isset($this->field[$el]) ? $this->field[$el] : '';

					}

				$this->order_by_checked = !empty($this->field['id']) && $this->field['id'] == $this->arr['id_order_by'] ? 'checked' : '';


				$this->field['id'] = bab_toHtml($this->field['id']);
				$this->field['name'] = bab_toHtml($this->field['name']);
				$this->field['id_print'] = bab_toHtml($this->field['id_print']);
				$this->field['description'] = bab_toHtml($this->field['description']);
				$this->field['filter_on_dir'] = bab_toHtml($this->field['filter_on_dir']);
				$this->field['filter_on'] = bab_toHtml($this->field['filter_on']);
				$this->field['filter_type'] = bab_toHtml($this->field['filter_type']);
				$this->field['field_use_key_val'] = bab_toHtml($this->field['field_use_key_val']);
				$this->field['field_copy_table_field'] = bab_toHtml($this->field['field_copy_table_field']);
				$this->field['field_copy_on_change'] = bab_toHtml($this->field['field_copy_on_change']);
				$this->field['field_help'] = bab_toHtml($this->field['field_help']);
				$this->field['field_link_to_fill'] = bab_toHtml($this->field['field_link_to_fill']);
				
				$this->index = $j;
				$i++;
				$j++;
				return true;
				}
			else
				{
				$deleted = 0;
				$i = 0;
				$j =0;
				return false;
				}
			}


		function getnexttype()
			{
			static $i=0;
			if( $i < $this->counttype)
				{
				$this->type = $this->db->db_fetch_array($this->restype);
				$this->selected = $this->type['id'] == $this->field['id_type'] ? 'selected' : '';
				$this->type['name'] = bab_toHtml($this->type['name']);
				$i++;
				return true;
				}
			else
				{
				if ($this->restype && $this->counttype >0)
					$this->db->db_data_seek($this->restype,0);
				$i=0;
				return false;
				}
			}

		function getnextlink(&$skip)
			{
			static $i=0;
			if( $i < $this->countlink)
				{
				$this->link = $this->db->db_fetch_array($this->reslink);
				$this->link['js_name'] = bab_toHtml($this->link['name'], BAB_HTML_JS);
					
				
				if (!empty($this->link['linkname']))
					{
					if ($this->index== 0 && !empty($this->link['lvl2table']) && isset($this->lnk[$this->link['lvl2table']]))
						{
						//print_r($this->link);
						//echo $this->link['linkname'].' '.$this->link['id'].'<br />';

						$this->lnk2[$this->link['lvl2table']] = array($this->link['id'],$this->lnk[$this->link['lvl2table']]);
						}
					$this->link['id'] = $this->link['id'].'-'.$this->link['linkid'];
					if ($this->create_link_arr)
						{
						if (!isset($this->optgroup[$this->link['name']]))
							$this->optgroup[$this->link['name']] = array();
						$this->optgroup[$this->link['name']][] = array($this->link['id'],$this->link['linkname']);
						}

					$skip = true;
					$i++;
					return true;
					}
				elseif (isset($this->field['id_table_field']))
					{
					$this->selected = ($this->field['id_table_field'] == $this->link['id']) ? 'selected' : '';
					}
				elseif (isset($this->arr['id_qte_table_col']))
					{
					$this->selected = ($this->arr['id_qte_table_col'] == $this->link['id']) ? 'selected' : '';
					}
					
				$this->link['name'] = bab_toHtml($this->link['name']);
				
				$i++;
				return true;
				}
			else
				{
				$this->create_link_arr = false;
				if ($this->countlink > 0)
					$this->db->db_data_seek($this->reslink,0);
				$i=0;
				return false;
				}
			}

		function getnextoptgroup()
			{
			if (isset($this->optgroup) && list($this->optgroupname) = each($this->optgroup))
				{
				$this->optgroupname_label = bab_toHtml($this->optgroupname);
				return true;
				}
			else
				{	
				if (isset($this->optgroup)) reset($this->optgroup);
				return false;
				}
			}

		function getnextoptgroupvalue()
			{
			if (!isset($this->optgroup)) {
				return false;
			}
			
			if (list($i,list($this->linkid,$this->linkname)) = each($this->optgroup[$this->optgroupname]))
				{
				$this->selected = '';
				if (!is_numeric($this->field['table_field']) && $this->linkid == $this->field['table_field'])
					{
					$this->selected = 'selected';
					}
				elseif (!empty($this->field['id_table_field_link']) && $this->linkid == $this->field['table_field'].'-'.$this->field['id_table_field_link'])
					{
					$this->selected = 'selected';
					}
					
				$this->linkid = bab_toHtml($this->linkid);
				$this->linkname = bab_toHtml($this->linkname);

				return true;
				}
			else
				{
				reset($this->optgroup[$this->optgroupname]);
				return false;
				}
			}

		

		function getnextform()
			{
			if ($this->form = $this->db->db_fetch_array($this->resform))
				{
				$this->form['name'] = bab_toHtml($this->form['name']);
				$this->selected = $this->form['id'] == $this->arr['id_form_card'] ? 'selected' : '';
				return true;
				}
			else
				return false;
			}

		function getnextlnk()
			{
			if (!isset($this->lnk[$this->arr['id_table']]))
				return false;
			
			if (list($this->lnk_id, $this->lnk_name) = each($this->lnk[$this->arr['id_table']]))
				{
				$this->reslnk = $this->db->db_query("
											SELECT 
												f.id, 
												CONCAT(n.name,'.',f.name) name
											FROM 
												".FORM_TABLES_FIELDS." f,
												".FORM_TABLES_LNK." t,
												".FORM_TABLES." n
											WHERE 
												t.id='".$this->db->db_escape_string($this->lnk_id)."' 
												AND t.id_table2 = f.id_table 
												AND n.id = t.id_table2
												");
												
				$this->lnk_name = bab_toHtml($this->lnk_name);
				return true;
				}
			else
				{
				reset($this->lnk[$this->arr['id_table']]);
				return false;
				}
			}

		function getnextlnkvalue()
			{
			if ($this->flnk = $this->db->db_fetch_array($this->reslnk))
				{
				

				$this->selected = false;

				if ($this->flnk['id'] == $this->field['id_table_field_lnk'] && $this->lnk_id == $this->field['id_lnk'])
					{
					$this->selected = true;
					}
				elseif (!empty($this->field['table_field']) && $this->field['table_field'] == 'lnk-'.$this->lnk_id.'-'.$this->flnk['id'])
					{
					$this->selected = true;
					}
					
				$this->flnk['name'] = bab_toHtml($this->flnk['name']);
					
				return true;
				}
			else
				{
				if ($this->db->db_num_rows($this->reslnk) > 0)
					$this->db->db_data_seek($this->reslnk,0);
				return false;
				}
			}

		function getnexttablelnk2()
			{
			if (isset($this->lnk2) && list($id_table,list($this->lnk2_id_field,$this->tbllnk2)) = each($this->lnk2))
				{
				$this->flnk['name'] = bab_toHtml($this->flnk['name']);
				return true;
				}
			else
				{
				if (isset($this->lnk2))
					reset($this->lnk2);
				return false;
				}
			}


		function getnextlnk2()
			{
			
			if (list($this->lnk_id, $this->lnk_name) = each($this->tbllnk2))
				{
				$this->reslnk2 = $this->db->db_query("
											SELECT 
												f.id, 
												CONCAT(n.name,'.',f.name) name
											FROM 
												".FORM_TABLES_FIELDS." f,
												".FORM_TABLES_LNK." t,
												".FORM_TABLES." n
											WHERE 
												t.id='".$this->db->db_escape_string($this->lnk_id)."' 
												AND t.id_table2 = f.id_table 
												AND n.id = t.id_table2
												");
				$this->lnk_name = bab_toHtml(lnk_name);
				return true;
				}
			else
				{
				reset($this->tbllnk2);
				return false;
				}
			}

		function getnextlnkvalue2()
			{
			if ($this->flnk = $this->db->db_fetch_array($this->reslnk2))
				{
				

				$this->selected = false;

				if ($this->flnk['id'] == $this->field['id_table_field_lnk'] && $this->lnk_id == $this->field['id_lnk'])
					{
					$this->selected = true;
					}
				elseif (!empty($this->field['table_field']) && $this->field['table_field'] == 'lnk-'.$this->lnk_id.'-'.$this->flnk['id'].'-'.$this->lnk2_id_field)
					{
					$this->selected = true;
					}
				$this->flnk['name'] = bab_toHtml($this->flnk['name']);
				return true;
				}
			else
				{
				if ($this->db->db_num_rows($this->reslnk2) > 0)
					$this->db->db_data_seek($this->reslnk2,0);
				return false;
				}
			}
        }
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "edit_form"));
}


function edit_form_mail($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			$this->t_mail = form_translate('Mail');
			$this->t_mail_user = form_translate('User e-mail');
			$this->t_mail_bcc = form_translate('Mail bcc');
			$this->t_id_table_field_mail = form_translate('Mail to field');
			$this->t_id_table_field_bcc = form_translate('Mail bcc to field');
			$this->t_help = form_translate('Help');
			$this->t_field_mail = form_translate('Use value as');
			$this->t_mailTo = form_translate('Mail to');
			$this->t_mailBcc = form_translate('Mail bcc');
			$this->t_mailToId = form_translate('Mail to (Id user)');
			$this->t_mailBccId = form_translate('Mail bcc (Id user)');
			$this->t_submit = form_translate('Submit');
			$this->t_name = form_translate('Name');
			$this->t_mail_onchange = form_translate('Send mail on change');
			$this->t_mail_subject = form_translate('Mail subject');
			$this->t_mail_ignore_empty_values = form_translate('Ignore empty values');
			$this->t_recipient = form_translate('Recipients');
			$this->t_bcc = form_translate('Blind carbon copy');
			$this->t_id_group_mail = form_translate('Ovidentia user group');

			$this->db = &$GLOBALS['babDB'];

			$this->t_record = form_translate('Record');

			$this->arr = $this->db->db_fetch_array($this->db->db_query("
				SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($id)."'
			"));
			
			$this->form_name = bab_toHtml($this->arr['name']);
			$this->arr['mail'] = bab_toHtml($this->arr['mail']);
			$this->arr['bcc'] = bab_toHtml($this->arr['bcc']);
			$this->arr['mail_subject'] = bab_toHtml($this->arr['mail_subject']);
			
			
			$this->res = $this->db->db_query("
				SELECT f.* FROM 
					".FORM_FORMS_FIELDS." f, 
					".FORM_FORMS_FIELDS_PRINT." p 
				WHERE 
					f.id_form='".$this->db->db_escape_string($id)."' 
					AND f.id_print=p.id 
					AND p.widget='0' 
				ORDER BY f.ordering
			");
			
			$this->count = $this->db->db_num_rows($this->res);

			$this->resmailcol = $this->db->db_query("
				SELECT 
					c.id,
					CONCAT(t.name,'.',c.name) name 
				FROM 
					".FORM_TABLES." t, 
					".FORM_TABLES_FIELDS." c, 
					".FORM_WORKSPACE_ENTRIES." e 
				WHERE 
					c.id_table = t.id 
					AND e.type='table' 
					AND e.id_object=t.id 
					AND e.id_workspace=".$this->db->quote(form_getWorkspace())." 
			");

			$this->groups = bab_getGroups();
            }

		function getnext()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->index = $this->field['id'];
				$this->field['name'] = bab_toHtml($this->field['name']);
				return true;
				}
			else
				{
				return false;
				}
			}

		function getnextmailcol()
			{
			$this->mailcol = $this->db->db_fetch_array($this->resmailcol);
			if (!$this->mailcol)
				{
				if ($this->resmailcol && $this->db->db_num_rows($this->resmailcol))
					$this->db->db_data_seek($this->resmailcol,0);
				return false;
				}
			return true;
			}

		function getnextgroup()
			{
			if (list($key,$this->group['id']) = each($this->groups['id']))
				{
				$this->group['name'] = bab_toHtml($this->groups['name'][$key]);
				return true;
				}
			reset($this->groups['id']);
			return false;
			}
        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_mail"));
}


function edit_form_search($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			$this->db = &$GLOBALS['babDB'];
			$this->t_record = form_translate('Record');

			$this->t_name = form_translate('Name');
			$this->t_search = form_translate('Search');
			$this->t_search_crit = form_translate('Criteria');
			$this->t_type = form_translate('Type');
			$this->t_free = form_translate('Free text');
			$this->t_listbox = form_translate('List box');
			$this->t_checkbox = form_translate('Check box');
			$this->t_date_interval = form_translate('Date interval');
			$this->t_interval = form_translate('Interval');
			$this->t_search_description = form_translate('Description');
			$this->t_help = form_translate('Help');
			$this->t_ovuser = form_translate('User picker');
			$this->t_ovdirectory = form_translate('Directory entry picker');

			$this->t_search_box_0 = form_translate('Global search at the bottom');
			$this->t_search_box_1 = form_translate('Criteria search fields');
			$this->t_search_box_2 = form_translate('No search');

			$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($id)."'"));


			$query = "
					SELECT
						f.id,
						f.name, 
						f.search, 
						f.search_crit,
						f.search_type,
						IF(t2.name IS NULL, CONCAT(t1t.name,'.',t1.name),CONCAT(t2t.name,'.',t2.name)) linkname, 
			            t3t.name values_field_type,
						t3t.sql_type,
						t3t.id_extend 
					FROM 
						".FORM_FORMS_FIELDS." f 
					LEFT JOIN 
						".FORM_TABLES_FIELDS." t1 
						ON t1.id=f.id_table_field_lnk 
					LEFT JOIN
						".FORM_TABLES." t1t
						ON t1t.id = t1.id_table 
					LEFT JOIN ".FORM_TABLES_FIELDS." t2 
						ON t2.id=f.id_table_field_link 
					LEFT JOIN
						".FORM_TABLES." t2t
						ON t2t.id = t2.id_table 
					LEFT JOIN ".FORM_TABLES_FIELDS." t3 
						ON t3.id = f.id_table_field 
					LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t3t
						ON t3t.id = t3.id_type 
					WHERE 
						f.id_form='".$this->db->db_escape_string($id)."' 
						AND (t1.field_function IS NULL OR t1.field_function<>'formula')
						AND (t2.field_function IS NULL OR t2.field_function<>'formula')
					ORDER BY 
						f.ordering
					";

			bab_debug($query);
			$this->res = $this->db->db_query($query);

            }

		function getnext()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->index = $this->field['id'];
				$this->field['id_search_description'] = $this->arr['id_search_description'] == $this->index;
				$this->field['name'] = bab_toHtml($this->field['name']);
				
				$this->enum_set = '21' === $this->field['sql_type'] || '22' === $this->field['sql_type'];
				$this->ovuser = '4' === $this->field['id_extend'];
				$this->ovdirectory = '5' === $this->field['id_extend'];

				return true;
				}
			else
				{
				return false;
				}
			}

        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_search"));
}


function edit_form_elements($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			
			$this->t_record = form_translate('Record');
			$this->t_name = form_translate('Name');
			$this->t_form_element = form_translate('Form element');
			$this->t_filter = form_translate('Filter');
			$this->t_notempty = form_translate('Not empty');
			$this->t_validation = form_translate('Validation');
			$this->t_form_modified = form_translate('The form has been modified, changes will be lost');
			$this->t_group_by = form_translate('Group by');
			$this->t_use = form_translate('Use');
			

			$this->db = &$GLOBALS['babDB'];

			$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($id)."'"));
			
			$this->arr['name'] = bab_toHtml($this->arr['name']);
			
			$this->res = $this->db->db_query("
					SELECT 
						f.id, 
						f.name, 
						f.notempty,
						f.id_print, 
						f.form_element_param, 
						IFNULL(l2.id_table, l.id_table) id_table,
						f.id_form_element, 
						f.id_list_filter, 
						IFNULL(IFNULL(IF(f.id_table_field_link > 0, '3,5,10,16', b2.id_forms_elements), a1.id_forms_elements),'4,6,12') elements,
						COUNT(v.id) validations,
						f.id_table_field_optgroup, 
						f.id_proposal  
					FROM 
						".FORM_FORMS_FIELDS." f
						
					LEFT JOIN 
						".FORM_TABLES_FIELDS." l 
						ON l.id = f.id_table_field_link 

					LEFT JOIN 
						".FORM_TABLES_FIELDS_TYPES." a1
						ON a1.id = f.id_type

					LEFT JOIN 
						".FORM_TABLES_FIELDS." b1 
						ON b1.id = f.id_table_field
					LEFT JOIN 
						".FORM_TABLES_FIELDS_TYPES." b2 
						ON b2.id = b1.id_type 

					LEFT JOIN 
						".FORM_TABLES_FIELDS." l2 
						ON l2.id = f.id_table_field_lnk 
						
					LEFT JOIN 
						".FORM_FORMS_FIELDS_CTRL." v 
						ON v.id_field = f.id

					WHERE 
						id_form='".$this->db->db_escape_string($id)."' 
						AND ( 
						b2.id_forms_elements IS NOT NULL 
						OR a1.id_forms_elements IS NOT NULL  
						OR f.id_table_field_link > 0  
						OR f.id_lnk > 0
						)
					GROUP BY f.id 
					ORDER BY f.ordering 
					
					");
			
			
			$this->resprop = $this->db->db_query('SELECT id, name FROM '.FORM_PROPOSAL.' ORDER BY name');
			
            }

		function getnext()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->index = $this->field['id'];

				$this->validation = $this->field['id_print'] == 1;

				if (empty($this->field['elements'])) {
					$this->field['elements'] = '4,6';
				}
					
				$this->reselement = $this->db->db_query("SELECT * FROM ".FORM_FORMS_ELEMENTS." WHERE id IN(".$this->field['elements'].") AND disabled='N' AND id_print='".$this->db->db_escape_string($this->field['id_print'])."'");

				$this->allowfilter = !empty($this->field['id_table']);

				$this->resfilters = $this->db->db_query("SELECT * FROM ".FORM_TABLES_FILTERS." WHERE id_table = '".$this->db->db_escape_string($this->field['id_table'])."'");
				
				$this->field['name'] = bab_toHtml($this->field['name']);
				
				$this->resog = $this->db->db_query('SELECT lvl3.id, lvl3.name, t.name AS tbl FROM 
						'.FORM_TABLES_FIELDS.' lvl1, 
						'.FORM_TABLES_FIELDS.' lvl2,
						'.FORM_TABLES_FIELDS.' lvl3,
						'.FORM_TABLES.' t    
					WHERE 
						lvl1.id_table='.$this->db->quote($this->field['id_table']).' 
						AND lvl2.id=lvl1.link_table_field_id 
						AND lvl3.id_table=lvl2.id_table 
						AND t.id=lvl2.id_table 
				');
				
				
				
				return true;
				}
			else
				{
				if ($this->res)
					$this->db->db_data_seek($this->res,0);
				return false;
				}
			}

		function getnextelement()
			{
			if ($this->element = $this->db->db_fetch_array($this->reselement))
				{
				$this->element['description'] = form_translate($this->element['description']);
				$this->selected = $this->element['id'] == $this->field['id_form_element'];
				return true;
				}
			else
				{
				if ($this->reselement && $this->db->db_num_rows($this->reselement))
					$this->db->db_data_seek($this->reselement,0);
				return false;
				}
			}

		function getnextfilter()
			{
			if ($this->filter = $this->db->db_fetch_array($this->resfilters))
				{
				$this->selected = $this->filter['id'] == $this->field['id_list_filter'];
				$this->filter['name'] = bab_toHtml($this->filter['name']);
				return true;
				}
			else
				{
				if ($this->resfilters && $this->db->db_num_rows($this->resfilters))
					$this->db->db_data_seek($this->resfilters,0);
				return false;
				}
			
			}
			
			
		public function getnextoptgroup()
			{
			if ($arr = $this->db->db_fetch_assoc($this->resog))
				{
				$this->selected = $arr['id'] == $this->field['id_table_field_optgroup'];
				$this->id = bab_toHtml($arr['id']);
				$this->name = bab_toHtml($arr['tbl'].'.'.$arr['name']);
				return true;
				}
			return false;
			}
			
			
		public function getnextproposal()
			{
			if ($arr = $this->db->db_fetch_assoc($this->resprop))
			{
				$this->selected = $arr['id'] == $this->field['id_proposal'];
				$this->id = bab_toHtml($arr['id']);
				$this->name = bab_toHtml($arr['name']);
				return true;
				}
				
			if ($this->db->db_num_rows($this->resprop) > 0) {
				$this->db->db_data_seek($this->resprop, 0);
				}
			return false;
			}
        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_elements"));
}





function edit_field_validations($id_form, $id_field)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id_form, $id_field)
            {
			$this->id_form = $id_form;
			$this->id_field = $id_field;
			
			$this->db = &$GLOBALS['babDB'];
			
			$req = "SELECT fo.name form_name, fi.name field_name FROM ".FORM_FORMS." fo, ".FORM_FORMS_FIELDS." fi WHERE fi.id_form=fo.id AND fi.id='".$this->db->db_escape_string($this->id_field)."'";
			$this->arr = $this->db->db_fetch_array($this->db->db_query($req));

			$this->t_validation = form_translate('Validation');
			$this->t_param = form_translate('Parameter');
			$this->t_alert = form_translate('Error message');
			$this->t_record = form_translate('Record');


			$this->res = $this->db->db_query("
							SELECT 
								t1.*,
								t2.id value,
								t2.parameter,
								t2.alert
							FROM 
								".FORM_FORMS_FIELDS_VALIDATION." t1
								
							LEFT JOIN
								".FORM_FORMS_FIELDS_CTRL." t2 
								ON t2.id_field='".$this->db->db_escape_string($this->id_field)."' 
								AND t2.id_validation =t1.id
							
						
							");
			
            }

		function getnext()
			{
			if ($this->validation = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->validation['name'] = form_translate($this->validation['name']);
				$this->validation['description'] = form_translate($this->validation['description']);
				
				return true;
				}
			else
				{
				return false;
				}
			}

		
        }
    $tp = new temp($id_form, $id_field);
    $babBody->addStyleSheet($GLOBALS['babAddonHtmlPath'].'forms.css');
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_field_validations"));
}






function edit_form_styles($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			
			$this->t_style = form_translate('Style');
			$this->t_name = form_translate('Name');
			$this->t_record = form_translate('Record');

			
			$this->db = &$GLOBALS['babDB'];

			$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($id)."'"));
			
			$this->arr['name'] = bab_toHtml($this->arr['name']);
			
			$this->res = $this->db->db_query("
					SELECT 
						f.id, 
						f.name,
						f.classname
					FROM 
						".FORM_FORMS_FIELDS." f
					WHERE 
						f.id_form='".$this->db->db_escape_string($id)."' 
					ORDER BY f.ordering
					");
			
			$this->resstyle = $this->db->db_query("
					SELECT 
						name,
						description
					FROM 
						".FORM_FORMS_FIELDS_CLASSNAME."
					");
            }

		function getnext()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->field['name'] = bab_toHtml($this->field['name']);
				return true;
				}
			else
				{
				return false;
				}
			}

		function getnextstyle()
			{
			if ($this->style = $this->db->db_fetch_array($this->resstyle))
				{
				$this->style['description'] = bab_toHtml(form_translate($this->style['description']));
				$this->selected = $this->style['name'] == $this->field['classname'];
				return true;
				}
			else
				{
				if ($this->resstyle && $this->db->db_num_rows($this->resstyle))
					$this->db->db_data_seek($this->resstyle,0);
				return false;
				}
			}
        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_styles"));
}







class edit_form_initCls
{
	var $altbg = false;
	public function __construct($id)
	{
		$this->id = $id;

		$this->t_init = form_translate('Initialisation');
		$this->t_name = form_translate('Name');
		$this->t_record = form_translate('Record');
		$this->t_col = form_translate('Test value on');
		$this->t_directory = form_translate('Directory');
		$this->t_functions = form_translate('Functions');
		$this->t_init_on_modify = form_translate('Init on modify');
		$this->t_help = form_translate('Help');

		$this->db = &$GLOBALS['babDB'];

		$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($id)."'"));

		$this->arr['name'] = bab_toHtml($this->arr['name']);

		$this->res = $this->db->db_query("
				SELECT
				f.id,
				f.name,
				f.init,
				f.init_dir,
				f.id_table_field_link,
				f.id_table_field_init,
				f.init_on_modify,
				f.id_print
				FROM
				".FORM_FORMS_FIELDS." f
				LEFT JOIN
				".FORM_TABLES_FIELDS." t
				ON t.id=f.id_table_field
				AND t.field_function<>''
				WHERE
				f.id_form='".$this->db->db_escape_string($id)."'
				AND t.id IS NULL
				ORDER BY f.ordering
				");

		$this->resstyle = $this->db->db_query("
				SELECT
				name,
				description
				FROM
				".FORM_FORMS_FIELDS_CLASSNAME."
				");


		$this->dirfields = bab_getDirEntry();

		$this->resinit = $this->db->db_query("SELECT id,description FROM ".FORM_FORMS_FIELDS_INIT."");
	}

	public function getnext()
	{
		if ($this->field = $this->db->db_fetch_array($this->res))
		{
			$this->altbg = !$this->altbg;
			$this->index = &$this->field['id'];


			if (!empty($this->field['id_table_field_link']) && 7 != $this->field['id_print']) {
				$this->link_col = true;

				$this->reslinkcol = $this->db->db_query("SELECT t1.id, t1.name FROM ".FORM_TABLES_FIELDS." t1, ".FORM_TABLES_FIELDS." t2 WHERE t2.id='".$this->db->db_escape_string($this->field['id_table_field_link'])."' AND t2.id_table=t1.id_table GROUP BY t1.id");

			} else {
				$this->link_col = false;
			}


			$this->field['name'] = bab_toHtml($this->field['name']);
			$this->field['init'] = bab_toHtml($this->field['init']);

			return true;
		}
		else
		{
			return false;
		}
	}

	public function getnextdirfields()
	{
		$x = list($this->key,$arr) = each($this->dirfields);
		$this->value = & $arr['name'];

		if (!$x)
		{
			reset($this->dirfields);
			return false;
		}
		else
		{
			$this->selected = $this->field['init_dir'] == $this->key;
			return true;
		}
	}


	public function getnextinit()
	{
		if (list($this->key,$this->value) = $this->db->db_fetch_array($this->resinit))
		{
			$this->value = form_translate($this->value);
			$this->selected = $this->field['init_dir'] == $this->key;
			return true;
		}
		else
		{
			if ($this->resinit && $this->db->db_num_rows($this->resinit) >0)
				$this->db->db_data_seek($this->resinit,0);
			return false;
		}
	}

	public function getnextlinkcol()
	{
		if (list($this->key,$this->value) = $this->db->db_fetch_array($this->reslinkcol))
		{
			if (!empty($this->field['id_table_field_init']))
				$this->selected = $this->field['id_table_field_init'] == $this->key;
			else
				$this->selected = $this->field['id_table_field_link'] == $this->key;
			return true;
		}
		else
		{
			if ($this->resinit && $this->db->db_num_rows($this->resinit) >0)
				$this->db->db_data_seek($this->resinit,0);
			return false;
		}
	}
}





function edit_form_init($id_form)
{
	global $babBody;
    
    $tp = new edit_form_initCls($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_init"));
}




function edit_form_transform($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			$this->db = &$GLOBALS['babDB'];
			$this->t_record = form_translate('Record');
			$this->t_name = form_translate('Name');
			$this->t_format = form_translate('Value type');
			$this->t_transform = form_translate('Transform for viewing');
			$this->t_username = form_translate('User name');
			$this->t_username = str_replace("'","\'",$this->t_username);
	
			$this->t_description = form_translate('List of fields with an extended type');
			
			
			$req = "
			
			SELECT 
				f.id,
				f.name, 
				f.transform_to, 
				t.id_extend,
				t.str_options 
			FROM 
				".FORM_FORMS_FIELDS." f, 
				".FORM_TABLES_FIELDS." tf,
				".FORM_TABLES_FIELDS_TYPES." t
			WHERE 
				f.id_form='".$this->db->db_escape_string($id)."' 
				AND tf.id = f.id_table_field 
				AND tf.id_type = t.id 
				AND t.id_extend > '1'
			ORDER BY 
				f.ordering
				";

			$this->res = $this->db->db_query($req);
            }


		function getTransformList($id_extend, $options) {
			
			$arr = array();
			
			switch($id_extend) {
				case 2: // date
				case 3: // date heure

					if (2 == $options['view_date_format']) { // formatage ovidentia
					
						$arr['bab_shortDate'] = form_translate('Date short format');
						$arr['bab_longDate'] = form_translate('Date long format');
						
					}
					
					break;

				case 4:
					
					$arr['user_link'] = form_translate('User name with link');
					$arr['user'] = form_translate('User name');
				
					// id_directory contient un id_group pour les annuaires de groupe
					$dir = bab_getDirEntry($options['id_directory'], BAB_DIR_ENTRY_ID_DIRECTORY);
					foreach($dir as $k => $v) {
						$arr[$k] = $v['name'];
						}

					break;
				case 5:

					$arr['user_link'] = form_translate('User name with link');
					$arr['user'] = form_translate('User name');

					$dir = bab_getDirEntry($options['id_directory'], BAB_DIR_ENTRY_ID_DIRECTORY);
					foreach($dir as $k => $v) {
						$arr[$k] = $v['name'];
						}
					break;
				}

			return $arr;
			}

		function getnextfield()
			{
			if ($this->field = $this->db->db_fetch_array($this->res)) {
				$this->altbg = !$this->altbg;
				$options = unserialize($this->field['str_options']);
				$this->transformList = $this->getTransformList($this->field['id_extend'], $options);
				$this->transformList = array_merge(array('none'=>''), $this->transformList);
				return true;
				}
			return false;
			}

		function getnexttransform()
			{
			if (list($this->value , $this->option) = each($this->transformList))
				{
				$this->selected = $this->field['transform_to'] == $this->value;
				return true;
				}
			return false;
			}
        }

    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_transform"));

}



function edit_form_validation($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id_form)
            {
			$this->id_form = $id_form;
			$this->db = &$GLOBALS['babDB'];
			$this->t_record = form_translate('Record');
			$this->t_name = form_translate('Name');
			$this->t_text = form_translate('Title for waiting approbation in the approbation menu of user section');
			$this->t_view = form_translate('Description');
			$this->t_refused = form_translate('Refused');
			$this->t_accepted = form_translate('Accepted');
			$this->t_help = form_translate('Help');
			
			$this->arr = $this->db->db_fetch_assoc($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->id_form)."'"));
			
			$req = "SELECT id, name, form_approb_description, form_approb_refused, form_approb_accepted FROM ".FORM_FORMS_FIELDS."  WHERE id_form='".$this->db->db_escape_string($this->id_form)."' AND id_lnk='0' ORDER BY ordering";
			$this->res = $this->db->db_query($req);
            }

		function getnextfield()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				return true;
				}
			return false;
			}
        }

    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_validation"));

}



function edit_form_approbation_list($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			$this->db = &$GLOBALS['babDB'];
			$this->t_record = form_translate('Record');
			$this->t_name = form_translate('Approbation status');
			$this->t_waiting = form_translate('Waiting');
			$this->t_validated = form_translate('Validated');
			$this->t_refused = form_translate('Refused');
			
			$req = "SELECT f.id,f.name, f.approb_list_status FROM ".FORM_FORMS." f WHERE f.id='".$this->db->db_escape_string($id)."'";
			$this->arr = $this->db->db_fetch_array($this->db->db_query($req));
			
			$arr = array_flip(explode(',',$this->arr['approb_list_status']));

			$this->s0 = isset($arr[0]);
			$this->s1 = isset($arr[1]);
			$this->s2 = isset($arr[2]);
            }

        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_approbation_list"));

}


function edit_form_curl($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id = $id;
			$this->db = &$GLOBALS['babDB'];
			$this->t_record = form_translate('Record');
			$this->t_name = form_translate('Name');
			$this->t_transaction = form_translate('Transaction');
			$this->t_unit_concat = form_translate('Formula result unit');
			$this->t_trans_formula_msg = form_translate('Total formula message');
			$this->t_formula = form_translate('Formula');
			$this->t_id_form_card = form_translate('Shopping cart form');
			$this->t_card_table_fields = form_translate('Transaction table fields');
			$this->t_sc_formula = form_translate('Shopping cart formula');
			$this->t_trans_formula = form_translate('Total formula');
			$this->t_submit = form_translate('Submit');
			$this->t_quantity = form_translate('Quantity');
			$this->t_total_qte = form_translate('Total quantity');
			$this->t_subtotal = form_translate('Sub-total');
			$this->t_help = form_translate('Help');
			$this->t_js_form_card_fields = form_translate('You must select a shopping card form');

			$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT f.*,t.name table_name FROM ".FORM_FORMS." f, ".FORM_TABLES." t WHERE f.id='".$this->db->db_escape_string($id)."' AND t.id=f.id_table"));

			

			$this->res = $this->db->db_query("SELECT * FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$this->db->db_escape_string($id)."' ORDER BY ordering");


			$this->restrans = $this->db->db_query("SELECT * FROM ".FORM_TRANS_FIELDS." WHERE id_api='".$this->db->db_escape_string(form_getConfigValue('transaction_api'))."'");


			$this->reslink = $this->db->db_query("
								SELECT 
									t1.id, 
									t1.name,
									IF(t5.id IS NULL,t3.id,t5.id) linkid,
									IF(t5.name IS NULL,CONCAT(t2bis.name,'.',t3.name),CONCAT(t5bis.name,'.',t5.name)) linkname
									
								FROM 
									".FORM_TABLES_FIELDS." t1
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t2 
									ON t2.id=t1.link_table_field_id 
								LEFT JOIN 
									".FORM_TABLES." t2bis
									ON t2bis.id = t2.id_table 
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t3 
									ON t3.id_table=t2.id_table 
									AND t3.id<>t1.id 
								LEFT JOIN 
									".FORM_TABLES." t3bis
									ON t3bis.id = t3.id_table 
								LEFT JOIN 
									".FORM_TABLES_FIELDS." t4 
									ON t4.id=t3.link_table_field_id 
								LEFT JOIN
									".FORM_TABLES_FIELDS." t5 
									ON t5.id_table=t4.id_table 
									AND t5.id<>t3.id
									AND t5.id<>t1.id 
								LEFT JOIN 
									".FORM_TABLES." t5bis 
									ON t5bis.id=t5.id_table 
								WHERE 
									t1.id_table='".$this->db->db_escape_string($this->arr['id_table'])."' 
								GROUP BY t1.id,t3.id,t5.id
								
								");
			$this->countlink = $this->db->db_num_rows($this->reslink);

			$this->create_link_arr = true;

			$this->resform = $this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id_type='5'");


            }

		function getnext()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->index = $this->field['id'];
				return true;
				}
			else
				{
				return false;
				}
			}

		function getnextlink(&$skip)
			{
			static $i=0;
			if( $i < $this->countlink)
				{
				$this->link = $this->db->db_fetch_array($this->reslink);
				$this->link['js_name'] = str_replace("'", "\'", $this->link['name']);
				$this->link['js_name'] = str_replace('"', "'+String.fromCharCode(34)+'",$this->link['js_name']);
				
				//echo $this->link['linkname'].'<br />';
				
				if (!empty($this->link['linkname']))
					{
					$this->link['id'] = $this->link['id'].'-'.$this->link['linkid'];
					if ($this->create_link_arr)
						{
						if (!isset($this->optgroup[$this->link['name']]))
							$this->optgroup[$this->link['name']] = array();
						$this->optgroup[$this->link['name']][] = array($this->link['id'],$this->link['linkname']);
						}

					$skip = true;
					$i++;
					return true;
					}
				elseif (isset($this->field['id_table_field']))
					{
					$this->selected = ($this->field['id_table_field'] == $this->link['id']) ? 'selected' : '';
					}
				elseif (isset($this->arr['id_qte_table_col']))
					{
					$this->selected = ($this->arr['id_qte_table_col'] == $this->link['id']) ? 'selected' : '';
					}
				
				$i++;
				return true;
				}
			else
				{
				$this->create_link_arr = false;
				if ($this->countlink > 0)
					$this->db->db_data_seek($this->reslink,0);
				$i=0;
				return false;
				}
			}

		function getnextform()
			{
			if ($this->form = $this->db->db_fetch_array($this->resform))
				{
				$this->selected = $this->form['id'] == $this->arr['id_form_card'] ? 'selected' : '';
				return true;
				}
			else
				return false;
			}

		function getnexttransaction()
			{
			if ($this->transaction = $this->db->db_fetch_array($this->restrans))
				{
				$this->selected = $this->transaction['id'] == $this->field['field_transaction'];
				$this->transaction['description'] = form_translate($this->transaction['description']);
				return true;
				}
			else 
				{
				$this->db->db_data_seek($this->restrans,0);
				return false;
				}
			}

        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_curl"));
}






function edit_form_ordgrp($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id_form = $id;
			$this->db = &$GLOBALS['babDB'];

			$this->t_order_primary = form_translate("Primary order");
			$this->t_order_secondary = form_translate("Secondary order");
			$this->t_asc = form_translate("Asc");
			$this->t_desc = form_translate("Desc");
			$this->t_disabled = form_translate("Disabled");
			$this->t_group_by = form_translate("Group by");
			$this->t_order_by = form_translate("Order by");
			$this->t_primary = form_translate("Primary");
			$this->t_secondary = form_translate("Secondary");
			$this->t_name = form_translate("Name");
			$this->t_record = form_translate("Record");
			$this->t_help = form_translate("Help");

			$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->id_form)."'"));

			$this->res = $this->db->db_query("SELECT id, name FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$this->db->db_escape_string($this->id_form)."' AND (id_table_field > 0 OR id_table_field_link > 0 OR id_lnk > 0 ) AND id_type<>'2' ORDER BY ordering");
            }

		function getnext()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->field['name'] = bab_toHtml($this->field['name']);
				$this->group_by = $this->field['id'] == $this->arr['id_group_by'];
				$this->group_by2 = $this->field['id'] == $this->arr['id_group_by2'];
				$this->order_by = $this->field['id'] == $this->arr['id_order_by'];
				$this->order_by2 = $this->field['id'] == $this->arr['id_order_by2'];
				return true;
				}
			else
				{
				return false;
				}
			}
        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_ordgrp"));
}




function edit_form_increment($id_form)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->id_form = $id;
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_increment = form_translate('Increment');
			$this->t_record = form_translate('Record');

			$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->id_form)."'"));

			$this->res = $this->db->db_query("
			SELECT 
				f.id, 
				f.name,
				f.int_increment 
			FROM 
				".FORM_FORMS_FIELDS." f, 
				".FORM_TABLES_FIELDS." t, 
				".FORM_TABLES_FIELDS_TYPES." type 
			WHERE 
				id_form='".$this->db->db_escape_string($this->id_form)."' 
				AND t.id=f.id_table_field 
				AND type.id=t.id_type 
				AND type.sql_type IN ('1','3','4','5','6','7','8','9','14')
				
			ORDER BY f.ordering
				");
            }

		function getnext()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				
				return true;
				}
			else
				{
				return false;
				}
			}
        }
    $tp = new temp($id_form);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."options.html", "edit_form_increment"));
}




function order_form($id)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_record = form_translate('Record');
			$this->moveup = form_translate('Move up');
			$this->movedown = form_translate('Move down');

			$this->form_name = form_getFormName($id);
			
			$this->res = $this->db->db_query("SELECT id,name FROM ".FORM_FORMS_FIELDS." WHERE id_form='".$this->db->db_escape_string($id)."' ORDER BY ordering");
			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->arr = $this->db->db_fetch_array($this->res);
				$this->arr['name'] = bab_toHtml($this->arr['name']);
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp($id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "order_form"));
}

function options_field($index,$id_type,$id_field)
{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($index,$id_type,$id_field)
            {
			$this->db = &$GLOBALS['babDB'];

			$this->index = $index;

			$this->t_options = form_translate('Options');
			$this->t_print = form_translate('Print mode');
			$this->t_validation = form_translate('Validation');
			$this->t_validation_param = form_translate('Validation parameter');
			$this->t_description = form_translate('Description');
			$this->t_alert = form_translate('Error message');
			$this->t_dir_init = form_translate('Initialize field with');
			$this->t_init_on_modify = form_translate('Init on modify');
			$this->t_element = form_translate('Form element');
			$this->t_filter_on = form_translate('Filter on');
			$this->t_notempty = form_translate('Not empty');
			$this->t_yes = form_translate('Yes');
			$this->t_no = form_translate('No');
			$this->t_ok = form_translate('Ok');
			$this->t_directory = form_translate('Directory');
			$this->t_functions = form_translate('Functions');
			$this->t_help = form_translate('Help');
			$this->t_transaction = form_translate('Transaction field');
			$this->t_field_copy_table_field = form_translate('Copy into table field');
			$this->t_field_help = form_translate('Help on field');
			$this->t_field_copy_on_change = form_translate('Copy if the field is modified');
			$this->t_field_link_to_fill = form_translate('Use link to fill the field');
			$this->t_filter_type = form_translate('filter operator');
			$this->t_field_use_key_val = form_translate('Use key value');

			$this->dirfields = bab_getDirEntry();


			$this->resinit = $this->db->db_query("SELECT id,description FROM ".FORM_FORMS_FIELDS_INIT."");
			
			$this->resprint = $this->db->db_query("SELECT * FROM ".FORM_FORMS_FIELDS_PRINT." WHERE ids_type LIKE '%,".$this->db->db_escape_like($id_type).",%'");
			$this->countprint = $this->db->db_num_rows($this->resprint);

			$this->restablecol = $this->db->db_query("SELECT f1.id, f1.name FROM ".FORM_TABLES_FIELDS." f1, ".FORM_FORMS_FIELDS." f2, ".FORM_FORMS." f3 WHERE f3.id = f2.id_form AND f2.id ='".$this->db->db_escape_string($id_field)."' AND f3.id_table_copy=f1.id_table");

			$this->reshelp = $this->db->db_query("
				SELECT 
					h.id, 
					h.name 
				FROM 
					".FORM_HELP." h,
					".FORM_WORKSPACE_ENTRIES." e 
				WHERE 
					e.type='form_help' 
					AND e.id_object=h.id 
					AND e.id_workspace=".$this->db->quote(form_getWorkspace())." 
			");

			list($id_table) = $this->db->db_fetch_array($this->db->db_query("SELECT fo.id_table FROM ".FORM_FORMS_FIELDS." f, ".FORM_FORMS." fo WHERE f.id='".$this->db->db_escape_string($id_field)."' AND fo.id=f.id_form"));

			$res = $this->db->db_query("SELECT f2.id_table FROM ".FORM_TABLES_FIELDS." f1, ".FORM_TABLES_FIELDS." f2 WHERE f1.id_table='".$this->db->db_escape_string($id_table)."' AND f1.link_table_field_id = f2.id");

			$link_tables = array();
			while (list($id_table) = $this->db->db_fetch_array($res))
				{
				$link_tables[] = $id_table;
				}

			if (count($link_tables))
				{
				$this->resfill = $this->db->db_query("SELECT f.id, CONCAT(t.name,'.',f.name) name FROM ".FORM_TABLES_FIELDS." f , ".FORM_TABLES." t WHERE t.id IN(".$this->db->quote($link_tables).") AND f.id_table=t.id");
				}


			$this->a_filter_type = form_getFilterType();

			
            }

		function getnextfiltertype()
			{
			return list($this->key,list(,$this->value)) = each($this->a_filter_type);
			}

		function getnextdirfields()
			{
			$x = list($this->key, $arr) = each($this->dirfields);
			$this->value = $arr['name'];
			if (!$x) 
				{
				reset($this->dirfields);
				return false;
				}
			else
				return true;
			}

		function getnextinit()
			{
			if (list($this->key,$this->value) = $this->db->db_fetch_array($this->resinit))
				{
				$this->value = form_translate($this->value);
				return true;
				}
			else
				{
				if ($this->resinit && $this->db->db_num_rows($this->resinit) >0)
					$this->db->db_data_seek($this->resinit,0);
				return false;
				}
			}

		function getnextformelement()
			{
			$out = $this->element = $this->db->db_fetch_array($this->res_elements);
			$this->element['description'] = form_translate($this->element['description']);
			return ($out == false) ? false : true;
			}

		function getnextprint()
			{
			$out = $this->print = $this->db->db_fetch_array($this->resprint);
			$this->print['name'] = form_translate($this->print['name']);
			return ($out == false) ? false : true;
			}

		function getnextvalidation()
			{
			static $i=0;
			if( $i < $this->countvalidation)
				{
				$this->validation = $this->db->db_fetch_array($this->resvalidation);
				$this->validation['name'] = form_translate($this->validation['name']);
				$i++;
				return true;
				}
			else
				{
				if ($this->resvalidation && $this->countvalidation >0)
					$this->db->db_data_seek($this->resvalidation,0);
				$i=0;
				return false;
				}
			}

		function getnexttablecol()
			{
			if ($this->tablecol = $this->db->db_fetch_array($this->restablecol))
				{
				
				return true;
				}
			else return false;
			}

		function getnexthelp()
			{
			return $this->help = $this->db->db_fetch_array($this->reshelp);
			}

		function getnextfill()
			{
			if (isset($this->resfill))
				return $this->fill = $this->db->db_fetch_array($this->resfill);
			else
				return false;
			}
        }
	$tp = new temp($index,$id_type,$id_field);
	die(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."main.html", "options_field"));
}

function list_applications()
	{
	global $babBody;
	
    class temp
        { 
		var $altbg = false;
        function temp()
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_nb_steps = form_translate('Number of steps');
			$this->t_list_steps = form_translate('List steps');
			$this->t_acces = form_translate('Rights');
			$this->t_copyurl = form_translate('Copy url');
			$this->t_close = form_translate('Close');
			$this->t_launch = form_translate('Launch');
			$this->t_links = form_translate('Links');
			$this->t_help = form_translate('Help');
			$this->t_export = form_translate('Export');
			$this->t_import = form_translate('Import');
			
			$this->forms_with_links = array();
			
			$res = $this->db->db_query("SELECT id_form FROM ".FORM_FORMS_FIELDS." WHERE id_print IN (5,8)");
			while (list($form) = $this->db->db_fetch_array($res))
				{
				$this->forms_with_links[] = $form;
				}
			
			$this->res = $this->db->db_query("
										SELECT 
											a.*,
											COUNT(s.id) nb_steps
										FROM 
											".FORM_APP_APPLICATIONS." a
											LEFT JOIN 
												".FORM_APP_STEPS." s 
												ON s.id_application=a.id ,
											".FORM_WORKSPACE_ENTRIES." w
										WHERE 
											w.id_workspace=".$this->db->quote(form_getWorkspace())." 
											AND w.id_object=a.id  
											AND w.type='application' 
										
										GROUP BY a.id ORDER BY a.name 
										
										");
			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);
				$this->edit_url = bab_toHtml($GLOBALS['babAddonUrl']."main&idx=edit_application&id_app=".$this->arr['id']);
				$this->list_steps_url = bab_toHtml($GLOBALS['babAddonUrl']."main&idx=list_steps&id_app=".$this->arr['id']);
				$this->app_acces_url = bab_toHtml($GLOBALS['babAddonUrl']."rights&idx=acces_applications&id_app=".$this->arr['id']);
				$this->app_url = bab_toHtml($GLOBALS['babUrlScript']."?tg=addon/forms/form&idx=application&id_app=".$this->arr['id']);
				
				if ($this->arr['https'] == 'Y') {
					form_urlToHttps($this->app_url);
					}
					
				$this->links_url = bab_toHtml($GLOBALS['babAddonUrl']."main&idx=link_list&id_app=".$this->arr['id']);
				$this->export_url = bab_toHtml($GLOBALS['babAddonUrl']."main&idx=export&id_app=".$this->arr['id']);
				$this->count_links = count(array_intersect(form_app_forms($this->arr['id']),$this->forms_with_links));
				$this->app_rights = bab_isAccessValid(FORM_APP_GROUPS,$this->arr['id']);


				$this->arr['name'] = bab_toHtml($this->arr['name']);
				$this->arr['description'] = bab_toHtml($this->arr['description']);

				list($this->arr['nb_links']) = $this->db->db_fetch_array($this->db->db_query("SELECT COUNT(*) FROM ".FORM_APP_LINKS." WHERE id_application='".$this->db->db_escape_string($this->arr['id'])."'"));
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}
        }
    $tp = new temp();
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."applications.html", "list_applications" ) );
	}

function edit_application($id = '')
	{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
			$this->db = &$GLOBALS['babDB'];
			$this->id = $id;

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_in_section = form_translate('In section');
			$this->t_https = form_translate('Link in https');
			$this->t_submit = form_translate('Submit');
			$this->t_yes = form_translate('Yes');
			$this->t_no = form_translate('No');
			$this->t_delete = false;
			$this->js_delete = form_translate('Do you really want to delete this application?');
			$this->t_help = form_translate('Help');
			$this->t_app_grp = form_translate('Application group');
			
			if (isset($id) && is_numeric($id))
				{
				$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_APP_APPLICATIONS." WHERE id='".$this->db->db_escape_string($id)."'"));
				$this->t_delete = form_translate('Delete');
				}
			else
				{
				$el_to_init = array('name','description');
				foreach ($el_to_init as $field)
					{
					$this->arr[$field] = '';
					}
				}

			$this->arr['name'] = bab_toHtml($this->arr['name']);
			$this->arr['description'] = bab_toHtml($this->arr['description']);

			$this->res = $this->db->db_query("SELECT r.id,r.name,a.id_app FROM ".FORM_GEST_RIGHTS." r LEFT JOIN ".FORM_GEST_RIGHTS_APP." a ON a.id_app='".$this->db->db_escape_string($this->id)."' AND a.id_right=r.id WHERE app_grp='1'");
            }


		function getnextag()
			{
			if ($this->app_grp = $this->db->db_fetch_array($this->res))
				{
				$this->checked = !empty($this->app_grp['id_app']);
				return true;
				}
			else
				return false;

			}
        }
    $tp = new temp($id);
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."applications.html", "edit_application" ) );
	}


function list_steps($id)
	{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id)
            {
            global $babBody;
			$this->db = &$GLOBALS['babDB'];
			$this->id = $id;

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_form = form_translate('Form');
			$this->t_edit = form_translate('Edit');
			$this->t_type = form_translate('Type');
			$this->t_id_first_step = form_translate('First');
			$this->t_submit = form_translate('Update');
			$this->t_help = form_translate('Help');
			
			$res = $this->db->db_query('
				SELECT name FROM '.FORM_APP_APPLICATIONS.' 
				WHERE id='.$this->db->quote($this->id).'
			');
			
			$arr = $this->db->db_fetch_assoc($res);
			$babBody->setTitle(sprintf(form_translate('Application steps for : %s'),$arr['name']));
			
			
			$this->res = $this->db->db_query("
			
				SELECT 
					s.*,
					IFNULL(f.name,'') form, 
					f.id_type form_type, 
					IFNULL(t.name,'???') type, 
					IF(a.id_first_step = s.id,'checked','') first_step 
				FROM 
					".FORM_APP_STEPS." s
					LEFT JOIN ".FORM_FORMS." f ON f.id=s.id_form 
					LEFT JOIN ".FORM_APP_STEP_TYPE." t ON t.id=s.id_type , 
					".FORM_APP_APPLICATIONS." a 
				WHERE 
					s.id_application='".$this->db->db_escape_string($this->id)."' 
					AND a.id='".$this->db->db_escape_string($this->id)."' 
				GROUP BY s.id 
				ORDER BY s.name
					
				");
			$this->count = $this->db->db_num_rows($this->res);
            }

		function getnext()
			{
			static $i=0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->db->db_fetch_array($this->res);
				$this->arr['type'] = form_translate($this->arr['type']);
				$this->edit_url = bab_toHtml($GLOBALS['babAddonUrl']."main&idx=edit_step&id_app=".$this->id."&id_step=".$this->arr['id']);
				$this->arr['form'] = bab_toHtml($this->arr['form']);
				$this->arr['name'] = bab_toHtml($this->arr['name']);
				$this->arr['description'] = bab_toHtml($this->arr['description']);
				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}

		
        }
    $tp = new temp($id);
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."applications.html", "list_steps" ) );
	}
	
	
	
	
function add_step($id_app) {

	global $babBody;
    class temp
        { 
		var $altbg = false;
		var $res;
		
        function temp($id_app) {
            global $babDB;
            
            $this->t_next = form_translate('Next');
            
            $this->id_app = bab_rp('id_app');
            
            $this->res = $babDB->db_query("SELECT * FROM ".FORM_APP_STEP_TYPE."");
        }

		function getnext(){
			global $babDB;
			if ($arr = $babDB->db_fetch_assoc($this->res)) {
				$this->altbg = !$this->altbg;
				$this->description = bab_toHtml(form_translate($arr['description']));
				$this->id_type = (int) $arr['id'];
				return true;
			}
			
			return false;
		}

		
    }
    
    $babBody->setTitle(form_translate('Step types'));
    
    $tp = new temp($id_app);
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."applications.html", "add_step" ) );
}
	
	
	
	
function edit_step($id_app, $id_step) {

	include_once $GLOBALS['babAddonPhpPath']."steptypeincl.php";
	
	global $babDB, $babBody;
	
	$res = $babDB->db_query('SELECT * FROM '.FORM_APP_STEPS.' WHERE id='.$babDB->quote($id_step));
	$step = $babDB->db_fetch_assoc($res);
	
	if ($step) {
		$id_type = (int) $step['id_type'];
	} else {
		$id_type = (int) bab_rp('id_type');
	}

	include_once $GLOBALS['babAddonPhpPath'].'steps/'.$id_type.'.php';
	$classname = 'form_stepType_'.$id_type;
	
	$res = $babDB->db_query('SELECT description FROM '.FORM_APP_STEP_TYPE.' WHERE id='.$babDB->quote($id_type));
	$step_type = $babDB->db_fetch_assoc($res);
	$type = form_translate($step_type['description']);
	
	$babBody->title = form_translate("Edit step").' : '.bab_toHtml($type);
	
	
	$babBody->babEcho(call_user_func_array(
			array($classname, 'getEditHtml'),
			array($id_app, $id_step)
		));
}
	
	
	
function link_list($id_app)
	{
	global $babBody;
    class temp
        { 
		var $altbg = true;
		var $arr = array();
        function temp($id_app)
            {
			$this->db = &$GLOBALS['babDB'];
			$this->id_app = $id_app;

			$this->t_add = form_translate('Add');
			$this->t_form_col = form_translate('Form field');
			$this->t_app_step = form_translate('Application step');

			$this->res = $this->db->db_query("SELECT l.id, CONCAT(a.name,' . ',f.name) name, s.name app_step FROM ".FORM_APP_LINKS." l, ".FORM_FORMS_FIELDS." f,".FORM_FORMS." a, ".FORM_APP_STEPS." s WHERE l.id_application='".$this->db->db_escape_string($this->id_app)."' AND l.id_field = f.id AND a.id=f.id_form AND s.id=l.id_step GROUP BY l.id");
			
            }

		function getnext()
			{

			if( $this->arr = $this->db->db_fetch_array($this->res) )
				{
				$this->altbg = !$this->altbg;
				
				return true;
				}
			return false;
			}

		
        }
    $tp = new temp($id_app);
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."applications.html", "link_list" ) );
	}


function link_edit($id_app, $id_link)
	{
	global $babBody;
    class temp
        { 
		var $altbg = true;
		var $arr = array();
        function temp($id_app, $id_link)
            {
			$this->db = &$GLOBALS['babDB'];
			$this->id_app = $id_app;
			$this->id_link = $id_link;
			$this->t_delete = form_translate('Delete');
			$this->t_submit = form_translate('Record');
			$this->t_help = form_translate('Help');
			$this->t_form_col = form_translate('Form field');
			$this->t_app_step = form_translate('Application step');
			$this->t_id_index = form_translate('Index');
			$this->t_index = form_translate('Index (default)');
			$this->t_if = form_translate('View link if');

			if (!empty($this->id_link))
				{
				$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_APP_LINKS." WHERE id='".$this->db->db_escape_string($this->id_link)."'"));
				}
			else
				{
				$this->arr = array(
						'id_field'		=> 0,
						'id_step'		=> 0,
						'id_index'		=> 0,
						'id_if_field'	=> 0,
						'if_operator'	=> 0,
						'if_value'		=> ''
					);
				}

			$this->sup = 0;

			$forms = form_app_forms($this->id_app);
			if ( count($forms) > 0 )
				{
				$this->reslink = $this->db->db_query("
				
					SELECT 
						f.id,
						CONCAT(a.name,' . ',f.name) name,
						a.id id_form 
					FROM 
						".FORM_FORMS_FIELDS." f
					LEFT JOIN 
						".FORM_APP_LINKS." l 
					ON 
						l.id_application = '".$this->db->db_escape_string($this->id_app)."' 
						AND l.id_field = f.id,
						".FORM_FORMS." a

					WHERE 
						f.id_print IN (5,8) 
						AND a.id = f.id_form 
						AND f.id_form IN (".$this->db->quote($forms).") 
						AND (l.id IS NULL OR l.id='".$this->db->db_escape_string($this->id_link)."')

					GROUP BY f.id
					ORDER BY name
					");
				}

			$this->restarget = $this->db->db_query("
				SELECT id,name 
				FROM ".FORM_APP_STEPS." 
				WHERE id_application='".$this->db->db_escape_string($this->id_app)."' 
				ORDER BY name
				");
			$this->counttarget = $this->db->db_num_rows($this->restarget);


			$this->operators = form_getFilterType();
            }
		
		
		function getnextlink()
			{
			if( $this->link = $this->db->db_fetch_array($this->reslink))
				{
				
				$this->selected = isset($this->arr['id_field']) && $this->link['id'] == $this->arr['id_field'] ? 'selected' : '';


				$this->resintcol = $this->db->db_query("
					SELECT 
						f.id, 
						f.name 
					FROM 
						".FORM_FORMS_FIELDS." f, 
						".FORM_TABLES_FIELDS." t,
						".FORM_TABLES_FIELDS_TYPES." ft 
					WHERE 
						f.id_form='".$this->db->db_escape_string($this->link['id_form'])."' 
						AND t.id=f.id_table_field 
					ORDER BY f.name
				");
				
				$this->link['id'] = bab_toHtml($this->link['id']);

				return true;
				}
			else
				{
				if ($this->reslink && $this->db->db_num_rows($this->reslink) > 0)
					$this->db->db_data_seek($this->reslink,0);
				return false;
				}
			}

		function getnextintcol()
			{
			if ($this->intcol = $this->db->db_fetch_array($this->resintcol))
				{
				$this->intcol['name'] = bab_toHtml($this->intcol['name'],BAB_HTML_JS);
				return true;
				}
			else
				{
				return false;
				}
			}

		function getnexttarget()
			{
			static $i=0;
			if( $i < $this->counttarget)
				{
				$this->target = $this->db->db_fetch_array($this->restarget);
				$this->selected = $this->target['id'] == $this->arr['id_step'] ? 'selected' : '';
				
				$this->target['id'] = bab_toHtml($this->target['id']);
				$this->target['name'] = bab_toHtml($this->target['name']);
				$i++;
				return true;
				}
			else
				{
				$i=0;
				if ($this->counttarget > 0)
					$this->db->db_data_seek($this->restarget,0);
				return false;
				}
			}

		function getnextoperator()
			{
			if (list($this->id,list(,$this->name)) = each($this->operators))
				{
				$this->selected = $this->arr['if_operator'] == $this->id;
				return true;
				}
			else
				return false;
			}
        }
    $tp = new temp($id_app, $id_link);
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."applications.html", "link_edit" ) );
	}



function forms_formula()
{
	class temp
	{
	function temp()
		{
		$this->t_ok = form_translate('OK');
		$this->t_delete = form_translate('Delete selected object');
		$this->t_customvalue = form_translate('Custom value');
		$this->callback = !empty($_GET['callback']) ? $_GET['callback'] : 'form_formula';
		$this->id_formula = !empty($_GET['id_formula']) ? $_GET['id_formula'] : 0;
		}
	}

	$tp = new temp();

	include_once $GLOBALS['babInstallPath']."utilit/uiutil.php";
	$GLOBALS['babBodyPopup'] = new babBodyPopup();
	$GLOBALS['babBodyPopup']->title = $GLOBALS['babBody']->title;
	$GLOBALS['babBodyPopup']->msgerror = $GLOBALS['babBody']->msgerror;
	$GLOBALS['babBodyPopup']->babecho(bab_printTemplate($tp,$GLOBALS['babAddonHtmlPath'].'formula.html'));
	printBabBodyPopup();
	die();
}

function card_fields($id_table, $id_form_card)
{
	class temp
	{
	function temp($id_table, $id_form_card)
		{
		$this->t_ok = form_translate('OK');
		$this->t_card_field_quantities = form_translate('Quantities');
		$this->t_card_field_total = form_translate('Total');
		$this->t_card_field_products_summary = form_translate('Products summary');
		$this->t_card_field_transaction_id = form_translate('Transaction ID');
		$this->t_card_field_status = form_translate('Transaction status');
		$this->t_card_field_product_name = form_translate('Product name');

		$this->db = & $GLOBALS['babDB'];

		$this->res = $this->db->db_query("SELECT id,name FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($id_table)."'");
		$this->resprod = $this->db->db_query("SELECT t.id, t.name FROM ".FORM_TABLES_FIELDS." t, ".FORM_FORMS." f WHERE t.id_table=f.id_table AND f.id='".$this->db->db_escape_string($id_form_card)."'");
		}

	function getnextfield()
		{
		if ($this->field = $this->db->db_fetch_array($this->res))
			return true;
		else
			{
			$this->db->db_data_seek($this->res,0);
			return false;
			}
		}

	function getnextprodfield()
		{
		if ($this->field = $this->db->db_fetch_array($this->resprod))
			return true;
		else
			{
			$this->db->db_data_seek($this->res,0);
			return false;
			}
		}
	}

	$tp = new temp($id_table, $id_form_card);

	include_once $GLOBALS['babInstallPath']."utilit/uiutil.php";
	$GLOBALS['babBodyPopup'] = new babBodyPopup();
	$GLOBALS['babBodyPopup']->title = $GLOBALS['babBody']->title;
	$GLOBALS['babBodyPopup']->msgerror = $GLOBALS['babBody']->msgerror;
	$GLOBALS['babBodyPopup']->babecho(bab_printTemplate($tp,$GLOBALS['babAddonHtmlPath'].'main.html','card_fields'));
	printBabBodyPopup();
	die();
}


function forms_import()
	{
	global $babBody;
    class temp
        { 
        function temp()
            {
			$this->t_submit = form_translate('Import');
            }
        }
    $tp = new temp();
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."applications.html", "import_application" ) );
	}


function copy_form()
	{
	global $babBody;
    class temp
        { 
        function temp()
            {
			$this->db = &$GLOBALS['babDB'];
			$this->t_form = form_translate('Form');
			$this->t_newname = form_translate('New name');
			$this->t_copy = form_translate('Copy');

			$this->res = $this->db->db_query("
				SELECT 
					f.id, 
					f.name 
				FROM 
					".FORM_FORMS." f,
					".FORM_WORKSPACE_ENTRIES." w 
				WHERE 
					w.type='form' 
					AND f.id=w.id_object 
					AND w.id_workspace=".$this->db->quote(form_getWorkspace())."
				ORDER BY name");
            }

		function getnext()
			{
			if ($this->arr = $this->db->db_fetch_array($this->res))
				{
				
				return true;
				}
			return false;
			}
        }
    $tp = new temp();
	$babBody->babecho( bab_printTemplate( $tp, $GLOBALS['babAddonHtmlPath']."main.html", "copy_form" ) );
	}

?>
