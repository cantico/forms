<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");


function forms_getWaitingItemsDisplay(bab_eventBeforeWaitingItemsDisplayed $event) {

	
	include_once $GLOBALS['babInstallPath']."utilit/wfincl.php";

	$inst = bab_WFGetWaitingInstances($GLOBALS['BAB_SESS_USERID']);
	
	
	if (!$inst || count($inst) == 0)
		return false;

	$title = form_translate('Forms');
	$db = & $GLOBALS['babDB'];

	$req = "
		SELECT 
			t.id id_table, 
			i.id_schi, 
			id_line, 
			t.name, 
			s.id_approb_form, 
			f.form_approb_text 
		FROM 
			".FORM_APPROB_SCHI." i, 
			".FORM_TABLES." t, 
			".FORM_APP_STEPS." s, 
			".FORM_FORMS." f 
		WHERE 
			i.id_table=t.id 
			AND s.id=i.id_step 
			AND i.id_schi IN(".$db->quote($inst).") 
			AND f.id=s.id_approb_form 
	";

	$res = $db->db_query($req);
	
	if (0=== $db->db_num_rows($res))
	{
		return;
	}
	
	if ($event instanceof bab_eventWaitingItemsCount)
	{
		$event->addItemCount($title, $db->db_num_rows($res));
		return;
	}
				
	$arr = array();

	while ($row = $db->db_fetch_array($res))
		{  
		$l = array();
	
		$req = "
			SELECT 
				*, 
				form_lastupdate_user uid, 
				form_lastupdate dt 
				
			FROM 
				".form_tbl_name($row['id_table'])." 
				
			WHERE 
				form_id='".$db->db_escape_string($row['id_line'])."'
		";
		
		$data = $db->db_fetch_array($db->db_query($req));

		if (empty($row['form_approb_text']))
			{
			$l['text'] = form_translate('Table').' '.$row['name'].', '.form_translate('line').' '.$row['id_line'];
			if ($data['uid'] > 0)
				{
				$l['text'] .= ', '.form_translate('modified by').' '.bab_getUserName($data['uid']);
				}
			$l['text'] .= ' - '.bab_shortDate(bab_mktime($data['dt']));
			}
		else { 
			$l['text'] = sprintf($row['form_approb_text'], bab_getUserName($data['uid']), bab_shortDate(bab_mktime($data['dt'])));
			}

		$l['description'] = form_getApprobDescription($row['id_approb_form'], $row['id_table'], $row['id_line']);
		$l['url'] = $GLOBALS['babAddonUrl'].'form&idx=7&id_form='.$row['id_approb_form'].'&form_row='.$row['id_line'].'&popup=1';
		$l['popup'] = 1;
		$l['idschi'] = $row['id_schi'];

		$arr[] = $l;
		}
		
	if ($arr) {	
		$event->addObject($title,$arr);
	}
}


class form_getApprobDescription_Cls
{
	function form_getApprobDescription_Cls($res, $line)
	{
		$this->db = & $GLOBALS['babDB'];
		$this->res = & $res;
		$this->line = & $line;
	}

	function getnext()
	{
	if ($arr = $this->db->db_fetch_array($this->res))
		{
		$this->name = $arr['name'];
		$this->value = $this->line['f'.$arr['id']];

		$this->value = form_getTransformedValue($arr['id'],$this->line['form_id'], $this->value, FORM_TRANS_RICH_HTML);
		
		return true;
		}
	else
		return false;
	}
}



function form_f_getApprobDescription($id_form, $id_table, $form_row)
{
	$db = &$GLOBALS['babDB'];
	
	$resdesc = $db->db_query("
		SELECT 
			f.id, 
			f.name 
		FROM ".FORM_FORMS_FIELDS." f 
		
		WHERE 
			f.id_form='".$db->db_escape_string($id_form)."' 
			AND f.form_approb_description='1'  
			ORDER BY f.ordering
		");

	if ($db->db_num_rows($resdesc) == 0)
		{
		return '';
		}
	else
		{

		require_once($GLOBALS['babAddonPhpPath'].'linksincl.php');

		$req = "SELECT 
				f.*,
				t.name name_table_field, 
				t.link_table_field_id, 
				t.field_function, 
				lnk.id_table, 
				e.name form_element, 
				ty.sql_type
			FROM 
				".FORM_FORMS_FIELDS." f 
			LEFT JOIN ".FORM_TABLES_FIELDS." t
				ON t.id=f.id_table_field 
			LEFT JOIN ".FORM_TABLES_FIELDS." lnk 
				ON lnk.id=t.link_table_field_id 
			LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." ty 
				ON ty.id = t.id_type ,
				".FORM_FORMS_ELEMENTS." e
	
			WHERE 
				e.id=f.id_form_element 
				AND f.id_form='".$db->db_escape_string($id_form)."'  
			GROUP BY f.id 
			ORDER BY f.ordering ";

		$rescol = $db->db_query($req);

		$col_print = array(1,3,4,6,7,9);
		$queryObj = new form_queryObj($id_table, false);

		while($arr = $db->db_fetch_assoc($rescol))
			{
			if ($arr['id_lnk'] == 0)
				$queryObj->addCol($arr, $col_print);
			}

		$queryObj->addWhere('p.form_id = \''.$db->db_escape_string($form_row).'\'');
		$req = $queryObj->getQuery();
		$sheet = $db->db_fetch_array($db->db_query($req));

		$temp = new form_getApprobDescription_Cls($resdesc, $sheet);
		return bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."user.html", "validation_description");
		}
}

?>