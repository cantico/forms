<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';


class form_transformValue {

	function form_transformValue($id_field) {
		$this->db = &$GLOBALS['babDB'];

		$arr = $this->db->db_fetch_assoc($this->db->db_query("
			SELECT 
				f1.name,
				f1.transform_to,
				f1.id_print, 
				f1.id_table_field id_table_field1,
				f1.id_table_field_link id_table_field2,
				f1.id_table_field_lnk id_table_field3, 
				
				t2.id_extend extend1,
				t3.id_extend extend2,
				t4.id_extend extend3,

				t2.str_options str_options1, 
				t3.str_options str_options2, 
				t4.str_options str_options3, 

				t2.sql_type sql_type1, 
				t3.sql_type sql_type2, 
				t4.sql_type sql_type3 
			FROM 
				".FORM_FORMS_FIELDS." f1 
			LEFT JOIN ".FORM_TABLES_FIELDS." f2 ON f2.id = f1.id_table_field 
			LEFT JOIN ".FORM_TABLES_FIELDS." f3 ON f3.id = f1.id_table_field_link 
			LEFT JOIN ".FORM_TABLES_FIELDS." f4 ON f4.id = f1.id_table_field_lnk 

			LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t2 ON t2.id = f2.id_type 
			LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t3 ON t3.id = f3.id_type 
			LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." t4 ON t4.id = f4.id_type 
			WHERE 
				 f1.id = '".$this->db->db_escape_string($id_field)."'
		"));

		if (!$arr) {
			trigger_error('erreur sur le champ '.$id_field);
			return false;
		}

		$this->name = &$arr['name'];
		$this->transform_to = &$arr['transform_to'];
		$this->image = 9 == $arr['id_print'];
			

		if (null !== $arr['extend1']) {
			$this->id_table_field = &$arr['id_table_field1'];
			$this->sql_type = &$arr['sql_type1'];
			$this->extend = &$arr['extend1'];
			$this->options = unserialize($arr['str_options1']);
			}
		else if (null !== $arr['extend2']) {

			$this->id_table_field = &$arr['id_table_field2'];
			$this->sql_type = &$arr['sql_type2'];
			$this->extend = &$arr['extend2'];
			$this->options = unserialize($arr['str_options2']);
			}
		else if (null !== $arr['extend3']) {

			$this->id_table_field = &$arr['id_table_field3'];
			$this->sql_type = &$arr['sql_type3'];
			$this->extend = &$arr['extend3'];
			$this->options = unserialize($arr['str_options3']);
			}

		if (!isset($this->sql_type)) { // champs sans table ou fonction de table
			$this->extend = 0;
			$this->sql_type = 0;
			}


		$blob_types = array(23 => 1, 24 => 1, 25 => 1, 26 => 1);
		$this->blob = isset($blob_types[$this->sql_type]);

		
	}


	function downloadCtrl($id_table_field, $form_row)
	{
	$arr = $this->db->db_fetch_array($this->db->db_query("
				SELECT  
					file_size,
					file_type,
					file_name 
				FROM ".FORM_EXTERNAL_BLOB_DATA." 
				WHERE  
					id_table_field='".$this->db->db_escape_string($id_table_field)."'
				AND form_row='".$this->db->db_escape_string($form_row)."'
				"));

	$arr['file_size'] = $arr['file_size']/1024;

	return array(
			'file_size' => sprintf("%01.2f %s",$arr['file_size'],form_translate('Kb')),
			'file_type' => isset($arr['file_type']) ? $arr['file_type'] : '',
			'file_name' => isset($arr['file_name']) ? $arr['file_name'] : '',
			'file_class'=> isset($arr['file_type']) ? str_replace('/','-',$arr['file_type']): '',
			'file_url'  => $GLOBALS['formAddonUrl'].'form&idx=blob&id_table_field='.$id_table_field.'&form_row='.$form_row
			);
	}


	function formatDate($view_date_format, $hours, &$value, $format) {
		
		if (FORM_TRANS_OVML == $format) {
			$value = bab_mktime($value);
			return;
		}
		
		if (1 == $this->options['view_date_format']) {
			form_dateFormat($value);
		}

		if (2 == $this->options['view_date_format']) {
			$transform_to = !empty($this->transform_to) ? $this->transform_to : 'bab_shortDate';
			
			if ('none' !== $transform_to) {
				$value = call_user_func($transform_to, bab_mktime($value), $hours);
			}
		}

		if (FORM_TRANS_RICH_HTML == $format || FORM_TRANS_RICH_HTML_SMALL == $format || FORM_TRANS_HTML == $format)
			$value = bab_toHtml($value);

		if (FORM_TRANS_CSV == $format)
			$value = form_cvs_value($value);
	}


	function getUserStr($transform_to, $str, $format, $url) {
		
		switch($format) {
	
			case FORM_TRANS_RICH_HTML:
			case FORM_TRANS_RICH_HTML_SMALL:
				if ('user_link' == $transform_to && false !== $url) {
					return '<a href="'.bab_toHtml($url).'" onclick="bab_popup(this.href,2);return false;" class="ovdirectorylink">'.bab_toHtml($str).'</a>';
					}
			
			case FORM_TRANS_HTML:
				return bab_toHtml($str);

			case FORM_TRANS_OVML:
			case FORM_TRANS_TXT:
				return $str;

			
			case FORM_TRANS_CSV:
				return form_cvs_value($str);
		}
	}


	function formatBlob($form_row, $format) {

		if (empty($form_row)) // lors d'un envoi de mail par exemple
			return '';

		$blob = $this->downloadCtrl($this->id_table_field, $form_row);

		if ($this->image) {
			$blob['file_url'] .= '&disposition=inline';
			}
		
		
		switch ($format) {
			case FORM_TRANS_RICH_HTML:

				if ($this->image) {
					return '<img src="'.bab_toHtml($blob['file_url']).'" alt="'.bab_toHtml($this->name.' : '.$blob['file_name']).'" />';
				} else {
					return '<a href="'.bab_toHtml($blob['file_url']).'" class="download '.$blob['file_class'].'" title="'.$blob['file_size'].'"><span>'.bab_toHtml($blob['file_name']).'</span></a>';
				}

			case FORM_TRANS_RICH_HTML_SMALL:

				return '<a href="'.bab_toHtml($blob['file_url']).'" title="'.$blob['file_name'].' ('.$blob['file_size'].')"><img src="'.$GLOBALS['babSkinPath'].'images/Puces/download.gif" alt="'.form_translate('Download').'" /></a>';
				
			
			case FORM_TRANS_HTML:
				return bab_toHtml($blob['file_name'].' ('.$blob['file_size'].')');

			case FORM_TRANS_OVML:
				return $blob;

			case FORM_TRANS_TXT:
				return $blob['file_name'].' ('.$blob['file_size'].') : '.$blob['file_url'];

			
			case FORM_TRANS_CSV:
				return form_cvs_value($blob['file_name'].' ('.$blob['file_size'].')');
		}
	}


	function formatImage($value, $format) {

		
		switch ($format) {
			case FORM_TRANS_RICH_HTML:
			case FORM_TRANS_RICH_HTML_SMALL:
				return '<img src="'.bab_toHtml($value).'" alt="'.bab_toHtml($this->name).'" />';

			case FORM_TRANS_HTML:
				return bab_toHtml($value);

			case FORM_TRANS_OVML:
			case FORM_TRANS_TXT:
				return $value;
			
			case FORM_TRANS_CSV:
				return form_cvs_value($value);
		}
	}


	function defaultValue($value, $format) {

		switch ($format) {
			case FORM_TRANS_RICH_HTML:
				return bab_toHtml($value, BAB_HTML_ALL & ~BAB_HTML_P);
				
			case FORM_TRANS_RICH_HTML_SMALL:
				return bab_toHtml($value, BAB_HTML_ALL & ~BAB_HTML_P);

			case FORM_TRANS_HTML:
				return bab_toHtml($value);

			case FORM_TRANS_OVML:
			case FORM_TRANS_TXT:
				return $value;
			
			case FORM_TRANS_CSV:
				return form_cvs_value($value);
		}
	}



	function getValue($form_row, $value, $format) {

		if ('' === $value)
			return $value;

		if (FORM_TRANS_DB_HTML == $format && !$this->blob) {
			return bab_toHtml($value);
		}

		
		switch($this->extend) {
			
			case 0: //  fonction sur le champ
				return $this->defaultValue($value, $format);
			
			
			case 1:  // type sql, non transform�

				if ($this->blob) {
					return $this->formatBlob($form_row, $format);
					}

				if ($this->image) {
					return $this->formatImage($value, $format);
					}

				return $this->defaultValue($value, $format);

			case 2: // Date
				$this->formatDate($this->options['view_date_format'], false, $value, $format);
				return $value;

			case 3: // Datetime
				$this->formatDate($this->options['view_date_format'], true, $value, $format);
				return $value;

			case 4: // Ovidentia user
				if (empty($value)) 
					return '';
				
				$transform_to = !empty($this->transform_to) ? $this->transform_to : 'user_link';
				
				if ('none' == $transform_to) {
					return $value;
				}
				
				if ('user_link' == $transform_to || 'user' == $transform_to) {
					return $this->getUserStr($transform_to, bab_getUserName($value), $format , bab_getUserDirEntryLink($value) );
				}

				$sheet = bab_getDirEntry($value, BAB_DIR_ENTRY_ID_USER);
				$value = isset($sheet[$transform_to]) ? $sheet[$transform_to]['value'] : '';
				if (FORM_TRANS_RICH_HTML == $format || FORM_TRANS_RICH_HTML_SMALL == $format || FORM_TRANS_HTML == $format) {
					$value = bab_toHtml($value);
				}

				if (FORM_TRANS_CSV == $format) {
					$value = form_cvs_value($value);
				}

				return $value;

			case 5: // Ovidentia Annuaire
				if (empty($value)) 
					return '';

				$transform_to = !empty($this->transform_to) ? $this->transform_to : 'user_link';
				if ('none' == $transform_to) {
					return $value;
				}
				
				$sheet = bab_getDirEntry($value, BAB_DIR_ENTRY_ID, $this->options['id_directory']);

				if (empty($sheet))
					return '';

				if ('user_link' == $transform_to || 'user' == $transform_to) {
					return $this->getUserStr($transform_to, $sheet['sn']['value'].' '.$sheet['givenname']['value'], $format, bab_getUserDirEntryLink($value, BAB_DIR_ENTRY_ID, $this->options['id_directory']) );
				}
				
				$value = isset($sheet[$transform_to]) ? $sheet[$transform_to]['value'] : '';
				if (FORM_TRANS_RICH_HTML == $format || FORM_TRANS_RICH_HTML_SMALL == $format || FORM_TRANS_HTML == $format) {
					bab_toHtml($value);
				}

				if (FORM_TRANS_CSV == $format) {
					$value = form_cvs_value($value);
				}
				return $value;
			
		}
	}

}


?>