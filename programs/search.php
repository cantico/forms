<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

include_once $GLOBALS['babAddonPhpPath']."linksincl.php";


class form_search
	{
	function form_search($q1, $q2, $option, $pos, $nb_result)
		{
		include_once $GLOBALS['babInstallPath']."utilit/searchincl.php";
		$this->db = &$GLOBALS['babDB'];

		$this->q1 = $q1;
		$this->q2 = $q2;
		$this->option = $option;
		$this->nb_result = $nb_result;

		$this->nbFetched = 0;

		$this->bulletins = array();


		$res = $this->db->db_query("
		
			SELECT 
				l.id,
				l.id_application,
				t.id id_table_search,
				t.approbation,
				fo1.id id_form_list,
				fo2.id id_form_view,
				s2.id id_step,
				IF(fi.description <> '', fi.description, fi.name) link_name,
				fi.id_table_field,
				fi.id_print,
				lj2.id desc_id,
				lj2.name desc_name,
				t.name tablename

			FROM 
				".FORM_APP_LINKS." l,
				".FORM_FORMS_FIELDS." fi,
				".FORM_FORMS." fo1 
				LEFT JOIN 
				".FORM_FORMS_FIELDS." lj1 
				ON lj1.id=fo1.id_search_description 
				LEFT JOIN 
				".FORM_TABLES_FIELDS." lj2 
				ON lj2.id=lj1.id_table_field AND lj2.field_function='' ,
				".FORM_TABLES." t,
				".FORM_APP_STEPS." s2,
				".FORM_FORMS." fo2
			
			 
			WHERE
				l.id_field = fi.id 
				AND fi.id_form = fo1.id 
				AND fo1.id_table = t.id 
				AND l.id_step = s2.id 
				AND s2.id_form = fo2.id 
				AND fo2.id_type = '3' 
				AND s2.id_application = l.id_application 
			");


		while ( $arr = $this->db->db_fetch_assoc($res))
			{
			if ( bab_isAccessValid(FORM_APP_GROUPS,$arr['id_application']) && bab_isAccessValid(FORM_TABLES_VIEW_GROUPS,$arr['id_table_search']) && bab_isAccessValid(FORM_FORMS_GROUPS,$arr['id_form_view']))
				{
				$this->searchon[$arr['id']] = $arr;
				}
			}

		if (!isset($this->searchon) || count ($this->searchon) == 0 ) 
			return false;

		$this->db->db_query("
					CREATE TEMPORARY TABLE form_searchresult (
						`id` int(10) unsigned NOT NULL default '0',
						`description` text NOT NULL,
						`name` varchar(255) NOT NULL default '',
						`form_row` int(10) unsigned NOT NULL default '0',
						`id_form` int(10) unsigned NOT NULL default '0',
						`id_step` int(10) unsigned NOT NULL default '0',
						`id_app` int(10) unsigned NOT NULL default '0',
						`popup` int(10) unsigned NOT NULL default '0',
						KEY  (`id`)
						)
						
					");


		$this->tbls = array();

		while($id_lnk = key($this->searchon))
			{
			$this->searchOnTable($id_lnk);
			next($this->searchon);
			}

		bab_debug("Form addon search on table(s) : \n".implode("\n",$this->tbls));

		$this->res = $this->db->db_query("SELECT s.*, a.name application FROM form_searchresult s, ".FORM_APP_APPLICATIONS." a WHERE id_app=a.id GROUP BY s.id");
		$this->count = $this->db->db_num_rows($this->res);
		}

	function searchOnTable($id_lnk)
		{
		$res = $this->db->db_query("
				SELECT 
					t.id,
					t.name 
				FROM 
					".FORM_FORMS_FIELDS." f,
					".FORM_TABLES_FIELDS." t
				WHERE 
					f.id_form='".$this->db->db_escape_string($this->searchon[$id_lnk]['id_form_list'])."' 
					AND f.search='Y' 
					AND t.id=f.id_table_field 
					AND t.field_function=''
				");

		$fields = array();
		while($arr = $this->db->db_fetch_array($res))
			{
			$tmp = bab_sql_finder($this->q2, form_col_name($arr['id'],$arr['name']), $this->option, $this->q1);
			if (!empty($tmp)) {
			$fields[] = $tmp;
			}

			if ($this->searchon[$id_lnk]['id_table_field'] == $arr['id'])
				{
				$link_col_name = form_col_name($arr['id'],$arr['name']);
				}
			}

		if (!isset($link_col_name))
			{
			$link_col_name = "''";
			}

		$this->tbls[] = $this->searchon[$id_lnk]['tablename'];

		$where = $fields ? '('.implode(' OR ',$fields).')' : '';

		if ($this->searchon[$id_lnk]['approbation'] == 1)
			{
			$where .= ' AND form_approb=\'1\'';
			}


		$desc_col = empty($this->searchon[$id_lnk]['desc_id']) ? "''" : form_col_name($this->searchon[$id_lnk]['desc_id'],$this->searchon[$id_lnk]['desc_name']);


		$popup = $this->searchon[$id_lnk]['id_print'] == 8 ? 1 : 0;

		$req = "
			INSERT INTO 
				form_searchresult 
			SELECT 
				CONCAT(form_id, '".$this->db->db_escape_string($this->searchon[$id_lnk]['id_form_view'])."', '".$this->db->db_escape_string($this->searchon[$id_lnk]['id_application'])."') id,
				".$desc_col." description,
				CONCAT('".$this->db->db_escape_string($this->searchon[$id_lnk]['link_name'])." : ',".$link_col_name.") name,
				form_id form_row,
				'".$this->db->db_escape_string($this->searchon[$id_lnk]['id_form_view'])."' id_form,
				'".$this->db->db_escape_string($this->searchon[$id_lnk]['id_step'])."' id_step,
				'".$this->db->db_escape_string($this->searchon[$id_lnk]['id_application'])."' id_app,
				'".$this->db->db_escape_string($popup)."' popup
				
			FROM 
				".$this->db->db_escape_string(form_tbl_name($this->searchon[$id_lnk]['id_table_search']));
				
		if ($where) {
			$req .= " 
			WHERE 
				".$where;
		}
				
		$this->db->db_query($req);
		}

	function length_limiter($txt, $limit = 100)
		{
		if ( strlen($txt) > $limit )
			return substr(strip_tags($txt), 0, $limit)."...";
		else
			return strip_tags($txt);
		}


	function setnextsearch($pos)
		{
		if (!isset($this->count) || $pos+$this->nbFetched+1 > $this->count) 
			return false;


		$this->db->db_data_seek($this->res, $pos+$this->nbFetched);
		$arr = $this->db->db_fetch_array($this->res);

		$this->nbFetched++;
		if (isset($arr) && $arr && $this->nbFetched <= $this->nb_result)
			{
			

			if ($arr['popup'])
				{
				$popup = array(
							$GLOBALS['babAddonUrl'].'form&idx=3&id_app='.$arr['id_app'].'&id_step='.$arr['id_step'].'&id_form='.$arr['id_form'].'&form_row='.$arr['form_row'].'&trt_step=1&popup=1', 
							$arr['name']
							);
				$link = false;
				}
			else
				{
				$link = array(
							$GLOBALS['babAddonUrl'].'form&idx=3&id_app='.$arr['id_app'].'&id_step='.$arr['id_step'].'&id_form='.$arr['id_form'].'&form_row='.$arr['form_row'].'&trt_step=1', 
							$arr['name']
							);

			$popup = false;
				}

			return array(
						bab_toHtml(bab_abbr(strip_tags($arr['description']),BAB_ABBR_FULL_WORDS,100)).' <br /> <em>('.form_translate('application').' : '.bab_toHtml($arr['application']).')</em>',
						$this->count,
						$link,
						$popup
						);
			}
		else
			return false;
		}
	}

?>