;<?php/*

[general]
name="forms"
version="7.77.2"
addon_type="EXTENSION"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description="Forms manager, tool to build complete application with table creation, forms, steps in application. Controls can be added on from fields"
description.fr="Module permettant de proposer des formulaires et des applications construites par configurations"
delete=1
db_prefix="form_"
ov_version="7.4.91"
php_version="4.1.2"
mysql_version="3.23"
author="paul de rosanbo ( paul.derosanbo@cantico.fr )"
icon="configure-toolbars.png"
tags="extension"

;*/?>