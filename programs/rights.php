<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once $GLOBALS['babInstallPath']."admin/acl.php";
require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function form_getFilterFrom($obj,$id)
	{
	$db = &$GLOBALS['babDB'];
	switch ($obj)
		{
		case 'table':
			$res = $db->db_query("SELECT t.id_group FROM ".FORM_FORMS." f, ".FORM_TABLES_VIEW_GROUPS." t WHERE t.id_object=f.id_table AND f.id='".$db->db_escape_string($id)."' GROUP BY t.id_group");
			break;
		case 'gest':
			$res = $db->db_query("SELECT id_group FROM ".FORM_GEST_GROUPS." WHERE id_object='".$db->db_escape_string($id)."' GROUP BY id_group");
			break;
		}
	if ($db->db_num_rows($res) > 0)
		{
		$grp = bab_getGroups();
		$grp = $grp['id'];
		while (list($id_object) = $db->db_fetch_array($res))
			{
			switch ($id_object)
				{
				case 0:
					return false;
					break;
				case 1:
					return array(0,0,1,0,1,array());
					break;
				case 2:
					return array(1,0,1,1,0,array());
					break;
				default:
					$key = array_search($id_object,$grp);
					if (isset($key) && $key !== false)
						unset($grp[$key]);
					break;
				}
			}
		return array(0,0,1,1,1,$grp);
		}
	else
		return false;
	}


function access_table($id_table)
	{
	$db = &$GLOBALS['babDB'];
	if ($id_table) {
		$row = $db->db_fetch_assoc($db->db_query("SELECT name, approbation FROM ".FORM_TABLES." WHERE id='".$db->db_escape_string($id_table)."'"));
		$tablename = $row['name'];
	} else {
		$tablename = null;
	}
	//$arr = form_getFilterFrom('gest',1);
		
	$macl = new macl($GLOBALS['babAddonTarget']."/rights", "acces_table", $id_table, "acl");
	if (isset($tablename))
	{
		$macl->addtable(FORM_TABLES_VIEW_GROUPS,form_translate("Table view").' : '.$tablename);
		$macl->addtable(FORM_TABLES_CHANGE_GROUPS,form_translate("Table change").' : '.$tablename);
		if ($row['approbation'] == 1)
		{
			$macl->filter(0, 0, 1, 0, 1);
		}
	} else {
		$macl->addtable(FORM_TABLES_VIEW_GROUPS,form_translate("View in all workspace tables"));
		$macl->addtable(FORM_TABLES_CHANGE_GROUPS,form_translate("Change in all workspace tables"));
	}
	
	if (isset($tablename))
	{
		$macl->addtable(FORM_TABLES_DELETE_GROUPS,form_translate("Table delete").' : '.$tablename);
		$macl->addtable(FORM_TABLES_INSERT_GROUPS,form_translate("Table insert").' : '.$tablename);
		if ($row['approbation'] == 1)
		{
			$macl->filter(0, 0, 1, 0, 1);
		}
	} else {
		$macl->addtable(FORM_TABLES_DELETE_GROUPS,form_translate("Delete in all workspace tables"));
		$macl->addtable(FORM_TABLES_INSERT_GROUPS,form_translate("Insert in all workspace tables"));
	}
	
	/*
	$macl->addtable(FORM_TABLES_FILING_GROUPS,form_translate("Table archive").' : '.$row['name']);
	$macl->filter($arr[0], $arr[1], $arr[2], $arr[3], $arr[4], $arr[5]);
	$macl->addtable(FORM_TABLES_LOCK_GROUPS,form_translate("Table lock").' : '.$row['name']);
	$macl->filter($arr[0], $arr[1], $arr[2], $arr[3], $arr[4], $arr[5]);
	*/
	$macl->babecho();
	}
	
/**
 * clone rights of id_object 0 to all table of current workspace
 */
function form_applyAccessTable()
{
	global $babDB;
	$res = $babDB->db_query("
				SELECT t.id 
				FROM 
					".FORM_TABLES." t,
					".FORM_WORKSPACE_ENTRIES." w 
				
				WHERE 
					t.id = w.id_object 
					AND w.type='table' 
					AND w.id_workspace=".$babDB->quote(form_getWorkspace())." 
			");
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$id_table = (int) $arr['id'];
		aclCloneRights(FORM_TABLES_VIEW_GROUPS, 0, FORM_TABLES_VIEW_GROUPS, $id_table);
		aclCloneRights(FORM_TABLES_CHANGE_GROUPS, 0, FORM_TABLES_CHANGE_GROUPS, $id_table);
		aclCloneRights(FORM_TABLES_DELETE_GROUPS, 0, FORM_TABLES_DELETE_GROUPS, $id_table);
		aclCloneRights(FORM_TABLES_INSERT_GROUPS, 0, FORM_TABLES_INSERT_GROUPS, $id_table);
	}
}



/**
 * clone rights of id_object 0 to all forms of current workspace
 */
function form_applyAccessForm()
{
	global $babDB;
	$res = $babDB->db_query("
				SELECT f.id 
				FROM 
					".FORM_FORMS." f,
					".FORM_WORKSPACE_ENTRIES." w 
				
				WHERE 
					f.id = w.id_object 
					AND w.type='form' 
					AND w.id_workspace=".$babDB->quote(form_getWorkspace())." 
			");
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$id_form = (int) $arr['id'];
		aclCloneRights(FORM_FORMS_GROUPS, 0, FORM_FORMS_GROUPS, $id_form);
	}
}
	

// main

$FORM_USER = form_getUserFormAcces();

if (!$FORM_USER['ADMIN'])
	{
	$babBody->msgerror = form_translate('Acess denied');
	return;
	}
	

$idx = bab_rp('idx');
$id_table = bab_rp('id_table');
$item = bab_rp('item');

if( bab_rp('acl'))
{
	maclGroups();
	
	if (0 === (int) $item)
	{
		form_applyAccessTable();
	}
	
	header("location:".$GLOBALS['babAddonUrl']."main&idx=list_tables");
	exit;
}

if( bab_rp('acl2'))
	{
	maclGroups();
	
	if (0 === (int) $item)
	{
		form_applyAccessForm();
	}
	
	header("location:".$GLOBALS['babAddonUrl']."main&idx=list_forms");
	exit;
	}

if( bab_rp('acl3'))
	{
	maclGroups();
	header("location:".$GLOBALS['babAddonUrl']."main&idx=list_applications");
	exit;
	}

switch ($idx)
	{
	case "acces_table":
		$id_table = (int) bab_rp('id_table', 0);
		$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
		$babBody->addItemMenu("acces_table", form_translate("Acces"),$GLOBALS['babAddonUrl']."rights&idx=acces_lock&id_table=".$id_table);

		if (0 === $id_table)
		{
			$babBody->setTitle(form_translate("Acces for all tables of workspace"));
		} else {
			$babBody->setTitle(form_translate("Acces table"));
		}
		
		access_table($id_table);
		
		break;
	
	case "acces_form":
		$id_form = (int) bab_rp('id_form', 0);
		if (0 === $id_form)
		{
			$title = form_translate("Acces for all forms of workspace");
		} else {
			$title = form_translate("Acces form");
		}
		
		$babBody->setTitle($title);
		
		$babBody->addItemMenu("list_forms", form_translate("List forms"),$GLOBALS['babAddonUrl']."main&idx=list_forms");
		$babBody->addItemMenu("acces_form", form_translate("Acces form"),$GLOBALS['babAddonUrl']."rights&idx=acces_form&id_form=".$id_form);
		$macl = new macl($GLOBALS['babAddonTarget']."/rights", "acces_form", $id_form, "acl2");
		$macl->addtable(FORM_FORMS_GROUPS, $title);
		$arr = form_getFilterFrom('table', $id_form);
		$macl->filter($arr[0], $arr[1], $arr[2], $arr[3], $arr[4], $arr[5]);
		$macl->babecho();
		break;

	case "acces_applications":
		$id_app = bab_rp('id_app');

		$babBody->title = form_translate("Acces applications");
		$babBody->addItemMenu("list_applications", form_translate("List applications"),$GLOBALS['babAddonUrl']."main&idx=list_applications");
		$babBody->addItemMenu("acces_applications", form_translate("Acces applications"),$GLOBALS['babAddonUrl']."rights&idx=acces_applications&id_application=".$id_app);
		aclGroups($GLOBALS['babAddonTarget']."/rights",$idx,FORM_APP_GROUPS,$id_app,"acl3");
		break;
	}
	
$babBody->setCurrentItemMenu($idx);
?>