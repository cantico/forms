<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");


function fc_list($id_table)
	{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id_table)
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_function = form_translate('Function');
			$this->t_edit = form_translate('Edit');
			
			$this->id_table = bab_toHtml($id_table);

			$this->res = $this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($id_table)."' AND field_function<>''");

            }

		function getnext()
			{
			if ($this->arr = $this->db->db_fetch_array($this->res))
				{
				$this->arr['field_function'] = bab_toHtml(form_translate($this->arr['field_function']));
				$this->arr['name'] = bab_toHtml($this->arr['name']);
				return true;
				}
			else
				{
				return false;
				}
			}
        }
    $tp = new temp($id_table);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."funccol.html", "list"));

	}


function fc_edit($id_table)
	{
	global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id_table)
            {
			$this->db = &$GLOBALS['babDB'];

			$this->t_name = form_translate('Name');
			$this->t_function = form_translate('Function');
			$this->t_index = form_translate('Index');
			$this->t_formula = form_translate('Formula');
			$this->t_field_formula = form_translate('Formula');
			$this->t_record = form_translate('Record');
			$this->t_delete = form_translate('Delete');
			$this->t_approb = form_translate('Approbation status');
			$this->t_num_approb = form_translate('Approbation status number');
			
			$this->id_table = bab_rp('id_table');


			if (isset($_POST) && count($_POST) > 0)
				$this->arr = $_POST;
			elseif (isset($_GET['id_func']))
				{
				$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($id_table)."' AND field_function<>'' AND id='".$this->db->db_escape_string($_GET['id_func'])."'"));
				}
			else
				{
				$el_to_init = array('id','name','field_formula');
				foreach($el_to_init as $v)
					$this->arr[$v] = '';
				}
/*
			$query = "
				SELECT 
					IF(l.id IS NULL, f.id, l.id) id, 
					IF(l.name IS NULL, f.name, CONCAT(f.name,'.',l.name)) name 
				FROM 
					".FORM_TABLES_FIELDS." f
						LEFT JOIN ".FORM_TABLES_FIELDS." l ON l.id=f.link_table_field_id 
				WHERE 
					f.id_table='".$this->db->db_escape_string($id_table)."' AND f.field_function=''
			";
*/
			$query = "
				SELECT 
					f.id, 
					f.name 
				FROM 
					".FORM_TABLES_FIELDS." f 
				WHERE 
					f.id_table='".$this->db->db_escape_string($id_table)."' AND f.field_function=''
			";
			
			$this->resfield = $this->db->db_query($query);

            }

		function getnextfield()
			{
			if ($arr = $this->db->db_fetch_assoc($this->resfield))
			{	
				$this->field['id'] = $arr['id'];
				$this->field['name'] = bab_toHtml($arr['name'], BAB_HTML_JS);
				return true;
			}
			else
				{
				return false;
				}
			}
        }
    $tp = new temp($id_table);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."funccol.html", "edit"));

	}


function record_func()
{
	$db = &$GLOBALS['babDB'];

	if (!isset($_POST['field_function']))
		return false;

	if (!empty($_POST['id_func']))
		{
		$db->db_query("UPDATE ".FORM_TABLES_FIELDS." SET name='".$db->db_escape_string($_POST['name'])."', field_function='".$db->db_escape_string($_POST['field_function'])."', field_formula='".$db->db_escape_string($_POST['field_formula'])."' WHERE id='".$db->db_escape_string($_POST['id_func'])."'");
		}
	else
		{
		$db->db_query("INSERT INTO ".FORM_TABLES_FIELDS." ( name, id_table, field_function, field_formula) VALUES ('".$db->db_escape_string($_POST['name'])."' ,'".$db->db_escape_string($_POST['id_table'])."', '".$db->db_escape_string($_POST['field_function'])."' , '".$db->db_escape_string($_POST['field_formula'])."')");
		}

	return true;
}


function delete_func()
{
	$db = & $GLOBALS['babDB'];
	$db->db_query("DELETE FROM ".FORM_TABLES_FIELDS." WHERE id='".$db->db_escape_string($_POST['id_func'])."'");
}


// main

$idx = isset($_REQUEST['idx']) ? $_REQUEST['idx'] : 'list';

if (isset($_POST['action']))
{
	switch ($_POST['action'])
	{
		case 'edit_func':
			
			if (!record_func())
				$idx = 'list';
			break;

		case 'delete_func':
			delete_func();
			break;
	}
}


$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
$babBody->addItemMenu("list", form_translate("List collums functions"),$GLOBALS['babAddonUrl']."funccol&idx=list&id_table=".$_REQUEST['id_table']);



switch ($idx)
{
	case 'edit':
		$babBody->addItemMenu("edit", form_translate("Edit function"),$GLOBALS['babAddonUrl']."funccol&idx=list");
		$babBody->title = form_translate("Edit function");
		fc_edit($_REQUEST['id_table']);
		break;
	
	default:
	case 'list':
		$babBody->addItemMenu("edit", form_translate("Add function"),$GLOBALS['babAddonUrl']."funccol&idx=edit&id_table=".$_REQUEST['id_table']);
		$babBody->title = form_translate("List collums functions");
		fc_list($_REQUEST['id_table']);
		break;
}

$babBody->setCurrentItemMenu($idx);
?>