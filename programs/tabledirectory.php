<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

class form_tableDirectory {

	var $id_table;
	
	function form_tableDirectory($id_table) {
		$this->id_table = $id_table;
	}
	
	/**
	 * @static
	 * @return array
	 */
	function getDirectoryTables() {
		global $babDB;
		
		$tables = array();
		$res = $babDB->db_query('SELECT id FROM '.FORM_TABLES.' WHERE id_group > \'0\'');
		while ($arr = $babDB->db_fetch_assoc($res)) {
			$tables[$arr['id']] = $arr['id'];
		}
		
		return $tables;
	}
	
	
	/**
	 * @static
	 * @param	int		$id_table
	 * @param	int		$form_row
	 * @return array
	 */
	function getIdUser($id_table, $form_row) {
		global $babDB;
		
		$tables = array();
		$res = $babDB->db_query('
		SELECT f.id, f.name, f.field_function FROM 
			'.FORM_TABLES_FIELDS.' f, 
			'.FORM_TABLES_FIELDS_DIRECTORY.' d 
		WHERE 
			f.id_table='.$babDB->quote($id_table).' 
			AND d.id_table_field = f.id 
			AND d.directory_field = \'id_user\'
		');
		if ($arr = $babDB->db_fetch_assoc($res)) {
			$field = form_col_name($arr['id'], $arr['name'], $arr['field_function']);
			list($id_user) = $babDB->db_fetch_array($babDB->db_query('SELECT '.$field.' FROM '.form_tbl_name($id_table).' WHERE form_id='.$babDB->quote($form_row)));
			
			return $id_user;
		}
		
		return false;
	}

	/**
	 * @private
	 */
	function table() {
		global $babDB;
		static $table = NULL;
		if (NULL === $table) {
			$table = $babDB->db_fetch_assoc($babDB->db_query('
				SELECT id_group, delete_password FROM '.FORM_TABLES.' WHERE id='.$babDB->quote($this->id_table)
			));
		}
		return $table;
	}
	
	/**
	 * @public
	 */
	function members() {
		$table = $this->table();
		if (!empty($table['id_group'])) {
			$members = bab_getGroupsMembers($table['id_group']);
			if (false === $members) {
				return array();
			}
			
			return $members;
		}
		
		return array();
	}
	
	/**
	 * @private
	 */
	function getEmptyMap() {
	
		static $map = NULL;
		
		if (NULL === $map) {
	
			global $babDB;
			$res = $babDB->db_query('
				SELECT 
					f.id, f.name, d.directory_field 
				FROM 
					'.FORM_TABLES_FIELDS.' f, 
					'.FORM_TABLES_FIELDS_DIRECTORY.' d 
				WHERE 
					f.id = d.id_table_field 
					AND f.id_table='.$babDB->quote($this->id_table).' 
					AND f.field_function=\'\''
				);
				
			$map = array();
			while ($arr = $babDB->db_fetch_assoc($res)) {
				$map[$arr['directory_field']] = array(
					'colname' 		=> form_col_name($arr['id'],$arr['name']),
					'value'			=> NULL
				);
			}
		}
		
		return $map;
	}
	
	
	
	function notifyUser($map) {
		
		// notification
		
		/*
		  '
			Your account has been created :
			
			Login : %s
			Password : %s
			
			Homepage : %s
		
		'
		
		*/
		
		$subject = form_translate('New account creation');
		$body = sprintf(form_translate('new account body'), 
		$map['nickname']['value'],
		$map['password']['value'],
		$GLOBALS['babUrl']
		);
		
		
		$mail = bab_mail();
		$mail->mailSubject($subject);
		$mail->mailBody($mail->mailTemplate(bab_toHtml($body, BAB_HTML_ALL)), "text/html");
		$mail->mailAltBody($body);
		$mail->mailTo($map['email']['value']);
		
		$retry = 0;
		while (true !== $mail->send() && $retry < 5 ) {
			$retry++;
		}

		if ($retry < 5) {
			bab_debug('User notified '.$map['email']['value']);
			}
		else {
			global $babBody;
			$babBody->addError(sprintf(form_translate("Error, can't send mail to : %s"), $map['email']['value']));
		}
		
		$this->debug = array();
	}
	
	
	/**
	 * @private
	 * @param	array		$map
	 */
	function createUser($map) {
	
		global $babDB, $babBody;
		
		$error = '';
		
		$middlename = isset($map['mn']['value']) ? $map['mn']['value'] : '';
		
		$id_user = bab_registerUser( 
			$map['givenname']['value'], 
			$map['sn']['value'], 
			$middlename, 
			$map['email']['value'], 
			$map['nickname']['value'], 
			$map['password']['value'], 
			$map['password']['value'], 
			1, 
			$error
		);
		
		if (!empty($error)) {
			$babBody->addError($error);
		}
		
		if ($id_user) {

			$infos = array();
			
			foreach($map as $key => $arr) {
				$infos[$key] = $arr['value'];
			}
		
			bab_updateUserById($id_user, $infos, $error);
			
			
			if ($map['email']['value']) {
				$this->notifyUser($map);
			}
			
			
			
			
			return $id_user;
		}
		
		return false;
	}
	
	
	/** 
	 * @private
	 */
	function userCreated($form_row, $id_user, $map) {
		global $babDB;
		$table = $this->table();

		$query = '
			UPDATE '.form_tbl_name($this->id_table).' 
			SET 
				'.$map['id_user']['colname'].'='.$babDB->quote($id_user).' ';

		if ($table['delete_password']) {
			$query .= ', '.$map['password']['colname'].'=\'\'';
		}
		
		$query .= ' WHERE form_id='.$babDB->quote($form_row);
		
		$babDB->db_query($query);
		
		// attention cette fonction notifie le module forms
		// dans self::updateRowFromUser()
		bab_attachUserToGroup($id_user, $table['id_group']);
	}
	
	/**
	 * Update a user in ovidentia from a row in the table
	 * create the user if not exists
	 * Modifiy directory fields if differents
	 * @param	int		$form_row
	 */
	function updateUserFromRow($form_row) {
		
		global $babDB;
		$map = $this->getEmptyMap();

		
		$res = $babDB->db_query('SELECT * FROM '.form_tbl_name($this->id_table).' WHERE form_id='.$babDB->quote($form_row));
		$result = $babDB->db_fetch_assoc($res);
		
		if ($result) {
			foreach($map as $fieldname => $arr) {
				if (isset($result[$arr['colname']])) {
					$map[$fieldname]['value'] = $result[$arr['colname']];
				}
			}
			
			bab_debug($map);
			
			$id_user = $map['id_user']['value'];
		} else {
		
			// creer le user si il n'est pas r�pertori� dans la table
			if ($id_user = $this->createUser($map)) {
				$this->userCreated($form_row, $id_user, $map);
				return true;
			} else {
				return false;
			}
		}
		
		// la fiche utilisateur du module formulaire est consid�r�e active et confirm�e
		$map['disabled']['value'] 		= 0;
		$map['is_confirmed']['value'] 	= 1;
		
		if ($id_user) {
			$userInfos = bab_getUserInfos($id_user);
		} else {
			$userInfos = false;
		}


		// cr�er le user si il n'est pas dans l'annuaire d'ovidentia
		if (false === $userInfos) {
			if ($id_user = $this->createUser($map)) {
				bab_debug('User Created');
				$this->userCreated($form_row, $id_user, $map);
				return true;
			} else {
				return false;
			}
		}
		
		$userInfos['id_user'] = $id_user;
		
		// chercher une modification sur la map
		$modifications = array();
		foreach($map as $fieldname => $arr) {
			
			if (isset($userInfos[$fieldname]) && 
			((string) $userInfos[$fieldname] !== (string) $arr['value'])) {
				bab_debug('Difference between ovidentia user and table in '.$fieldname.' : '.$userInfos[$fieldname].' !== '.$arr['value']);
				// a field is modified
				$modifications[$fieldname] = $arr['value'];
			}
		}
		
		$error = '';

		if (!empty($modifications) && !bab_updateUserById($id_user, $modifications, $error)) {
			bab_debug('bab_updateUserById : '.$error);
			return NULL;
		}
		
		return true;
	}
	
	
	
	/**
	 * Update a row in the form table from a user in ovidentia
	 * Create the row if not exists
	 * If add_to_group is true, the user will be added to the group after the update
	 * If add_to_group is false, the user is ignored if not member of the group
	 * @param	int		$id_user
	 * @param	boolean	$add_to_group
	 * @return	boolean
	 */
	function updateRowFromUser($id_user, $add_to_group = false) {
	
		$id_user = (int) $id_user;
	
		// ne rien faire si pas de id_user
		if (empty($id_user)) {
			return false;
		}
		
		
		
		global $babDB;
		$table = $this->table();
		
		if (!$add_to_group && !bab_isMemberOfGroup($table['id_group'], $id_user)) {
			return false;
		}
	
	
		// chercher si le user est d�ja dans la table en utilisant le nickname et l'id car le id user n'est pas encore dans la table si le groupe utilis� est 'utilisateur enregistr�es'
		
	
		$map = $this->getEmptyMap();
		$id_user_col = $map['id_user']['colname'];
		$nickname_col = $map['nickname']['colname'];
		$nickname = bab_getUserNickname($id_user);

		if (empty($id_user_col) || empty($nickname_col)) {
			return false;
		}


		
		$res = $babDB->db_query('
			SELECT * 
				FROM '.form_tbl_name($this->id_table).' 
			WHERE 
				('.$babDB->db_escape_string($nickname_col).'='.$babDB->quote($nickname).' 
				OR '.$babDB->db_escape_string($id_user_col).'='.$babDB->quote($id_user).')'
		);
		
		$result = $babDB->db_fetch_assoc($res);
		
		$userInfos = bab_getUserInfos($id_user);
		$userInfos['id_user'] = $id_user;
		
		if ($result) {

			$changes = array();
			foreach($map as $fieldname => $arr) {
				if (isset($result[$arr['colname']]) 
				&& isset($userInfos[$fieldname]) 
				&& ((string) $userInfos[$fieldname]) !== ((string) $result[$map[$fieldname]['colname']])) {
					$changes[] = $map[$fieldname]['colname'].'='.$babDB->quote($userInfos[$fieldname]);
				}
			}
			
			$form_row = $result['form_id'];
			
			if (empty($changes)) {
				return false;
			}
			
			$query = '
				UPDATE '.form_tbl_name($this->id_table).' 
				SET 
					'.implode(', ',$changes).', 
					form_lastupdate=NOW(), 
					form_lastupdate_user='.$babDB->quote($GLOBALS['BAB_SESS_USERID']).'
				WHERE 
					form_id='.$babDB->quote($form_row);

			bab_debug($query);
			$babDB->db_query($query);
			
		} else {
		
			
			$insert = array();
			foreach($map as $fieldname => $arr) {
				if (isset($userInfos[$fieldname])) {
					$insert[$map[$fieldname]['colname']] = $userInfos[$fieldname];
				}
			}

			if (empty($insert)) {
				return false;
			}
			
			$query = '
			INSERT INTO '.form_tbl_name($this->id_table).' 
				('.implode(', ',array_keys($insert)).', form_create, form_lastupdate, form_lastupdate_user, form_create_user) 
			VALUES 
				('.$babDB->quote($insert).', NOW(), NOW(),
				'.$babDB->quote($GLOBALS['BAB_SESS_USERID']).', 
				'.$babDB->quote($GLOBALS['BAB_SESS_USERID']).' )
			';
			
			bab_debug($query);
			$babDB->db_query($query);
		}
		
		
		
		
		
		if ($add_to_group && !bab_isMemberOfGroup($table['id_group'], $id_user)) {
			bab_attachUserToGroup($id_user, $table['id_group']);
		}
		
		return true;
	}
}


?>