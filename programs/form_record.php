<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
include_once $GLOBALS['babInstallPath']."utilit/uploadincl.php";


/**
 * Sanitizes text sent by a form.
 *
 * If the skin is not in UTF-8, when UTF-8 characters are sent through a form, they are
 * converted to html entities by the web browser.
 * Here we convert them back to UTF-8 en then convert again to the current encoding with
 * transliteration.
 *
 * @param string	$value	A text value as sent by a form (through http POST or GET).
 *
 * @return string	The equivalent of $value in the current character encoding.
 */
function form_textInput($value)
{
	if (!function_exists('iconv') || !is_string($value)) {
		return $value;
	}

	if (bab_Charset::getIso() !== bab_Charset::UTF_8) {
		$value = iconv(bab_Charset::getIso(), bab_Charset::UTF_8, $value);
		$value = html_entity_decode($value, ENT_QUOTES, bab_Charset::UTF_8);
		$value = iconv(bab_Charset::UTF_8, bab_Charset::getIso() . '//TRANSLIT', $value);
	}
	return $value;
}





function form_record()
	{
	$db = & $GLOBALS['babDB'];

	$babInstallPath = $GLOBALS['babInstallPath'];
	include_once $GLOBALS['babInstallPath']."utilit/mailincl.php";
	require_once( $GLOBALS['babInstallPath']."addons/forms/linksincl.php");


	/**
	 * Objet visualisation
	 * trouver les valeurs qui doivent etre affichees en fonction de la ligne de la table
	 */
	class view
		{
		function view($id_form)
			{
			$this->db = & $GLOBALS['babDB'];

			$arr = $this->db->db_fetch_array($this->db->db_query("SELECT t.id id_table, t.approbation FROM ".FORM_TABLES." t, ".FORM_FORMS." f WHERE f.id='".$this->db->db_escape_string($id_form)."' AND t.id=f.id_table"));



			$this->queryObj = new form_queryObj($arr['id_table'], $arr['approbation']);


			// Links

			$res = $this->db->db_query("
					SELECT
						f.id,
						f.id_table_field,
						f.id_table_field_link ,
						tfl.link_table_field_id col_ref,
						tfv.id col_to_get
					FROM
						".FORM_FORMS_FIELDS." f,
						".FORM_TABLES_FIELDS." tfv,
						".FORM_TABLES_FIELDS." tfl
					WHERE
						f.id_form='".$this->db->db_escape_string($id_form)."'
					AND f.id_table_field_link > 0
					AND tfv.id=f.id_table_field_link
					AND tfl.id=f.id_table_field
				");

			$this->linked_values = array();
			while ($arr = $this->db->db_fetch_array($res))
				{
				$this->linked_values[$arr['id']] = array( $arr['id_table_field'], $arr['id_table_field_link']);
				}


			// tables de liaison

			$res = $this->db->db_query("
					SELECT
						ff.id,
						lnk.id id_lnk,
						lnk.lnk_type,
						lnk.id_lnk_table,
						lnk.id_lnk_f2,
						lnk.id_field2,
						tf1.name name_field2,
						tf1.field_function function_field2,
						lnk.id_table2,
						ff.id_table_field_lnk id_view,
						tf2.name name_view,
						tf2.field_function function_view
					FROM
						".FORM_FORMS_FIELDS." ff
					LEFT JOIN
						".FORM_TABLES_FIELDS." tf2
						ON tf2.id=ff.id_table_field_lnk,
						".FORM_TABLES_LNK." lnk
					LEFT JOIN
						".FORM_TABLES_FIELDS." tf1
						ON tf1.id=lnk.id_field2

					WHERE
						ff.id_form='".$this->db->db_escape_string($id_form)."'
						AND lnk.id=ff.id_lnk

				");

			$this->lnk_values = array();
			while ($arr = $this->db->db_fetch_array($res))
				{
				$col_to_get = form_col_name($arr['id_view'], $arr['name_view'] , $arr['function_view']);
				$table_name = form_tbl_name($arr['id_table2']);

				switch ($arr['lnk_type'])
					{
					case 0:
						$col_ref = 'form_id';
						break;
					case 1:
						$col_ref = form_col_name($arr['id_field2'], $arr['name_field2'] , $arr['field_function']);
						break;
					}
				$this->lnk_values[$arr['id']] = array(
													'col_ref' => $col_ref ,
													'col_to_get' => $col_to_get,
													'table_name' => $table_name
													);
				}
			}

		function evaltrans($idx, &$value, $format) {
			$value = form_getTransformedValue($idx , 0, $value, $format);
			}

		function getvalue($idx,$value, $format = FORM_TRANS_TXT)
			{
			if (isset($this->linked_values[$idx]))
				{

				$value = $this->queryObj->getValueFromOption($this->linked_values[$idx][0], $this->linked_values[$idx][1], $value);
				}


			if (isset($this->lnk_values[$idx]))
				{
				if (!is_array($value))
					{
					$value = array($value);
					}

				$req = "SELECT ".$this->lnk_values[$idx]['col_to_get']." FROM ".$this->lnk_values[$idx]['table_name']."  WHERE ".$this->lnk_values[$idx]['col_ref']." IN(".$this->db->quote($value).")";

				$value = array();

				$res = $this->db->db_query($req);

				while (list($v) = $this->db->db_fetch_array($res))
					{
					$value[] = $v;
					}
				}

			if (is_array($value))
				{
				foreach($value as $k => $v) {
					$this->evaltrans($idx, $value[$k], $format);
					}
				}
			else
				$this->evaltrans($idx, $value, $format);


			if (is_array($value))
				$value = implode(', ',$value);

			return $value;
			}
		}




	/**
	 * Notfication
	 */
	class mailmsg
        {
		var $stack = array();
		var $debug = array();
		var $altbg = true;

		/**
		 * @access private
		 */
		var $ignore_empty_values = 0;

		/**
		 * @param	1|0		$ignore_empty_values
		 * @param	view	$view					objet visualisation
		 */
        function mailmsg($ignore_empty_values, &$view)
            {
			$this->mail = bab_mail();
			$this->view = &$view;

			$this->ignore_empty_values = (int) $ignore_empty_values;

			if (!$this->mail)
				return false;

			if ($GLOBALS['BAB_SESS_LOGGED']) {
				$this->mail->mailFrom($GLOBALS['BAB_SESS_EMAIL'],$GLOBALS['BAB_SESS_USER']);
			}

			$this->fields = array();
			$this->links = array();
			$this->recipients_by_mail = form_getConfigValue('recipients_by_mail');
			if (empty($this->recipients_by_mail)) $this->recipients_by_mail = 10;
			$this->lb = "\n";
            }

		function form_by_mail(&$txt)
			{
			$txt = bab_toHtml($txt);
			}

		function mailSubject($default, $str = '')
			{
			if (empty($str))
				$str = $default;

			if (!$this->mail)
				return false;

			$this->mail->mailSubject($str);
			}

		function addfield($idx, $value, $name, $description, $function)
			{
			if (!$this->mail)
				return false;


			$row_name = $name;
			$row_description = $description;
			$row_value	= $this->view->getvalue($idx, $value);
			$value		= $this->view->getvalue($idx, $value, FORM_TRANS_RICH_HTML);

			$this->form_by_mail($name);
			$this->form_by_mail($description);

			$this->fields[] = array(
									'name'				=> $name,
									'row_name'			=> $row_name,
									'description'		=> $description,
									'row_description'	=> $row_description,
									'value'				=> $value,
									'row_value'			=> $row_value
									);

			if (!empty($function))
				{
				$this->field_function[count($this->fields)-1] = $function;
				}
			}

		function addlink($name,$description,$url)
			{
			if (!$this->mail)
				return false;

			$row_name = $name;
			$row_description = $description;

			$this->form_by_mail($name);
			$this->form_by_mail($description);

			$this->links[] = array(
								'name'				=> $name,
								'row_name'			=> $row_name,
								'description'		=> $description,
								'row_description'	=> $row_description,
								'url'				=> $url
								);
			}

		function mailDestArray($arr,$type)
			{
			if (!$this->mail)
				return false;

			if (!in_array($type,array('mailTo','mailBcc')))
				return;
			if (!is_array($arr) || count($arr) == 0)
				return;
			if (isset($this->stack[$type]))
				$this->stack[$type] = array_merge($this->stack[$type],$arr);
			else
				$this->stack[$type] = $arr;

			$this->stack[$type] = array_unique($this->stack[$type]);
			}

		function mail_pop($type)
			{
			for ($i = 0 ; $i < $this->recipients_by_mail ; $i++ )
				{
				$mail = array_pop($this->stack[$type]);
				if (!$mail && $i == 0)
					return false;
				elseif (!empty($mail))
					{
					$this->debug[] = 'Add '.$type.' : '.$mail;
					$this->mail->$type($mail);
					}
				}
			return true;
			}

		function get_gust_recipients()
			{
			$this->mail->clearAllRecipients();
			$out = false;
			$types = array_keys($this->stack);
			foreach ($types as $type)
				{
				if ($this->mail_pop($type))
					$out = true;
				}

			return $out;
			}

		function getnext(&$skip)
			{
			static $i=0;
			if( $i < count($this->fields))
				{
				$this->altbg = !$this->altbg;
				$this->arr = $this->fields[$i];

				if (1 === $this->ignore_empty_values && '' === (string) $this->arr['value']) {
					$skip = true;
					$i++;
					return true;
				}


				$i++;
				return true;
				}
			else
				{
				$i=0;
				return false;
				}
			}

		function getnextlink()
			{
			return list(,$this->arr) = each($this->links);
			}

		function field_functions()
			{
			if (isset($this->field_function))
				{

				$form_row = form_currentDbRow();
				if (NULL === $form_row) {
					$form_row = bab_pp('form_row');
				}

				foreach ($this->field_function as $field => $f)
					{
					if ($f == 'index' && !empty($form_row))
						{
						$this->fields[$field]['value'] = $form_row;
						}
					}
				}
			}

		/**
		 *
		 * @param bab_fileHandler	$file
		 *
		 */
		function mailFileAttach($file, $source) {
			if (!$this->mail)
				{
				return false;
				}

			if (false === $file->error)
				{
				$this->mail->mailFileAttach($source, $file->filename, $file->mime );
				}
			}

		function mailmsgsend()
			{
			if (!$this->mail)
				{
				bab_debug("Mail disabled in site configuration");
				return false;
				}

			$this->field_functions();

			$html = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath']."email.html", "html");
			$text = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath']."email.html", "text");
			$this->mail->mailBody($this->mail->mailTemplate($html), "text/html");
			$this->mail->mailAltBody(strip_tags($text));

			if ($this->recipients_by_mail < 1)
				{
				$GLOBALS['babBody']->msgerror = form_translate('Configuration error, recipients by email must be greater than 0');
				return;
				}

			while($this->get_gust_recipients())
				{

				$retry = 0;
				while ( true !== $this->mail->send() && $retry < 5 )
					{
					$retry++;
					}

				if ($retry < 5)
					$this->debug[] = '<span style="color:green">MAIL SEND OK</span> (try : '.$retry.'/5)';
				else
					$this->debug[] = '<span style="color:red">MAIL SEND ERROR</span>';

				bab_debug(implode("\n",$this->debug));
				$this->debug = array();
				}

			$this->stack = array();
			}
        }

	class db_transfer
		{

		/**
		 * id du formulaire fournis par le submit
		 * @access private
		 */
		var $id_form;


		/**
		 * id de la table
		 * @access private
		 */
		var $id_table;

		/**
		 * les colonnes de la table qui doivent �tre trait�s
		 * cette variable est renseign�e en interne a la classe
		 * @see db_transfer::addCol()
		 * @access private
		 */
		var $col = array();

		/**
		 * Les valeurs � traiter
		 * cette variable est renseign�e en interne a la classe
		 * Les index doivent �tre en correlation avec le tableau $this->col
		 * @access private
		 */
		var $value = array();


		/**
		 * contient les colonnes de la tables a modifier lors de la copie vers une table suppl�mentaire
		 * query_copy a �t� cr�er pour faire un historisation des modification
		 * @see db_transfer::query_copy()
		 * @access private
		 */
		var $col_copy = array();

		/**
		 * contient les valeurs de la tables a modifier lors de la copie vers une table suppl�mentaire
		 * @see db_transfer::query_copy()
		 * @access private
		 */
		var $value_copy = array();


		/**
		 * valeur du champ field_copy_table_field en fonction de l'id du champ de formulaire
		 * @access private
		 */
		var $colcopy_by_formfield = array();



		/**
		 * Type de requete SQL a effectuer
		 * @access private
		 */
		var $query_type = 'INSERT';


		/**
		 * tableau contenant le nom de la colonne dans le formulaire pour chaque champs
		 * la clef est l'id du champ de formulaire
		 * @access private
		 */
		var $colname_by_formfield = array();


		/**
		 * tableau contenant le id_type de la colonne dans le formulaire pour chaque champs
		 * la clef est l'id du champ de formulaire
		 * @access private
		 */
		var $coltype_by_formfield = array();

		/**
		 * Tableau contenant l'id du champ de formulaire en clef et l'id  du champ de la table en valeur
		 * @access private
		 */
		var $fieldid_by_colid = array();


		/**
		 * Contient le type SQL des types enregistr�s dans le formulaire
		 * le type 0 est celui des fonctions
		 * @access private
		 */
		var $coltype_cache = array(0 => 0);



		/**
		 * Contient "field_link_to_fill" en valeur et l'id du champ de formulaire en clef
		 * Correspond a la fonction : "utiliser le lien pour remplir le champ"
		 * @access private
		 */
		var $fill_by_formfield = array();



		/**
		 * Contient "link_table_field_id" en valeur et l'id du champ de formulaire en clef
		 * Le tableau contiens le champs de table qui doit �tre utilis� lors de l'affichage quand le champ de fomulaire contient une liaison
		 * @access private
		 */
		var $link_by_formfield = array();


		/**
		 * Contient les liens des tables de liaison
		 * La clef est l'id de la table de liaison
		 * La valeur est un tableau contenant toutes les valeurs qui doivent �tre ins�r�es dans la table de liaison
		 * @access private
		 */
		var $record_lnk_values = array();



		/**
		 * Contiens les champs soumis par le formulaire
		 * en clef, l'id du champ de formulaire
		 * en valeur, la valeur soumise par le formulaire apr�s la transformation des formats de date
		 * @see db_transfer::addField()
		 * @access private
		 */
		var $fields = array();



		/**
		 * @deprecated
		 */
		var $error = false;

		/**
		 * @deprecated
		 */
		var $onchange = array();



		function db_transfer($id_form,$id_table)
			{
			$this->db = & $GLOBALS['babDB'];

			$this->id_form = $id_form;
			$this->id_table = $id_table;


			// coltype
			$res = $this->db->db_query("SELECT id,sql_type FROM ".FORM_TABLES_FIELDS_TYPES."");
			while($arr = $this->db->db_fetch_array($res))
				{
				$this->coltype_cache[$arr['id']] = $arr['sql_type'];
				}

			$req = "SELECT
						f.id f_id,
						f.name formname,
						t.id t_id,
						t.name,
						t.id_type,
						t.link_table_field_id,
						f.field_copy_table_field,
						f.field_copy_on_change,
						t.field_function,
						f.field_link_to_fill,
						f.field_mailonchange
					FROM
						".FORM_FORMS_FIELDS." f,
						".FORM_TABLES_FIELDS." t,
						".FORM_FORMS." tmp
					WHERE
						f.id_table_field=t.id
					AND t.id_table='".$this->db->db_escape_string($id_table)."'
					AND tmp.id=f.id_form
					AND tmp.id_table='".$this->db->db_escape_string($id_table)."'
					AND tmp.id='".$this->db->db_escape_string($id_form)."'
					AND f.id_form='".$this->db->db_escape_string($id_form)."'
					AND t.field_function=''
					GROUP BY t.id,f.id
					";


			$res = $this->db->db_query($req);
			while($arr = $this->db->db_fetch_array($res))
				{
				$tmp_colname = form_col_name($arr['t_id'],$arr['name']);
				$this->fieldid_by_colid[$arr['f_id']] = $arr['t_id'];
				$this->colname_by_formfield[$arr['f_id']] = $tmp_colname;
				$this->coltype_by_formfield[$arr['f_id']] = $this->coltype($arr['id_type'],$arr['link_table_field_id']);
				$this->coltype_by_fieldname[$this->colname_by_formfield[$arr['f_id']]] = $this->coltype_by_formfield[$arr['f_id']];

				if (!empty($arr['field_link_to_fill']))
					$this->fill_by_formfield[$arr['f_id']] = $arr['field_link_to_fill'];
				if (!empty($arr['link_table_field_id']))
					$this->link_by_formfield[$arr['f_id']] = $arr['link_table_field_id'];
				}


			$req = "SELECT
						f.id f_id,
						f.field_copy_table_field
					FROM
						".FORM_FORMS_FIELDS." f
					WHERE
						f.id_form='".$this->db->db_escape_string($id_form)."' AND field_copy_table_field > 0";


			$res = $this->db->db_query($req);
			while($arr = $this->db->db_fetch_array($res))
				{
				$this->colcopy_by_formfield[$arr['f_id']] = $arr['field_copy_table_field'];
				}
			}

		function coltype($id_type, $link_table_field_id)
			{
			if (!empty($id_type))
				{
				return $this->coltype_cache[$id_type];
				}
			elseif (!empty($link_table_field_id))
				{
				list($id_type) = $this->db->db_fetch_array($this->db->db_query("SELECT id_type FROM ".FORM_TABLES_FIELDS." WHERE id='".$this->db->db_escape_string($link_table_field_id)."'"));
				return $this->coltype_cache[$id_type];
				}
			}

		function setQueryType($type,$id = '')
			{
			$this->query_type = $type;
			$this->id = $id;
			}

		function addField($col,$value)
			{
			if (isset($this->colname_by_formfield[$col]))
				{
				if (in_array($this->coltype_by_formfield[$col],array(10,11))) // date & datetime
					form_formatDate($value);
				}

			$this->fields[$col] = $value;
			}


		function addLnk($id_form_field, $id_lnk, $value)
			{
			global $babDB;

			// il faut verifier que la liaison est bien un champ de formulaire visible
			// avant d'enregister la valeur qui peut �tre vide

			$res = $babDB->db_query('SELECT id_print FROM '.FORM_FORMS_FIELDS.' WHERE id='.$babDB->quote($id_form_field));
			$arr = $babDB->db_fetch_assoc($res);

			if ($arr && 1 === (int) $arr['id_print'])
				{
				$this->record_lnk_values[$id_lnk] = $value;
				}
			}

		function record_lnk()
			{
			if (empty($this->record_lnk_values)) {
				return;
				}

			foreach($this->record_lnk_values as $id_lnk => $value)
				{

				$this->emptyLnk($id_lnk);

				if (is_array($value)) {
					foreach($value as $v)
						{
						$this->db->db_query("INSERT INTO ".FORM_TABLES_LNK_VALUES." (id_lnk,row1,row2) VALUES ('".$this->db->db_escape_string($id_lnk)."','".$this->db->db_escape_string($this->id)."','".$this->db->db_escape_string($v)."')");
						}
					}
				}
			}


		function emptyLnk($id_lnk)
			{
			if (!empty($this->id))
				$this->db->db_query("DELETE FROM ".FORM_TABLES_LNK_VALUES." WHERE id_lnk='".$this->db->db_escape_string($id_lnk)."' AND row1='".$this->db->db_escape_string($this->id)."'");
			}

		/**
		 * Correspond � la fonction "Utiliser le lien pour remplir le champ"
		 * La m�thode r�cupere la valeur a copier
		 */
		function setFieldsLink()
			{
			foreach($this->fill_by_formfield as $fcol => $field)
				{
				$res = $this->db->db_query("
					SELECT
						f2.id,
						f2.name,
						f2.field_function,
						f2.id_table
					FROM
						".FORM_TABLES_FIELDS." f1,
						".FORM_TABLES_FIELDS." f2
					WHERE
						f1.id='".$this->db->db_escape_string($field)."'
						AND f1.id_table=f2.id_table
				");

				$tmp = array_flip($this->link_by_formfield);


				while ($arr = $this->db->db_fetch_array($res)) {
					if ($arr['id'] == $field) {
						$field_to_get = form_col_name($arr['id'],$arr['name'],$arr['field_function']);
					}
				}


				if (isset($field_to_get))
					{
					$this->db->db_data_seek($res,0);
					$val = NULL;

					// recup�rer la clef
					while ($arr = $this->db->db_fetch_array($res))
						{

						if (isset($tmp[$arr['id']]))
							{
							$keyval = $this->fields[$tmp[$arr['id']]];
							$req = "SELECT ".$field_to_get." FROM ".form_tbl_name($arr['id_table'])." WHERE ".form_col_name($arr['id'],$arr['name'])." = '".$this->db->db_escape_string($keyval)."'";

							$res = $this->db->db_query($req);

							if ($found = $this->db->db_fetch_array($res)) {
								$val = (string) $found[0];
								}
							}
						}

					// modifier la valeur uniquemeent si une correspondance a �t� trouv�e
					if (NULL !== $val) {
							$this->fields[$fcol] = $val;
						}
					}
				}
			}


		function setCols()
			{

			foreach($this->fields as $col => $value)
				{
				if (isset($this->colname_by_formfield[$col]))
					{
					$this->addCol($this->colname_by_formfield[$col], $value);
					}

				if (isset($this->colcopy_by_formfield[$col]))
					{
					$this->addCol_copy($col, $value);
					}
				}
			}


		function addCol($name, $value)
			{
			if (is_array($value))
				$value = implode(',',$value);

			if (!in_array($name,$this->col))
				{
				$this->col[] = $name;
				$this->value[] = "'".$this->db->db_escape_string($value)."'";
				}
			}


		/**
		 * Ajouter des colonnes a copier dans une autre table
		 */
		function addCol_copy($col, $value)
			{
			if (is_array($value))
				$value = implode(',',$value);
			$this->col_copy[] = $col;
			$this->value_copy[] = "'".$this->db->db_escape_string($value)."'";
			}


		function addFileField($col,$ofile, $source)
			{
			if (isset($this->colname_by_formfield[$col]))
				{

				if( !get_cfg_var('safe_mode')) {
					set_time_limit(0);
				}

				$this->col[] = $this->colname_by_formfield[$col];

				$file = '';

				if ($fd = fopen($source, "rb")) {
					$i = 0;
					while ($i <= $ofile->size) {
						$file .= fread($fd, 4194304);
						$i += 4194304;
					}
					fclose($fd);
				}

				$file = "X'" . bin2hex($file)."'";

				$this->value[] = $file;

				if (!isset($this->blob_data))
					$this->blob_data = array();

				$this->blob_data[] = array(
					'fieldid' 	=> $this->fieldid_by_colid[$col],
					'name'		=> $ofile->filename,
					'size'		=> $ofile->size,
					'type'		=> $ofile->mime
					);

				return true;
				}
			}

		function query_copy()
			{

			if (count($this->col_copy) == 0) {
				return;
			}

			$req = "SELECT id_table_copy FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->id_form)."'";
			list($id_table_copy) = $this->db->db_fetch_array($this->db->db_query($req));

			$res = $this->db->db_query("

			SELECT
				f.id,
				t.id t_id,t.name,
				ft.field_function
			FROM
				".FORM_FORMS_FIELDS." f
				LEFT JOIN ".FORM_TABLES_FIELDS." ft ON ft.id=f.id_table_field ,
				".FORM_TABLES_FIELDS." t
			WHERE
				f.id IN(".$this->db->quote($this->col_copy).")
				AND t.id=f.field_copy_table_field
			");

			$tmp  =array();
			$field_function = array();
			while ($arr = $this->db->db_fetch_array($res))
				{
				$tmp[$arr['id']] = form_col_name($arr['t_id'],$arr['name']);
				if (!empty($arr['field_function']))
					{
					$field_function[$arr['id']] = $arr['field_function'];
					}
				}

			foreach($this->col_copy as $index => $val)
				{
				$this->col_copy[$index] = $tmp[$val];
				if (isset($field_function[$val]))
					{
					$row = array('form_id' => $this->id);
					$this->value_copy[$index] = $this->db->db_escape_string(form_field_function($field_function[$val],$row));
					}
				}

			if ($id_table_copy > 0 && count($this->col_copy) > 0)
				{
				$query = "INSERT INTO ".form_tbl_name($id_table_copy)."\n
				(".implode(',',$this->col_copy).", form_create, form_lastupdate, form_lastupdate_user, form_create_user)\n
				VALUES \n
					(".implode(",",$this->value_copy).", NOW(), NOW(), '".$this->db->db_escape_string($GLOBALS['BAB_SESS_USERID'])."', '".$GLOBALS['BAB_SESS_USERID']."')";

				$this->db->db_query($query);

				bab_debug($query);

				return $this->db->db_insert_id();
				}
			else
				return false;
			}

		function test_change($dbcol)
			{
			if ($this->query_type == 'INSERT' )
				{
				return true;
				}
			else
				{
				$res = $this->db->db_query("

				SELECT
					t.id,
					t.name,
					f.field_copy_on_change,
					f.field_mailonchange
				FROM
					".FORM_FORMS_FIELDS." f
					LEFT JOIN ".FORM_TABLES_FIELDS." t ON f.id_table_field=t.id
					LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." ty ON ty.id = t.id_type
				WHERE
					id_form='".$this->db->db_escape_string($this->id_form)."'
					AND (ty.sql_type NOT IN('23','24','25','26') OR ty.sql_type IS NULL)
				");

				$indexes = array_flip($this->col);

				$cols = array();
				$query = "SELECT COUNT(*) FROM ".form_tbl_name($this->id_table)." WHERE ";

				while($arr = $this->db->db_fetch_assoc($res))
					{

					if ( $arr[$dbcol] == 1 && !$arr['id'] )
						{
						return true;
						}

					$name = form_col_name($arr['id'],$arr['name']);

					if ( $arr[$dbcol] == 1 && isset($indexes[$name]) ) {
						$query .= $name."=".$this->value[$indexes[$name]]." AND \n";
						}
					}

				$query .= "form_id='".$this->db->db_escape_string($this->id)."'";

				list($n) = $this->db->db_fetch_array($this->db->db_query($query));

				if ($n == 0) {
					return true;
					}
				}

			return false;
			}

		function prepare_query()
			{
			$this->setFieldsLink();
			$this->setCols();
			}



		/**
		 * Executer la requete
		 * @return boolean
		 */
		function query()
			{
			if (count($this->col) == 0 || !is_array($this->value)) {
				return false;
			}

			$table = $this->db->db_fetch_array($this->db->db_query("SELECT name, approbation FROM ".FORM_TABLES." WHERE id='".$this->db->db_escape_string($this->id_table)."'"));

			$this->approbation = &$table['approbation'];


			$bcopy = $this->test_change('field_copy_on_change');

			switch ($this->query_type)
				{
				case 'UPDATE':
					$query = "UPDATE ".form_tbl_name($this->id_table)." SET \n";
					foreach ($this->col as $k => $col)
						$query .= "\t".$col."=".$this->value[$k].", \n";
					$query .= "form_lastupdate=NOW(), form_lastupdate_user=".$this->db->quote($GLOBALS['BAB_SESS_USERID'])."\n ";
					$query .= "WHERE form_id=".$this->db->quote($this->id)."";
					break;

				case 'INSERT':
					$query = "INSERT INTO ".form_tbl_name($this->id_table)."\n (".implode(",\n",$this->col).",\n form_create, form_lastupdate,\n form_lastupdate_user, form_create_user)\n VALUES\n (".implode(",\n",$this->value).",\n NOW(), NOW(),\n '".$GLOBALS['BAB_SESS_USERID']."', '".$GLOBALS['BAB_SESS_USERID']."')";
					break;
				}

			$arr = $this->db->db_fetch_array($this->db->db_query("SHOW VARIABLES LIKE 'max_allowed_packet'"));
			$max_allowed_packet = $arr['Value'];

			if ($max_allowed_packet <= strlen($query)) {
				$max = $max_allowed_packet/1024/1024;
				$GLOBALS['babBody']->msgerror = sprintf(form_translate("The total submited data is too large, you are not allowed to submit more than %01.2f Mo"), $max);
				if (bab_isUserAdministrator()) {
					$GLOBALS['babBody']->msgerror .= ' '.form_translate("(Administrator : the limit can be modified in my.ini, add max_allowed_packet=100M in the [mysqld] section)");
				}
				return false;
			}


			$res = $this->db->db_query($query);

			if ('INSERT' == $this->query_type) {
				$this->id = $this->db->db_insert_id($res);
			}


			form_currentDbRow($this->id);

			$this->record_lnk();

			if (isset($this->blob_data))
				{
				$existing_table_field = array();
				$res = $this->db->db_query("SELECT id_table_field FROM ".FORM_EXTERNAL_BLOB_DATA." WHERE form_row='".$this->db->db_escape_string($this->id)."'");
				while(list($id_table_field) = $this->db->db_fetch_array($res))
					{
					$existing_table_field[] = $id_table_field;
					}

				foreach($this->blob_data as $arr)
					{
					if (in_array($arr['fieldid'],$existing_table_field))
						{
						$this->db->db_query("UPDATE ".FORM_EXTERNAL_BLOB_DATA." SET file_size='".$this->db->db_escape_string($arr['size'])."', file_type='".$this->db->db_escape_string($arr['type'])."', file_name='".$this->db->db_escape_string($arr['name'])."' WHERE form_row='".$this->db->db_escape_string($this->id)."' AND id_table_field = '".$this->db->db_escape_string($arr['fieldid'])."'");
						}
					else
						{
						$this->db->db_query("INSERT INTO ".FORM_EXTERNAL_BLOB_DATA." (id_table_field, form_row, file_size, file_type, file_name) VALUES ('".$this->db->db_escape_string($arr['fieldid'])."', '".$this->db->db_escape_string($this->id)."', '".$this->db->db_escape_string($arr['size'])."', '".$this->db->db_escape_string($arr['type'])."', '".$this->db->db_escape_string($arr['name'])."')");
						}
					}
				} else {
					bab_debug($query);
				}

			if ($bcopy)
				{
				$this->query_copy();
				}


			return true;
			}
		}

	class validation
		{
		var $fields = array();
		function validation($id_form,$insert = false)
			{
			$this->insert = $insert;
			$this->db = $GLOBALS['babDB'];
			$res = $this->db->db_query("

					SELECT
						f.id,
						f.name,
						IF(f.id_type='0',tt2.sql_type,tt1.sql_type) sql_type,
						f.validations,
						f.notempty,
						f.id_table_field,
						t.name table_field_name,
						t.id_table
					FROM
						".FORM_FORMS_FIELDS." f
					LEFT JOIN ".FORM_TABLES_FIELDS." t ON t.id = f.id_table_field
					LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." tt1 ON tt1.id=f.id_type
					LEFT JOIN ".FORM_TABLES_FIELDS_TYPES." tt2 ON tt2.id=t.id_type
					WHERE
						f.id_form='".$this->db->db_escape_string($id_form)."'
					");

			while ($arr = $this->db->db_fetch_array($res))
				{
				$this->fields[$arr['id']]['name'] = $arr['name'];
				$this->fields[$arr['id']]['validations'] = $arr['validations'];
				$this->fields[$arr['id']]['id_table_field'] = $arr['id_table_field'];
				$this->fields[$arr['id']]['table_field_name'] = $arr['table_field_name'];
				$this->fields[$arr['id']]['id_table'] = $arr['id_table'];
				$this->fields[$arr['id']]['sql_type'] = isset($arr['sql_type']) ? $arr['sql_type'] : '';
				$this->fields[$arr['id']]['unique_key'] = isset($arr['unique_key']) && $arr['unique_key'] == 'Y' ? true : false;
				$this->fields[$arr['id']]['notempty'] = isset($arr['notempty']) && $arr['notempty'] == 'Y' ? true : false;
				}
			}

		function phpValidSystem($id_field,$value, $file = false)
			{

			$fileloaded = false;
			if ($this->fields[$id_field]['sql_type'] > 22 && !$this->insert)
				{
				// test if a blob is allready loaded
				$id_table = $this->fields[$id_field]['id_table'];
				$id_table_field = $this->fields[$id_field]['id_table_field'];
				$table_field_name = $this->fields[$id_field]['table_field_name'];

				if ($id_table && $id_table_field)
					{
					$res = $this->db->db_query("
						SELECT LENGTH(".form_col_name($id_table_field,$table_field_name).") bloblength
						FROM ".form_tbl_name($id_table)." WHERE form_id=".$this->db->quote($_POST['form_row'])."
					");

					if ($arr = $this->db->db_fetch_assoc($res))
						{
							if ($arr['bloblength'] > 0) {
								$fileloaded = true;
							}
						}

					}

				}

			if ($this->fields[$id_field]['notempty'] == 'Y' && (!isset($value) || '' === $value) && !$fileloaded)
				{
				$GLOBALS['babBody']->msgerror = $this->fields[$id_field]['name'].' : '.form_translate('this field must be filled');
				return false;
				}



			if (!empty($this->fields[$id_field]['validations']) && !empty($value))
				{
				$res = $this->db->db_query("
								SELECT
									c.alert,
									c.parameter,
									c.id_validation,
									v.unique_key,
									v.php
								FROM
									".FORM_FORMS_FIELDS_CTRL." c,
									".FORM_FORMS_FIELDS_VALIDATION." v
								WHERE
									c.id_field='".$this->db->db_escape_string($id_field)."'
									AND c.id_validation=v.id
								");

				while ($arr = $this->db->db_fetch_array($res))
					{

					if (!empty($this->fields[$id_field]['id_table_field']) && $arr['unique_key'] == 'Y')
						{
						if ($file)
							{
							$file_arr = unserialize($value);


							$req = "SELECT COUNT(*) FROM ".FORM_EXTERNAL_BLOB_DATA." WHERE  id_table_field =  '".$this->db->db_escape_string($this->fields[$id_field]['id_table_field'])."' AND file_name='".$this->db->db_escape_string($file_arr['name'])."'";

							if (!$this->insert)
								{
								$req .= " AND form_row<>'".$this->db->db_escape_string($_POST['form_row'])."'";
								}
							}
						else
							{
							$req = "SELECT COUNT(*) FROM ".form_tbl_name($this->fields[$id_field]['id_table'])." WHERE  ".form_col_name($this->fields[$id_field]['id_table_field'],$this->fields[$id_field]['table_field_name'])." = '".$this->db->db_escape_string($value)."'";

							if (!$this->insert)
								{
								$req .= " AND form_id<>'".$this->db->db_escape_string($_POST['form_row'])."'";
								}
							}

						list($count) = $this->db->db_fetch_array($this->db->db_query($req));

						if ($count > 0)
							{
							$GLOBALS['babBody']->msgerror = form_msgError($arr['alert'],$this->fields[$id_field]['name']);
							return false;
							}
						}




					if (false === $this->phpValidationEval($arr['php'],$value, $arr['parameter'], $file))
						{
						$GLOBALS['babBody']->msgerror = form_msgError($arr['alert'],$this->fields[$id_field]['name']);
						return false;
						}

					}
				}

			return true;
			}


		function phpValidationEval($php,$value,$parameter, $no_escape)
			{
			if (empty($value)) return true;

			if (!$no_escape)
				{
				$value = addslashes($value);
				}

			$php = sprintf($php, $value, $parameter);
			bab_debug($php);
			return eval($php);
			}
		}

	// transaction

	class transaction
		{
		function transaction()
			{
			$this->db = & $GLOBALS['babDB'];
			}

		function init($id_app, $id_form, $id_card_form, $id_table)
			{

			$this->id_form = $id_form;

			// api

			$res = $this->db->db_query("SELECT * FROM ".FORM_TRANS_API." WHERE id='".$this->db->db_escape_string(form_getConfigValue('transaction_api'))."'");
			$this->api = $this->db->db_fetch_array($res);

			$res = $this->db->db_query("SELECT f.id, t.name FROM ".FORM_FORMS_FIELDS." f, ".FORM_TRANS_FIELDS." t WHERE t.id_api = '".$this->api['id']."' AND f.id_form='".$this->db->db_escape_string($id_form)."' AND f.field_transaction = t.id");

			$this->transaction_fields = array();

			while($arr = $this->db->db_fetch_array($res))
				{
				$this->transaction_fields[$arr['id']] = array('api' => $arr['name'], 'value' => false );
				}


			// card

			$sc_card = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($id_card_form)."'"));

			require_once( $GLOBALS['babInstallPath']."addons/forms/card.php" );

			$form_card = new form_card($id_app);
			$card = & $form_card->get($sc_card['id_table']);

			if (false === $card)
				{
				$GLOBALS['babBody']->msgerror = form_translate("Error : can't retrieve shopping cart data");
				return false;
				}

			// colonnes
			$cols = form_getTableCols($sc_card['id_table']);
			$field = form_getTableFields($sc_card['id_table']);

			// card
			$req = "SELECT * FROM ".form_tbl_name($sc_card['id_table'])." WHERE form_id IN(".$this->db->quote(array_keys($card)).")";
			$resrow = $this->db->db_query($req);

			$total_sub = 0;
			$this->total_qte = 0;

			$summary = array();

			while($arr = $this->db->db_fetch_array($resrow))
				{
				$tmp = array();
				foreach ($arr as $k => $v)
					{
					if (eregi("([0-9]{4})_.*",$k))
						{
						$tmp[] = $field[$k].':'.str_replace(':',' ',$v);
						}
					}


				$arr['qte'] = $card[$arr['form_id']];
				$this->total_qte += $arr['qte'];

				$card_product_cols = array();
				foreach ( $cols as $id => $name )
					{
					$card_product_cols[$id] = $arr[$name];
					}

				$arr['total'] = form_formula_sc( $sc_card['sc_formula'], $card_product_cols, $arr['qte'] );

				if (false === $arr['total'])
					{
					$GLOBALS['babBody']->msgerror = form_translate('Error : shopping cart formula');
					return false;
					}

				$total_sub += $arr['total'];

				$tmp[] = form_translate('Quantity').':'.$arr['qte'];
				$tmp[] = form_translate('Total').':'.$arr['total'];
				$summary[] = str_replace(';',' ',implode(",\t",$tmp));
				}

			$this->summary = implode(";\n",$summary);

			$this->total = form_formula_trans( $sc_card['trans_formula'], $total_sub,$this->total_qte );
			if ( false === $this->total || $this->total === 0 )
				{
				$GLOBALS['babBody']->msgerror = form_translate('Error : total formula');
				return false;
				}

			return true;
			}

		function addField($idx,$value)
			{
			if (isset($this->transaction_fields[$idx]))
				$this->transaction_fields[$idx]['value'] = $value;
			}

		function getURLData()
			{
			$this->data = array();

			$this->data[] = $this->api['login_varname'].'='.$this->api['login'];
			$this->data[] = $this->api['password_varname'].'='.$this->api['password'];
			$this->data[] = $this->api['tran_key_varname'].'='.$this->api['tran_key'];
			$this->data[] = $this->api['amount_varname'].'='.$this->total;

			foreach( $this->transaction_fields as $field )
				{
				$this->data[] = $field['api'].'='.$field['value'];
				}

			if ( !empty($this->api['additional_parameters']))
				{
				$this->data = array_merge($this->data,explode('&',$this->api['additional_parameters']));
				}

			}

		function submit( &$obj_db, &$obj_mail)
			{
			$ch = curl_init();

			$this->getURLData();

			curl_setopt( $ch, CURLOPT_URL, $this->api['url'] );
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER , 1 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, implode('&',$this->data) );

			$response = curl_exec( $ch );
			curl_close( $ch );

			$authnet_array = explode( ',', $response );
			array_unshift($authnet_array,'');
			unset($authnet_array[0]);

			$form = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_FORMS." WHERE id='".$this->db->db_escape_string($this->id_form)."'"));

			$fieldname_by_id = array();
			$res = $this->db->db_query("SELECT id,name FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$this->db->db_escape_string($form['id_table'])."'");
			while ($arr = $this->db->db_fetch_array($res))
				{
				$fieldname_by_id[$arr['id']] = form_col_name($arr['id'], $arr['name']);
				}


			$obj_db->addCol($fieldname_by_id[$form['card_field_total']], $this->total);
			$obj_db->addCol($fieldname_by_id[$form['card_field_quantities']], $this->total_qte);
			$obj_db->addCol($fieldname_by_id[$form['card_field_products_summary']], $this->summary);
			$obj_db->addCol($fieldname_by_id[$form['card_field_transaction_id']], $authnet_array[$this->api['pos_transaction_id']] );
			$obj_db->addCol($fieldname_by_id[$form['card_field_status']], $authnet_array[$this->api['pos_response_code']] );

			if (false !== $obj_mail)
				{
				$obj_mail->addfield($this->api['name'].' '.form_translate('response'), '', $authnet_array[$this->api['pos_response_text']]);
				$obj_mail->addfield($this->api['name'].' '.form_translate('Transaction ID'), '', $authnet_array[$this->api['pos_transaction_id']]);
				}

			if( $authnet_array[1] == 1 )
				{
				return true;
				}
			else
				{
				$GLOBALS['babBody']->msgerror = bab_toHtml(form_translate($authnet_array[4]));
				return false;
				}
			}
		}

	// get email adresses

	function form_getArrayMailFrom($table, $col)
		{
		$out = array();
		$db = & $GLOBALS['babDB'];
		$res = $db->db_query("SELECT ".$db->db_escape_string($col)." FROM ".$db->db_escape_string($table)."");
		while (list($mail) = $db->db_fetch_array($res))
			{
			if (ereg ("^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$",$mail))
				$out[] = $mail;
			}
		return $out;
		}


	function form_getArrayMailGroup($id_group)
		{
			$return = array();
			$arr = bab_getGroupsMembers($id_group);
			if (false !== $arr)
				foreach($arr as $i => $u)
					{
					if (preg_match("/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$/",$u['email']))
						$return[] = $u['email'];
					}
			return $return;
		}


	function form_approb_row($id_table,$form_row,$delete,&$mailmsg)
		{
		$db = & $GLOBALS['babDB'];

		$res = $db->db_query("
					SELECT
						s.id_schi idschi,
						s.id_user,
						d.name,
						d.description,
						w.id_approb_form,
						w.approb_mail_subject,
						w.valid_mail_subject,
						w.refused_mail_subject,
						w.approb_mail_applicant,
						w.approb_mail,
						w.approb_mail_group
					FROM
						".FORM_APPROB_SCHI." s,
						".FORM_TABLES." d,
						".FORM_APP_STEPS." w
					WHERE
						s.id_line='".$db->db_escape_string($form_row)."'
						AND s.id_table='".$db->db_escape_string($id_table)."'
						AND d.id='".$db->db_escape_string($id_table)."'
						AND w.id=s.id_step
					");

		if ($row = $db->db_fetch_array($res))
			{
			require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
			$arr = bab_WFGetWaitingApproversInstance($row['idschi'], false);
			if (in_array($GLOBALS['BAB_SESS_USERID'],$arr))
				{
				$table_description = !empty($row['description']) ? $row['description'] : $row['name'];

				if ($_POST['form_approb'] == 1)
					{
					$result = bab_WFUpdateInstance($row['idschi'], $GLOBALS['BAB_SESS_USERID'], true);
					}
				elseif ($_POST['form_approb'] == 2)
					{
					$result = bab_WFUpdateInstance($row['idschi'], $GLOBALS['BAB_SESS_USERID'], false);
					}

				if (isset($result) && $result === 0 )
					{
					if ($delete)
						{
						$db->db_query("DELETE FROM ".form_tbl_name($id_table)." WHERE form_id='".$db->db_escape_string($form_row)."'");
						}
					else
						{
						$db->db_query("UPDATE ".form_tbl_name($id_table)." SET form_approb='2' WHERE form_id='".$db->db_escape_string($form_row)."'");
						}

					$mailmsg->mailSubject(form_translate('FORM').' : '.form_translate('Approbation').' : '.$row['name'], $row['refused_mail_subject']);
					$mailmsg->message = form_translate('Your submission has been refused').' : '.$table_description;

					}
				elseif (isset($result) && $result === 1 )
					{
					$db->db_query("UPDATE ".form_tbl_name($id_table)." SET form_approb='1' WHERE form_id='".$db->db_escape_string($form_row)."'");
					$mailmsg->mailSubject(form_translate('FORM').' : '.form_translate('Approbation').' : '.$row['name'], $row['valid_mail_subject']);
					$mailmsg->message = form_translate('Your submission has been accepted').' : '.$table_description;
					}
				else
					{
					// instance non-termin�e, envoyer le mail aux suivants

					$arr = bab_WFGetWaitingApproversInstance($row['idschi'], true);

					$arrmail = array();
					foreach ($arr as $id_user)
						{
						$arrmail[] = bab_getUserEmail($id_user);
						}

					$mailmsg->mailDestArray($arrmail, 'mailTo');
					$mailmsg->message = form_translate('Waiting approbation').' : '.$table_description;
					$url = $GLOBALS['babUrlScript'].'?tg=addon/forms/form&idx=7&id_form='.$row['id_approb_form'].'&form_row='.$form_row.'&popup=1';
					$mailmsg->addlink(form_translate('Approbation form'), '', $GLOBALS['babUrlScript'].'?tg=login&cmd=detect&referer='.urlencode($url));
					$url = $GLOBALS['babUrlScript'].'?tg=approb';
					$mailmsg->addlink(form_translate('Approbation list'), '', $GLOBALS['babUrlScript'].'?tg=login&cmd=detect&referer='.urlencode($url));

					$mailmsg->mailSubject(form_translate('FORM').' : '.form_translate('Approbation').' : '.$row['name'],  $row['approb_mail_subject']);
					$mailmsg->mailmsgsend();


					}

				if (isset($result) && ($result == 0 || $result == 1 ))
					{
					// ligne accept�e ou refus�e, envoyer un mail � la personne qui a cr�er l'instance

					bab_WFDeleteInstance($row['idschi']);
					$db->db_query("DELETE FROM ".FORM_APPROB_SCHI." WHERE id_schi='".$db->db_escape_string($row['idschi'])."'");

					$arrmail = array();
					if (1 == $row['approb_mail_applicant'])
						{
						$arrmail[] = bab_getUserEmail($row['id_user']);
						}

					if (!empty($row['approb_mail']))
						{
						$tmparr = explode(MAIL_SEPARATOR,$row['approb_mail']);
						foreach($tmparr as $k => $v)
							{
							if (ereg("^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$",$v))
								{
								$arrmail[] = $v;
								}
							}
						}

					if (!empty($row['approb_mail_group']))
						{
						$arrmail = array_merge( $arrmail, form_getArrayMailGroup($row['approb_mail_group']) );
						}

					if (count($arrmail) > 0)
						{
						$mailmsg->mailDestArray($arrmail,'mailTo');
						$mailmsg->mailmsgsend();
						}
					return false;
					}

				return isset($row['idschi']) ? $row['idschi'] : 0;
				}
			else
				{
				trigger_error('current user is not a waiting user for the instance, form_row='.$form_row.' id_table='.$id_table);
				}
			}
		else
			{
			trigger_error('no instance for approbation, form_row='.$form_row.' id_table='.$id_table);
			}
		return false;
		}



	function form_directory_files_errors()
		{
		if (isset($_FILES))
			{
			foreach($_FILES as $name => $arr)
				{
				if ($arr['error'])
					{
					$GLOBALS['babBody']->msgerror = sprintf(form_translate('The file %s is too large'), $arr['name']);
					return true;
					}
				}
			}
		return false;
		}


	function form_directory_files($id_table, $id)
		{
		if (isset($_FILES) && !empty($_FILES))
			{
			$row_directory = $GLOBALS['babAddonUpload'];
			if (!is_dir($row_directory)) bab_mkdir($row_directory);
			$row_directory .= 'directories/';
			if (!is_dir($row_directory)) bab_mkdir($row_directory);
			$row_directory .= form_tbl_name($id_table).'/';
			if (!is_dir($row_directory)) bab_mkdir($row_directory);
			$row_directory .= $id.'/';
			if (!is_dir($row_directory)) bab_mkdir($row_directory);

			foreach($_FILES as $name => $arr)
				{
				if (0 === strpos($name,'dirfile'))
					{
					move_uploaded_file($arr['tmp_name'],$row_directory.$arr['name']);
					}
				}
			}

		if (isset($_POST['dirfile_delete']))
			{
			foreach($_POST['dirfile_delete'] as $filename)
				{
				unlink($GLOBALS['babAddonUpload'].'directories/'.form_tbl_name($id_table).'/'.$id.'/'.$filename);
				}
			}
		}






	function form_captchaVerification($id_form) {

		global $babDB;

		$res = $babDB->db_query('SELECT id FROM '.FORM_FORMS_FIELDS.' WHERE id_print=\'12\' AND id_form='.$babDB->quote($id_form));
		if ($arr = $babDB->db_fetch_assoc($res)) {
			$oCaptcha = @bab_functionality::get('Captcha');
			if(false !== $oCaptcha) {
					$sCaptchaSecurityCode = bab_pp('sCaptchaSecurityCode', '');
					if(!$oCaptcha->securityCodeValid($sCaptchaSecurityCode)) {
					return false;
				}
			}
		}

		return true;
	}









	// main

	$arr = $db->db_fetch_array($db->db_query("SELECT f.*, t1.id_table id_table_mail, t1.name name_table_field_mail, t2.id_table id_table_bcc, t2.name name_table_field_bcc FROM ".FORM_FORMS." f LEFT JOIN ".FORM_TABLES_FIELDS." t1 ON t1.id=f.id_table_field_mail LEFT JOIN ".FORM_TABLES_FIELDS." t2 ON t2.id=f.id_table_field_bcc WHERE f.id='".$db->db_escape_string($_POST['record_id_form'])."'"));

	$mail = trim($arr['mail']) != '' ? explode(MAIL_SEPARATOR,$arr['mail']) : array();
	$bcc = trim($arr['bcc']) != '' ? explode(MAIL_SEPARATOR,$arr['bcc']) : array();

	if ($arr['mail_user'] == 'Y' && !empty($GLOBALS['BAB_SESS_EMAIL']))
		$mail[] = $GLOBALS['BAB_SESS_EMAIL'];

	if (!empty($arr['id_table_field_mail']))
		{
		$table = form_tbl_name($arr['id_table_mail']);
		$col = form_col_name($arr['id_table_field_mail'],$arr['name_table_field_mail']);
		$mail = array_merge($mail, form_getArrayMailFrom($table, $col));
		}

	if (!empty($arr['id_group_mail']))
		{
		$mail = array_merge($mail, form_getArrayMailGroup($arr['id_group_mail']));
		}

	if (!empty($arr['id_table_field_bcc']))
		{
		$table = form_tbl_name($arr['id_table_bcc']);
		$col = form_col_name($arr['id_table_field_bcc'],$arr['name_table_field_bcc']);
		$bcc = array_merge($bcc, form_getArrayMailFrom($table, $col));
		}

	if (!empty($arr['id_group_bcc']))
		{
		$bcc = array_merge($bcc, form_getArrayMailGroup($arr['id_group_bcc']));
		}


	$view = new view($arr['id']);
	$mailmsg = new mailmsg($arr['mail_ignore_empty_values'], $view);


	//

	if (!form_captchaVerification($_POST['record_id_form'])) {
		$GLOBALS['babBody']->addError(form_translate('The captcha value is incorrect'));
		return false;
	}




	$insert = false;

	if (!empty($_POST['id_table']))
		{
		$db_transfer = new db_transfer($_POST['record_id_form'], $_POST['id_table']);

		if (!bab_isAccessValid(FORM_TABLES_CHANGE_GROUPS, $_POST['id_table'])) {
			$GLOBALS['babBody']->msgerror = form_translate('Access denied, modification on the table is not permitted');
			return false;
		}

		if (isset($_POST['form_row']) && is_numeric($_POST['form_row']))
			{
			$db_transfer->setQueryType('UPDATE',$_POST['form_row']);
			$insert = false;
			}
		else
			{
			$db_transfer->setQueryType('INSERT');
			$insert = true;
			}

		if ($arr['id_type'] == 4)
			{
			$transaction = new transaction();
			if (!$transaction->init($_POST['id_app'],$arr['id'], $arr['id_form_card'], $arr['id_table']))
				{
				return false;
				}
			}
		}

	$last_field_insert = false;
	$prefix = 'form_field_';
	$validation = new validation($_POST['record_id_form'],$insert);
	$send = true;

	// temporary files to delete after record
	$tmpfiles = array();


	$res = $db->db_query("
				SELECT
					f.id,
					f.name,
					f.description,
					f.mail,
					IFNULL(t.field_function,'') function,
					f.field_usemail,
					f.id_print,
					f.id_lnk,
					f.form_approb_refused,
					f.form_approb_accepted,
					l.lnk_type,
					f.id_form_element
				FROM
					".FORM_FORMS_FIELDS." f
				LEFT JOIN
					".FORM_TABLES_FIELDS." t
					ON t.id=f.id_table_field
				LEFT JOIN
					".FORM_TABLES_LNK." l
					ON l.id=f.id_lnk
				WHERE
					f.id_form='".$db->db_escape_string($arr['id'])."'
				ORDER BY f.ordering
				");


	while($f = $db->db_fetch_assoc($res))
		{
		$idx = &$f['id'];
		
		
		if (!empty($_POST[$prefix.$f['id']]) && 17 === (int) $f['id_form_element']) // proposal
		{
			if (!empty($_POST[$prefix.$f['id'].'_specify']))
			{
				if (is_array($_POST[$prefix.$f['id']]))
				{
					$_POST[$prefix.$f['id']][count($_POST[$prefix.$f['id']]) - 1] .= ' : '.$_POST[$prefix.$f['id'].'_specify'];
				} else {
					$_POST[$prefix.$f['id']] .= ' : '.$_POST[$prefix.$f['id'].'_specify'];
				}
			}
		}
		

		if (
				isset($_POST[$prefix.$f['id']]) ||
				in_array($f['id_form_element'], array(6,4)) // checkbox | select multiple
			)
			{


			if (isset($_POST[$prefix.$f['id']])) {
				$value = $_POST[$prefix.$f['id']];

				$value = form_textInput($value);

			} else {
				$value = '';
			}

			if (!$validation->phpValidSystem($idx,$value))
				{
				form_isError($idx, true);
				$send = false;
				break;
				}

			if (!empty($_POST['id_table']))
				{
				if (!empty($f['id_lnk']))
					{
					if ($f['lnk_type'] == 0)
						{
						$db_transfer->addLnk($f['id'], $f['id_lnk'], $value);
						}
					else
						{
						$GLOBALS['babBody']->msgerror = form_translate('the table cannot be used as a link for writing');
						}
					}
				else
					{
					if ($arr['id_type'] == 7)
						{
						if (!empty($f['form_approb_refused'])  && 2 == $_POST['form_approb']) $value = $f['form_approb_refused'];
						if (!empty($f['form_approb_accepted']) && 1 == $_POST['form_approb']) $value = $f['form_approb_accepted'];
						}
					$db_transfer->addField($idx,$value);
					}

				if ($arr['id_type'] == 4)
					{
					$transaction->addField($idx,$value);
					}
				}


			if ($f['mail'] == 'Y')
				{
				$mailmsg->addfield($idx, $value, $f['name'], $f['description'], $f['function'] );
				}
			if (!empty($f['field_usemail']))
				{


				if (false !== strpos($f['field_usemail'],'Id'))
					{
					$mailarr = explode(MAIL_SEPARATOR,$value);
					$f['field_usemail'] = substr($f['field_usemail'],0,-2);
					foreach($mailarr as $k => $v) $mailarr[$k] = bab_getUserEmail($v);
					}
				else
					{
					$mailarr = explode(MAIL_SEPARATOR,$view->getvalue($idx,$value));
					}

				foreach($mailarr as $k => $v)
					{
					$mailarr[$k] = trim($v);

					if (0 === preg_match("/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,6}$/",$mailarr[$k]))
						{
						unset($mailarr[$k]);
						}
					}



				$mailmsg->mailDestArray($mailarr,$f['field_usemail']);
				}
			}
		else
			{
			if ('index' == $f['function'])
				{
				$db_transfer->addField($idx,$_POST['form_row']);
				}

			// champs a valeur non-defini
			if (!empty($f['id_lnk']) && 1 == $f['id_print'])
				{
				$db_transfer->emptyLnk($f['id_lnk']);
				}
			}


		$value = @bab_fileHandler::upload($prefix.$f['id']);
		if ($value)
			{
			if (false !== $value->error)
				{
				if (isset($_FILES[$prefix.$f['id']]['error']) && 4 === $_FILES[$prefix.$f['id']]['error'])
					{ // UPLOAD_ERR_NO_FILE
						// effectuer une validation avec une valeur vide quand aucun fichier n'est charg�
						if (!$validation->phpValidSystem($idx,'', true)) {
							form_isError($idx, true);
							$send = false;
							break;
						} else {

							// si le formulaire valide avec un fichier vide, on autorise le formulaire mais on ignore la valeur du champ
							continue;
						}

					} else {
						// si c'est une erreur autre que pas de fichier
						$GLOBALS['babBody']->addError($value->error);
						form_isError($idx, true);
						$send = false;
						break;
					}
				}


			if (!$validation->phpValidSystem($idx,serialize($_FILES[$prefix.$f['id']]), true))
				{
				form_isError($idx, true);
				$send = false;
				break;
				}

			// create a temporary copy
			$source = $value->importTemporary();
			$tmpfiles[] = $source;


			if ($f['mail'] == 'Y') {
				$mailmsg->mailFileAttach($value, $source);
				}

			if (!empty($_POST['id_table']))
				{
				$db_transfer->addFileField($idx,$value, $source);
				}
			}
		}


	if ($send)
		{
		if ($arr['id_type'] == 4)
			{
			if (!$transaction->submit( $db_transfer, $mailmsg))
				return false;
			}

		if (!empty($_POST['id_table']))
			{
			//if (form_directory_files_errors())
			//	return false;

			$db_transfer->prepare_query();

			$mailonchange = $db_transfer->test_change('field_mailonchange');

			if (false === $db_transfer->query()) {
				return false;
			}

			form_directory_files($_POST['id_table'], $db_transfer->id);

			// notification des approbations
			if ($db_transfer->approbation)
				$GLOBALS['form_mailmsg'] = & $mailmsg;

			if ($arr['id_type'] == 7 && !empty($_REQUEST['form_row']))
				{
				form_approb_row($_POST['id_table'],$_REQUEST['form_row'],$arr['delete_refused'], $mailmsg);
				}
			}



		if ( empty($_POST['id_table']) || $mailonchange )
			{
			$mailmsg->mailSubject(form_translate('FORM').' : '.$arr['name'], $arr['mail_subject']);
			$mailmsg->mailDestArray($mail,'mailTo');
			$mailmsg->mailDestArray($bcc,'mailBcc');
			$mailmsg->mailmsgsend();
			}

		// remove temporary files
		foreach($tmpfiles as $source)
			{
			@unlink($source);
			}

		return true;
		}
	else
		return false;
	}
