<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once dirname(__FILE__).'/define.php';

function form_translate($str)
	{
	return  bab_translate($str,'forms');
	}

function form_tbl_name($id)
	{
	return 'form_tbl_'.sprintf("%04d",$id);
	}
	
function form_col_name($id,$name, $function = false)
{
	if (false !== $function)
	{
		if ('index' == $function)
			return 'form_id';
		
		if ('approb' == $function)
			return 'form_approb';
		
		
	}
	
	$allowed_chars = preg_split('//', 'abcdefghijklmnopqrstuvwxyz0123456789', -1, PREG_SPLIT_NO_EMPTY);
	$chars_in_name = preg_split('//', strtolower($name), -1, PREG_SPLIT_NO_EMPTY);
	$name = '';
	foreach ($chars_in_name as $chr)
		$name .= in_array($chr,$allowed_chars) ? $chr : '';
	return sprintf("%04d",$id).'_'.$name;
}
	

	
function form_getTableName($id)
	{
	$db = &$GLOBALS['babDB'];
	list($out) = $db->db_fetch_array($db->db_query("SELECT name FROM ".FORM_TABLES." WHERE id='".$db->db_escape_string($id)."'"));
	return $out;
	}

function form_getFormName($id)
	{
	$db = &$GLOBALS['babDB'];
	list($out) = $db->db_fetch_array($db->db_query("SELECT name FROM ".FORM_FORMS." WHERE id='".$db->db_escape_string($id)."'"));
	return $out;
	}


function form_cvs_value($str) {
	$str = str_replace('"','""',$str);
	$str = preg_replace('/(\r|\n)/', ' ', $str);
	return $str;
	}

function form_trim_quotes(&$str)
	{
	$str = trim($str,"'");
	$str = stripslashes($str);
	}

function enumToArray($str)
	{
	$str = substr(strrchr(trim($str),'('),1,-1);
	$arr = preg_split("/['],[']/", $str, -1, PREG_SPLIT_NO_EMPTY);
	array_walk($arr,'form_trim_quotes');
	return $arr;
	}



// allowed form elements, type, for a form field
function form_getInputTypeFormField($id)
	{
	$db = &$GLOBALS['babDB'];
	$req = "SELECT 
				IF(f.id_type='0', tt2.id_forms_elements, tt1.id_forms_elements) id_forms_elements,
				IF(f.id_type='0', tt2.str_options, tt1.str_options) str_options,
				IF(f.id_type='0', tt2.col_type, tt1.col_type) col_type
			FROM 
				".FORM_FORMS_FIELDS." f
			LEFT JOIN 
				".FORM_TABLES_FIELDS_TYPES." tt1
				ON tt1.id=f.id_type 
			LEFT JOIN 
				".FORM_TABLES_FIELDS." tf
				ON tf.id=f.id_table_field 
			LEFT JOIN 
				".FORM_TABLES_FIELDS_TYPES." tt2
				ON tt2.id=tf.id_type 
			WHERE 
				f.id='".$db->db_escape_string($id)."'
				";
	$arr = $db->db_fetch_assoc($db->db_query($req));

	if (isset($arr['str_options'])) {
		$arr['options'] = unserialize($arr['str_options']);
		unset($arr['str_options']);
	}

	return $arr;
	}


function form_getElementByType($id_type)
	{
	$db = &$GLOBALS['babDB'];
	$req = "SELECT id_forms_elements FROM ".FORM_TABLES_FIELDS_TYPES." WHERE id='".$db->db_escape_string($id_type)."'";
	list($tmp) = $db->db_fetch_array($db->db_query($req));
	if (!empty($tmp) && substr_count($tmp,',') > 0)
		{
		$arr = explode(',',$tmp);
		return $arr[0];
		}
	else
		return $tmp;
	}

function form_urlToHttps(&$url)
	{
	$url = str_replace($GLOBALS['babUrl'],form_getConfigValue('https_url'),$url);
	}



function form_getConfigValue($name)
	{
	$db = &$GLOBALS['babDB'];
	if (isset($GLOBALS['FORM_CONFIGVALUE'][$name]))
		{
		return $GLOBALS['FORM_CONFIGVALUE'][$name];
		}
	else
		{
		list($GLOBALS['FORM_CONFIGVALUE'][$name]) = $db->db_fetch_array($db->db_query("SELECT value FROM ".FORM_CONFIG." WHERE name='".$db->db_escape_string($name)."'"));
		return $GLOBALS['FORM_CONFIGVALUE'][$name];
		}
	
	}


function form_getDirValue($field_name, $id_user)
{
	if (empty($id_user))
		return '';

	if ('user' == $field_name)
		return bab_getUserName($id_user);

	$sheets = bab_getDirEntry($id_user, BAB_DIR_ENTRY_ID_USER);
	return $sheets[$field_name]['value'];
}


// g�re la transformation de se qui sort de la base.
function form_getTransformedValue($id_field, $form_row, $value, $format) {

	if (0 == $id_field)
		return $value;
	
	static $data_type = array();

	if (!isset($data_type[$id_field])) {
		include_once $GLOBALS['babInstallPath']."addons/forms/f_extend_types.php";
		$data_type[$id_field] = new form_transformValue($id_field);
		}
	
	return $data_type[$id_field]->getValue($form_row,  $value, $format);
}


function form_getMetaSelectValue($key)
{
	$db = &$GLOBALS['babDB'];

	

	if (!empty($key) && !is_numeric($key)) {
		if (!$GLOBALS['BAB_SESS_LOGGED']) {
			return '';
		}
		$user_db_dir = bab_getDirEntry();
		return !empty($user_db_dir[$key]) ? $user_db_dir[$key]['value'] : '';
	}
	elseif(!empty($key) && is_numeric($key)) {
		list($eval_code) = $db->db_fetch_array($db->db_query("SELECT eval_code FROM ".FORM_FORMS_FIELDS_INIT." WHERE id='".$db->db_escape_string($key)."'"));
		$tmp = eval($eval_code);
		if ('' !== (string) $tmp) {
			return $tmp;
		}
	}

	return false;
}


function form_getFilterValue($id, $filter_on_dir, $filter_on, $filter_type )
	{
	if (isset($_POST['filters']))
		{
		$filters = unserialize($_POST['filters']);
		return isset($filters[$id]) && $filters[$id] != '' ? $filters[$id] : false;
		}

	if (isset($GLOBALS['FORMS_filters']))
		{
		$filters = unserialize($GLOBALS['FORMS_filters']);
		}
	
	$out = array();
	
	if ($filter_on != '')
		{
		$value = $filter_on;
		}
	elseif (!empty($filter_on_dir))
		{
		$value = form_getMetaSelectValue($filter_on_dir);
		}

	if (isset($value))
		{
		if (substr($value,0,1) == '$')
			{
			$value = form_getMacroValue($value);
			}

		if ($value != '')
			{

			$filters[$id] = $value;
			$GLOBALS['FORMS_filters'] = serialize($filters);

			return $value;
			}
		}

	return false;
	}
	
function form_getInit($id_form)
	{

	$db = &$GLOBALS['babDB'];
	
	$res = $db->db_query("SELECT t.id, t.name, f.id_form_element, f.init_dir, f.init,f.id idfield, t.id_table FROM ".FORM_FORMS_FIELDS." f LEFT JOIN ".FORM_TABLES_FIELDS." t ON f.id_table_field = t.id WHERE f.id_form='".$db->db_escape_string($id_form)."' GROUP BY f.id");
	$table = array();
	$notable = array();
	while ($arr = $db->db_fetch_array($res))
		{
		if (isset($arr['init']) && $arr['init'] != '')
			{
			$table[form_col_name($arr['id'],$arr['name'])] = form_getMacroValue($arr['init']);
			$notable[$arr['idfield']] = form_getMacroValue($arr['init']);
			}
		elseif (!empty($arr['init_dir']))
			{
			if (is_numeric($arr['init_dir']))
				{
				list($eval_code) = $db->db_fetch_array($db->db_query("SELECT eval_code FROM ".FORM_FORMS_FIELDS_INIT." WHERE id='".$db->db_escape_string($arr['init_dir'])."'"));

				$value = eval($eval_code);

				$table[form_col_name($arr['id'],$arr['name'])] = $value;
				$notable[$arr['idfield']] = $value;
				}
			elseif ($GLOBALS['BAB_SESS_LOGGED']) {
			
				static $user_db_dir = NULL;
				if (NULL === $user_db_dir) {
					$user_db_dir = bab_getDirEntry();
				}
			
				$table[form_col_name($arr['id'],$arr['name'])] = !empty($user_db_dir[$arr['init_dir']]) ? $user_db_dir[$arr['init_dir']]['value'] : '';
				$notable[$arr['idfield']] = !empty($user_db_dir[$arr['init_dir']]) ? $user_db_dir[$arr['init_dir']]['value'] : '';
				}
			}
		}
	return array($table,$notable);
	}


function form_auto_filing($id_table)
	{
	$db = &$GLOBALS['babDB'];
	list($nb_days) = $db->db_fetch_array($db->db_query("SELECT filing_delay FROM ".FORM_TABLES." WHERE id='".$db->db_escape_string($id_table)."'"));
	if (!empty($nb_days) && is_numeric($nb_days))
		$db->db_query("UPDATE ".form_tbl_name($id_table)." SET form_filed='Y' WHERE form_filed='N' AND DATE_ADD(form_create, INTERVAL ".$db->db_escape_string($nb_days)." DAY) <= NOW()");
	}

// formulaires impliqu�s dans une application
function form_app_forms($id_app)
	{
	$db = &$GLOBALS['babDB'];
	$res = $db->db_query("SELECT DISTINCT id_form FROM ".FORM_APP_STEPS." s WHERE id_application='".$db->db_escape_string($id_app)."'");
	$out = array();
	while($arr = $db->db_fetch_array($res))
		{
			$out[] = $arr['id_form'];
		}
	return $out;
	}


// dates

function form_dateFormat(&$str)
	{

	$n = strlen($str);
	if ($n <= 10 && substr_count($str,'-') == 2)
		{
		$dt = explode('-',$str);

		if (0 === (int) $dt[0]) {
			$str = '';
			return;
			}

		switch (form_getConfigValue('date_format'))
			{
			case 'fr':
				$str = $dt[2].'/'.$dt[1].'/'.$dt[0];
				break;
			case 'en':
				$str = $dt[1].'/'.$dt[2].'/'.$dt[0];
				break;
			}
		}
	elseif ($n > 10 && $n <= 19 && substr_count($str,'-') == 2 && substr_count($str,':') == 2)
		{
		list($date,$time) = explode(' ',$str);
		$dt = explode('-',$date);

		if (0 === (int) $dt[0]) {
			$str = '';
			return;
			}

		switch (form_getConfigValue('date_format'))
			{
			case 'fr':
				$str = $dt[2].'/'.$dt[1].'/'.$dt[0].' '.$time;
				break;
			case 'en':
				$str = $dt[1].'/'.$dt[2].'/'.$dt[0].' '.$time;
				break;
			}
		}
	}


function form_getDateTime($arr_values,$arr_format)
	{
	$hour = 0;
	$min = 0;
	$sec = 0;

	for ($i = 0 ; $i < count($arr_format) ; $i++ )
		{
		
		switch ($arr_format[$i])
			{
			case '%D': /* day */
			case '%j': /* Day of the month with leading zeros */ 
				$day = $arr_values[$i];
				break;
			case '%M': /* Month */
			case '%n': /* Numeric representation of a month, with leading zeros */
				$month = $arr_values[$i];
				break;
			case '%Y': /* A full numeric representation of a year, 4 digits */
				$year = $arr_values[$i];
				break;
			case '%y': /* A two digit representation of a year */
				$tmp = date('Y');
				$year = substr($tmp,0,2).$arr_values[$i];
				break;
			case 'H': /* 24-hour format of an hour with leading zeros */
				$hour = $arr_values[$i];
				break;
			case 'i': /* Minutes with leading zeros */
				$min = $arr_values[$i];
				break;
			case 's':
				$sec = $arr_values[$i];
				break;
			}
		}

	if (!isset($day) || !isset($month) || !isset($year))
		return false;

	settype($day, "integer");
	settype($month, "integer");
	settype($year, "integer");
	settype($hour, "integer");
	settype($min, "integer");
	settype($sec, "integer");

	return mktime ( $hour , $min , $sec , $month, $day , $year );
	}

function form_formatDate(&$value)
	{
	$f = form_getConfigValue('date_format');

	switch ($f)
		{
		case 'fr':
			$date_format = '%j/%n/%Y H:i:s';
			break;
		case 'en':
			$date_format = '%n/%j/%Y H:i:s';
			break;
		}

	$value = trim($value);
	list($fdate,$ftime) = explode(' ',$date_format);
	if (substr_count($value,' ') == 1)
		{
		list($date,$time) = explode(' ',$value);
		}
	else
		{
		$date = $value;
		$time = '';
		}
	
	$arr = array('/','-',' ',':');
	foreach($arr as $separator)
		{
		//echo substr_count($date, $separator).' '.substr_count($fdate, $separator).' '.$fdate.'<br />';
		if (!empty($date) && substr_count($date, $separator) > 0 && substr_count($date, $separator) == substr_count($fdate, $separator))
			{
			$arr_values = explode($separator,$date);
			$arr_format = explode($separator,$fdate);
			
			if (!empty($time) && substr_count($time, $separator) > 0 && substr_count($time, $separator) == substr_count($ftime, $separator))
				{
				$arr_values = array_merge($arr_values,explode($separator,$time));
				$arr_format = array_merge($arr_format,explode($separator,$ftime));
				break;
				}
			}
		
		}

	if (isset($arr_values) && isset($arr_format))
		{
		
		$ts = form_getDateTime($arr_values,$arr_format);
		$p1 = date('Y-m-d',$ts);
		$p2 = date('H:i:s',$ts);
		if ($p2 == '00:00:00')
			$value = $p1;
		else
			$value = $p1.' '.$p2;
		}
	}




// functions

function form_field_function($function,&$row)
{
	if (count($row) > 0)
		{
		switch ($function)
			{
			case 'index':
				return isset($row['form_id']) ? $row['form_id'] : '';
				break;
			}
		}
	else
		return '';
}


/**
 * List of accessible applications
 * for the section and ovml contener
 */
class form_app_list
	{
	function form_app_list($id_right = false)
		{
		$this->db = & $GLOBALS['babDB'];

		$user_access = form_getUserFormAcces();

		if (count($user_access['APPLICATIONS']) > 0)
			{
			if ($id_right)
				{
				if (!bab_isAccessValid(FORM_GEST_GROUPS,$id_right))
					return false;
				$right = "AND id_right='".$this->db->db_escape_string($id_right)."'";
				}
			else {
				$right = '';
			}
			$query = "SELECT a.id,a.name,a.description,a.https FROM ".FORM_APP_APPLICATIONS." a, ".FORM_GEST_RIGHTS_APP." g WHERE g.id_app=a.id ".$right." AND a.id IN(".$this->db->quote($user_access['APPLICATIONS']).") GROUP BY a.id ORDER BY a.name";
			$this->res = $this->db->db_query($query);
			}
		else
			$this->res = false;
		}

	function getnextlink()
		{
		if (isset($this->res) && list($this->id, $this->name, $this->description, $https) = $this->db->db_fetch_array($this->res))
			{
			$this->url = $GLOBALS['babAddonUrl']."form&idx=application&id_app=".$this->id;
			if ($https == 'Y')
				form_urlToHttps($this->url);
			return true;
			}
		else
			{
			return false;
			}
		}

	function countrows()
		{
		return !isset($this->res) || false === $this->res ? 0 : $this->db->db_num_rows($this->res);
		}

	function printout_section()
		{
		include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
		$list = new bab_myAddonSection();
		$id = $list->addElement('list');
		while ($arr = $this->db->db_fetch_array($this->res))
			{
			$url = $GLOBALS['babAddonUrl']."form&idx=application&id_app=".$arr['id'];
			if ($arr['https'] == 'Y')
				form_urlToHttps($url);
			$list->pushHtmlData($id,$arr['name'], array('title' => $arr['description'], 'href' => $url));
			}

		return $list->getHtml();
		}
	}

/**
 * error message for field control
 * @param	string	$alert
 * @param	string	$name
 * @return	string
 */
function form_msgError($alert,$name)
	{
	return !empty($alert) ? $alert : form_translate('Error on field').' : '.$name;
	}


/**
 * @return array
 */
function form_getTableCols($id_table)
	{
	$db = &$GLOBALS['babDB'];
	$cols = array();
	$res = $db->db_query("SELECT id,name FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$db->db_escape_string($id_table)."'");
	while (list($id,$name) = $db->db_fetch_array($res))
		{
		$cols[$id] = form_col_name($id,$name);
		}
	return $cols;
	}

function form_getTableFields($id_table)
	{
	$db = &$GLOBALS['babDB'];
	$res = $db->db_query("SELECT id,name FROM ".FORM_TABLES_FIELDS." WHERE id_table='".$db->db_escape_string($id_table)."'");
	$cols = array();
	while (list($id,$name) = $db->db_fetch_array($res))
		{
		$cols[form_col_name($id,$name)] = $name;
		}
	return $cols;
	}

function form_getFilterType()
	{
	return array(
				0 => array('=\'%s\'',				form_translate('Equal'),			"%s == %s"),
				1 => array('<>\'%s\'',				form_translate('Different'),		"%s != %s"),
				2 => array('>\'%s\'',				form_translate('Superior'),			"%s > %s"),
				3 => array('<\'%s\'',				form_translate('Inferior'),			"%s < %s"),
				4 => array('>=\'%s\'',				form_translate('Superior or equal'),"%s >= %s"),
				5 => array('<=\'%s\'',				form_translate('Inferior or equal'),"%s >= %s"),
				6 => array('IN(\'%s\')',			form_translate('In'),				"in_array(%s, explode(',',%s))"),
				7 => array('NOT IN(\'%s\')',		form_translate('Not in'),			"!in_array(%s, explode(',',%s))"),
				8 => array('LIKE \'%%%s%%\'',		form_translate('Like'),				"false !== strpos(%2\$s,%1\$s)"),
				9 => array('NOT LIKE \'%%%s%%\'',	form_translate('Not like'),			"false === strpos(%2\$s,%1\$s)")
				);
	}


/**
 * Replace macro in filter string
 * @param	string	$macro		filter string
 * @param	array	$replace	contain the current row, used with $FIELD()
 * @return 	string
 */
function form_getMacroValue($macro, $replace = false)
	{
	$reg = "/\\\$([A-Z]*?)\((.*?)\)/";
	$m = null;
	if (preg_match_all($reg, $macro, $m))
		{
		for($k = 0; $k < count($m[1]); $k++ )
			{
			$param = $m[2][$k];
			$new = '';

			switch ($m[1][$k])
				{
				case 'REQUEST':
					if (isset($_REQUEST['form_field_'.$param]))
						$new = $_REQUEST['form_field_'.$param];
					break;

				case 'FIELD':
					$key = 'f'.$param;
					if (array_key_exists($key,$replace))
						$new = $replace[$key];
					break;

				case 'GET':
					if (isset($_GET[$param]))
						$new = $_GET[$param];
					break;

				case 'POST':
					if (isset($_POST[$param]))
						$new = $_POST[$param];
					break;


				case 'YEARINCREMENT': // (id_table, id_col_to_get)
					$args = explode(',',$param);
					if (2 == count($args))
						{
						$db = &$GLOBALS['babDB'];

						$queryObj = new form_queryObj($args[0]);
						$queryObj->addTableCol($args[1], 'macro');
						$queryObj->orderby[] = 'macro DESC';
						$query = $queryObj->getQuery();

						$arr = $db->db_fetch_assoc($db->db_query($query));
						if (isset($arr['macro']))
							{
							$old = &$arr['macro'];
							$year = substr($old,0,strpos($old, "/"));
							$num = (int) substr(strrchr($old, "/"), 1);
							if (date('Y') != $year)
								{
								$year = date('Y');
								$num = 0;
								}
							$num++;
							$new = $year.'/'.$num;
							}
						else
							{
							$new = date('Y').'/1';
							}
						}
					break;

				case 'QUERY': // (id_table, id_col_to_get, form_row)
					$args = explode(',',$param);
					if (3 == count($args) && isset($_GET[$args[2]]))
						{
						$db = &$GLOBALS['babDB'];

						$queryObj = new form_queryObj($args[0]);
						$queryObj->addTableCol($args[1], 'macro');
						$queryObj->addWhere('p.form_id = \''.$db->db_escape_string($_GET[$args[2]]).'\'');
						$query = $queryObj->getQuery();

						$arr = $db->db_fetch_assoc($db->db_query($query));
						if (isset($arr['macro']))
							{
							$new = $arr['macro'];
							}
						}
					break;
				}

			$macro = preg_replace("/".preg_quote($m[0][$k],"/")."/", $new, $macro);
			}
		}
	return $macro;
	}


function form_setMacro(&$txt, &$replace )
	{
	$txt = form_getMacroValue($txt, $replace);
	}


/** 
 * user acces
 * @return array
 */
function form_getUserFormAcces()
	{
	static $out = null;

	if (null === $out) {


		$out = array(
			'TABLES'		=> array_keys(bab_getUserIdObjects(FORM_TABLES_VIEW_GROUPS)),
			'FORMS'			=> array_keys(bab_getUserIdObjects(FORM_FORMS_GROUPS)),
			'APPLICATIONS'	=> array_keys(bab_getUserIdObjects(FORM_APP_GROUPS))
			);


		$db = &$GLOBALS['babDB'];

		$res = $db->db_query("SELECT * FROM ".FORM_GEST_RIGHTS." WHERE manager='1' OR section='1'");
		while( $arr = $db->db_fetch_array($res))
			{
			if ($arr['manager'] == 1)
				$out['ADMIN'] = bab_isAccessValid(FORM_GEST_GROUPS,$arr['id']);

			if ($arr['section'] == 1)
				$out['SECTION'] = $arr['id'];
			}
		}

	return $out;
	}






function form_debug($str)
	{
		bab_debug($str);	
	}


function form_getDefaultFormElement($id_table_field)
	{
	$db = &$GLOBALS['babDB'];

	
	
	$res = $db->db_query("SELECT t.id_forms_elements FROM ".FORM_TABLES_FIELDS." f,".FORM_TABLES_FIELDS_TYPES." t WHERE f.id='".$db->db_escape_string($id_table_field)."' AND t.id=f.id_type");

	if (list($str) = $db->db_fetch_array($res))
		{
		$arr = explode(',', $str);
		return $arr[0];
		}
	return 1;
	}


function form_deldir($dir)
	{
	  $current_dir = opendir($dir);
	  while($entryname = readdir($current_dir)){
		 if(is_dir("$dir/$entryname") and ($entryname != "." and $entryname!="..")){
		   form_deldir($dir.'/'.$entryname);
		 }elseif($entryname != "." and $entryname!=".."){
		   unlink($dir.'/'.$entryname);
		 }
	  }
	  closedir($current_dir);
	  rmdir($dir);
	}


 function form_getFileMimeType($filename){
	require_once($GLOBALS['babInstallPath'].'addons/forms/file2mime.php');
	$file2mime = & form_load_file2mime();
	$ext=substr(strtolower(strrchr($filename, '.')), 1);
	return isset($file2mime[$ext]) ? $file2mime[$ext] : 'application/octet-stream';
}


function form_getApprobDescription($id_form, $id_table, $form_row)
{
	include_once $GLOBALS['babAddonPhpPath'].'approbincl.php';
	return form_f_getApprobDescription($id_form, $id_table, $form_row);
}


/**
 * @param	int		$id_table
 * @param	array	$arr_id
 */
function form_deleteTableRows($id_table, $arr_id) {

	if (0 < count($arr_id)) {
		include_once $GLOBALS['babAddonPhpPath'].'f_delete.php';
		form_f_deleteTableRows($id_table, $arr_id);
	}
}


define('FORM_USER_INIT'					, 1);
define('FORM_USER_DELETE'				, 2);
define('FORM_USER_DELETE_IF_OPTION'		, 3);

/**
 * @param	int		$id_user
 * @param	int		$option
 * @param	int		[$id_table]
 */
function form_modifiyUserRows($id_user, $option, $id_table=NULL) {

	global $babDB;
	static $fields_user = array();

	if (!$fields_user) {
	
		$query = "SELECT f.id_table, f.id, f.name, t.on_delete_user FROM ".FORM_TABLES_FIELDS." f, ".FORM_TABLES_FIELDS_TYPES." t WHERE t.id=f.id_type AND t.id_extend='4'";
		
		if (NULL !== $id_table) {
			$query .= ' AND id_table='.$babDB->quote($id_table);
		}
		
		$res = $babDB->db_query($query);

		while ($arr = $babDB->db_fetch_assoc($res)) {
			$fields_user[] = array(
				'id_table' 			=> $arr['id_table'],
				'field' 			=> form_col_name($arr['id'],$arr['name']),
				'on_delete_user' 	=> $arr['on_delete_user']
			);
		}
	}

	foreach($fields_user as $arr) {
		
		if ((FORM_USER_DELETE_IF_OPTION === $option && $arr['on_delete_user'])
		|| FORM_USER_DELETE === $option) {
		
			$res = $babDB->db_query("SELECT form_id FROM ".form_tbl_name($arr['id_table'])." WHERE ".$arr['field']."='".$babDB->db_escape_string($id_user)."'");
			$arr_id = array();
			while ($row = $babDB->db_fetch_assoc($res)) {
				$arr_id[] = $row['form_id'];
			}
			form_deleteTableRows($arr['id_table'], $arr_id);
		}
		
		
		if ((FORM_USER_DELETE_IF_OPTION === $option && !$arr['on_delete_user'])
		|| FORM_USER_INIT === $option) {
			$res = $babDB->db_query("UPDATE ".form_tbl_name($arr['id_table'])." SET ".$arr['field']."='0' WHERE ".$arr['field']."='".$babDB->db_escape_string($id_user)."'");
		}
	}
}


/**
 * Current form row ID
 * Set & Get function
 * @param	int	[$param]
 * @return 	NULL|int
 */
function form_currentDbRow($param = NULL) {
	static $memory = NULL;
	if (NULL !== $param) {
		$memory = $param;
	}
	return $memory;
}


/**
 * Current id_step row ID
 * Set by step processor 
 * Get by application
 * @see		form_stepFlow::gotoStep($id_step)
 * @param	int	[$param]
 * @return 	NULL|int
 */
function form_currentIdStep($param = NULL) {
	static $memory = NULL;
	if (NULL !== $param) {
		$memory = $param;
	}
	
	return $memory;
}



/**
 * Get current workspace ID
 * @return int;
 */
function form_getWorkspace() {

	if (isset($_SESSION['form_current_workspace']) && $_SESSION['form_current_workspace'] > 0) {
		return $_SESSION['form_current_workspace'];
	} else {
		
		if (isset($GLOBALS['BAB_SESS_USERID']))
		{
			$id_user = $GLOBALS['BAB_SESS_USERID'];
		} else {
			
			$id_user = bab_getUserId();
		}
		
		$reg = bab_getRegistryInstance();
		$reg->changeDirectory('/forms/user/'.$id_user);
		$workspace = $reg->getValue('workspace');
		
		if (NULL === $workspace || 0 === $workspace) {
			global $babDB;
			$res = $babDB->db_query('
				SELECT id FROM '.FORM_WORKSPACES.' LIMIT 1
			');
			if ($arr = $babDB->db_fetch_assoc($res)) {
				$workspace = $arr['id'];
				$_SESSION['form_current_workspace'] = $workspace;
			} else {
				$workspace = 0;
			}
		}

		return $workspace;
	}
}


/**
 * Actions on exportable object
 */
class form_ieO {

	/**
	 * @static
	 * @param	string	$type
	 * @param	int		$id_object
	 */
	public static function inserted($type, $id_object) {
		form_ieO::linkToWorkspace($type, $id_object, form_getWorkspace());
	}
	
	/**
	 * Link object to workspace
	 * @static
	 * @param	string	type
	 * @param	int		$id_object
	 * @param	int		$id_workspace
	 */
	public static function linkToWorkspace($type, $id_object, $id_workspace) {
		global $babDB;
		
		$babDB->db_query('
			INSERT INTO '.FORM_WORKSPACE_ENTRIES.' 
				(id_workspace, id_object, type) 
			VALUES 
				(
					'.$babDB->quote($id_workspace).',
					'.$babDB->quote($id_object).',
					'.$babDB->quote($type).' 
				)
		');
	}
	
	
	/**
	 * On object insertion
	 * @static
	 * @param	string 	$table
	 */
	public static function hasAccess($table, $id_object) {
	
		// T4975
		
		require_once $GLOBALS['babInstallPath']."admin/acl.php";
		aclSetGroups_registered($table, $id_object);
	
	}
	
	
	/**
	 * @static
	 * @param	string	type
	 */
	public static function deleted($type, $id_object) {
		global $babDB;
		
		$babDB->db_query('
			DELETE FROM '.FORM_WORKSPACE_ENTRIES.' 
				WHERE id_object='.$babDB->quote($id_object).' AND type='.$babDB->quote($type).'
		');
	}
}
