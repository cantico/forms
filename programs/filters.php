<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';


require_once( $GLOBALS['babInstallPath']."addons/forms/functions.php");

function list_filters($id_table)
{
global $babBody;
    class temp
        { 
		var $altbg = false;
        function temp($id_table)
            {
			$this->db = & $GLOBALS['babDB'];
			$this->id_table = $id_table;

			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_rules = form_translate('Rules');
			$this->t_edit = form_translate('Edit');
			$this->t_delete = form_translate('Delete');
			$this->t_uncheckall = form_translate('Uncheck all');
			$this->t_checkall = form_translate('Check all');
			
			$this->res = $this->db->db_query("SELECT l.*,COUNT(f.id) rules FROM ".FORM_TABLES_FILTERS." l LEFT JOIN ".FORM_TABLES_FILTERS_FIELDS." f ON f.id_filter=l.id WHERE l.id_table='".$this->db->db_escape_string($this->id_table)."' GROUP BY l.id");
            }

		function getnext()
			{
			if ( $this->arr = $this->db->db_fetch_array($this->res))
				{
				$this->altbg = !$this->altbg;
				$this->arr['name'] = bab_toHtml($this->arr['name']);
				$this->arr['description'] = bab_toHtml($this->arr['description']);
				return true;
				}
			else
				{
				return false;
				}
			}
        }
    $tp = new temp($id_table);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."filters.html", "list"));
}

function edit_filter($id_table,$id = '')
{
global $babBody;
    class temp
        {
		var $arr = array();
		var $altbg = false;

        function temp($id_table,$id)
            {
			$this->id_table = $id_table;
			$this->id_filter = $id;
			$this->db = & $GLOBALS['babDB'];

			$this->t_record = form_translate('Record');
			
			$this->t_name = form_translate('Name');
			$this->t_description = form_translate('Description');
			$this->t_operator = form_translate('Operator');
			$this->t_comparator = form_translate('Value');
			$this->t_order = form_translate('Order');
			$this->t_no_order = form_translate('Order by visible collumn');


			$el_to_init = array('name', 'description', 'ordercol');

			if (!empty($this->id_filter))
				{

				$this->arr = $this->db->db_fetch_array($this->db->db_query("SELECT * FROM ".FORM_TABLES_FILTERS." WHERE id='".$this->db->db_escape_string($this->id_filter)."'"));


				$this->res = $this->db->db_query("
								SELECT 
									f1.name,
									f1.id id_table_field,
									v.id,
									v.operator,
									v.comparator
								FROM 
									".FORM_TABLES_FIELDS." f1 
									LEFT JOIN
										".FORM_TABLES_FILTERS_FIELDS." v 
										ON v.id_filter = '".$this->db->db_escape_string($this->id_filter)."' AND v.id_table_field=f1.id ,
									".FORM_TABLES_FILTERS." f2 
								
								WHERE 
									f2.id_table=f1.id_table 
									AND f2.id='".$this->db->db_escape_string($this->id_filter)."' 
									AND f1.field_function=''
									");


				$this->reslnk = $this->db->db_query("
								SELECT 
									f.id id_table_field, 
									CONCAT(lnk.name,' : ',f.name) name,
									v.id,
									v.operator,
									v.comparator,
									lnk.id lnk
								FROM 
									".FORM_TABLES_LNK." lnk,
									".FORM_TABLES_FIELDS." f 
								LEFT JOIN
									".FORM_TABLES_FILTERS_FIELDS." v 
									ON v.id_filter = '".$this->db->db_escape_string($this->id_filter)."' AND v.id_table_field=f.id 
								WHERE 
									lnk.id_table1='".$this->db->db_escape_string($this->id_table)."' 
									AND f.id_table=lnk.id_lnk_table 
									AND f.field_function=''
								
								GROUP BY f.id
									");
				}
			else
				{
				$this->res = $this->db->db_query("
								SELECT 
									id id_table_field, name
								FROM 
									".FORM_TABLES_FIELDS." 
								WHERE 
									id_table='".$this->db->db_escape_string($this->id_table)."' 
									AND field_function=''
									");


				$this->reslnk = $this->db->db_query("
								SELECT 
									f.id id_table_field, 
									CONCAT(lnk.name,' : ',f.name) name,
									lnk.id lnk 
								FROM 
									".FORM_TABLES_LNK." lnk,
									".FORM_TABLES_FIELDS." f
								WHERE 
									lnk.id_table1='".$this->db->db_escape_string($this->id_table)."' 
									AND f.id_table=lnk.id_lnk_table
									AND f.field_function=''

								GROUP BY f.id
									");

				foreach ($el_to_init as $field)
					{
					$this->arr[$field] = '';
					}
				}
				
			$this->arr['name'] 			= bab_toHtml($this->arr['name']);
			$this->arr['description'] 	= bab_toHtml($this->arr['description']);

			$this->operators = form_getFilterType();
			$this->noorder = true;
			}

		function getnextfield()
			{
			if ($this->field = $this->db->db_fetch_array($this->res))
				{
				$this->field['id'] = isset($this->field['id']) ? $this->field['id'] : '';
				$this->field['comparator'] = isset($this->field['comparator']) ? bab_toHtml($this->field['comparator']) : '';
				$this->checked = $this->arr['ordercol'] == $this->field['id_table_field'];
				if ($this->checked) {
					$this->noorder = false;
				}
				$this->altbg = !$this->altbg;
				$this->field['name'] = bab_toHtml($this->field['name']);
				return true;
				}
			else
				{
				
				return false;
				}
			}

		function getnextlnk()
			{
			if ($this->lnk = $this->db->db_fetch_array($this->reslnk))
				{
				$this->lnk['id'] = isset($this->lnk['id']) ? $this->lnk['id'] : '';
				$this->lnk['comparator'] = isset($this->lnk['comparator']) ? $this->lnk['comparator'] : '';
				$this->altbg = !$this->altbg;
				return true;
				}
			else
				{
				
				return false;
				}
			}


		function getnextoperator()
			{
			if (list($this->id, list(,$this->value)) = each($this->operators))
				{
				$this->selected = (isset($this->field['operator']) && $this->field['operator'] ==$this->id) || (isset($this->lnk['operator']) && $this->lnk['operator'] ==$this->id);
				return true;
				}
			else
				{
				reset($this->operators);
				return false;
				}
			}

        }
    $tp = new temp($id_table,$id);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath']."filters.html", "edit" ));
}


// record


function record_filter()
	{
	$db = &$GLOBALS['babDB'];

	if (empty($_POST['name'])) {
		$GLOBALS['babBody']->msgerror = form_translate("Name must be defined");
		return false;
	}

	$ordercol = isset($_POST['ordercol']) ? (int) $_POST['ordercol'] : 0;

	if (empty($_POST['id_filter']))
		{
		$db->db_query("
				INSERT INTO 
					".FORM_TABLES_FILTERS." 
					(id_table, name, description, ordercol) 
				VALUES 
					('".$db->db_escape_string($_POST['id_table'])."',
					'".$db->db_escape_string($_POST['name'])."',
					'".$db->db_escape_string($_POST['description'])."',
					'".$db->db_escape_string($ordercol)."')
					");

		$id_filter = $db->db_insert_id();
		}
	else
		{
		$db->db_query("
				UPDATE 
					".FORM_TABLES_FILTERS." 
				SET
					id_table = '".$db->db_escape_string($_POST['id_table'])."',
					name = '".$db->db_escape_string($_POST['name'])."',
					description = '".$db->db_escape_string($_POST['description'])."',
					ordercol = '".$db->db_escape_string($ordercol)."'
				WHERE 
					id='".$db->db_escape_string($_POST['id_filter'])."'
					");

		$id_filter = $_POST['id_filter'];
		}

	$prefix = 'field_operator_';
	$worked_id = array();

	foreach($_POST as $field => $value)
		{
		if (substr($field,0,strlen($prefix)) == $prefix)
			{
			$tmp = explode('_',$field);
			$index = &$tmp[2];

			if ($_POST['field_operator_'.$index] == '')
				continue;

			$lnk = isset($_POST['field_lnk_'.$index]) ? $_POST['field_lnk_'.$index] : '';

			if (empty($_POST['field_id_'.$index]))
				{
				$db->db_query("
							INSERT INTO 
								".FORM_TABLES_FILTERS_FIELDS." 
								(id_filter, 
								id_table_field,
								operator,
								comparator,
								id_lnk) 
							VALUES
								('".$db->db_escape_string($id_filter)."',
								'".$db->db_escape_string($index)."',
								'".$db->db_escape_string($_POST['field_operator_'.$index])."',
								'".$db->db_escape_string($_POST['field_comparator_'.$index])."',
								'".$db->db_escape_string($lnk)."')
								");

				$worked_id[] = $db->db_insert_id();
				}
			else
				{
				$db->db_query("
							UPDATE 
								".FORM_TABLES_FILTERS_FIELDS." 
							SET
								operator = '".$db->db_escape_string($_POST['field_operator_'.$index])."',
								comparator = '".$db->db_escape_string($_POST['field_comparator_'.$index])."' 
							WHERE 
								id = '".$db->db_escape_string($_POST['field_id_'.$index])."'
								");

				$worked_id[] = $_POST['field_id_'.$index];
				}

			}
		}


	if (count($worked_id) > 0 )
		$db->db_query("DELETE FROM ".FORM_TABLES_FILTERS_FIELDS." WHERE id NOT IN(".$db->quote($worked_id).") AND id=".$db->quote($id_filter));

	return true;
	}


function delete_filters()
{
	if (count($_POST['delete_filters']) > 0)
		{
		$db = & $GLOBALS['babDB'];
		$db->db_query("DELETE FROM ".FORM_TABLES_FILTERS." WHERE id IN(".$db->quote($_POST['delete_filters']).")");
		}
}

// main

$idx = isset($_REQUEST['idx']) ? $_REQUEST['idx'] : 'list_lnk';


if (isset($_POST['action']))
{
switch ($_POST['action'])
	{
	case 'delete_filters':
		delete_filters();
		break;

	case 'record_filter':
		if (!record_filter())
			{
			$idx = 'edit';
			}
		break;
	}
}

$babBody->addItemMenu("list_tables", form_translate("List tables"),$GLOBALS['babAddonUrl']."main&idx=list_tables");
$babBody->addItemMenu("list", form_translate("List filters"),$GLOBALS['babAddonUrl']."filters&idx=list&id_table=".$_REQUEST['id_table']);

switch($idx)
{
	case "edit":
		$babBody->addItemMenu("edit", form_translate("Edit filter"),$GLOBALS['babAddonUrl']."filters&idx=edit&id_table=".$_REQUEST['id_table']);
		$babBody->title = form_translate("Edit filters");
		edit_filter($_REQUEST['id_table'], $_REQUEST['id_filter']);
		break;

	case "list":
		$babBody->addItemMenu("add", form_translate("Add filter"),$GLOBALS['babAddonUrl']."filters&idx=add&id_table=".$_REQUEST['id_table']);
		$babBody->title = form_translate("List filters");
		list_filters($_REQUEST['id_table']);
		break;

	case "add":
		$babBody->addItemMenu("add", form_translate("Add filter"),$GLOBALS['babAddonUrl']."filters&idx=add&id_table=".$_REQUEST['id_table']);
		$babBody->title = form_translate("Add filter");
		edit_filter($_REQUEST['id_table']);
		break;
}


$babBody->setCurrentItemMenu($idx);

?>