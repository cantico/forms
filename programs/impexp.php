<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';








/**
 * Workspace object
 * export objects manager
 * 
 * voir le contenu des fichiers export :
 * php -r "print_r(unserialize(file_get_contents('./table')));"
 * 
 */
class form_wsOworkspace {

	var $tablename;
	var $sql_data = array();
	var $error;
	var $primary_key;
	
	/**
	 * insert or update
	 * @access public
	 */
	var $modified_primary_keys = array();
	
	/**
	 * insert only
	 * @access public
	 */
	var $inserted_primary_keys = array();
	
	/**
	 * update only
	 * @access public
	 */
	var $updated_primary_keys = array();
	
	
	/**
	 * transformed id for each references columns
	 * @var array
	 */
	var $transformed = array();
	
	
	/**
	 * Get all types with object
	 * @static
	 * @return array
	 */
	function getSub() {
		return array(

			'type',
			'table',
			'table_lnk',
			'form',
			'form_help',
			'application',
			'workspace_object' // workspace_object doit �tre dernier pour la suppression
		);
	}
	
	/**
	 * Get label for list of objects
	 * @return string
	 */
	function getTypeTitle() {
		return form_translate('Workspaces');
	}
	
	
	function isSelectable() {
		return false;
	}
	
	/**
	 * Get all objects
	 * @static
	 * @return array
	 */
	function getItems() {
		
		global $babDB;
		
		$id_workspace = form_getWorkspace();
		if (0 === $id_workspace) {
			return array();
		}
		
		
		$res = $babDB->db_query('SELECT id,name FROM '.FORM_WORKSPACES.' WHERE id='.$babDB->quote($id_workspace));
		$arr = $babDB->db_fetch_assoc($res);
		
		return array(
			$arr['id'] => array(
				'name' 			=> $arr['name'],
				'selected' 		=> true
			)
		);
	}
	
	
	function form_wsOworkspace() {
		$this->tablename = FORM_WORKSPACES;
	}
	
	
	function getPrimaryKey() {
		
		
		if (!isset($this->primary_key)) {
			$this->primary_key = false;
			global $babDB;
			$res = $babDB->db_query('DESCRIBE '.$babDB->db_escape_string($this->tablename));
			while ($arr = $babDB->db_fetch_assoc($res)) {
				
				if ('PRI' == $arr['Key']) {
					$this->primary_key = $arr['Field'];
				}
			}
		}

		return $this->primary_key;
	}
	
	
	function getUniqueCols() {
	
		static $cols = NULL;
		
		if (NULL === $cols) {
			global $babDB;
			$cols = array();
			$res = $babDB->db_query('DESCRIBE '.$babDB->db_escape_string($this->tablename));
			while ($arr = $babDB->db_fetch_assoc($res)) {
				if ('UNI' == $arr['Key']) {
					$cols[$arr['Field']] = $arr['Field'];
				}
			}
		}
		
		return $cols;
	}
	
	
	
	function record() {
		global $babDB;
		
		$primary_key = $this->getPrimaryKey();
		
		$arr_id = array();
		$items = $this->getItems();
		
		foreach($items as $id => $item) {
			if ($item['selected']) {
				$arr_id[] = $id;
			}
		}

		$res = $babDB->db_query('
			SELECT * FROM '.$babDB->db_escape_string($this->tablename).' 
			WHERE '.$primary_key.' IN('.$babDB->quote($arr_id).')'
		);
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
			$id = $arr[$primary_key];
			unset($arr[$primary_key]);
			$this->sql_data[$id] = $arr;
		}
	}
	
	
	
	function __wakeup() {
		$this->error = '';
		while ($arr = $this->nextInsert($this->error)) {
			// modified_primary_keys[old_id] = new_id
			$this->modified_primary_keys[$arr[0]] = $arr[1];
			
			if ($arr[2]) {
				$this->inserted_primary_keys[$arr[0]] = $arr[1];
			} else {
				$this->updated_primary_keys[$arr[0]] = $arr[1];
			}
		};
	}
	

	
	/**
	 * Apr�s les requetes insert de tout les objets import�s
	 * Chaque objet sera pass� en param�tre par cette m�thode
	 * pour tout les types
	 * Cette m�thode est charg� de remplacer les anciens ID par les nouveau dans les nouvelles lignes ins�r�es
	 *
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;
		
		foreach($this->inserted_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {
			
				case 'workspace_object':
					
					// mettre a jour le numero de workspace sur les elements inseres 
				
					$babDB->db_query('
						UPDATE '.FORM_WORKSPACE_ENTRIES.' SET 
							id_workspace='.$babDB->quote($new_id).' 
						WHERE 
							id_workspace='.$babDB->quote($old_id).' 
							AND id IN('.$babDB->quote($instance->inserted_primary_keys).')
						');

					// ajouter les elements utilises dans le nouveau workspace
					
					foreach($instance->updated_primary_keys as $old_id_entry => $new_id_entry) 
					{
						$babDB->db_query('
							INSERT INTO '.FORM_WORKSPACE_ENTRIES.' 
								(id_workspace, id_object, type) 
							VALUES 
								('.$babDB->quote($new_id).', '.$babDB->quote($instance->sql_data[$old_id_entry]['id_object']).', '.$babDB->quote($instance->sql_data[$old_id_entry]['type']).')
						');
					}
					
					
					// define new id as the default workspace
					
					if (isset($_SESSION['form_current_workspace'])) {
						unset($_SESSION['form_current_workspace']);
					}
					
					$reg = bab_getRegistryInstance();
					$reg->changeDirectory('/forms/user/'.$GLOBALS['BAB_SESS_USERID']);
					$reg->setKeyValue('workspace', $new_id);
				
					break;
					
			}
		}
	}
	
	
	
	/**
	 * Update reference on table $this->tablename from inserted id
	 * @param	string	$colname
	 * @param 	int 	$old_id
	 * @param 	int 	$new_id
	 * 
	 */
	function updateRef($colname, $old_id, $new_id, $primary_key = 'id')
	{
		global $babDB;
		
		if (!isset($this->transformed[$colname])) {
			$this->transformed[$colname] = array();
		}
		
		$res = $babDB->db_query('SELECT '.$primary_key.' FROM '.$this->tablename.' 
			WHERE 
				'.$colname.'='.$babDB->quote($old_id).' 
				AND '.$primary_key.' IN('.$babDB->quote($this->inserted_primary_keys).') 
				AND '.$primary_key.' NOT IN('.$babDB->quote($this->transformed[$colname]).') 
		');
		
		while (list($id) = $babDB->db_fetch_array($res))
		{
			$babDB->db_query('
				UPDATE '.$this->tablename.' SET 
					'.$colname.'='.$babDB->quote($new_id).' 
				WHERE 
					'.$primary_key.'='.$babDB->quote($id).' 
			');
			
			$this->transformed[$colname][$id] = $id;
		}
	}
	
	
	
	
	
	function getNextSqldata() {
		return each($this->sql_data);
	}
	
	/**
	 * Import 
	 * Insert a row from the object
	 * @param	string	$error
	 * @return 	array|false
	 */
	function nextInsert(&$error) {

		global $babDB;
	
		if (list($old_id, $row) = $this->getNextSqldata()) {
		
			// verify unique
			
			$unique = $this->getUniqueCols();
			if (!empty($unique)) {
				foreach($row as $colname => $value) {
					if (isset($unique[$colname])) {
						$unique[$colname] = $value;
						
						$res = $babDB->db_query('SELECT COUNT(*) 
							FROM '.$babDB->db_escape_string($this->tablename).' 
							WHERE '.$babDB->db_escape_string($colname).' LIKE '.$babDB->quote($value) 
						);
						
						list($n) = $babDB->db_fetch_array($res);
						
						if (0 < $n) {
							$error = sprintf(form_translate('Duplicate entry in %s, import is not possible'), $this->getTypeTitle());
							return false;
						}
					}
				}
			}
			
			// si la ligne existe d�ja a l'identique ne pas la creer mais utiliser sont id
			$primarykey = $this->getPrimaryKey();
			if ($primarykey && FORM_WORKSPACE_ENTRIES !== $this->tablename) {
				$test_without_id = $row;

				if (!empty($test_without_id)) {
				
					unset($test_without_id[$primarykey]);
				
					$query = '
						SELECT 
							'.$babDB->db_escape_string($primarykey).' 
						FROM '.$babDB->db_escape_string($this->tablename).' 
						WHERE 
					';
					
					$arr = array();
					foreach($test_without_id as $col => $value) {
						$arr[] = $col.'='.$babDB->quote($value)." \n";
					}
					
					$query .= implode($arr, ' AND ');
					
					$res = $babDB->db_query($query);
					if (list($id) = $babDB->db_fetch_array($res)) {
						return array($old_id, $id, false);
					}
				}
			}
			
			// insert
			
		
			
			
			return array($old_id, $this->doInsert($row), true);
		}
		
		return false;
	}
	
	
	/**
	 * 
	 * @param array $row
	 * @return int		the new id
	 */
	function doInsert($row)
	{
		global $babDB;
		
		$babDB->db_query('
				INSERT INTO '.$babDB->db_escape_string($this->tablename).' 
					('.implode(',',array_keys($row)).') VALUES ('.$babDB->quote($row).')
			');
		
		return $babDB->db_insert_id();
	}
	
	
	
	
	/**
	 * recursive delete of the items
	 * @param	string	$error
	 */
	function onDeleteWorkspace(&$obj, &$error) {
		
		global $babDB;
		
		
		$type = substr(get_class($obj),8);
		
		$sub = $obj->getSub();
		foreach($sub as $type) {
			$classname = 'form_wsO'.$type;
			$child = new $classname();
			if (false === form_wsOworkspace::onDeleteWorkspace($child, $error)) {
				bab_debug($classname.'::onDeleteWorkspace return false');
				return false;
			}
		}
		
		
		$selected = array();
		$items = $obj->getItems();
		foreach($items as $id_object => $arr) {
			if ($arr['selected']) {
				
				$res = $babDB->db_query('
				SELECT COUNT(*) FROM '.FORM_WORKSPACE_ENTRIES.' 
					WHERE 
						type='.$babDB->quote($type).' 
						AND id_workspace<>'.$babDB->quote(form_getWorkspace()).' 
						AND id_object='.$babDB->quote($id_object).'
				');
				
				list($n) = $babDB->db_fetch_array($res);
				if ($n == 0) {
					// l'objet n'est pas r�f�renc� dans un autre workspace
					$selected[$id_object] = $id_object;
				}
			}
		}
		
		if (!$selected) {
			bab_debug(get_class($obj).'::onDeleteWorkspace() nothing to delete');
		} else {
			bab_debug(get_class($obj).'::delete('.implode(', ',$selected).')');
			$obj->delete($selected, $error);
		} 
	}
	
	/**
	 * Delete objects when the workspace is deleted
	 * Overright this method for more complex object delete
	 * @param	array 	$id_objects		an array of deletable id_object
	 * @param	string	&$error
	 * @return 	boolean
	 */
	function delete($id_objects, &$error) {

		global $babDB;
		$req = 'DELETE FROM '.$this->tablename.' WHERE '.$this->getPrimaryKey().' IN('.$babDB->quote($id_objects).')';
		bab_debug($req);
		$babDB->db_query($req);
		return true;
	}
	
	
	
	/**
	 * Called after all the tables are created
	 */
	function end() {
		// nothing to do
	}
	
	

	
	
	
	/**
	 * Called to set access to all workspace
	 */
	function setAccess() {
		// nothing to do at this level
	}
}


/**
 * Workspace objects
 */
class form_wsOworkspace_object extends form_wsOworkspace {

	function getSub() {
		return array();
	}
	
	function getTypeTitle() {
		return form_translate('Workspace links to objects');
	}
	
	function isSelectable() {
		return false;
	}
	
	/**
	 * Get all objects
	 * @return array
	 */
	function getItems() {
		
		global $babDB;
		
		$id_workspace = form_getWorkspace();
		if (0 === $id_workspace) {
			return array();
		}
		
		
		$res = $babDB->db_query('SELECT 
			id FROM '.FORM_WORKSPACE_ENTRIES.' WHERE id_workspace='.$babDB->quote($id_workspace));
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['id'],
				'selected' => true
			);
		}
		
		
		return $return;
	}
	
	function form_wsOworkspace_object() {
		$this->tablename = FORM_WORKSPACE_ENTRIES;
	}
	
	
	function getNextSqldata() {
		if ($row = each($this->sql_data)) {
			// set the new id workspace
			// the import must be donne on the workspace before
			static $first = NULL;
			if (NULL === $first) {
				$first = true;
				

				if (isset($_SESSION['form_current_workspace'])) {
					unset($_SESSION['form_current_workspace']);
				}
				
				$reg = bab_getRegistryInstance();
				$reg->changeDirectory('/forms/user/'.$GLOBALS['BAB_SESS_USERID']);
				$reg->setKeyValue('workspace', form_getWorkspace());
			}
			return $row;
		}
		
		return false;
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {

		global $babDB;
		
		// trouver le type de l'objet
		$type = substr(get_class($instance),8);
		
		
		$id_workspace = form_getWorkspace();
		
		$transformed = array();

		/**
		 * Pour chaque type d'objet importe, on cherche dans la liste des liens import�s et on remplace l'id contenu dans le lien par le nouvel id
		 */
		foreach($instance->modified_primary_keys as $old_id => $new_id) {
			
			if ($old_id !== $new_id) {
				
				$res = $babDB->db_query('
					SELECT 
						id 
					FROM '.FORM_WORKSPACE_ENTRIES.' WHERE 
						id_object='.$babDB->quote($old_id).' 
						AND type='.$babDB->quote($type).' 
						AND id IN('.$babDB->quote($this->inserted_primary_keys).')
						AND id NOT IN('.$babDB->quote($transformed).')
				');
				
				if (list($id) = $babDB->db_fetch_array($res)) {
				
					// mettre a jour l'id objet
					$babDB->db_query('
						UPDATE '.FORM_WORKSPACE_ENTRIES.' SET 
							id_object='.$babDB->quote($new_id).' 
						WHERE 
							id = '.$babDB->quote($id).'
					');

					$transformed[$id] = $id;
				}
				
				
				
				
			}

			
			// bab_debug($type.' '.$old_id.' assigned to workspace '.form_getWorkspace().' with new id : '.$new_id);
		}
	}
}



/**
 * Types
 */
class form_wsOtype extends form_wsOworkspace {

	function getSub() {
		return array();
	}
	
	function getTypeTitle() {
		return form_translate('Types');
	}
	
	function isSelectable() {
		return true;
	}
	
	/**
	 * Get all objects
	 * @return array
	 */
	function getItems() {
		
		global $babDB;
		
		$id_workspace = form_getWorkspace();
		if (0 === $id_workspace) {
			return array();
		}
		
		
		$res = $babDB->db_query('SELECT 
			t.id,
			t.name, 
			w.id selected 
			
		FROM '.FORM_TABLES_FIELDS_TYPES.' t 
		LEFT JOIN '.FORM_WORKSPACE_ENTRIES." w 
			ON w.type='type' 
			AND w.id_object=t.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace())." 
		ORDER BY t.name	
		");
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => NULL !== $arr['selected']
			);
		}
		
		
		return $return;
	}
	
	function form_wsOtype() {
		$this->tablename = FORM_TABLES_FIELDS_TYPES;
	}
	
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;
		
		foreach($this->modified_primary_keys as $old_id_type => $new_id_type) {
		
			$type = substr(get_class($instance),8);
		
			// trouver le type de l'objet
			switch($type) {
			
				// mettre a jour l'id_type des tables ou il est pr�sent
				
				case 'table_field':
					$instance->updateRef('id_type', $old_id_type, $new_id_type);
					break;
					
					
				case 'form_field':
					$instance->updateRef('id_type', $old_id_type, $new_id_type);
					break;
			}
		}
	}
}


/**
 * Tables
 */
class form_wsOtable extends form_wsOworkspace {

	function getSub() {
		return array(
			'table_field',
			'table_filter'
		);
	}
	
	function getTypeTitle() {
		return form_translate('Tables');
	}
	
	function isSelectable() {
		return true;
	}
	
	
	/**
	 * Get all objects
	 * @return array
	 */
	function getItems() {
		
		global $babDB;
		
		$res = $babDB->db_query('SELECT 
			t.id,
			t.name, 
			w.id selected 
			
		FROM '.FORM_TABLES.' t 
		LEFT JOIN '.FORM_WORKSPACE_ENTRIES." w 
			ON w.type='table' 
			AND w.id_object=t.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace())."
			
		ORDER BY t.name");
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => NULL !== $arr['selected']
			);
		}
		
		
		return $return;
	}
	
	function form_wsOtable() {
		$this->tablename = FORM_TABLES;
	}
	
	
	/**
	 * Delete objects when the workspace is deleted
	 * Overright this method for more complex object delete
	 * @param	array 	$id_objects		an array of deletable id_object
	 * @param	string	&$error
	 * @return 	boolean
	 */
	function delete($id_objects, &$error) {
	
		$db = &$GLOBALS['babDB'];
		require_once $GLOBALS['babInstallPath']."admin/acl.php";
	
		foreach($id_objects as $id_table) {
		
			$res = $db->db_query("SELECT id_schi FROM ".FORM_APPROB_SCHI." WHERE id_table=".$db->quote($id_table)."");
			if (0 < $db->db_num_rows($res)) {
				require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
				while ($arr = $db->db_fetch_array($res)) {
					bab_WFDeleteInstance($arr['id_schi']);
				}
			}
		
			
			$db->db_query("DROP TABLE IF EXISTS ".$db->db_escape_string(form_tbl_name($id_table))."");
		
			aclDelete(FORM_TABLES_CHANGE_GROUPS	, $id_table);
			aclDelete(FORM_TABLES_VIEW_GROUPS	, $id_table);
			aclDelete(FORM_TABLES_INSERT_GROUPS	, $id_table);
			aclDelete(FORM_TABLES_FILING_GROUPS	, $id_table);
			aclDelete(FORM_TABLES_LOCK_GROUPS	, $id_table);
			
			$db->db_query("DELETE FROM ".FORM_APPROB_SCHI." WHERE id_table='".$db->db_escape_string($id_table)."'");
			$db->db_query("DELETE FROM ".FORM_TABLES." WHERE id='".$db->db_escape_string($id_table)."'");
			$db->db_query("DELETE FROM ".FORM_TABLES_VIEW_COLS." WHERE id_table='".$db->db_escape_string($id_table)."'");
		
			$directory = $GLOBALS['babAddonUpload'].'directories/'.form_tbl_name($id_table).'/';
			if (is_dir($directory))
				{
				form_deldir($directory);
			}
		}
		
		return true;
	}
	
	
	function record() {
		global $babDB;

		$arr_id = array();
		$items = $this->getItems();
		
		foreach($items as $id => $item) {
			if ($item['selected']) {
				$arr_id[] = $id;
			}
		}

		$res = $babDB->db_query('
			SELECT * FROM '.FORM_TABLES.' 
			WHERE id IN('.$babDB->quote($arr_id).')'
		);
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
			$id = $arr['id'];
			unset($arr['id']);
			$arr['created']='N';
			$this->sql_data[$id] = $arr;
		}
	}
	
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;
		


		foreach($this->modified_primary_keys as $old_id_table => $new_id_table) {
		
			$type = substr(get_class($instance),8);
			switch($type) {
			
				case 'form':
					$instance->updateRef('id_table', $old_id_table, $new_id_table);
					$instance->updateRef('id_table_copy', $old_id_table, $new_id_table);
					
					break;
					
					
				case 'table_lnk':
					$instance->updateRef('id_table1', $old_id_table, $new_id_table);
					$instance->updateRef('id_table2', $old_id_table, $new_id_table);
						
					break;
					
					
					
				case 'table_field':
					$instance->updateRef('id_table', $old_id_table, $new_id_table);
					break;
					
					
				case 'table_filter':
					$instance->updateRef('id_table', $old_id_table, $new_id_table);	
					break;
			}
		}
	}
	
	
	
	/**
	 * Called after all the tables are created
	 */
	function end() {
		
		if ('form_wsOtable' !== get_class($this)) {
			return;
		}
		
		
		global $babDB;
		
		include_once $GLOBALS['babAddonPhpPath'].'data_table_editor.php';

		$res1 = $babDB->db_query('
			SELECT id, approbation FROM '.FORM_TABLES.' WHERE id IN('.$babDB->quote($this->inserted_primary_keys).')
		');
	
		while (list($new_id_table, $approbation) = $babDB->db_fetch_array($res1)) {

			if (data_table_editor::tableCreated($new_id_table)) {
				$babDB->db_query('DROP TABLE '.form_tbl_name($new_id_table));
			}
			
			$data_table = new data_table_editor($new_id_table, $approbation);
			
			$res2 = $babDB->db_query('SELECT * FROM '.FORM_TABLES_FIELDS.' WHERE id_table='.$babDB->quote($new_id_table));
			while($arr = $babDB->db_fetch_assoc($res2)) {
			
				if ('' == $arr['field_function']) {
			
					$data_table->add_field(
						form_col_name($arr['id'],$arr['name']), 
						$arr['id_type'], 
						$arr['link_table_field_id'], 
						$arr['default_val'], 
						''
					);
				}
			}
		}
		
		
	}
	
	
	
	
	/**
	 * Set access to table
	 */
	function setAccess() {
	
		$items = $this->getItems();
		
		foreach($items as $id_table => $item) {
			aclSetGroups_registered(FORM_TABLES_CHANGE_GROUPS	, $id_table);
			aclSetGroups_registered(FORM_TABLES_VIEW_GROUPS		, $id_table);
			aclSetGroups_registered(FORM_TABLES_INSERT_GROUPS	, $id_table);
		}
	}
}




/**
 * Tables
 * table fields
 */
class form_wsOtable_field extends form_wsOtable {

	function getSub() {
		return array(
			'table_field_directory'
		);
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Table fields');
	}
	
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('
		
		SELECT 
			f.id,
			t.name tablename, 
			f.name 
			
		FROM 
			'.FORM_TABLES_FIELDS.' f, 
			'.FORM_TABLES.' t, 
			'.FORM_WORKSPACE_ENTRIES." w 
		WHERE 
			t.id=f.id_table 
			AND w.type='table' 
			AND w.id_object=t.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).' 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['tablename'].'.'.$arr['name'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOtable_field() {
		$this->tablename = FORM_TABLES_FIELDS;
	}
	
	
	function record() {
		form_wsOworkspace::record();
	}
	
	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {

		
		global $babDB;
		$type = substr(get_class($instance),8);
		


		foreach($this->modified_primary_keys as $old_id_field => $new_id_field) {

			switch($type) {
			
				case 'form':
					$instance->updateRef('id_table_field_mail', $old_id_field, $new_id_field);
					$instance->updateRef('id_table_field_bcc', $old_id_field, $new_id_field);

					break;
			

				case 'form_field':
					
					$instance->updateRef('id_table_field', $old_id_field, $new_id_field);
					$instance->updateRef('id_table_field_link', $old_id_field, $new_id_field);
					$instance->updateRef('id_table_field_lnk', $old_id_field, $new_id_field);
					$instance->updateRef('field_copy_table_field', $old_id_field, $new_id_field);
					$instance->updateRef('id_table_field_init', $old_id_field, $new_id_field);

					break;
					
					
				case 'table_filter_field':
					$instance->updateRef('id_table_field', $old_id_field, $new_id_field);
					
					break;
					
					
				case 'table_field_directory':
					$instance->updateRef('id_table_field', $old_id_field, $new_id_field);
					break;
					
					
				case 'table_field':
					$instance->updateRef('link_table_field_id', $old_id_field, $new_id_field);
					break;

			}
		}
		
		
		if ('table_field' == $type) {
			// update formulas
						
			$res = $babDB->db_query('
				SELECT 
					id,field_formula 
				FROM 
					'.FORM_TABLES_FIELDS." 
				WHERE 
					field_function='formula' 
					AND id IN(".$babDB->quote($instance->inserted_primary_keys).')
			');
			
			while ($arr = $babDB->db_fetch_assoc($res)) {

				foreach($this->modified_primary_keys as $old_id_field => $new_id_field) {
					$arr['field_formula'] = str_replace('F'.$old_id_field, 'F'.$new_id_field, $arr['field_formula']);
				}
				
				$babDB->db_query('
					UPDATE '.FORM_TABLES_FIELDS.' 
					SET 
						field_formula='.$babDB->quote($arr['field_formula']).' 
					WHERE 
						id='.$babDB->quote($arr['id']).'
				');
			}
		}
	}
}






/**
 * Tables
 * table fields
 * link to ovidentia directory fields
 */
class form_wsOtable_field_directory extends form_wsOtable_field {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Table field directory');
	}
	
	
	function getItems() {
		global $babDB;
		
		$id_field = parent::getItems();
		
		$res = $babDB->db_query('
		
		SELECT 
			f.id_table_field id,
			f.directory_field name 
			
		FROM 
			'.FORM_TABLES_FIELDS_DIRECTORY.' f 
		WHERE 
			f.id_table_field IN('.$babDB->quote(array_keys($id_field)).') 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOtable_field_directory() {
		$this->tablename = FORM_TABLES_FIELDS_DIRECTORY;
	}
	
	
	
}






/**
 * Tables
 * table filter
 */
class form_wsOtable_filter extends form_wsOtable {

	function getSub() {
		return array(
			'table_filter_field'
		);
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Table filters');
	}
	
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('
		
		SELECT 
			f.id,
			f.name 
			
		FROM 
			'.FORM_TABLES_FILTERS.' f, 
			'.FORM_TABLES.' t, 
			'.FORM_WORKSPACE_ENTRIES." w 
		WHERE 
			t.id=f.id_table 
			AND w.type='table' 
			AND w.id_object=t.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).' 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOtable_filter() {
		$this->tablename = FORM_TABLES_FILTERS;
	}
	
	
	function record() {
		form_wsOworkspace::record();
	}
	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;


		foreach($this->modified_primary_keys as $old_id_filter => $new_id_filter) {
		
			$type = substr(get_class($instance),8);

			switch($type) {
			
				case 'table_filter_field':
					$instance->updateRef('id_filter', $old_id_filter, $new_id_filter);
					
					break;

				case 'form_field':
					$instance->updateRef('id_list_filter', $old_id_filter, $new_id_filter);
					break;

			}
		}
	}
}








/**
 * Tables
 * table filter
 * fields
 */
class form_wsOtable_filter_field extends form_wsOtable_filter {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Table filter fields');
	}
	
	
	function getItems() {
		global $babDB;
		
		$filters = parent::getItems();
		
		$res = $babDB->db_query('
		
		SELECT 
			f.id 
		FROM 
			'.FORM_TABLES_FILTERS_FIELDS.' f 
		WHERE 
			f.id_filter IN('.$babDB->quote(array_keys($filters)).') 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['id'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOtable_filter_field() {
		$this->tablename = FORM_TABLES_FILTERS_FIELDS;
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		
	}
}







/**
 * Tables links
 */
class form_wsOtable_lnk extends form_wsOworkspace {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return true;
	}
	
	function getTypeTitle() {
		return form_translate('Tables links');
	}
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('SELECT 
			t.id,
			t.name, 
			w.id selected 
			
		FROM '.FORM_TABLES_LNK.' t 
		LEFT JOIN '.FORM_WORKSPACE_ENTRIES." w 
			ON w.type='table_lnk' 
			AND w.id_object=t.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).'
		
		ORDER BY t.name
		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => NULL !== $arr['selected']
			);
		}
		
		
		return $return;
	}
	
	function form_wsOtable_lnk() {
		$this->tablename = FORM_TABLES_LNK;
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;
		
	
		foreach($this->modified_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {
			
				case 'form_field':
					$instance->updateRef('id_lnk', $old_id, $new_id);
					break;
					
					
					
				case 'table_filter_field':
					$instance->updateRef('id_lnk', $old_id, $new_id);
					break;

			}
		}
	}
}



/**
 * Forms
 */
class form_wsOform extends form_wsOworkspace {

	function getSub() {
		return array(
			'form_field'
		);
	}
	
	function isSelectable() {
		return true;
	}
	
	function getTypeTitle() {
		return form_translate('Forms');
	}
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('SELECT 
			f.id,
			f.name, 
			w.id selected 
			
		FROM '.FORM_FORMS.' f 
		LEFT JOIN '.FORM_WORKSPACE_ENTRIES." w 
			ON w.type='form' 
			AND w.id_object=f.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).'
			
		ORDER BY f.name');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => NULL !== $arr['selected']
			);
		}
		
		
		return $return;
	}
	
	function form_wsOform() {
		$this->tablename = FORM_FORMS;
	}
	
	/**
	 * Delete objects when the workspace is deleted
	 * @param	array 	$id_objects		an array of deletable id_object
	 * @param	string	&$error
	 * @return 	boolean
	 */
	function delete($id_objects, &$error) {
		global $babDB;
		require_once $GLOBALS['babInstallPath']."admin/acl.php";
		
		foreach($id_objects as $id_form) {
			aclDelete(FORM_FORMS_GROUPS	, $id_form);
			$babDB->db_query('
				DELETE FROM '.FORM_FORMS.' WHERE id='.$babDB->quote($id_form).'
			');
		}

		return true;
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;

		foreach($this->modified_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {
			
				case 'app_step':
					$instance->updateRef('id_form', $old_id, $new_id);
					break;



				case 'form_field':
					$instance->updateRef('id_form', $old_id, $new_id);
					break;
			}
		}
	}
	
	
	
	/**
	 * Set access to forms
	 */
	function setAccess() {

		$items = $this->getItems();
		
		foreach($items as $id => $item) {
			aclSetGroups_registered(FORM_FORMS_GROUPS	, $id);
		}
	}
}


/**
 * Forms
 * Form fields
 */
class form_wsOform_field extends form_wsOform {

	function getSub() {
		return array(
			'field_ctrl'	
		);
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Form fields');
	}
	
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('
		
		SELECT 
			f.id,
			t.name form, 
			f.name 
			
		FROM 
			'.FORM_FORMS_FIELDS.' f, 
			'.FORM_FORMS.' t, 
			'.FORM_WORKSPACE_ENTRIES." w 
		WHERE 
			t.id=f.id_form 
			AND w.type='form' 
			AND w.id_object=t.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).' 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['form'].'.'.$arr['name'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOform_field() {
		$this->tablename = FORM_FORMS_FIELDS;
	}
	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;

		foreach($this->modified_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {
			
				case 'app_links':
					$instance->updateRef('id_field', $old_id, $new_id);
					$instance->updateRef('id_if_field', $old_id, $new_id);
					$instance->updateRef('id_index', $old_id, $new_id);
					
					break;
					

				case 'app_step_cases':
					
					$instance->updateRef('id_form_field', $old_id, $new_id);
					$instance->updateRef('id_form_field_validation', $old_id, $new_id);
					break;
					
					
				case 'app_step_approb':
					$instance->updateRef('id_form_field', $old_id, $new_id);
					break;
					
					
				case 'field_ctrl':
					
					// bug fix, wrong table name in export
					// new exports will not have this error
					
					if ($instance->tablename !== FORM_FORMS_FIELDS_CTRL) {
						$instance->tablename = FORM_FORMS_FIELDS_CTRL;
					}

					$instance->updateRef('id_field', $old_id, $new_id);
					
					break;
					
				case 'form':
					$instance->updateRef('id_order_by'	, $old_id, $new_id);
					$instance->updateRef('id_order_by2'	, $old_id, $new_id);
					$instance->updateRef('id_group_by'	, $old_id, $new_id);
					$instance->updateRef('id_group_by2'	, $old_id, $new_id);
					break;

			}
		}
	}
}








/**
 * Forms help
 */
class form_wsOform_help extends form_wsOworkspace {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return true;
	}
	
	function getTypeTitle() {
		return form_translate('Form help popups');
	}
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('SELECT 
			f.id,
			f.name, 
			w.id selected 
			
		FROM '.FORM_HELP.' f 
		LEFT JOIN '.FORM_WORKSPACE_ENTRIES." w 
			ON w.type='form_help' 
			AND w.id_object=f.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).'
			
		ORDER BY f.name
		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => NULL !== $arr['selected']
			);
		}
		
		
		return $return;
	}
	
	function form_wsOform_help() {
		$this->tablename = FORM_HELP;
	}
	
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;

		foreach($this->modified_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {

				case 'form_field':
					$instance->updateRef('field_help', $old_id, $new_id);
					
					break;

			}
		}
	}
}





/**
 * Forms
 * Form fields
 * Controls
 */
class form_wsOfield_ctrl extends form_wsOform_field {

	function form_wsOfield_ctrl() {
		$this->tablename = FORM_FORMS_FIELDS_CTRL;
	}
	
	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Form field controls');
	}
	
	
	function getItems() {
		global $babDB;
		
		$arr = parent::getItems();
		
		$res = $babDB->db_query('
			SELECT id FROM '.FORM_FORMS_FIELDS_CTRL.' WHERE id_field IN('.$babDB->quote(array_keys($arr)).')
		');
		
		$return = array();
		
		while ($arr = $babDB->db_fetch_array($res)) {
			
			$return[$arr['id']] = array(
				'name' 		=> $arr['id'],
				'selected' 	=> true
			);
		}
		
		return $return;
	}

	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
	
	function otherObject($instance) {
	
	}
}










/**
 * Applications
 */ 
class form_wsOapplication extends form_wsOworkspace {

	function getSub() {
		return array(
			'app_step',
			'gest_rights',
			'app_links'
		);
	}
	
	function isSelectable() {
		return true;
	}
	
	function getTypeTitle() {
		return form_translate('Applications');
	}
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('
		SELECT 
			a.id,
			a.name, 
			w.id selected 
			
		FROM '.FORM_APP_APPLICATIONS.' a 
		LEFT JOIN '.FORM_WORKSPACE_ENTRIES." w 
			ON w.type='application' 
			AND w.id_object=a.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).' 
		ORDER BY a.name
		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => NULL !== $arr['selected']
			);
		}
		
		
		return $return;
	}
	
	function form_wsOapplication() {
		$this->tablename = FORM_APP_APPLICATIONS;
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;

		foreach($this->modified_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {

				case 'app_links':
					$instance->updateRef('id_application', $old_id, $new_id);
					
					break;
					
					
				case 'app_step':
					$instance->updateRef('id_application', $old_id, $new_id);
					
					break;
					
					
				case 'gest_rights':
					$instance->updateRef('id_app', $old_id, $new_id);
					
				
					break;

			}
		}
	}
	
	
	
	
	/**
	 * Set access to applications
	 */
	function setAccess() {
	
		$items = $this->getItems();
		
		foreach($items as $id => $item) {
			aclSetGroups_registered(FORM_APP_GROUPS	, $id);
		}
	}
}












/**
 * Applications
 * gest rights
 */ 
class form_wsOgest_rights extends form_wsOapplication {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Applications');
	}
	
	function getItems() {
		global $babDB;
		
		$app = parent::getItems();
		
		foreach($app as $key => $arr) {
			if (!$arr['selected']) {
				unset($app[$key]);
			}
		}

		
		$res = $babDB->db_query('SELECT 
			id 
		FROM '.FORM_GEST_RIGHTS_APP.'  
		WHERE 
			id_app IN('.$babDB->quote(array_keys($app)).')
		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['id'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOgest_rights() {
		$this->tablename = FORM_GEST_RIGHTS_APP;
	}
	
	
	function record() {
		form_wsOworkspace::record();
	}
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;

		foreach($this->modified_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {

				case 'gest_rights':
					$instance->updateRef('id_right', $old_id, $new_id);
					
					break;

			}
		}
	}
	
	
}







/**
 * Applications
 * Application steps
 */ 
class form_wsOapp_step extends form_wsOapplication {

	function getSub() {
		return array(
			'app_step_btn',
			'app_step_cases',
			'app_step_approb',
			'app_step_menu'
		);
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Application steps');
	}
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('
		
		SELECT 
			f.id,
			t.name application, 
			f.name 
			
		FROM 
			'.FORM_APP_STEPS.' f, 
			'.FORM_APP_APPLICATIONS.' t, 
			'.FORM_WORKSPACE_ENTRIES." w 
		WHERE 
			t.id=f.id_application 
			AND w.type='application' 
			AND w.id_object=t.id 
			AND w.id_workspace=".$babDB->quote(form_getWorkspace()).' 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['application'].'.'.$arr['name'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOapp_step() {
		$this->tablename = FORM_APP_STEPS;
	}
	
	
	/**
	 * Delete objects when the workspace is deleted
	 * Overright this method for more complex object delete
	 * @param	array 	$id_objects		an array of deletable id_object
	 * @param	string	&$error
	 * @return 	boolean
	 */
	function delete($id_objects, &$error) {
	
		$db = &$GLOBALS['babDB'];
		require_once( $GLOBALS['babInstallPath']."utilit/wfincl.php");
	
		foreach($id_objects as $id_step) {
		
			$res = $db->db_query("SELECT id_line,id_schi,id_table FROM ".FORM_APPROB_SCHI." WHERE id_step='".$db->db_escape_string($id_step)."'");
			while ($arr = $db->db_fetch_array($res))
				{
				$db->db_query("UPDATE ".form_tbl_name($arr['id_table'])." SET form_approb='2' WHERE form_id='".$db->db_escape_string($arr['id_line'])."'");
				bab_WFDeleteInstance($arr['id_schi']);
				}
			$db->db_query("DELETE FROM ".FORM_APPROB_SCHI." WHERE id_step='".$db->db_escape_string($id_step)."'");
			$db->db_query("DELETE FROM ".FORM_APP_LINKS." WHERE id_step='".$db->db_escape_string($id_step)."'");
			$db->db_query("DELETE FROM ".FORM_APP_STEPS." WHERE id='".$db->db_escape_string($id_step)."'");
		}
		
		return true;
	}
	
	
	
	/**
	 * @param	object	$instance
	 */
	function otherObject($instance) {
		
		global $babDB;

		foreach($this->modified_primary_keys as $old_id => $new_id) {
		
			$type = substr(get_class($instance),8);

			switch($type) {
			
				case 'application':
					$instance->updateRef('id_first_step', $old_id, $new_id);
					
					break;
					

				case 'app_links':
					$instance->updateRef('id_step', $old_id, $new_id);
					
					break;
					
					
				case 'app_step':
					$instance->updateRef('switch_id_step', $old_id, $new_id);
					$instance->updateRef('id_approb_next_step', $old_id, $new_id);
					$instance->updateRef('id_validate_next_step', $old_id, $new_id);
					$instance->updateRef('id_register_next_step', $old_id, $new_id);
					$instance->updateRef('id_import_user_next_step', $old_id, $new_id);
					
					break;
					
					
				case 'app_step_cases':
					$instance->updateRef('id_step', $old_id, $new_id);
					$instance->updateRef('step', $old_id, $new_id);
					
					
					break;
					
				
				case 'app_step_btn':
					
					$instance->updateRef('id_step', $old_id, $new_id, 'id_btn');
					$instance->updateRef('btn_id_goto_step', $old_id, $new_id, 'id_btn');
					break;
					
					
				case 'app_step_approb':
					
					$instance->updateRef('id_step', $old_id, $new_id);
					
					break;

				
				case 'app_step_menu':
					$instance->updateRef('id_step', $old_id, $new_id);
					$instance->updateRef('id_menustep', $old_id, $new_id);
					
					break;

			}
		}
	}
}



/**
 * Applications
 * Application steps
 * buttons
 */ 
class form_wsOapp_step_btn extends form_wsOapp_step {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Application buttons');
	}
	
	function getItems() {
		global $babDB;
		
		// r�cup�rer les �tapes autoris�es
		$arr = parent::getItems();
		
		$res = $babDB->db_query('
			
			SELECT 
				b.id_btn,
				b.btn_name 
				
			FROM 
				'.FORM_APP_STEP_BTN.' b 
			WHERE 
				b.id_step IN('.$babDB->quote(array_keys($arr)).') 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id_btn']] = array(
				'name' => $arr['btn_name'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOapp_step_btn() {
		$this->tablename = FORM_APP_STEP_BTN;
	}
	
	
	function otherObject($instance) {
	
	}
	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
}




/**
 * Applications
 * Application steps
 * test cases
 */ 
class form_wsOapp_step_cases extends form_wsOapp_step {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Application buttons');
	}
	
	function getItems() {
		global $babDB;
		
		// r�cup�rer les �tapes autoris�es
		$arr = parent::getItems();
		
		$res = $babDB->db_query('
			
			SELECT 
				id
			FROM 
				'.FORM_APP_STEP_CASES.'  
			WHERE 
				id_step IN('.$babDB->quote(array_keys($arr)).') 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['id'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOapp_step_cases() {
		$this->tablename = FORM_APP_STEP_CASES;
	}
	
	function otherObject($instance) {
	
	}
	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
}






/**
 * Applications
 * Application steps
 * contextual menu items
 */ 
class form_wsOapp_step_menu extends form_wsOapp_step {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Contextual menu items');
	}
	
	function getItems() {
		global $babDB;
		
		// r�cup�rer les �tapes autoris�es
		$arr = parent::getItems();
		
		$res = $babDB->db_query('
			
			SELECT 
				id,
				name 
			FROM 
				'.FORM_APP_STEP_MENU.'  
			WHERE 
				id_step IN('.$babDB->quote(array_keys($arr)).') 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOapp_step_menu() {
		$this->tablename = FORM_APP_STEP_MENU;
	}
	
	
	function otherObject($instance) {
	
	}
	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
}



/**
 * Applications
 * Application steps
 * approbation test cases
 */ 
class form_wsOapp_step_approb extends form_wsOapp_step {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Contextual menu items');
	}
	
	function getItems() {
		global $babDB;
		
		// r�cup�rer les �tapes autoris�es
		$arr = parent::getItems();
		
		$res = $babDB->db_query('
			
			SELECT 
				id 
			FROM 
				'.FORM_APP_STEP_APPROB.'  
			WHERE 
				id_step IN('.$babDB->quote(array_keys($arr)).') 

		');
			
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['id'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOapp_step_approb() {
		$this->tablename = FORM_APP_STEP_APPROB;
	}
	
	function otherObject($instance) {
	
	}
	
	function delete($id_objects, &$error) {
		form_wsOworkspace::delete($id_objects, $error);
	}
}















/**
 * Application Links
 */ 
class form_wsOapp_links extends form_wsOapplication {

	function getSub() {
		return array();
	}
	
	function isSelectable() {
		return false;
	}
	
	function getTypeTitle() {
		return form_translate('Application links');
	}
	
	function getItems() {
		global $babDB;
		
		$res = $babDB->db_query('
			SELECT 
				l.id 
			FROM 
				'.FORM_APP_LINKS.' l,  
				'.FORM_WORKSPACE_ENTRIES." w 
			WHERE 
				w.type='application' 
				AND w.id_object=l.id_application 
				AND w.id_workspace=".$babDB->quote(form_getWorkspace()).' 
		');
		
		$return = array();
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$return[$arr['id']] = array(
				'name' => $arr['id'],
				'selected' => true
			);
		}

		return $return;
	}
	
	function form_wsOapp_links() {
		$this->tablename = FORM_APP_LINKS;
	}
	
	function otherObject($instance) {
	
	}
}

?>