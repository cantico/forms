<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';


/**
 * Step type class for default step functionalities
 * All step types are inherited from this class
 * interface
 */
class form_stepType {

	
	
	
	/**
	 * Call step parameters editor
	 * @static
	 * @param	int	$id_app
	 * @param	int	$id_step	step to edit (0 for new step)
	 * @return	string	html
	 */
	function getEditHtml($id_app, $id_step) {
		return 'step type implementation error';
	}
	
	/**
	 * record the edit form 
	 * if false is returned, stay on the same page
	 * @return boolean
	 */
	function record() {
		return false;
	}

	/**
	 * Application step activation
	 * @static
	 * @param	form_stepFlow	$stepFlow	all stored step informations
	 * @return	boolean
	 */
	function fire($stepFlow) {
		return false;
	}
	
}


/**
 * Step flow object
 * Used to transport informations within cascading steps 
 */
class form_stepFlow {

	/** 
	 * @public
	 */
	var $id_step;
	
	/**
	 * @private
	 */
	var $arr_step = NULL;
	

	
	
	function form_stepFlow() {
		$this->id_step = NULL;
	}


	/**
	 * set step flow informations
	 * @param	int		$id_step
	 * @return	array
	 */
	function getStep() {
	
	
		
		if (NULL === $this->arr_step) {

			global $babDB;
			
			$req = "SELECT 
					s.name,
					s.id_application, 
					s.id_type, 
					s.id_form, 
					f.id_type idx, 
					s.redirect_url, 
					s.switch_id_step, 
					s.id_approb_next_step,
					s.id_approb_form,
					s.id_approb_sh,
					s.approb_mail_subject, 
					s.approb_change, 
					s.id_validate_next_step, 
					s.id_register_next_step,
					s.id_error_register_next_step,
					s.id_import_user_next_step, 
					s.register_delete_invalid,  
					s.id_paybox_system,  
					s.id_paybox_total,  
					s.id_paybox_cmd,  
					s.id_paybox_porteur,  
					s.id_paybox_step_effectue,  
					s.id_paybox_step_annule,  
					s.id_paybox_step_refuse 
					FROM ".FORM_APP_STEPS." s 
				LEFT JOIN ".FORM_FORMS." f 
					ON s.id_form=f.id 
				WHERE s.id='".$babDB->db_escape_string($this->id_step)."'";
				
			$this->arr_step = $babDB->db_fetch_assoc($babDB->db_query($req));
		
		}

		return $this->arr_step;
	}
	
	
	/**
	 * Fire a new step
	 * @param	int		$id_step
	 * @return 	boolean	
	 */
	function gotoStep($id_step) {
		if ($this->id_step == $id_step) {
			return false;
		}
		
		form_currentIdStep($id_step);

		// change context
		
		$this->id_step = $id_step;
		$this->arr_step = NULL;
		$step = $this->getStep();
		$id_type = (int) $step['id_type'];
		include_once $GLOBALS['babAddonPhpPath'].'steps/'.$id_type.'.php';
		$classname = 'form_stepType_'.$id_type;
		
		// fire step
		
		return call_user_func_array(
			array($classname, 'fire'), 
			array(&$this)
		);
	
	}
	
	
	/**
	 * @param	string	$str
	 */
	function addError($str) {
		global $babBody;
		$babBody->addError($str);
	}
}


/**
 * Contener for template variables
 * Edit step
 */
class form_stepTypeTemplate {

	function form_stepTypeTemplate($id_app, $id_step) {
	
		$this->id_app = $id_app;
		$this->id_step = $id_step;
	
		$this->t_name = form_translate('Name');
		$this->t_description = form_translate('Description');
		$this->t_form = form_translate('Form');
		$this->t_id_form = form_translate('Form');
		$this->t_btn_previous = form_translate('Previous button');
		$this->t_disabled = form_translate('Disabled');
		$this->t_btn_next = form_translate('Next button');
		$this->t_id_form_field = form_translate('Previous field');
		$this->t_id_form_field_validation = form_translate('Validation');
		$this->t_form_field_validation_param = form_translate('Param.');
		$this->t_field_value = form_translate('Value');
		$this->t_disabled = form_translate('Disabled');
		$this->t_del = form_translate('Del.');
		$this->t_add = form_translate('Add');
		$this->t_fields = form_translate('Fields');
		$this->t_ok = form_translate('Ok');
		$this->t_record = form_translate('Record');
		$this->t_close = form_translate('Close');
		$this->t_delete = false;
		$this->t_type = form_translate('Type');
		$this->t_delete = form_translate('Delete');
		$this->t_step = form_translate('Step');
		$this->t_default_step = form_translate('Default step');
		$this->t_id_article = form_translate('Article');
		$this->t_redirect_url = form_translate('Redirect url');
		$this->t_btn_name = form_translate('Button name');
		$this->t_btn_type = form_translate('type');
		$this->t_btn = form_translate('button(s)');
		$this->t_help = form_translate('Help');
		$this->t_id_approb_next_step = form_translate('Next step');
		$this->t_id_approb_sh = form_translate('Default approbation schema');
		$this->t_id_approb_form = form_translate('Default validation form');
		$this->t_id_sh = form_translate('Approbation schema');
		$this->t_id_export_next_step = form_translate('Next step');
		$this->t_menu_name = form_translate('Menu name');
		$this->t_menu_id_menustep = form_translate('Menu step');
		$this->t_menu_selmenu = form_translate('Selection');
		$this->t_approb_mail_subject = form_translate('Mail subject for approvers');
		$this->t_valid_mail_subject = form_translate('Mail subject for granted requests');
		$this->t_refused_mail_subject = form_translate('Mail subject for refused requests');
		$this->t_approb_change = form_translate('In case of modification');
		$this->t_approb_change_0 = form_translate('Do nothing');
		$this->t_approb_change_1 = form_translate('Create workflow instance with the new user');
		$this->t_approb_change_2 = form_translate('Create workflow instance with the original user');
		$this->t_approb_mail_applicant = form_translate('Email to applicant');
		$this->t_approb_mail = form_translate('others email to notify');
		$this->t_approb_mail_group = form_translate('ovidentia group to notify');
		$this->t_id_validate_next_step = form_translate('Next step');
		$this->t_id_register_next_step = form_translate('Next step');
		$this->t_id_error_register_next_step = form_translate('On error goto step');
		$this->t_id_import_user_next_step = form_translate('Next step');
		$this->t_ovml_file = form_translate('Ovml file');
		$this->t_register_delete_invalid = form_translate('Delete row if user cannot be created');
		

		$this->js_title = form_translate('You must set a title');
		$this->js_alert = form_translate('Do you really want to delete these items?');

		$this->js_delete = form_translate('Do you really want to delete this step').'?';
		
		$this->db = &$GLOBALS['babDB'];
		
		if (!empty($this->id_step))
			{
			$this->arr = $this->db->db_fetch_array($this->db->db_query("
				SELECT 
					s.*,
					a.name application_titre  
				FROM 
					".FORM_APP_STEPS." s,
					".FORM_APP_APPLICATIONS." a 
				WHERE 
					s.id='".$this->db->db_escape_string($this->id_step)."' 
					AND a.id=s.id_application
			"));
			
			$this->application_titre = bab_toHtml($this->arr['application_titre']);
			}
		else
			{
			$app = $this->db->db_fetch_assoc($this->db->db_query('
				SELECT name FROM '.FORM_APP_APPLICATIONS.' WHERE id='.$this->db->quote($this->id_app).'
			'));
			$this->arr = array();
			$this->application_titre = bab_toHtml($app['name']);
			}
			
			

		
		
		
			
			
		$default = array('approb_mail_applicant' => 1);

		$el_to_init = array('nb_fields','name', 'description', 'default_form', 'application_titre', 'id_form', 'redirect_url', 'id_type','switch_id_step','approb_mail_subject', 'valid_mail_subject', 'refused_mail_subject', 'approb_change', 'approb_mail_applicant', 'approb_mail', 'approb_mail_group','ovml_file');

		foreach ($el_to_init as $field) {
			$this->arr[$field] = isset($this->arr[$field]) ? $this->arr[$field] : (isset($_POST[$field]) ? $_POST[$field] : (isset($default[$field]) ? $default[$field] : ''));
		}
		
		
		
	}
}


?>