<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
	
function form_upgrade()
	{

	if (!is_dir($GLOBALS['babAddonUpload'])) {
		bab_mkdir($GLOBALS['babAddonUpload']);
	}

	global $babBody;
	$db = $GLOBALS['babDB'];
	$res = array();

	$db->db_query("DROP TABLE IF EXISTS `form_forms_fields_validation`");
	$db->db_query("DROP TABLE IF EXISTS `form_sql_types`");
	$db->db_query("DROP TABLE IF EXISTS `form_app_step_type`");
	$db->db_query("DROP TABLE IF EXISTS `form_forms_fields_print`");
	$db->db_query("DROP TABLE IF EXISTS `form_forms_fields_init`");
	$db->db_query("DROP TABLE IF EXISTS `form_forms_types`");
	$db->db_query("DROP TABLE IF EXISTS `form_trans_fields`");
	$db->db_query("DROP TABLE IF EXISTS `form_forms_elements`");
	$db->db_query("DROP TABLE IF EXISTS `form_app_step_btn_type`");
	$db->db_query("DROP TABLE IF EXISTS `form_forms_fields_classname`");
	$db->db_query("DROP TABLE IF EXISTS `form_tables_fields_types_extend`");

	
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_applications'"));
	if ( $arr[0] != 'form_app_applications' )
		$res[] = $db->db_query("
					  CREATE TABLE `form_app_applications` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_app_type` int(10) unsigned NOT NULL default '0',
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  `https` enum('Y','N') NOT NULL default 'N',
					  `lastupdate` datetime NOT NULL default '0000-00-00 00:00:00',
					  `id_first_step` int(10) unsigned NOT NULL default '0',
					  PRIMARY KEY  (`id`),
					  KEY `id_app_type` (`id_app_type`)
					)
					");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_applications https"));
		if ( $arr[0] != 'https' )
			{
			$db->db_query("ALTER TABLE `form_app_applications` ADD https enum('Y','N') NOT NULL default 'N'");
			}
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_groups'"));
	if ( $arr[0] != 'form_app_groups' )
		$res[] = $db->db_query("
					  CREATE TABLE `form_app_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					)
					");
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_links'"));
	if ( $arr[0] != 'form_app_links' )
		{
		$res[] = $db->db_query("
					  CREATE TABLE `form_app_links` (
						  `id` int(10) unsigned NOT NULL auto_increment,
						  `id_application` int(10) unsigned NOT NULL default '0',
						  `id_field` int(10) unsigned NOT NULL default '0',
						  `id_step` int(10) unsigned NOT NULL default '0',
						  `id_index` int(10) unsigned NOT NULL default '0',
						  `id_if_field` int(10) unsigned NOT NULL default '0',
						  `if_operator` tinyint(3) unsigned NOT NULL default '0',
						  `if_value` varchar(255) NOT NULL default '',
						  PRIMARY KEY  (`id`),
						  KEY `id_field` (`id_field`,`id_step`),
						  KEY `id_application` (`id_application`)
						)
					");
			}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_links id_index"));
	if ( $arr[0] != 'id_index' )
		{
		$db->db_query("ALTER TABLE `form_app_links` ADD `id_index` INT UNSIGNED NOT NULL");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_links id_if_field"));
	if ( $arr[0] != 'id_if_field' )
		{
		$db->db_query("ALTER TABLE `form_app_links` ADD `id_if_field` INT UNSIGNED NOT NULL , ADD `if_operator` TINYINT UNSIGNED NOT NULL , ADD `if_value` VARCHAR( 255 ) NOT NULL");
		}

	
	
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_step_cases'"));
	if ( $arr[0] != 'form_app_step_cases' )
		{
		$res[] = $db->db_query("
					  CREATE TABLE `form_app_step_cases` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_step` int(10) unsigned NOT NULL default '0',
					  `id_form_field` int(10) unsigned NOT NULL default '0',
					  `id_form_field_validation` int(11) unsigned NOT NULL default '0',
					  `form_field_validation_param` varchar(255) NOT NULL default '',
					  `field_value` varchar(255) NOT NULL default '',
					  `step` int(10) unsigned NOT NULL default '0',
					  `message` text NOT NULL,
					  `disabled` enum('Y','N') NOT NULL default 'N',
					  PRIMARY KEY  (`id`)
					)
					");
		}
	else
		{
		$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_step_cases step"));
		if ( $arr[0] != 'step' )
			{
			$db->db_query("ALTER TABLE `form_app_step_cases` CHANGE `id_form` `step` INT( 10 ) UNSIGNED DEFAULT '0' NOT NULL");
			$db->db_query("ALTER TABLE `form_app_steps` ADD `switch_id_step` INT UNSIGNED NOT NULL");
			$db->db_query("ALTER TABLE `form_app_steps` ADD `id_article` INT UNSIGNED NOT NULL");
			$db->db_query("ALTER TABLE `form_app_steps` ADD `id_topic` INT UNSIGNED NOT NULL");
			}
		}
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_step_type'"));
	if ( $arr[0] != 'form_app_step_type' )
		{
		$res[] = $db->db_query("
					  CREATE TABLE `form_app_step_type` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  PRIMARY KEY  (`id`)
					)
					");
					
		$db->db_query("INSERT INTO `form_app_step_type` (`id`, `name`, `description`) VALUES 
					(1, 'Form', 'Form'),
					(2, 'Redirect', 'Redirect to url'),
					(3, 'Switch', 'Switch step'),
					(4, 'Workflow', 'Workflow'),
					(5, 'Export', 'List export'),
					(6, 'Contextual menu', 'Contextual menu'),
					(7, 'Validate row', 'Validate row'),
					(8, 'Register user', 'Register user into ovidentia directory'),
					(9, 'Import user', 'Import user into table'),
					(10, 'Paybox payment', 'Paybox payment gateway')
					");
		}

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_step_btn_type'"));
	if ( $arr[0] != 'form_app_step_btn_type' )
		{
		$res[] = $db->db_query("
					  CREATE TABLE `form_app_step_btn_type` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  PRIMARY KEY  (`id`)
					)
					");
					
		$db->db_query("
					INSERT INTO 
					`form_app_step_btn_type` 
						(`id`, `name`, `description`) 
					VALUES 
						(1, 'submit', 'submit form and go to step'),
						(2, 'goto', 'go to step'),
						(3, 'close', 'close window'),
						(4, 'previous', 'previous table row'),
						(5, 'next', 'next table row'),
						(6, 'disabled', 'button disabled'),
						(7, 'popup', 'Go to step into popup'),
						(10, 'no record', 'Submit without record'),
						(11, 'Print', 'Print window'),
						(12, 'Goto id', 'Goto step with same line'),
						(13, 'Popup id', 'Popup step with same line')
					");
		
		/*
		 * Disabled
		 * 
		  		(8, 'submit cart', 'Submit, empty cart, goto step'),
				(9, 'empty cart', 'empty shopping cart'),
		 * 
		 * */
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_steps'"));
	if ( $arr[0] != 'form_app_steps' )
		{
		$res[] = $db->db_query("
						CREATE TABLE `form_app_steps` (
						  `id` int(10) unsigned NOT NULL auto_increment,
						  `id_application` int(10) unsigned NOT NULL default '0',
						  `id_type` int(10) unsigned NOT NULL default '1',
						  `name` varchar(255) NOT NULL default '',
						  `description` text NOT NULL,
						  `id_form` int(10) unsigned NOT NULL default '0',
						  `message` text NOT NULL,
						  `redirect_url` varchar(255) NOT NULL default '',
						  `switch_id_step` int(10) unsigned NOT NULL default '0',
						  `id_approb_next_step` int(10) unsigned NOT NULL default '0',
						  `id_approb_form` int(10) unsigned NOT NULL default '0',
						  `id_approb_sh` int(10) unsigned NOT NULL default '0',
						  `approb_mail_subject` varchar(255) NOT NULL default '',
						  `valid_mail_subject` varchar(255) NOT NULL default '',
						  `refused_mail_subject` varchar(255) NOT NULL default '',
						  `approb_change` tinyint(1) NOT NULL default '0',
						  `approb_mail_applicant` tinyint(1) unsigned NOT NULL default '1',
						  `approb_mail` varchar(255) NOT NULL,
						  `approb_mail_group` int(10) unsigned NOT NULL default '0',
						  `id_validate_next_step` int(10) unsigned NOT NULL default '0',
						  `ovml_file` varchar(255) NOT NULL,
						  `id_register_next_step` int(10) unsigned NOT NULL,
						  `id_error_register_next_step` int(10) unsigned NOT NULL,
						  `register_delete_invalid` tinyint(3) unsigned NOT NULL default '1',
						  `id_import_user_next_step` int(10) unsigned NOT NULL,
						  PRIMARY KEY  (`id`),
						  KEY `id_form` (`id_form`),
						  KEY `id_application` (`id_application`),
						  KEY `id_type` (`id_type`)
						) 
					");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_steps id_approb_next_step"));
	if ( $arr[0] != 'id_approb_next_step' )
		{
		$res[] = $db->db_query("ALTER TABLE `form_app_steps` ADD `id_approb_next_step` INT UNSIGNED NOT NULL ,
		ADD `id_approb_form` INT UNSIGNED NOT NULL ,
		ADD `id_approb_sh` INT UNSIGNED NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_steps id_article"));
	if ( $arr[0] == 'id_article' )
		{
		$res[] = $db->db_query("ALTER TABLE `form_app_steps` DROP `id_topic`");
		$res[] = $db->db_query("ALTER TABLE `form_app_steps` DROP `id_article`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_steps approb_mail_subject"));
	if ( $arr[0] != 'approb_mail_subject' )
		{
		$db->db_query("ALTER TABLE `form_app_steps` ADD `approb_mail_subject` VARCHAR( 255 ) NOT NULL");
		
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_steps valid_mail_subject"));
	if ( $arr[0] != 'valid_mail_subject' )
		{
		$db->db_query("ALTER TABLE `form_app_steps` ADD `valid_mail_subject` VARCHAR( 255 ) NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `refused_mail_subject` VARCHAR( 255 ) NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `approb_change` TINYINT( 1 ) NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `approb_mail_applicant` TINYINT( 1 ) UNSIGNED NOT NULL default '1' , ADD `approb_mail` VARCHAR( 255 ) NOT NULL , ADD `approb_mail_group` INT UNSIGNED NOT NULL");


		$db->db_query("UPDATE `form_app_steps` SET approb_mail_applicant='1'");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_steps id_validate_next_step"));
	if ( $arr[0] != 'id_validate_next_step' )
		{
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_validate_next_step` INT UNSIGNED NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_steps ovml_file"));
	if ( $arr[0] != 'ovml_file' )
		{
		$db->db_query("ALTER TABLE `form_app_steps` ADD `ovml_file` VARCHAR( 255 ) NOT NULL");
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_step_btn'"));
	if ( $arr[0] != 'form_app_step_btn' )
		{
		$res[] = $db->db_query("
					  CREATE TABLE `form_app_step_btn` (
					  `id_btn` int(10) unsigned NOT NULL auto_increment,
					  `id_step` int(10) unsigned NOT NULL default '0',
					  `btn_name` varchar(64) NOT NULL default '',
					  `btn_id_type` int(10) unsigned NOT NULL default '0',
					  `btn_id_goto_step` int(10) unsigned NOT NULL default '0',
					  PRIMARY KEY  (`id_btn`)
					)
					");

		}
		
		
	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_app_steps btn_previous_id_step"));
	if ( $arr[0] == 'btn_previous_id_step' )
		{
		$res2 = $db->db_query("SELECT * FROM form_app_steps");
		while ($arr = $db->db_fetch_array($res2))
			{
			if (!empty($arr['btn_previous_id_step']))
				{
				$db->db_query("INSERT INTO form_app_step_btn (id_step,btn_name,btn_id_type,btn_id_goto_step) VALUES ('".$db->db_escape_string($arr['id'])."','".$db->db_escape_string($arr['btn_previous'])."','2','".$db->db_escape_string($arr['btn_previous_id_step'])."')");
				}

			if (!empty($arr['btn_next_id_step']))
				{
				$db->db_query("INSERT INTO form_app_step_btn (id_step,btn_name,btn_id_type,btn_id_goto_step) VALUES ('".$db->db_escape_string($arr['id'])."','".$db->db_escape_string($arr['btn_next'])."','2','".$db->db_escape_string($arr['btn_next_id_step'])."')");
				}
			}

		$db->db_query("ALTER TABLE `form_app_steps`
				  DROP `btn_previous`,
				  DROP `btn_previous_id_step`,
				  DROP `btn_next`,
				  DROP `btn_next_id_step`,
				  DROP `btn_next_center`");
		}
		
		
	
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_config'"));
	if ( $arr[0] != 'form_config' )
		{
		$res[] = $db->db_query("
					  CREATE TABLE `form_config` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` varchar(255) NOT NULL default '',
					  `value` varchar(255) NOT NULL default '',
					  PRIMARY KEY  (`id`),
					  KEY `name` (`name`)
					)
					");
					
		$db->db_query("INSERT INTO `form_config` (`id`, `name`, `description`, `value`) VALUES (1, 'section_title', 'Title of the section and search', 'Applications'),(2, 'recipients_by_mail', 'Recipients by mail', '30'),(3, 'html_file', 'Html template file', 'template_constructor_css.html'),(4, 'date_format', 'date acquisition format', 'fr'),(5, 'transaction_api', 'Transaction service', '1'),(6, 'https_url', 'https url', 'https://localhost/')");
		}
	else
		{
		$db->db_query("UPDATE `form_config` SET value='template_constructor_css.html' WHERE id='3'");
		}

	$cfg = $db->db_query("SELECT id FROM form_config");
	
	$config_id = array();
	while(list($id) = $db->db_fetch_array($cfg))
		{
		$config_id[] = $id;
		}

	if (!in_array(1,$config_id))
		$db->db_query("INSERT INTO `form_config` VALUES (1, 'section_title', 'Title of the section', 'Applications')");
	if (!in_array(2,$config_id))
		$db->db_query("INSERT INTO `form_config` VALUES (2, 'recipients_by_mail', 'Recipients by mail', '30')");
	if (!in_array(3,$config_id))
		$db->db_query("INSERT INTO `form_config` VALUES (3, 'html_file', 'Html template file', 'template_constructor_css.html')");
	if (!in_array(4,$config_id))
		$db->db_query("INSERT INTO `form_config` VALUES (4, 'date_format', 'date acquisition format', 'fr')");
	if (!in_array(5,$config_id))
		$db->db_query("INSERT INTO `form_config` VALUES (5, 'transaction_api', 'Transaction service', '1')");
	if (!in_array(6,$config_id))
		$db->db_query("INSERT INTO `form_config` VALUES (6, 'https_url', 'https url', 'https://localhost/')");

	if (in_array(7,$config_id)) {
		$db->db_query("DELETE FROM `form_config` WHERE id='7'");
	}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_dbdir_fields'"));
	if ( $arr[0] != 'form_dbdir_fields' )
		{
		$res[] = $db->db_query("
					  CREATE TABLE `form_dbdir_fields` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` tinytext NOT NULL,
					  PRIMARY KEY  (`id`),
					  KEY `name` (`name`)
					)
					");
					
		$db->db_query("INSERT INTO `form_dbdir_fields` (`id`, `name`, `description`) VALUES (1, 'cn', 'Common Name'),
					(2, 'sn', 'Last Name'),
					(3, 'mn', 'Middle Name'),
					(4, 'givenname', 'First Name'),
					(6, 'email', 'E-mail Address'),
					(7, 'btel', 'Business Phone'),
					(8, 'mobile', 'Mobile Phone'),
					(9, 'htel', 'Home Phone'),
					(10, 'bfax', 'Business Fax'),
					(11, 'title', 'Title'),
					(12, 'departmentnumber', 'Department'),
					(13, 'organisationname', 'Company'),
					(14, 'bstreetaddress', 'Business Street'),
					(15, 'bcity', 'Business City'),
					(16, 'bpostalcode', 'Business Postal Code'),
					(17, 'bstate', 'Business State'),
					(18, 'bcountry', 'Business Country'),
					(19, 'hstreetaddress', 'Home Street'),
					(20, 'hcity', 'Home City'),
					(21, 'hpostalcode', 'Home Postal Code'),
					(22, 'hstate', 'Home State'),
					(23, 'hcountry', 'Home Country'),
					(24, 'user1', 'User 1'),
					(25, 'user2', 'User 2'),
					(26, 'user3', 'User 3')");
		}
		
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms' )
		$res[] = $db->db_query("
					  CREATE TABLE `form_forms` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_table` int(10) unsigned NOT NULL default '0',
					  `id_type` int(10) unsigned NOT NULL default '0',
					  `name` varchar(32) NOT NULL default '',
					  `description` varchar(255) NOT NULL default '',
					  `longdesc` text NOT NULL,
					  `mail` varchar(255) NOT NULL default '',
					  `mail_user` enum('Y','N') NOT NULL default 'N',
					  `id_table_field_mail` int(10) unsigned NOT NULL default '0',
					  `bcc` varchar(255) NOT NULL default '',
					  `id_table_field_bcc` int(10) unsigned NOT NULL default '0',
					  `id_group_mail` int(10) unsigned NOT NULL default '0',
					  `id_group_bcc` int(10) unsigned NOT NULL default '0',
					  `mail_subject` VARCHAR( 255 ) NOT NULL default '',
					  `buttons_next_previous` enum('Y','N') NOT NULL default 'Y',
					  `list_filed_rows` enum('HIDE','VIEWABLE','ALWAYS') NOT NULL default 'VIEWABLE',
					  `rows_per_page` int(4) NOT NULL default '0',
					  `id_order_by` int(11) unsigned NOT NULL default '0',
					  `id_order_by2` int(11) unsigned NOT NULL default '0',
					  `id_group_by` int(11) unsigned NOT NULL default '0',
					  `id_group_by2` int(11) unsigned NOT NULL default '0',
					  `id_search_description` int(10) unsigned NOT NULL default '0',
					  `order_type` enum('ASC','DESC') NOT NULL default 'ASC',
					  `order_type2` enum('ASC','DESC') NOT NULL default 'ASC',
					  `view_notempty` enum('Y','N') NOT NULL default 'Y',
					  `allow_delete` enum('Y','N') NOT NULL default 'Y',
					  `sc_formula` varchar(255) NOT NULL default '',
					  `trans_formula` varchar(255) NOT NULL default '',
					  `unit_concat_before` varchar(32) NOT NULL default '',
					  `unit_concat` varchar(32) NOT NULL default '',
					  `trans_formula_msg` varchar(255) NOT NULL default '',
					  `id_form_card` int(10) unsigned NOT NULL default '0',
					  `card_field_total` int(10) unsigned NOT NULL default '0',
					  `card_field_quantities` int(10) unsigned NOT NULL default '0',
					  `card_field_products_summary` int(10) unsigned NOT NULL default '0',
					  `card_field_transaction_id` int(10) unsigned NOT NULL default '0',
					  `card_field_status` int(10) unsigned NOT NULL default '0',
					  `card_field_product_name` int(10) unsigned NOT NULL default '0',
					  `css_file` varchar(64) NOT NULL default '',
					  `id_table_copy` int(11) NOT NULL default '0',
					  `waiting_modify` tinyint(3) unsigned NOT NULL default '0',
					  `delete_refused` tinyint(3) unsigned NOT NULL default '0',
					  `search_box` tinyint(3) unsigned NOT NULL default '0',
					  `approb_list_status` varchar(16) NOT NULL default '1',
					  PRIMARY KEY  (`id`),
					  UNIQUE KEY `name` (`name`),
					  KEY `id_table` (`id_table`),
					  KEY `id_type` (`id_type`)
					)
					");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms view_notempty"));
	if ( $arr[0] != 'view_notempty' )
		$db->db_query("ALTER TABLE `form_forms` ADD `view_notempty` ENUM( 'Y', 'N' ) DEFAULT 'Y' NOT NULL");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms allow_delete"));
	if ( $arr[0] != 'allow_delete' )
		$db->db_query("ALTER TABLE `form_forms` ADD `allow_delete` ENUM( 'Y', 'N' ) DEFAULT 'N' NOT NULL");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms mail_user"));
	if ( $arr[0] != 'mail_user' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `mail_user` ENUM( 'Y', 'N' ) DEFAULT 'N' NOT NULL AFTER `mail`");
		$db->db_query("ALTER TABLE `form_forms` ADD `id_table_field_mail` INT UNSIGNED NOT NULL AFTER `mail_user`");
		$db->db_query("ALTER TABLE `form_forms` ADD `id_table_field_bcc` INT UNSIGNED NOT NULL AFTER `bcc`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms card_field_total"));
	if ( $arr[0] != 'card_field_total' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `sc_formula` varchar(255) NOT NULL default ''");
		$db->db_query("ALTER TABLE `form_forms` ADD `trans_formula` varchar(255) NOT NULL default ''");
		$db->db_query("ALTER TABLE `form_forms` ADD `unit_concat` varchar(32) NOT NULL default ''");
		$db->db_query("ALTER TABLE `form_forms` ADD `trans_formula_msg` varchar(255) NOT NULL default ''");
		$db->db_query("ALTER TABLE `form_forms` ADD `id_form_card` int(10) unsigned NOT NULL default '0'");
		$db->db_query("ALTER TABLE `form_forms` ADD `card_field_total` int(10) unsigned NOT NULL default '0'");
		$db->db_query("ALTER TABLE `form_forms` ADD `card_field_quantities` int(10) unsigned NOT NULL default '0'");
		$db->db_query("ALTER TABLE `form_forms` ADD `card_field_products_summary` int(10) unsigned NOT NULL default '0'");
		$db->db_query("ALTER TABLE `form_forms` ADD `card_field_transaction_id` int(10) unsigned NOT NULL default '0'");
		$db->db_query("ALTER TABLE `form_forms` ADD `card_field_status` int(10) unsigned NOT NULL default '0'");
		$db->db_query("ALTER TABLE `form_forms` ADD `card_field_product_name` int(10) unsigned NOT NULL default '0'");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms css_file"));
	if ( $arr[0] != 'css_file' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `css_file` VARCHAR( 64 ) NOT NULL");
		}
		
	if (!bab_isTableField(FORM_FORMS,'unit_concat_before')) {
		$db->db_query("ALTER TABLE `form_forms` ADD `unit_concat_before` VARCHAR( 32 ) NOT NULL AFTER `trans_formula`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms id_table_copy"));
	if ( $arr[0] != 'id_table_copy' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `id_table_copy` INT NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms waiting_modify"));
	if ( $arr[0] != 'waiting_modify' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `waiting_modify` TINYINT NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms delete_refused"));
	if ( $arr[0] != 'delete_refused' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `delete_refused` TINYINT UNSIGNED NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms search_box"));
	if ( $arr[0] != 'search_box' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `search_box` TINYINT UNSIGNED NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms id_search_description"));
	if ( !is_array($arr) || $arr[0] != 'id_search_description' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `id_search_description` INT UNSIGNED NOT NULL AFTER `id_order_by`");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms mail_subject"));
	if ( !is_array($arr) || $arr[0] != 'mail_subject' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `mail_subject` VARCHAR( 255 ) NOT NULL AFTER `id_table_field_bcc`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms approb_list_status"));
	if ( !is_array($arr) || $arr[0] != 'approb_list_status' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `approb_list_status` VARCHAR( 16 ) NOT NULL default '1'");
		$db->db_query("UPDATE form_forms SET approb_list_status='1' WHERE id_type='1'");
		$db->db_query("UPDATE form_forms SET approb_list_status='0', id_type='1' WHERE id_type='8'");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms id_group_mail"));
	if ( !is_array($arr) || $arr[0] != 'id_group_mail' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `id_group_mail` INT UNSIGNED NOT NULL AFTER `id_table_field_bcc` , ADD `id_group_bcc` INT UNSIGNED NOT NULL AFTER `id_group_mail` ");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms form_approb_text"));
	if ( !is_array($arr) || $arr[0] != 'form_approb_text' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `form_approb_text` VARCHAR( 255 ) NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms id_order_by2"));
	if ( !is_array($arr) || $arr[0] != 'id_order_by2' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `id_order_by2` INT UNSIGNED NOT NULL AFTER `id_order_by` , ADD `id_group_by` INT UNSIGNED NOT NULL AFTER `id_order_by2` , ADD `id_group_by2` INT UNSIGNED NOT NULL AFTER `id_group_by`");

		$db->db_query("ALTER TABLE `form_forms` ADD `order_type2` ENUM( 'ASC', 'DESC' ) DEFAULT 'ASC' NOT NULL AFTER `order_type`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms mail_ignore_empty_values"));
	if ( !is_array($arr) || $arr[0] != 'mail_ignore_empty_values' )
		{
		$db->db_query("ALTER TABLE `form_forms` ADD `mail_ignore_empty_values` TINYINT UNSIGNED NOT NULL");
		}



					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_elements'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms_elements' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_forms_elements` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  `disabled` enum('Y','N') NOT NULL default 'N',
					  `id_print` tinyint(3) unsigned NOT NULL default '1',
					  `parameter` tinyint(3) unsigned NOT NULL default '0',
					  PRIMARY KEY  (`id`),
					  KEY `disabled` (`disabled`),
					  KEY `id_print` (`id_print`)
					)
					");
					
		$db->db_query("INSERT INTO `form_forms_elements` (`id`, `name`, `description`, `disabled`, `id_print`, `parameter`) VALUES 	
					(1, 'text', 'single line text field', 'N', 1, 0),
					(2, 'textarea', 'multi-line text field', 'N', 1, 0),
					(3, 'select', 'select', 'N', 1, 0),
					(4, 'selectmultiple', 'multiple select', 'N', 1, 0),
					(5, 'radio', 'radio buttons', 'N', 1, 0),
					(6, 'checkbox', 'check box', 'N', 1, 0),
					(7, 'file', 'file loader', 'N', 1, 0),
					(8, 'date', 'calendar popup', 'N', 1, 0),
					(9, 'readonly', 'Force readonly', 'N', 1, 0),
					(10, 'selectblank', 'select with blank value', 'N', 1, 0),
					(11, 'functionfield', 'Javascript function field', 'N', 1, 1),
					(12, 'functionlist', 'Javascript function list', 'N', 1, 1),
					(13, 'ovusers', 'Ovidentia users', 'N', 1, 0),
					(14, 'ovdirectory', 'Ovidentia directory', 'N', 1, 0),
					(15, 'password', 'Password', 'N', 1, 0),
					(16, 'selectdouble', 'Double select', 'N', 1, 0),
					(17, 'proposal', 'Predefined proposals', 'N', 1, 0)
			");
		}
		
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_fields'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms_fields' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_forms_fields` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_form` int(10) unsigned NOT NULL default '0',
					  `id_type` int(10) unsigned NOT NULL default '0',
					  `id_table_field` int(10) unsigned NOT NULL default '0',
					  `id_table_field_link` int(10) unsigned NOT NULL default '0',
					  `id_table_field_optgroup` int(10) unsigned NOT NULL default '0',
					  `id_lnk` int(10) unsigned NOT NULL default '0',
					  `id_table_field_lnk` int(10) unsigned NOT NULL default '0',
					  `id_form_element` int(11) unsigned NOT NULL default '1',
					  `id_proposal` int(11) unsigned NOT NULL default '0',
					  `id_list_filter` int(10) unsigned NOT NULL default '0',
					  `id_print` int(10) unsigned NOT NULL default '0',
					  `id_validation` int(10) unsigned NOT NULL default '0',
					  `id_transaction` int(10) unsigned NOT NULL default '0',
					  `transform_to` varchar(128) NOT NULL default '',
					  `validation_param` varchar(64) NOT NULL default '',
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  `alert` text NOT NULL,
					  `notempty` enum('Y','N') NOT NULL default 'N',
					  `mail` enum('Y','N') NOT NULL default 'Y',
					  `search` enum('Y','N') NOT NULL default 'Y',
					  `search_crit` tinyint(3) unsigned NOT NULL default '0',
					  `search_type` tinyint(3) unsigned NOT NULL default '0',
					  `field_use_key_val` varchar(128) NOT NULL default '0',
					  `init_dir` varchar(128) NOT NULL default '',
					  `init` varchar(255) NOT NULL default '',
					  `init_on_modify` enum('Y','N') NOT NULL default 'N',
					  `filter_on_dir` varchar(128) NOT NULL default '0',
					  `filter_on` varchar(255) NOT NULL default '',
					  `filter_type` tinyint(4) NOT NULL default '0',
					  `ordering` int(10) unsigned NOT NULL default '0',
					  `field_transaction` int(10) unsigned NOT NULL default '0',
					  `field_copy_table_field` int(10) unsigned NOT NULL default '0',
					  `field_copy_on_change` tinyint(3) unsigned NOT NULL default '0',
					  `field_link_to_fill` int(10) unsigned NOT NULL default '0',
					  `field_usemail` varchar(16) NOT NULL default '',
					  `field_mailonchange` tinyint(3) unsigned NOT NULL default '1',
					  `field_help` int(10) unsigned NOT NULL default '0',
					  `classname` varchar(128) NOT NULL default '',
					  `id_table_field_init` int(10) unsigned NOT NULL default '0',
					  `form_element_param` varchar( 255 ) NOT NULL default '',
					  `form_approb_description` tinyint( 1 ) UNSIGNED NOT NULL default '0',
					  `form_approb_refused` varchar( 255 ) NOT NULL default '',
					  `form_approb_accepted` varchar( 255 ) NOT NULL default '',
					  `int_increment` tinyint UNSIGNED NOT NULL default '0',
					  PRIMARY KEY  (`id`),
					  KEY `id_form` (`id_form`),
					  KEY `id_print` (`id_print`),
					  KEY `id_table_field_link` (`id_table_field_link`),
					  KEY `form_approb_description` (`form_approb_description`),
					  KEY `int_increment` (`int_increment`)
					)
					");
	
	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_proposal"));
	if ( !is_array($arr) || $arr[0] != 'id_proposal' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `id_proposal` int(10) unsigned NOT NULL default '0' AFTER `id_form_element`");
	
	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_table_field_optgroup"));
	if ( !is_array($arr) || $arr[0] != 'id_table_field_optgroup' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `id_table_field_optgroup` int(10) unsigned NOT NULL default '0' AFTER `id_table_field_link`");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields notempty"));
	if ( !is_array($arr) || $arr[0] != 'notempty' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `notempty` ENUM( 'Y', 'N' ) DEFAULT 'N' NOT NULL AFTER `alert`");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields ordering"));
	if ( !is_array($arr) || $arr[0] != 'ordering' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `ordering` int(10) unsigned NOT NULL default '0' AFTER `alert`");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_transaction"));
	if ( !is_array($arr) || $arr[0] != 'field_transaction' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_transaction` int(10) unsigned NOT NULL default '0' AFTER `alert`");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_copy_table_field"));
	if ( !is_array($arr) || $arr[0] != 'field_copy_table_field' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_copy_table_field` INT UNSIGNED NOT NULL");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_help"));
	if ( !is_array($arr) || $arr[0] != 'field_help' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_help` INT UNSIGNED NOT NULL");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_copy_on_change"));
	if ( !is_array($arr) || $arr[0] != 'field_copy_on_change' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_copy_on_change` TINYINT UNSIGNED NOT NULL");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_link_to_fill"));
	if ( !is_array($arr) || $arr[0] != 'field_link_to_fill' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_link_to_fill` INT UNSIGNED NOT NULL");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_usemail"));
	if ( !is_array($arr) || $arr[0] != 'field_usemail' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_usemail` VARCHAR( 16 ) NOT NULL");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_mailonchange"));
	if ( !is_array($arr) || $arr[0] != 'field_mailonchange' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_mailonchange` TINYINT UNSIGNED DEFAULT '1' NOT NULL");
		$db->db_query("UPDATE `form_forms_fields` SET `field_mailonchange`='1'");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_table_field_lnk"));
	if ( !is_array($arr) || $arr[0] != 'id_table_field_lnk' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `id_table_field_lnk` INT UNSIGNED NOT NULL AFTER `id_table_field_link`");
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `id_lnk` INT UNSIGNED NOT NULL AFTER `id_table_field_link`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields filter_type"));
	if ( !is_array($arr) || $arr[0] != 'filter_type' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `filter_type` TINYINT NOT NULL AFTER `filter_on`");
		}
	
	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields search_crit"));
	if ( !is_array($arr) || $arr[0] != 'search_crit' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `search_crit` TINYINT UNSIGNED NOT NULL AFTER `search`");


		$restmp = $db->db_query("SELECT fo.id id_form, f.id id_field FROM form_forms fo,form_tables_fields t, form_forms_fields f WHERE fo.id_order_by=t.id AND t.id=f.id_table_field AND fo.id=f.id_form AND t.id_table=fo.id_table");
		while ($arr = $db->db_fetch_array($restmp))
			{
			$db->db_query("UPDATE form_forms SET id_order_by='".$arr['id_field']."' WHERE id='".$arr['id_form']."'");
			}
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields search_type"));
	if ( !is_array($arr) || $arr[0] != 'search_type' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `search_type` TINYINT UNSIGNED NOT NULL AFTER `search_crit`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_use_key_val"));
	if ( !is_array($arr) || $arr[0] != 'field_use_key_val' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `field_use_key_val` varchar(128) NOT NULL default '0' AFTER `search_type`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields field_use_key"));
	if ($arr[0] == 'field_use_key' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` DROP `field_use_key`"); 
		}

	


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_list_filter"));
	if ( !is_array($arr) || $arr[0] != 'id_list_filter' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `id_list_filter` INT UNSIGNED NOT NULL AFTER `id_form_element`");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields classname"));
	if ( !is_array($arr) || $arr[0] != 'classname' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `classname` varchar(128) NOT NULL default ''");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_table_field_init"));
	if ( !is_array($arr) || $arr[0] != 'id_table_field_init' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `id_table_field_init` INT UNSIGNED NOT NULL");

		$tmp = $db->db_query("SELECT id, id_table_field_link FROM `form_forms_fields`");
		while ($arr = $db->db_fetch_array($tmp))
			{
			$db->db_query("UPDATE form_forms_fields SET id_table_field_init='".$arr['id_table_field_link']."' WHERE id='".$arr['id']."'");
			}
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields form_element_param"));
	if ( !is_array($arr) || $arr[0] != 'form_element_param' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `form_element_param` VARCHAR( 255 ) NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_transaction"));
	if ( !is_array($arr) || $arr[0] != 'id_transaction' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `id_transaction` INT UNSIGNED NOT NULL AFTER `id_print`");
		}
	
	

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields form_approb_description"));
	if ( !is_array($arr) || $arr[0] != 'form_approb_description' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `form_approb_description` TINYINT( 1 ) UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_forms_fields` ADD INDEX ( `form_approb_description` )");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields form_approb_refused"));
	if ( !is_array($arr) || $arr[0] != 'form_approb_refused' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `form_approb_refused` VARCHAR( 255 ) NOT NULL , ADD `form_approb_accepted` VARCHAR( 255 ) NOT NULL");
		}


	$arr2 = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_transform"));
	$arr3 = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields id_transform_from"));
	
	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields transform_to"));
	if ( !is_array($arr) || $arr[0] != 'transform_to' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `transform_to` VARCHAR( 128 ) NOT NULL AFTER `id_transaction`");
		
		if ( $arr2[0] == 'id_transform' && $arr3[0] != 'id_transform_from' )
			{
			// si id_transform_from n'existe pas :

			$restmp = $db->db_query("SELECT f.id, t.id transform, d.name FROM form_forms_fields f, form_forms_fields_transform t LEFT JOIN  form_dbdir_fields d ON d.id=f.id_transform WHERE f.id_transform=t.id");
			while($arr = $db->db_fetch_assoc($restmp)) {

				switch ($arr['transform']) {
					case 1:
						$name = 'user';
						break;
					case 2:
						$name = 'cn';
						break;
					case 3:
						$name = 'sn';
						break;
					case 4:
						$name = 'mn';
						break;
					case 5:
						$name = 'givenname';
						break;
					default:
						$name = $arr['name'];
						break;
					}

				$db->db_query("UPDATE form_forms_fields SET transform_to='".$name."' WHERE id='".$arr['id']."'");
				}
			}
		}

	// si id_transform existe :
	if ( $arr2[0] == 'id_transform' )
		{
		$db->db_query("DROP TABLE IF EXISTS `form_forms_fields_transform`");
		$db->db_query("ALTER TABLE `form_forms_fields` DROP `id_transform`");
		}

	if ( $arr3[0] == 'id_transform_from') {
		// si id_transform_from existe, transform_to reste le meme :
		$db->db_query("ALTER TABLE `form_forms_fields` DROP `id_transform_from`");
		}




	 $arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields int_increment"));
	if ( !is_array($arr) || $arr[0] != 'int_increment' )
		{
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `int_increment` TINYINT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_forms_fields` ADD INDEX ( `int_increment` )");
		}



	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_fields_classname'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms_fields_classname' )
		{
		$res[] = $db->db_query("
						CREATE TABLE `form_forms_fields_classname` (
						`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
						`name` VARCHAR( 255 ) NOT NULL ,
						`description` VARCHAR( 255 ) NOT NULL ,
						PRIMARY KEY ( `id` )
						)
					");
					


		$db->db_query("INSERT INTO `form_forms_fields_classname` (`id`, `name`, `description`) 
		VALUES 
			(1, 'textbox', 'Text box with border'),
			(2, 'colored', 'Colored text'),
			(3, 'fieldtitle', 'Field as title'),
			(4, 'shortfield', 'Short text field'),
			(5, 'longfield', 'Long text field')
		");

		}

					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_fields_print'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms_fields_print' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_forms_fields_print` (
						  `id` int(10) unsigned NOT NULL auto_increment,
						  `name` varchar(255) NOT NULL default '',
						  `description` text NOT NULL,
						  `ids_type` varchar(255) NOT NULL default '',
						  `widget` tinyint(1) NOT NULL default '0',
						  PRIMARY KEY  (`id`),
						  KEY `ids_type` (`ids_type`),
						  KEY `widget` (`widget`)
						)
					");
					
		$db->db_query("
				INSERT INTO `form_forms_fields_print` (`id`, `name`, `description`, `ids_type`, `widget`) 
				VALUES 
				(1, 'View', 'Default mode', ',1,2,3,4,5,6,7,8,', 0),
				(2, 'Disable', 'Field disabled', ',1,2,3,4,5,6,7,8,', 0),
				(3, 'Optional hide', 'hide field option', ',1,8,', 0),
				(4, 'Optional view', 'view field option', ',1,8,', 0),
				(5, 'Link', 'Link to another form', ',1,5,6,8,', 0),
				(6, 'Read only', 'Read only', ',2,4,7,', 0),
				(7, 'Hidden', 'hidden field', ',1,2,3,4,7,', 0),
				(8, 'Popup link', 'Open form into popup', ',1,5,6,8,', 0),
				(9, 'Image view', 'Image view', ',2,3,4,7,', 0),
				(10, 'Files directory', 'Files directory', ',2,7,', 1),
				(11, 'Read-only directory', 'Read-only file directory', ',2,7,3,', 1),
				(12, 'Captcha', 'Captcha', ',2,7,', 1)
			");
		}
		
		
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_fields_validation'"));
	if (!is_array($arr) || $arr[0] != 'form_forms_fields_validation' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_forms_fields_validation` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  `javascript` text NOT NULL,
					  `php` text NOT NULL,
					  `unique_key` enum('Y','N') NOT NULL default 'N',
					  PRIMARY KEY  (`id`)
					)
					");

		$db->db_query(implode("", @file($GLOBALS['babInstallPath'].'addons/forms/validations.sql')));
		}
		
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms_groups' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_forms_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					)
					");
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_types'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms_types' )
		{
		$res[] = $db->db_query("
					 CREATE TABLE `form_forms_types` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  `curl_required` enum('Y','N') NOT NULL default 'N',
					  PRIMARY KEY  (`id`)
					)
					");

		$db->db_query("
			INSERT INTO `form_forms_types` 
				(`id`, `name`, `description`, `curl_required`) 
			VALUES 
				(1, 'list', 'List table', 'N'),
				(2, 'edit', 'Create and modify', 'N'),
				(3, 'view', 'View', 'N'),
				(4, 'transaction', 'Payment transaction', 'Y'),
				(5, 'shoppingcart', 'Shopping cart', 'Y'),
				(6, 'shoppinglist', 'Shopping list', 'Y'),
				(7, 'approbation', 'Approbation', 'N')
			");	
		}




		
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_gest_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_gest_groups' )
		{
		$res[] = $db->db_query("
					 CREATE TABLE `form_gest_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					)
					");

		$db->db_query("INSERT INTO `form_gest_groups` VALUES (1, 1, 3)");
		$db->db_query("INSERT INTO `form_gest_groups` VALUES (2, 2, 0)");
        

		}
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_sql_types'"));
	if ( !is_array($arr) || $arr[0] != 'form_sql_types' )
		{
		$res[] = $db->db_query("
					 CREATE TABLE `form_sql_types` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(128) NOT NULL default '',
					  `description` text NOT NULL,
					  `id_forms_elements` varchar(255) NOT NULL default 'text',
					  `form_size` enum('Y','N') NOT NULL default 'Y',
					  `form_deci` enum('Y','N') NOT NULL default 'N',
					  `form_vals` enum('Y','N') NOT NULL default 'N',
					  `form_attrib` enum('Y','N') NOT NULL default 'Y',
					  PRIMARY KEY  (`id`)
					)
					");
		$db->db_query("INSERT INTO `form_sql_types` (`id`, `name`, `description`, `id_forms_elements`, `form_size`, `form_deci`, `form_vals`, `form_attrib`) VALUES 
					(1, 'TINYINT', '', '1', 'Y', 'N', 'N', 'Y'),
					(3, 'SMALLINT', '', '1', 'Y', 'N', 'N', 'Y'),
					(4, 'MEDIUMINT', '', '1', 'Y', 'N', 'N', 'Y'),
					(5, 'INT', '', '1,11', 'Y', 'N', 'N', 'Y'),
					(6, 'BIGINT', '', '1', 'Y', 'N', 'N', 'Y'),
					(7, 'FLOAT', '', '1', 'Y', 'N', 'N', 'Y'),
					(8, 'DOUBLE', '', '1', 'Y', 'N', 'N', 'Y'),
					(9, 'DECIMAL', '', '1', 'N', 'N', 'N', 'N'),
					(10, 'DATE', '', '8,1', 'N', 'N', 'N', 'N'),
					(11, 'DATETIME', '', '8,1', 'N', 'N', 'N', 'N'),
					(12, 'TIMESTAMP', '', '1', 'N', 'N', 'N', 'Y'),
					(13, 'TIME', '', '1', 'N', 'N', 'N', 'N'),
					(14, 'YEAR', '', '1', 'N', 'N', 'N', 'N'),
					(15, 'CHAR', '', '1', 'N', 'N', 'N', 'N'),
					(16, 'VARCHAR', '', '1,15,17', 'Y', 'N', 'N', 'N'),
					(17, 'TINYTEXT', '', '1,2', 'Y', 'N', 'N', 'N'),
					(18, 'TEXT', '', '2,1', 'N', 'N', 'N', 'N'),
					(19, 'MEDIUMTEXT', '', '2,1', 'N', 'N', 'N', 'N'),
					(20, 'LONGTEXT', '', '2,1', 'N', 'N', 'N', 'N'),
					(21, 'ENUM', '', '3,5,10', 'N', 'N', 'Y', 'N'),
					(22, 'SET', '', '4,6', 'N', 'N', 'Y', 'N'),
					(23, 'TINYBLOB', '', '7', 'N', 'N', 'N', 'N'),
					(24, 'MEDIUMBLOB', '', '7', 'N', 'N', 'N', 'N'),
					(25, 'LONGBLOB', '', '7', 'N', 'N', 'N', 'N'),
					(26, 'BLOB', '', '7', 'N', 'N', 'N', 'N')");		
		}
		
	
	if ( !bab_isTable('form_tables') ) {
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables` (
						  `id` int(10) unsigned NOT NULL auto_increment,
						  `name` varchar(255) NOT NULL default '',
						  `description` text NOT NULL,
						  `filing_delay` int(10) unsigned NOT NULL default '0',
						  `created` enum('Y','N') NOT NULL default 'N',
						  `approbation` tinyint(3) unsigned NOT NULL default '0',
						  `id_group` int(10) unsigned NOT NULL,
						  `delete_password` tinyint(3) unsigned NOT NULL default '1',
						  `calendar_name` varchar(255) NOT NULL default '',
						  PRIMARY KEY  (`id`),
						  KEY `created` (`created`),
						  KEY `id_group` (`id_group`)
						)
					");
	}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables approbation"));
	if ( !is_array($arr) || $arr[0] != 'approbation' )
		{
		$db->db_query("ALTER TABLE `form_tables` ADD `approbation` TINYINT UNSIGNED DEFAULT '0' NOT NULL");
		}
					

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_lnk'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_lnk' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_lnk` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  `id_table1` int(10) unsigned NOT NULL default '0',
					  `id_table2` int(10) unsigned NOT NULL default '0',
					  `id_lnk_table` int(10) unsigned NOT NULL default '0',
					  `id_lnk_f1` int(10) unsigned NOT NULL default '0',
					  `id_lnk_f2` int(10) unsigned NOT NULL default '0',
					  `id_field1` int(10) unsigned NOT NULL default '0',
					  `id_field2` int(10) unsigned NOT NULL default '0',
					  `lnk_type` tinyint(2) NOT NULL default '0',
					  PRIMARY KEY  (`id`),
					  KEY `id_table1` (`id_table1`),
					  KEY `id_table2` (`id_table2`)
					)
					");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_lnk id_lnk_table"));
	if ( !is_array($arr) || $arr[0] != 'id_lnk_table' )
		{
		$db->db_query("ALTER TABLE `form_tables_lnk` ADD `id_lnk_table` INT UNSIGNED NOT NULL , ADD `id_field1` INT UNSIGNED NOT NULL , ADD `id_field2` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_tables_lnk` ADD `lnk_type` TINYINT( 2 ) NOT NULL");

		$db->db_query("ALTER TABLE `form_tables_lnk` ADD `id_lnk_f1` INT UNSIGNED NOT NULL AFTER `id_lnk_table` , ADD `id_lnk_f2` INT UNSIGNED NOT NULL AFTER `id_lnk_f1`");
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_change_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_change_groups' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_change_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					)
					");
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_fields'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_fields' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_fields` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_table` int(10) unsigned NOT NULL default '0',
					  `name` varchar(32) NOT NULL default '',
					  `description` varchar(255) NOT NULL default '',
					  `id_type` int(255) unsigned NOT NULL default '0',
					  `default_val` varchar(255) NOT NULL default '',
					  `link_table_field_id` int(10) unsigned NOT NULL default '0',
					  `ordering` int(10) unsigned NOT NULL default '0',
					  `field_function` varchar(32) NOT NULL default '',
					  `field_formula` varchar(255) NOT NULL default '',
					  `calendar_property` varchar(255) NOT NULL default '',
					  PRIMARY KEY  (`id`),
					  KEY `id_table` (`id_table`),
					  KEY `link_table_field_id` (`link_table_field_id`)
					)
					");

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_fields field_function"));
	if ( !is_array($arr) || $arr[0] != 'field_function' )
		{
		$db->db_query("ALTER TABLE `form_tables_fields` ADD `field_function` VARCHAR( 32 ) NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_fields field_formula"));
	if ( !is_array($arr) || $arr[0] != 'field_formula' )
		{
		$db->db_query("ALTER TABLE `form_tables_fields` ADD `field_formula` VARCHAR( 255 ) NOT NULL");
		}
		
	if (!bab_isTableField('form_tables_fields', 'calendar_property'))
	{
		$db->db_query("ALTER TABLE `form_tables_fields` ADD `calendar_property` VARCHAR( 255 ) NOT NULL");
	}

					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_fields_types'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_fields_types' )
		{
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_fields_types` (
						  `id` int(10) unsigned NOT NULL auto_increment,
						  `name` varchar(32) NOT NULL default '',
						  `description` varchar(255) NOT NULL default '',
						  `sql_type` int(3) unsigned NOT NULL default '0',
						  `col_type` text NOT NULL,
						  `id_forms_elements` varchar(16) NOT NULL default '',
						  `str_options` varchar(255) NOT NULL default '',
						  `id_extend` tinyint(3) unsigned NOT NULL default '1',
						  `on_delete_user` tinyint(3) unsigned NOT NULL,
						  PRIMARY KEY  (`id`)
						)
					");

		$db->db_query(implode("", @file($GLOBALS['babInstallPath'].'addons/forms/form_tables_fields_types.sql')));
		}
		
		
	$db->db_query('UPDATE form_tables_fields_types SET id_forms_elements='.$db->quote('1,17').' WHERE sql_type='.$db->quote(16));

	$db->db_query("
			CREATE TABLE `form_tables_fields_types_extend` (
		  `id` int(10) unsigned NOT NULL auto_increment,
		  `name` varchar(255) NOT NULL default '',
		  `id_forms_elements` varchar(16) NOT NULL default '',
		  `col_type` text NOT NULL,
		  `sql_type` tinyint(3) unsigned NOT NULL default '0',
		  PRIMARY KEY  (`id`)
		)
	");


	$db->db_query("
		INSERT INTO `form_tables_fields_types_extend` (`id`, `name`, `id_forms_elements`, `col_type`, `sql_type`) 
			VALUES 
		(1, 'SQL type', '', '', 0),
		(2, 'Date format', '8,1', 'date', 10),
		(3, 'Date time format', '8,1', 'datetime', 11),
		(4, 'Ovidentia User', '13,11', 'int(10) unsigned', 5),
		(5, 'Ovidentia Directory', '14,11', 'int(10) unsigned', 5)
	");


					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_filing_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_filing_groups' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_filing_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					) 
					");
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_insert_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_insert_groups' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_insert_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					) 
					");
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_lock_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_lock_groups' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_lock_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					) 
					");
					
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_view_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_view_groups' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_view_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					) 
					");


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_delete_groups'"));
	if ( !is_array($arr) || $arr[0] != 'form_tables_delete_groups' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_tables_delete_groups` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `id_object` int(11) unsigned NOT NULL default '0',
					  `id_group` int(11) unsigned NOT NULL default '0',
					  UNIQUE KEY `id` (`id`),
					  KEY `id_object` (`id_object`),
					  KEY `id_group` (`id_group`)
					) 
					");


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_external_blob_data'"));
	if ( !is_array($arr) || $arr[0] != 'form_external_blob_data' )
		$res[] = $db->db_query("
					 CREATE TABLE `form_external_blob_data` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_table_field` int(10) unsigned NOT NULL default '0',
					  `form_row` int(10) unsigned NOT NULL default '0',
					  `file_size` int(10) unsigned NOT NULL default '0',
					  `file_type` varchar(128) NOT NULL default '',
					  `file_name` varchar(255) NOT NULL default '',
					  PRIMARY KEY  (`id`),
					  KEY `id_table_field` (`id_table_field`,`form_row`)
					) 
					");


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_fields_init'"));
	if ( !is_array($arr) || $arr[0] != 'form_forms_fields_init' )
		{
		$res[] = $db->db_query("
					 CREATE TABLE `form_forms_fields_init` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(64) NOT NULL default '',
					  `description` varchar(255) NOT NULL default '',
					  `eval_code` text NOT NULL,
					  PRIMARY KEY  (`id`)
					) 
					");


		$db->db_query(implode("", @file($GLOBALS['babInstallPath'].'addons/forms/fields_init.sql')));

		}

	list($count) = $db->db_fetch_array($db->db_query("SELECT COUNT(*) FROM `form_config` WHERE id='3'"));
	if (empty($count))
		{
		$db->db_query("INSERT INTO `form_config` ( `id` , `name` , `description` , `value` ) VALUES ('3', 'html_file', 'Html template file', 'template_constructor_css.html')");
		}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_forms_fields init_on_modify"));
	if ( $arr[0] != 'init_on_modify' )
		$db->db_query("ALTER TABLE `form_forms_fields` ADD `init_on_modify` ENUM( 'Y', 'N' ) DEFAULT 'N' NOT NULL AFTER `init`");

	
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_trans_fields'"));
	if ($arr[0] != 'form_trans_fields' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_trans_fields` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_api` int(10) unsigned NOT NULL default '0',
					  `name` varchar(255) NOT NULL default '',
					  `description` varchar(255) NOT NULL default '',
					  PRIMARY KEY  (`id`)
					) 
					");

		$res[] = $db->db_query("INSERT INTO `form_trans_fields` 
				(`id`, `id_api`, `name`, `description`) VALUES 
					(1, 1, 'x_first_name', 'First name'),
					(2, 1, 'x_last_name', 'Last name'),
					(3, 1, 'x_company', 'Company'),
					(4, 1, 'x_address', 'Address'),
					(5, 1, 'x_city', 'City'),
					(6, 1, 'x_state', 'State'),
					(7, 1, 'x_zip', 'Zip code'),
					(8, 1, 'x_country', 'Country'),
					(9, 1, 'x_phone', 'Phone number'),
					(10, 1, 'x_email', 'Email address'),
					(11, 1, 'x_customer_organisation_type','Customer organisation type'),
					(12, 1, 'x_card_num', 'Card number'),
					(13, 1, 'x_exp_date', 'Card expiration date'),
					(14, 1, 'x_ship_to_first_name', 'Customer shipping first name'),
					(15, 1, 'x_ship_to_last_name', 'Customer shipping last name'),
					(16, 1, 'x_ship_to_company', 'Customer shipping company'),
					(17, 1, 'x_ship_to_address', 'Customer shipping address'),
					(18, 1, 'x_ship_to_city', 'Customer shipping city'),
					(19, 1, 'x_ship_to_state', 'Customer shipping state'),
					(20, 1, 'x_ship_to_zip', 'Customer shipping zip'),
					(21, 1, 'x_ship_to_country', 'Customer shipping country'),
					(22, 1, 'x_fax', 'Fax'),
					(23, 1, 'x_recurring_Billing', 'Recurring Billing'),
					(24, 1, 'x_cust_id', 'Customer ID'),
					(25, 1, 'x_customer_ip', 'Customer ip'),
					(26, 1, 'x_customer_tax_id', 'Customer tax ID'),
					(27, 1, 'x_merchant_email', 'Merchant email for a confirmation copy')
				");
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_trans_api'"));
	if ($arr[0] != 'form_trans_api' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_trans_api` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `description` text NOT NULL,
					  `login` varchar(255) NOT NULL default '',
					  `password` varchar(255) NOT NULL default '',
					  `tran_key` varchar(255) NOT NULL default '',
					  `url` varchar(255) NOT NULL default '',
					  `login_varname` varchar(255) NOT NULL default '',
					  `password_varname` varchar(255) NOT NULL default '',
					  `tran_key_varname` varchar(255) NOT NULL default '',
					  `amount_varname` varchar(255) NOT NULL default '',
					  `additional_parameters` varchar(255) NOT NULL default '',
					  `pos_transaction_id` tinyint(3) unsigned NOT NULL default '0',
					  `pos_response_code` tinyint(3) unsigned NOT NULL default '0',
					  `pos_response_text` tinyint(3) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) 
					");

		$res[] = $db->db_query("INSERT INTO `form_trans_api` VALUES (1, 'Authorizenet', 'Authorizenet', '', '', '', 'https://secure.authorize.net/gateway/transact.dll', 'x_login', 'x_password', 'x_tran_key', 'x_amount', 'x_version=3.1&x_delim_data=true&x_delim_char=,&x_method=cc&x_type=auth_capture', 7, 1, 4)");
		}

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_help'"));
	if ($arr[0] != 'form_help' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_help` (
					`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
					`name` VARCHAR( 255 ) NOT NULL ,
					`help_title` VARCHAR( 255 ) NOT NULL ,
					`help_text` LONGTEXT NOT NULL ,
					PRIMARY KEY ( `id` )
					)
					");

		}
		
		
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_proposal'"));
		if ($arr[0] != 'form_proposal' )
		{
			$res[] = $db->db_query("
					CREATE TABLE `form_proposal` (
					`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
					`name` VARCHAR( 255 ) NOT NULL, 
					`specify` tinyint(1) default '0' NOT NULL,
					`checkbox` tinyint(1) default '0' NOT NULL,
					PRIMARY KEY ( `id` )
			)
			");
		
		}
		
	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_proposal_item'"));
		if ($arr[0] != 'form_proposal_item' )
		{
			$res[] = $db->db_query("
					CREATE TABLE `form_proposal_item` (
					`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
					`id_proposal` INT UNSIGNED NOT NULL default '0',
					`name` VARCHAR( 255 ) NOT NULL,
					`sortkey` INT UNSIGNED NOT NULL default '0',
					PRIMARY KEY ( `id` )
			)
			");
		
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_view_cols'"));
	if ($arr[0] != 'form_tables_view_cols' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_tables_view_cols` (
					`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
					`id_table_field` INT UNSIGNED NOT NULL ,
					`id_table` INT UNSIGNED NOT NULL ,
					PRIMARY KEY ( `id` ) ,
					INDEX ( `id_table` )
					)
					");
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_step_approb'"));
	if ($arr[0] != 'form_app_step_approb' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_app_step_approb` (
					`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
					`id_step` INT UNSIGNED NOT NULL ,
					`id_form_field` INT UNSIGNED NOT NULL ,
					`field_value` VARCHAR( 255 ) NOT NULL ,
					`id_sh` INT UNSIGNED NOT NULL ,
					`id_form` INT UNSIGNED NOT NULL ,
					PRIMARY KEY ( `id` ) ,
					INDEX ( `id_step` )
					)");
		}

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_export_fav'"));
	if ($arr[0] != 'form_tables_export_fav' )
		{
		$res[] = $db->db_query("
					CREATE TABLE `form_tables_export_fav` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_table` int(10) unsigned NOT NULL default '0',
					  `name` varchar(255) NOT NULL default '',
					  `filename` varchar(255) NOT NULL default '',
					  `cols` varchar(255) NOT NULL default '',
					  `csv_separator` char(2) NOT NULL default '',
					  PRIMARY KEY  (`id`),
					  KEY `id_table` (`id_table`)
					) ");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_export_fav csv_separator"));
	if ( $arr[0] != 'csv_separator' )
		{
		$db->db_query("ALTER TABLE `form_tables_export_fav` CHANGE `separator` `csv_separator` CHAR( 2 ) NOT NULL");
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_approb_schi'"));
	if ($arr[0] != 'form_approb_schi' )
		{
		$res[] = $db->db_query("
				CREATE TABLE `form_approb_schi` (
				  `id` int(10) unsigned NOT NULL auto_increment,
				  `id_line` int(10) unsigned NOT NULL default '0',
				  `id_schi` int(10) unsigned NOT NULL default '0',
				  `id_step` int(10) unsigned NOT NULL default '0',
				  `id_table` int(10) unsigned NOT NULL default '0',
				  `id_user` int(10) unsigned NOT NULL default '0',
				  PRIMARY KEY  (`id`),
				  KEY `id_schi` (`id_schi`,`id_step`),
				  KEY `id_table` (`id_table`)
				)");
		}
		
	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_approb_schi id_user"));
	if ( $arr[0] != 'id_user' )
		{
		$db->db_query("ALTER TABLE `form_approb_schi` ADD `id_user` INT UNSIGNED NOT NULL");
		}

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_app_step_menu'"));
	if ($arr[0] != 'form_app_step_menu' )
		{
		$res[] = $db->db_query("
				CREATE TABLE `form_app_step_menu` (
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
				`id_step` INT UNSIGNED NOT NULL ,
				`name` VARCHAR( 64 ) NOT NULL ,
				`id_menustep` INT UNSIGNED NOT NULL ,
				`selmenu` TINYINT UNSIGNED NOT NULL ,
				PRIMARY KEY ( `id` )
				)");
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_lnk_values'"));
	if ($arr[0] != 'form_tables_lnk_values' )
		{
		$res[] = $db->db_query("
				CREATE TABLE `form_tables_lnk_values` (
				  `id_lnk` int(10) unsigned NOT NULL default '0',
				  `row1` int(10) unsigned NOT NULL default '0',
				  `row2` int(10) unsigned NOT NULL default '0',
				  KEY `id_lnk` (`id_lnk`,`row1`,`row2`)
				) 
				");
		}

	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_filters'"));
	if ($arr[0] != 'form_tables_filters' )
		{
		$res[] = $db->db_query("
				CREATE TABLE `form_tables_filters` (
				  `id` int(10) unsigned NOT NULL auto_increment,
				  `id_table` int(10) unsigned NOT NULL default '0',
				  `name` varchar(32) NOT NULL default '',
				  `description` text NOT NULL,
				  PRIMARY KEY  (`id`),
				  KEY `id_table` (`id_table`)
				)
				");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_filters ordercol"));
	if ( $arr[0] != 'ordercol' )
		$db->db_query("ALTER TABLE `form_tables_filters` ADD `ordercol` TINYINT( 1 ) UNSIGNED NOT NULL");



	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_filters_fields'"));
	if ($arr[0] != 'form_tables_filters_fields' )
		{
		$res[] = $db->db_query("
				CREATE TABLE `form_tables_filters_fields` (
				  `id` int(10) unsigned NOT NULL auto_increment,
				  `id_filter` int(10) unsigned NOT NULL default '0',
				  `id_table_field` int(10) unsigned NOT NULL default '0',
				  `operator` varchar(10) NOT NULL default '',
				  `comparator` varchar(255) NOT NULL default '',
				  `id_lnk` int(10) unsigned NOT NULL default '0',
				  PRIMARY KEY  (`id`),
				  KEY `id_table_field` (`id_table_field`),
				  KEY `id_filter` (`id_filter`)
				)
				");
		}


	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_filters_fields id_lnk"));
	if ( $arr[0] != 'id_lnk' )
		$db->db_query("ALTER TABLE `form_tables_filters_fields` ADD `id_lnk` int(10) unsigned NOT NULL default '0'");


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_locks'"));
	if ($arr[0] != 'form_tables_locks' )
		{
		$res[] = $db->db_query("
				CREATE TABLE `form_tables_locks` (
					  `id` int(10) unsigned NOT NULL auto_increment,
					  `id_table` int(10) unsigned NOT NULL default '0',
					  `message` varchar(255) NOT NULL default '',
					  `creationdate` datetime NOT NULL default '0000-00-00 00:00:00',
					  `locked_by` varchar(255) NOT NULL default '',
					  PRIMARY KEY  (`id`),
					  KEY `id_table` (`id_table`),
					  KEY `locked_by` (`locked_by`)
					)
				");
		}


	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_gest_rights'"));
	if ($arr[0] != 'form_gest_rights' )
		{
		$res[] = $db->db_query("
				CREATE TABLE `form_gest_rights` (
					`id` INT(10) UNSIGNED NOT NULL auto_increment,
					`name` VARCHAR( 255 ) NOT NULL ,
					`manager` TINYINT( 1 ) UNSIGNED NOT NULL ,
					`section` TINYINT( 1 ) UNSIGNED NOT NULL ,
					`app_grp` TINYINT( 1 ) UNSIGNED NOT NULL ,
					PRIMARY KEY ( `id` ) ,
					INDEX ( `manager` , `section`, `app_grp` )
					)
				");

			$db->db_query("INSERT INTO `form_gest_rights` VALUES (1, 'Manager', 1, 0, 0)");
			$db->db_query("INSERT INTO `form_gest_rights` VALUES (2, 'Section', 0, 1, 1)");
			

		$res[] = $db->db_query("
					CREATE TABLE `form_gest_rights_app` (
					  `id` int(10) unsigned NOT NULL,
					  `id_app` int(10) unsigned NOT NULL,
					  `id_right` int(10) unsigned NOT NULL 
					)
				");
    
		$arr1 = $db->db_fetch_array($db->db_query("DESCRIBE form_app_applications in_section"));
		if ( $arr1[0] == 'in_section' )
			{
			$tmp = $db->db_query("SELECT id,in_section FROM form_app_applications");
			while ($arr = $db->db_fetch_array($tmp))
				{
				if ($arr['in_section'] == 'Y')
					{
					$db->db_query("INSERT INTO form_gest_rights_app (id_app, id_right) VALUES ('".$arr['id']."','2')");
					}
				}

			$db->db_query("ALTER TABLE `form_app_applications` DROP `in_section`"); 
			}
		}



	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_forms_fields_ctrl'"));
	if ($arr[0] != 'form_forms_fields_ctrl' )
		{
		$db->db_query("
			CREATE TABLE `form_forms_fields_ctrl` (
			  `id` int(10) unsigned NOT NULL auto_increment,
			  `id_field` int(10) unsigned NOT NULL default '0',
			  `id_validation` int(10) unsigned NOT NULL default '0',
			  `parameter` varchar(255) NOT NULL default '',
			  `alert` varchar(255) NOT NULL default '',
			  PRIMARY KEY  (`id`),
			  KEY `id_field` (`id_field`)
			)
			"); 

		$db->db_query("ALTER TABLE `form_forms_fields` ADD `validations` TINYINT NOT NULL");

		$tmp = $db->db_query("SELECT id, id_validation, validation_param, alert FROM form_forms_fields WHERE id_validation>1");
		while ($arr = $db->db_fetch_array($tmp))
			{
			$db->db_query("
					INSERT INTO form_forms_fields_ctrl 
						(id_field, id_validation, parameter, alert)
					VALUES
						('".$arr['id']."',
						'".$arr['id_validation']."',
						'".$arr['validation_param']."',
						'".$arr['alert']."'
						)
			");

			$db->db_query("UPDATE form_forms_fields SET validations='1' WHERE id='".$arr['id']."'");
			}


		$db->db_query("ALTER TABLE `form_forms_fields` DROP `id_validation` , DROP `validation_param` , DROP `alert`");
		
		}



	$arr = $db->db_fetch_array($db->db_query("SHOW TABLES LIKE 'form_tables_sync'"));
	if ($arr[0] != 'form_tables_sync' )
		{
		$db->db_query("
				CREATE TABLE `form_tables_sync` (
				`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
				`id_table` INT( 11 ) UNSIGNED NOT NULL ,
				`use_sync` TINYINT( 1 ) UNSIGNED NOT NULL ,
				`srv_host` VARCHAR( 255 ) NOT NULL ,
				`srv_login` VARCHAR( 255 ) NOT NULL ,
				`srv_pass` VARCHAR( 255 ) NOT NULL ,
				`srv_dir` VARCHAR( 255 ) NOT NULL ,
				`row_overwrite` TINYINT( 1 ) UNSIGNED NOT NULL ,
				`lastupdate` DATETIME,
				PRIMARY KEY ( `id` ) ,
				INDEX ( `id_table` , `use_sync` )
				)
			");
		}


	// modification des types  6.10 --> 6.20

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_fields_types id_forms_elements"));
	if ( $arr[0] != 'id_forms_elements' ) {
		$db->db_query("ALTER TABLE `form_tables_fields_types` ADD `id_forms_elements` varchar(16) NOT NULL default ''");
	}

	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_fields_types str_options"));
	if ( $arr[0] != 'str_options' ) {
		$db->db_query("ALTER TABLE `form_tables_fields_types` ADD `str_options` varchar(255) NOT NULL default ''");
		$db->db_query("ALTER TABLE `form_tables_fields_types` ADD `id_extend` tinyint(3) unsigned NOT NULL default '1'");

		// mettre les types comme extension de 'SQL type'
		$db->db_query("UPDATE `form_tables_fields_types` SET `id_extend`='1'");

		// reprendre les id_forms_elements
		$restmp = $db->db_query("SELECT t.id,s.id_forms_elements FROM form_sql_types s, form_tables_fields_types t WHERE t.sql_type=s.id");

		while ($tmp = $db->db_fetch_assoc($restmp)) {
			$db->db_query("UPDATE form_tables_fields_types SET id_forms_elements='".$tmp['id_forms_elements']."' WHERE id='".$tmp['id']."'");
			}


		// traitement des date :
		$db->db_query("UPDATE `form_tables_fields_types` SET `id_extend`='2', str_options='a:1:{s:16:\"view_date_format\";s:1:\"1\";}' WHERE sql_type='10'");

		// traitement des datetime :
		$db->db_query("UPDATE `form_tables_fields_types` SET `id_extend`='3', str_options='a:1:{s:16:\"view_date_format\";s:1:\"1\";}' WHERE sql_type='11'");

		// si il existe des champs de table qui utilisent la transformation, il faut les mettre sur 1 type utilisateur d'ovidentia
		$all_user_dir = 1;
		
		$directories = bab_getUserDirectories();
		foreach($directories as $dir => $arrd) {
			if (BAB_REGISTERED_GROUP === $arrd['id_group']) {
				$all_user_dir = $dir;
				break;
			}
		}
		
		$db->db_query("INSERT INTO `form_tables_fields_types` 
			(name, sql_type, col_type, id_forms_elements, str_options, id_extend) 
				VALUES 
			('Ovidentia user', '5', 'int(10) unsigned', '13,11', 'a:1:{s:12:\"id_directory\";s:1:\"".$all_user_dir."\";}', '4')
		");

		$new_type = $db->db_insert_id();
		
		$restmp = $db->db_query("SELECT t.id FROM form_tables_fields t, form_forms_fields f WHERE f.id_table_field=t.id AND f.transform_to <>''");
		while ($row = $db->db_fetch_assoc($restmp)) {
			// mettre a jour les champs avec le nouveau type
			$db->db_query("UPDATE form_tables_fields SET id_type='".$new_type."' WHERE id='".$row['id']."'");
		}
	}




	$arr = $db->db_fetch_array($db->db_query("DESCRIBE form_tables_fields_types on_delete_user"));
	if ( $arr[0] != 'on_delete_user' ) {

		$db->db_query("ALTER TABLE `form_tables_fields_types` ADD `on_delete_user` TINYINT UNSIGNED NOT NULL"); 
	}
	
	
	
	if (!bab_isTable(FORM_TABLES_FIELDS_DIRECTORY)) {
		$res[] = $db->db_query("
			CREATE TABLE `form_tables_fields_directory` (
			  `id` int(10) unsigned NOT NULL auto_increment,
			  `id_table_field` int(10) unsigned NOT NULL,
			  `directory_field` varchar(32) NOT NULL,
			  PRIMARY KEY  (`id`)
			)
		");
	}

	if (!bab_isTableField(FORM_TABLES,'id_group')) {
		$db->db_query("ALTER TABLE `form_tables` ADD `id_group` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_tables` ADD INDEX ( `id_group` )");
		$db->db_query("ALTER TABLE `form_tables` ADD `delete_password` TINYINT UNSIGNED NOT NULL DEFAULT '1'");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_register_next_step` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `register_delete_invalid` TINYINT UNSIGNED NOT NULL DEFAULT '1'");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_import_user_next_step` INT UNSIGNED NOT NULL");
		$db->db_query("UPDATE `form_tables_fields_types` SET `id_forms_elements` = '1,15' WHERE `id_forms_elements` ='1'");
		
	}
	
	if (!bab_isTableField(FORM_TABLES,'calendar_name')) {
		$db->db_query("ALTER TABLE `form_tables` ADD `calendar_name` varchar(255) NOT NULL default ''");
	}

	
	
	
	
	if (!bab_isTable(FORM_WORKSPACES)) {
		$res[] = $db->db_query('
			CREATE TABLE `form_workspaces` (
			  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
			  `name` varchar(255) NOT NULL,
			  `description` text NOT NULL,
			  `uid` varchar(255) NOT NULL,
			  PRIMARY KEY  (`id`),
			  UNIQUE KEY `uid` (`uid`)
			)
		');
	}
	
	if (!bab_isTable(FORM_WORKSPACE_ENTRIES)) {
		$res[] = $db->db_query("
			CREATE TABLE `form_workspace_entries` (
			  `id` int(10) unsigned NOT NULL auto_increment,
			  `id_workspace` int(10) unsigned NOT NULL default '0',
			  `id_object` int(10) unsigned NOT NULL default '0',
			  `type` varchar(255) NOT NULL default '',
			  PRIMARY KEY  (`id`) 
			) 
		");
		
		// create default workspace
		
		$db->db_query("
			INSERT INTO `form_workspaces` (name, uid) VALUES ('workspace 1', ".$db->quote(md5(uniqid(rand(), true))).")
		");

		if (!function_exists('form_getWorkspace')) {
			function form_getWorkspace() {
				static $id_workspace = NULL;
				if (NULL === $id_workspace) {
					global $babDB;
					$id_workspace = $babDB->db_insert_id();
				}
				
				return $id_workspace;
			}
		}
		
		
		form_getWorkspace();
		
		include_once $GLOBALS['babAddonPhpPath']."impexp.php";
		
		function form_ws_addObject($type) {
		
			global $babDB;
		
			$classname = 'form_wsO'.$type;
			$obj = new $classname();
			if ($obj->isSelectable()) {
				$items = $obj->getItems();
				foreach($items as $id_object => $item) {
					$babDB->db_query("
						INSERT INTO `".FORM_WORKSPACE_ENTRIES."` 
							(id_workspace, id_object, type) 
						VALUES 
							(
								".$babDB->quote(form_getWorkspace()).", 
								".$babDB->quote($id_object).",
								".$babDB->quote($type)."
							)
					");
				}
			}
			$arr = $obj->getSub();
	
			foreach($arr as $type) {
				form_ws_addObject($type);
			}
		}
		
		form_ws_addObject('workspace');
		
	} else {
		// workspace_entries exists
		
		$res = $db->db_query('SHOW INDEX FROM `form_workspace_entries`');
		if (1 < $db->db_num_rows($res)) {
		
			$db->db_query('
				ALTER TABLE `form_workspace_entries` DROP INDEX `id_workspace` 
			');
		}
	
	}
	

	
	if (!bab_isTableField(FORM_GEST_RIGHTS_APP,'id')) {
	
		// enlever autoincrement
		$db->db_query('
			ALTER TABLE `form_gest_rights_app` CHANGE `id_app` `id_app` INT( 10 ) UNSIGNED NOT NULL 
		');
	
		$db->db_query('
			ALTER TABLE `form_gest_rights_app` 
			ADD `id` INT( 10 ) UNSIGNED NOT NULL
		');
		
		
		$resd = $db->db_query('SELECT id_app, id_right FROM `form_gest_rights_app`');
		$i = 1;
		while ($row = $db->db_fetch_assoc($resd)) {
			$db->db_query('
				UPDATE `form_gest_rights_app` SET id='.$db->quote($i).' 
				WHERE id_app='.$db->quote($row['id_app']).' AND id_right='.$db->quote($row['id_right']).'
			');
			
			$i++;
		}
		
		$db->db_query('
			ALTER TABLE `form_gest_rights_app` ADD PRIMARY KEY ( `id` ) 
		');
		
		$db->db_query('
			ALTER TABLE `form_gest_rights_app` CHANGE `id` `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT 
		');
	}
	
	else {
	
	
	
		/**
		 * Dans certains cas il existe une colonne id mais qui n'a pas d'auto increment et qui n'est pas primary key
		 * ajouter la clef si possible
		 */
	 	$res = $db->db_query('
			describe form_gest_rights_app id
		');
		
		$arr = $db->db_fetch_assoc($res);
		if ('auto_increment' !== $arr['Extra']) {

			// mettre a jour les ID pour s'assurer que les ID sont bien uniques avant d'ajouter la clef

			$idtaskres = $db->db_query('SELECT id_app, id_right FROM `form_gest_rights_app`');
			$i = 1;
			while ($idtask = $db->db_fetch_assoc($idtaskres)) {
				$db->db_query('
					UPDATE 
						`form_gest_rights_app` SET id='.$db->quote($i).' 
					WHERE 
						id_app = '.$db->quote($idtask['id_app']).' 
						AND id_right = '.$db->quote($idtask['id_right'])
				);
				$i++;
			}


			$db->db_queryWem('
				ALTER TABLE `form_gest_rights_app` ADD PRIMARY KEY ( `id` ) 
			');
			
			$db->db_queryWem('
				ALTER TABLE `form_gest_rights_app` CHANGE `id` `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT 
			');
		}
	
	}
	
	
	if (!bab_isTableField(FORM_TABLES_FIELDS_DIRECTORY,'id')) {
		$db->db_query('ALTER TABLE `form_tables_fields_directory` ADD `id` INT UNSIGNED NOT NULL FIRST');
		$db->db_query('ALTER TABLE `form_tables_fields_directory` CHANGE `id_table_field` `id_table_field` INT( 10 ) UNSIGNED NOT NULL');
		
		$resd = $db->db_query('SELECT id_table_field FROM '.FORM_TABLES_FIELDS_DIRECTORY.'');
		$i = 1;
		while ($row = $db->db_fetch_assoc($resd)) {
			$db->db_query('
				UPDATE '.FORM_TABLES_FIELDS_DIRECTORY.' SET id='.$db->quote($i).' 
				WHERE id_table_field='.$db->quote($row['id_table_field']).'
			');
			
			$i++;
		}
		
		$db->db_query('
			ALTER TABLE `form_tables_fields_directory` DROP PRIMARY KEY ,
			ADD PRIMARY KEY ( `id` ) 
		');
		
		$db->db_query('ALTER TABLE `form_tables_fields_directory` CHANGE `id` `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT');
	}
	
	
	
	if (!bab_isTableField(FORM_APP_STEPS,'id_error_register_next_step')) {
	
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_error_register_next_step` INT UNSIGNED NOT NULL");
	}
	


	if (isset($res) && is_array($res))
		{
		foreach ($res as $value) {
			if ( !$value )
				{
				$babBody->msgerror = bab_translate("ERROR in database upgrade",$GLOBALS['babAddonFolder']);
				return false;
				}
			}
		}
		
	
	if (!bab_isTableField(FORM_APP_STEPS,'id_paybox_system')) {
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_paybox_system` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_paybox_total` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_paybox_cmd` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_paybox_porteur` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_paybox_step_effectue` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_paybox_step_annule` INT UNSIGNED NOT NULL");
		$db->db_query("ALTER TABLE `form_app_steps` ADD `id_paybox_step_refuse` INT UNSIGNED NOT NULL");
	}
	
	
	$db->db_query("UPDATE form_forms_types SET curl_required='Y' WHERE id='6'");
		
	return true;
	}
