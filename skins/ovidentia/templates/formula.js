function formula()
{
	this.config = new formula.config();
	this._buildselect(this.config.id_operators,this.config.operators,'O');
	
	if (this.config.formula != '')
	{
		var arr = this.config.formula.split(this.config.separator);
		this._createFormula(arr);
	}
}

formula.config = function()
{
	this.id_contener = 'formula_contener';
	this.id_operators = 'formula_operators';
	this.id_functions = 'formula_functions';
	this.operators = ['+','-','*','/','(',')'];
	this.functions = new Array();
	this.separator = ' ';
	this.el_height = 30;
	this.formula = '';
	this.currentValueId = 0;
	this.currentSelectedIndex = -1;

	this.error1 = 'error';
	this.error2 = 'a field value is empty';
	this.error3 = 'can\'t create formula';
}

formula.prototype.add = function(arr,key)
{

	var arr2 =new Array();
	for( var i in arr)
		{
		if (arr[i] != '')
			{
			arr2[i] = arr[i];
			this.config.functions[i] = arr[i];
			}
		}
	this._buildselect(this.config.id_functions, arr2, key);
}

formula.prototype._buildselect = function(id,arr,key)
{
	var sel = document.getElementById(id);
	for( var i in arr )
	{
		sel.options[sel.options.length] = new Option(arr[i],key+i);
	}
}

formula.prototype._createElement = function(key,el,value)
{
	if (typeof value == 'undefined')
	{
		var value = '';
	}
	var html = '';
	switch (key)
	{
		case 'F':
			html = this.config.functions[el];
			break;
		case 'V':
			this.config.currentValueId++;
			html = '<input type="text" size="4" id="formulaValueId_'+this.config.currentValueId+'" style="border:#000 2px solid;" value="'+value+'" />';
			el = '['+this.config.currentValueId+']';
			break;
		case 'O':
			html = this.config.operators[el];
			break;
		default:
			html ='error';
			break;
	
	}
	if (this.config.formula != '')
	{
		this.config.formula += this.config.separator;
	}
	this.config.formula += key+el;
	var index = this.config.formula.split(this.config.separator).length - 1;
	this._appendElement(html, index);
	return key == 'V' ? this.config.currentValueId : false;
}

formula.prototype._createFormula = function(arr)
{
	var esp = document.getElementById(this.config.id_contener);
	esp.innerHTML ='';
	var value = '';
	
	this.config.formula = '';
	for (var i in arr )
	{
		var key = arr[i].substr(0,1);
		var el = arr[i].substr(1);
		switch (key)
		{
			case 'V':
				
			value = this._createElement(key,el,el.replace(/%20/g, ' '));
			
			break;
			case 'O':
				for(var i in this.config.operators)
					{
					if (this.config.operators[i] == el)
						{
						var tmp = i;
						}
					}

				if (typeof tmp != 'undefined')
				{
					el = tmp;
				}
				else
				{
					return false;
				}
			default:
				this._createElement(key,el);
			break;
		
		}
	}
	return true;
}

formula.prototype.onchange = function(obj)
{
	var key = obj.value.substr(0,1);
	var el = obj.value.substr(1);
	this._createElement(key,el);
	
	obj.options[0].selected =true;
}

formula.prototype._selectElement = function(index)
{
	this.config.currentSelectedIndex = index;
	var arr = this.config.formula.split(this.config.separator);
	for (var i in arr )
		{
		var div = document.getElementById('formulaElementId_'+i);
		if (i == index)
			{
			div.style.backgroundColor = '#ccc';
			}
		else
			{
			div.style.backgroundColor = '#eee';
			}
		}
	
}

formula.prototype._appendElement = function(el,index)
{
	var esp = document.getElementById(this.config.id_contener);
	var div = document.createElement("div");
	esp.appendChild(div);

	div.style.cssFloat = 'left';
	div.style.styleFloat = 'left';

	div.style.height = this.config.el_height + 'px';
	div.style.textAlign = 'center';
	div.style.backgroundColor = '#eee';
	div.style.margin = '2px';
	div.style.padding = '3px 6px';
	div.style.fontWeight = 'bolder';
	div.style.fontSize = '14px';
	div.style.border = '#000 1px solid';
	div.id = 'formulaElementId_'+index;
	div.onclick = function() { formula._selectElement(index); };
	div.innerHTML = el;
}


formula.prototype.getFormula = function(noerror)
{
	var output = '';
	var arr = this.config.formula.split(this.config.separator);
	for (var i in arr )
	{
		var key = arr[i].substr(0,1);
		var el = arr[i].substr(1);

		switch (key)
		{
		case 'V':
			el = el.substr(el.indexOf('[')+1);
			el = el.substr(0,el.indexOf(']'));
			if (tmp = document.getElementById('formulaValueId_'+el))
				{
				tmp.style.borderColor = '#000';
				el = tmp.value.replace(/\s/g,'%20');
				//el = parseFloat(tmp.value);
				if (!noerror && el == '' && el != 0)
					{
					alert(this.config.error2);
					tmp.style.borderColor = 'red';
					return false;
					}
				}
			else if (!noerror)
				{
				alert(this.config.error1);
				return false;
				}

			break;

		case 'O':
			el = this.config.operators[el];
			break;

		}

		if (output != '')
		{
			output += this.config.separator;
		}
		output += key+el;
	}

	return output;
}


formula.prototype.deleteSelected = function()
{
	var formula = this.getFormula(true);
	if (formula)
	{
		var arr = formula.split(this.config.separator);
		
		if (this.config.currentSelectedIndex < arr.length && this.config.currentSelectedIndex >= 0)
		{
			arr.splice(this.config.currentSelectedIndex,1);

			var tmp = this._createFormula(arr)

			if (!tmp)
			{
				alert(this.config.error3);
			}

		}
		

		this.config.currentSelectedIndex = -1;
	}
	else
	{
		alert(this.config.error1);
	}
}

formula.prototype.setFormula = function(formula)
{
if (formula != '')
	{
	this.config.formula = formula;
	var arr = this.config.formula.split(this.config.separator);
	this._createFormula(arr);
	}
}